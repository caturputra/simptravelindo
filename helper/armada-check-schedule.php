<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Cron untuk cek selisih
** jika < 1 maka akan dimasukkan ke tabel going atau siap jalan-> cek armada
** =3 kirim notif apabila masih ada tagihan dan reminder
*/

require_once '../core/init.php';

$var_sql_checkdate = "
SELECT ot.od_id, ot.od_from, ot.od_to, ot.od_request, ot.od_status, u.user_detail_firstname, u.user_detail_lastname, u.user_detail_phone, s.schedule_id, a.armada_regnumber, DATEDIFF(ot.od_from, CURRENT_TIMESTAMP()) as date_dispute, s.schedule_id, ot.od_invoice, am.model_name as armada_brand
FROM sp_order_transaction ot
JOIN sp_schedule s ON s.od_id = ot.od_id
JOIN sp_user_detail u ON u.user_id = ot.user_id
JOIN sp_armada a ON a.armada_regnumber = ot.armada_regnumber
JOIN sp_armada_model am ON am.model_id = ot.model_id
WHERE od_status = '4'
ORDER BY ot.od_id ASC
";
$var_query_checkdate = mysqli_query($var_con, $var_sql_checkdate);
$var_numrow_checkdate = mysqli_num_rows($var_query_checkdate);

if ($var_numrow_checkdate > 0) {
    for ($i=0; $i < $var_numrow_checkdate ; $i++) {
        while($var_data_checkdate[$i] = mysqli_fetch_array($var_query_checkdate)) {
            switch ($dispute[$i] = $var_data_checkdate[$i]['date_dispute']) {
                case $dispute <= 1:
                    $var_sql_getg = "SELECT schedule_id FROM sp_going WHERE schedule_id = '{$var_data_checkdate[$i]['schedule_id']}'";
                    $var_query_getg = mysqli_query($var_con, $var_sql_getg);
                    $var_numrows_getg = mysqli_num_rows($var_query_getg);

                    $var_sql_getacd = "SELECT schedule_id FROM sp_armada_conditiononday WHERE schedule_id = '{$var_data_checkdate[$i]['schedule_id']}'";
                    $var_query_getacd = mysqli_query($var_con, $var_sql_getacd);
                    $var_numrows_getacd = mysqli_num_rows($var_query_getacd);
                    if ($var_numrows_getg == 0 && $var_numrows_getacd == 0) {
                        // $var_data_going[$i] = ['schedule_id' => $var_data_checkdate[$i]['schedule_id']];
                        // $var_insert_going = insert($var_con, "sp_going", $var_data_going[$i]);

                        $var_sql_going = "REPlACE INTO sp_going(schedule_id) VALUES('{$var_data_checkdate[$i]['schedule_id']}')";
                        $var_query_going = mysqli_query($var_con, $var_sql_going);

                        if (!$var_query_going) {
                            writeLog($var_con, ['sistem' => 'tidak bisa insert ke tabel sp_going, check armada-check-schdule.php'], false);
                        } else {
                            echo "OK going";
                        }

                        //$var_data_condition[$i] = ['schedule_id' => $var_data_checkdate[$i]['schedule_id']];
                        $var_sql_condition = "REPlACE INTO sp_armada_conditiononday(schedule_id) VALUES('{$var_data_checkdate[$i]['schedule_id']}')";
                        $var_query_condition = mysqli_query($var_con, $var_sql_condition);

                        //$var_insert_condition = insert($var_con, "sp_armada_conditiononday", $var_data_condition[$i]);
                        if (!$var_query_condition) {
                            writeLog($var_con, ['sistem' => 'tidak bisa insert ke tabel sp_armada_conditiononday, check armada-check-schdule.php'], false);
                        } else {
                            echo "OK condition day";
                        }

                        // $var_data_sstatus[$i] = ['schedule_status' => $var_data_checkdate[$i]['3']];
                        // $var_cond_update[$i] = ['schedule_id' => $var_data_checkdate[$i]['schedule_id']];
                        // $var_update_sstatus = update($var_con, "sp_schedule", $var_data_sstatus[$i], $var_cond_update[$i]);
                        // if (!$var_update_sstatus) {
                        //     writeLog($var_con, ['sistem' => 'tidak bisa update sp_schedule, check armada-check-schdule.php'], false);
                        // } else {
                        //     echo "OK schedule";
                        // }
                    }
                break;

                case $dispute == 3:
                    $var_sql_payment = "
                    SELECT payment_lackofpay
                    FROM sp_payment
                    WHERE payment_verdate =
                    (
                        SELECT MAX(p.payment_verdate)
                        FROM sp_payment p
                        JOIN sp_schedule s ON p.schedule_id = s.schedule_id
                        JOIN sp_order_transaction ot ON ot.od_id = s.od_id
                        WHERE ot.od_invoice = '{$var_data_checkdate[$i]['od_invoice']}' GROUP BY ot.od_invoice
                    ) LIMIT 1
                    ";
                    // var_dump($var_sql_payment);
                    $var_query_payment = mysqli_query($var_con, $var_sql_payment);
                    $var_data_payment = mysqli_fetch_row($var_query_payment);
                    if ($var_data_payment[0] < 0) {
                        $var_send_phone[$i] = sendPhone($var_con, ['phone' => $var_data_checkdate[$i]['user_detail_phone'], 'message' => 'H-3. Pesanan armada dengan kode ' . $var_data_checkdate[$i]['od_id'] . ' masih ada tagihan. Mohon segera dilunasi']);
                    }

                    $var_send_phone[$i] = sendPhone($var_con, ['phone' => $var_data_checkdate[$i]['user_detail_phone'], 'message' => 'Pengingat! Kode Pesan '. $var_data_checkdate[$i]['od_id'] . ' || Periode ' . dateFormat($var_data_checkdate[$i]['od_from']) . ' - ' . dateFormat($var_data_checkdate[$i]['od_to']) . ' || Armada ' . $var_data_checkdate[$i]['armada_regnumber'] .' '.$var_data_checkdate[$i]['armada_brand']]);
                break;
            }
        }
    }
}

//delete temporary data dimana deposit sudah divalidasi
$var_cond = ['temp_status' => '0'];
delete($var_con, "sp_deposit_temp", $var_cond);
?>

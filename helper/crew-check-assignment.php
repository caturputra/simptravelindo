<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Cron untuk cek selisih waktu konfirmasi penugasan
** Dijalankan tiap hari jam 8 WIB
**
*/

require_once '../core/init.php';

$var_sql_checkdate = "
SELECT crew_datefrom, crew_createdat, sp_crew_detail.crew_armadaid, schedule_id, TIMESTAMPDIFF(HOUR,crew_createdat, CURRENT_TIME()) as dispute, crew_createdby, user_id
FROM sp_crew_detail
JOIN sp_crew_armada ON sp_crew_armada.crew_armadaid = sp_crew_detail.crew_armadaid
WHERE crew_action = '0'
";
$var_query_checkdate = mysqli_query($var_con, $var_sql_checkdate);
$var_numrow_checkdate = mysqli_num_rows($var_query_checkdate);
// debug($var_sql_checkdate);
if ($var_numrow_checkdate > 0) {
    for ($i=0; $i <= $var_numrow_checkdate ; $i++) {
        while($var_data_checkdate[$i] = mysqli_fetch_array($var_query_checkdate)) {
            if ($var_data_checkdate[$i]['dispute'] >= 24) {
                $var_data_assignment[$i] = ['crew_action' => '3'];
                $var_cond_assignment[$i] = ['crew_armadaid' =>$var_data_checkdate[$i]['crew_armadaid'], 'schedule_id' => $var_data_checkdate[$i]['schedule_id']];
                $var_update_assignment = update($var_con, "sp_crew_detail", $var_data_assignment[$i], $var_cond_assignment[$i]);
                if (!$var_update_assignment) {
                    writeLog($var_con, ['sistem' => 'tidak bisa update sp_crew_detail, check crew_check-assignment.php'], false);
                } else {
                    notification($var_con, [
                        'from' => $var_data_checkdate[$i]['user_id'],
                        'to' => $var_data_checkdate[$i]['crew_createdby'],
                        'description' => 'Penugasan dibatalkan oleh sistem karena tidak konfirmasi dalam 24 jam.',
                        'type' => 'Sistem',
                    ]);
                    writeLog($var_con, ['sistem' => 'update sp_crew_detail, aksi dibatalkan, check crew_check-assignment.php'], false);
                }
            }
        }
    }
}

// if ($var_numrow_checkdate > 0) {
//     for ($i=0; $i <= $var_numrow_checkdate ; $i++) {
//         while($var_data_checkdate[$i] = mysqli_fetch_array($var_query_checkdate)) {
//             $interval[$i] = date_diff(date_create($var_data_checkdate[$i]['crew_createdat']), date_create(date($var_data_checkdate[$i]['crew_datefrom'])));
//             var_dump($interval[$i]);
//             if ($interval[$i]->format('%h') >= 24 && $interval[$i]->invert = 0) {
//                 $var_data_assignment[$i] = ['crew_action' => '3'];
//                 $var_cond_assignment[$i] = ['crew_armadaid' =>$var_data_checkdate[$i]['crew_armadaid'], 'schedule_id' => $var_data_checkdate[$i]['schedule_id']];
//                 $var_update_assignment = update($var_con, "sp_crew_detail", $var_data_assignment[$i], $var_cond_assignment[$i]);
//                 if (!$var_update_assignment) {
//                     writeLog($var_con, ['sistem' => 'tidak bisa update sp_crew_detail, check crew_check-assignment.php'], false);
//                 } else {
//                     writeLog($var_con, ['sistem' => 'update sp_crew_detail, aksi dibatalkan, check crew_check-assignment.php'], false);
//                 }
//             }
//         }
//     }
// }
?>

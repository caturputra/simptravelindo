<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman yang memuat tanggal tanggal yang sudah dipesan
*/

require_once __DIR__ . '/../core/init.php';

$var_sid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)): "";

$var_sql_user = "
SELECT user_detail_firstname, user_detail_lastname, user_detail_birthdate, user_detail_gender, user_detail_address, user_detail_phone, user_detail_avatar, user_id
FROM sp_user_detail
WHERE user_id = '{$var_sid}'
LIMIT 1
";
$var_query_user = mysqli_query($var_con, $var_sql_user);
$var_numrows_user = mysqli_num_rows($var_query_user);
$var_data_user = mysqli_fetch_array($var_query_user);
if ($var_numrows_user > 0) :
    if ($var_data_user[0] == null || $var_data_user[1] == null || $var_data_user[2] == null || $var_data_user[3] == null || $var_data_user[4] == null || $var_data_user[5] == null || $var_data_user[6] == null) :
        routeUrl('../dashboard/index.php?p=home-' . trim(strtolower($_SESSION['levelname'])));
    endif;
endif;

$var_title = "Pemesanan";

$head_component = [
    'fccss' => '../assets/global/plugins/fullcalendar/fullcalendar.min.css',
];
require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>

<!-- Content -->
<div class="page-content">
    <!-- inner page banner -->
    <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(../assets/global/home/images/background/bg4.jpg);">
        <div class="container">
            <div class="dez-bnr-inr-entry">
                <h1 class="text-white"><?php echo $var_title; ?></h1>
                <p class="text-white">
                    Reservation
                </p>
            </div>
        </div>
    </div>
    <!-- inner page banner END -->
    <!-- Breadcrumb row -->
    <div class="breadcrumb-row">
        <div class="container">
            <ul class="list-inline">
                <li><a href="index.php?p=home.php">Beranda</a></li>
                <li><?php echo $var_title; ?></li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb row END -->

    <!-- contact area -->
    <div class="content-area">
        <!-- Our Awesome Services -->
        <div class="section-full box-shadow bg-white p-t70 p-b40" >
            <div class="container">
                <div class="section-content">
                    <!-- MESSAGE -->
                    <div class="row">
                        <div class="col-md-12">
                            <?php if (isset($_SESSION['notif']['success'])) :  ?>
                                <div class="alert alert-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
                                <?php unsetNotif() ?>
                            <?php elseif (isset($_SESSION['notif']['info'])) : ?>
                                <div class="alert alert-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
                                <?php unsetNotif() ?>
                            <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
                                <div class="alert alert-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
                                <?php unsetNotif() ?>
                            <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
                                <div class="alert alert-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
                                <?php unsetNotif() ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- END MESSAGE -->
                    <div class="row">
                        <div class="col-md-12 text-center section-head">
                            <h2 class="h2">Pemesanan</span></h2>
                            <div class="dez-separator-outer "><div class="dez-separator bg-primary style-liner"></div></div>
                            <div class="clear"></div>
                            <p>Gunakan ikon dibawah untuk transaksi.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="dez-tabs border">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#vehicles-book"><i class="fa fa-bus"></i> Armada</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="vehicles-book" class="tab-pane active">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div id="calendar" class="has-toolbar"> </div>
                                            </div>
                                            <div class="col-md-4">
                                                <h2 class="font-26 text-center" style="margin-top: 2em;">Pesan Armada</span></h2>
                                                <div class="icon-lg-img aligncenter">
                                                    <a href="index.php?p=customer-booking-armada.php" data-toggle="tooltip" title="Pesan Armada"><img class="" src="../images/reservasi-armada.png" alt="reservasi-armada.png" class="img img-responsive"/></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.row-->
                </div>
                <!-- /.section-content -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.section-full -->
    </div>
    <!-- /.contact area END -->
</div>
<!-- /.Content END-->

<?php
$footer_component = [
    'momentdate' => '../assets/global/plugins/moment.js',
    'fcjs' => '../assets/global/plugins/fullcalendar/fullcalendar.min.js',
    'jqueryui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
];

require_once 'inc-template/inc-template-footer.php';
?>
<script type="text/javascript">
$(document).ready(function() {
    $('#calendar').fullCalendar({
        header: {
            right: 'today prev,next',
            center: 'title',
            left: 'month'
        },
        defaultDate: moment(),
        navLinks: true, // can click day/week names to navigate views
        editable: false,
        eventLimit: true, // allow "more" link when too many events
        events: {
            url: 'ajax-get-order.php?type=fetch&id=1',
        },
    });

    $('#calendar-tour').fullCalendar({
        header: {
            right: 'today prev,next',
            center: 'title',
            left: 'month'
        },
        defaultDate: moment(),
        navLinks: true, // can click day/week names to navigate views
        editable: false,
        eventLimit: true, // allow "more" link when too many events
        events: {
            url: 'ajax-get-order.php?type=fetch&id=2',
        },
    });
});

</script>
</body>
</html>

<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman yang digunakan untuk mengirim feedback ke internal travelindo
*/

$var_title = "Hubungi Kami";

require_once __DIR__ . '/../core/init.php';
require_once __DIR__ . '/../inc/helper/Mail.php';

//send email ke admin transport

//set variabel
$var_name = isset($_POST['frm_contact_name']) ? filter_var($_POST['frm_contact_name'], FILTER_SANITIZE_STRING) : "";
$var_email = isset($_POST['frm_contact_email']) ? filter_var($_POST['frm_contact_email'], FILTER_SANITIZE_EMAIL) : "";
$var_message = isset($_POST['frm_contact_message']) ? filter_var($_POST['frm_contact_message'], FILTER_SANITIZE_STRING) : "";
$var_err = array();

if (isset($_POST['btn_send_email'])) {
	if (empty(trim($var_name))) {
		$var_err['err']['name'] = "Please fill in your name.";
	}

	if (empty(trim($var_email))) {
		$var_err['err']['email'] = "Please fill in your email.";
	}

	if (empty(trim($var_message))) {
		$var_err['err']['message'] = "Please fill in your message.";
	}

	//jika lolos validasi
	if (empty($var_err)) {
		// send email
        $mail = new Mail();
        $from = "info@travelindo.com";
        $subject = "Feedback";
        $dir = "content-mail";
        $template = "feedback"; //nama file isi email
        $var_opt = [
            'content' => $var_message
        ];

		if ($mail->sendMail($from, $var_email, $subject, $template, $dir, $var_opt)) {
			echo "<script>alert(\"Terima kasih telah memberikan umpan balik.\")</script>";
			routeUrl('index.php?p=contact-us');
			die();
		} else {
			echo "<script>alert(\"Galat! Pesan tidak terkirim.\")</script>";
			routeUrl('index.php?p=contact-us');
			die();
		}
	}
}


require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>
<!-- Content -->
<div class="page-content">
	<!-- inner page banner -->
	<div class="dez-bnr-inr overlay-black-middle" style="background-image:url(../assets/global/home/images/background/bg4.jpg);">
		<div class="container">
			<div class="dez-bnr-inr-entry">
				<h1 class="text-white"><?= $var_title; ?></h1>
				<p class="text-white">
					Contact Us
				</p>
			</div>
		</div>
	</div>
	<!-- inner page banner END -->
	<!-- Breadcrumb row -->
	<div class="breadcrumb-row">
		<div class="container">
			<ul class="list-inline">
				<li><a href="?p=home.php">Beranda</a></li>
				<li><?= $var_title; ?></li>
			</ul>
		</div>
	</div>
	<!-- Breadcrumb row END -->

	<!-- contact area -->
	<div class="content-area">
		<!-- Our Awesome Services -->
		<div class="section-full awesome-services bg-gray p-t70 p-b70" >
			<div class="container">
				<div class="section-content">
					<div class="row">
						<div class="col-md-12 text-center section-head">
							<h2 class="h2"> Hubungi <span class="text-primary">Travelindo</span></h2>
							<div class="dez-separator-outer "><div class="dez-separator bg-primary style-liner"></div></div>
							<div class="clear"></div>
						</div>
					</div>
					<!-- MESSAGE -->
					<div class="row">
					    <div class="col-md-12">
					        <?php if (isset($_SESSION['notif']['success'])) :  ?>
					            <div class="alert alert-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
					            <?php unsetNotif() ?>
					        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
					            <div class="alert alert-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
					            <?php unsetNotif() ?>
					        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
					            <div class="alert alert-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
					            <?php unsetNotif() ?>
					        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
					            <div class="alert alert-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
					            <?php unsetNotif() ?>
					        <?php endif; ?>
					    </div>
					</div>
					<!-- END MESSAGE -->
					<div class="row">
						<div class="col-sm-8">
							<h4>Hubungi Kami</h4>
							<div class="status alert alert-success" style="display: none"></div>
							<form action="" method="post" name="contact-form">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<input type="text" class="form-control" required autofocus placeholder="Nama" name="frm_contact_name">
											<span class="help-block warning-text"><?php if(isset($var_err['err']['name'])) { echo $var_err['err']['name'];} ?></span>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<input type="email" class="form-control" required placeholder="Alamat surel" name="frm_contact_email">
											<span class="help-block warning-text"><?php if(isset($var_err['err']['email'])) { echo $var_err['err']['email'];} ?></span>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<textarea id="message" required class="form-control" rows="10" placeholder="Pesan" name="frm_contact_message"></textarea>
											<span class="help-block warning-text"><?php if(isset($var_err['err']['message'])) { echo $var_err['err']['message'];} ?></span>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<div class="pull-right">
												<input type="submit" class="btn btn-primary btn-lg" name="btn_send_email" value="Kirim">
											</div>
										</div>
									</div>
								</div>
							</form>
						</div><!--/.col-sm-8-->
						<div class="col-sm-4">
							<h4>Peta Lokasi</h4>
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.1400424233357!2d110.39132761477802!3d-7.774971494396688!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6912dc289a3e29cd!2sTravelindo!5e0!3m2!1sen!2s!4v1488907912907" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div><!--/.col-sm-4-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- contact area END -->
</div>
<!-- Content END-->

<?php require_once 'inc-template/inc-template-footer.php'; ?>
</body>
</html>

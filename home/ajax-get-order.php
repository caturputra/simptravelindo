<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Ajax untuk mendapatkan detail pesanan
** yang ditampilkan di full calendar
*/

require_once '../core/init.php';
$type = isset($_GET['type']) ? mysqli_escape_string($var_con, filter_var($_GET['type'], FILTER_SANITIZE_STRING)) : "";
$id = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT)) : "";

if($type == 'fetch') {
    switch ($id) {
        case $id:
        $data = [];
        $var_sql_data = "SELECT * FROM sp_order_transaction";
        $var_query_data = mysqli_query($var_con, $var_sql_data);
        while ($var_data_data = mysqli_fetch_array($var_query_data)) {
            $temp = [];
            $temp['id'] = $var_data_data['od_id'];
            $temp['title'] = $var_data_data['od_id'];
            $temp['start'] = $var_data_data['od_from'];
            $temp['end'] = $var_data_data['od_to'];
            $temp['url'] = "http://ta.simp.dev/dashboard/index.php?p=booking-armada.php&id=".$var_data_data['od_id'];
            switch ($var_data_data['od_status']) {
                case 0:
                $temp['backgroundColor'] = "purple";
                break;
                case 1:
                $temp['backgroundColor'] = "green";
                break;
                case 2:
                $temp['backgroundColor'] = "red";
                break;
                case 3:
                $temp['backgroundColor'] = "yellow";
                break;
                case 4:
                $temp['backgroundColor'] = "grey";
                break;
            }

            array_push($data, $temp);
        }
        break;
    }
}

echo json_encode($data);
?>

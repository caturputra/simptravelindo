<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman yang memuat detail pesana
*/

$var_title = "Detail Pemesanan";

require_once __DIR__ . '/../core/init.php';

//proses store item
$var_cart_id = session_id();

//cek isi sesi tanggal keberangkatan dan tanggal pulang
$var_sesfrom = isset($_SESSION['book']['date_from']) ? $_SESSION['book']['date_from'] : "";
$var_sesto = isset($_SESSION['book']['date_to']) ? $_SESSION['book']['date_to'] : "";

//pesan peringatan jika belum set tanggal
if (empty($var_sesfrom) && empty($var_sesto)) {
    setNotif(['danger' => 'Mohon tentukan tanggal berangkat dan pulang terlebih dahulu.']);
    routeUrl('index.php?p=customer-booking-armada');
    die();
}

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>

<!-- Content -->
<div class="page-content">
    <!-- inner page banner -->
    <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(../assets/global/home/images/background/bg4.jpg);">
        <div class="container">
            <div class="dez-bnr-inr-entry">
                <h1 class="text-white"><?php echo $var_title; ?></h1>
                <p class="text-white">
                    Checkout
                </p>
            </div>
        </div>
    </div>
    <!-- inner page banner END -->
    <!-- Breadcrumb row -->
    <div class="breadcrumb-row">
        <div class="container">
            <ul class="list-inline">
                <li><a href="?p=home.php">Beranda</a></li>
                <li><?php echo $var_title; ?></li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb row END -->

    <!-- contact area -->
    <div class="content-area">
        <!-- Our Awesome Services -->
        <div class="section-full awesome-services bg-white p-t70 p-b70" >
            <div class="container">
                <div class="section-content">
                    <div class="row">
                        <div class="col-sm-12 text-center section-head">
                            <h2 class="h2">Detail Pemesanan</span></h2>
                            <div class="dez-separator-outer "><div class="dez-separator bg-primary style-liner"></div></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <!-- MESSAGE -->
                    <div class="row">
                        <div class="col-sm-12">
                            <?php if (isset($_SESSION['notif']['success'])) :  ?>
                                <div class="alert alert-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
                                <?php unsetNotif() ?>
                            <?php elseif (isset($_SESSION['notif']['info'])) : ?>
                                <div class="alert alert-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
                                <?php unsetNotif() ?>
                            <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
                                <div class="alert alert-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
                                <?php unsetNotif() ?>
                            <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
                                <div class="alert alert-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
                                <?php unsetNotif() ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- END MESSAGE -->

                    <div class="row">
                        <?php
                        //retrive armada temp
                        $var_sql_book = "
                        SELECT ot.temp_id, ot.temp_data, ot.temp_sessionid, u.user_detail_firstname, u.user_detail_lastname, u.user_detail_phone, u.user_detail_address, u.user_detail_avatar, am.model_name, am.model_image, am.model_fuel, am.model_fuelmax, at.type_seat, at.type_model, type_price
                        FROM sp_order_temp ot
                        JOIN sp_user_detail u ON u.user_id = ot.temp_id
                        JOIN sp_armada_model am ON am.model_id = ot.temp_data
                        JOIN sp_armada_type at ON at.type_id = am.type_id
                        WHERE ot.temp_sessionid = '{$var_cart_id}'
                        ";
                        $var_query_book = mysqli_query($var_con, $var_sql_book);
                        $var_price = 0;
                        while($var_data_bookdetail = mysqli_fetch_array($var_query_book)){
                            $var_price += $var_data_bookdetail['type_price'];
                        }
                        ?>
                        <!-- Left part start -->
                        <div class="col-sm-4 m-b30">
                            <div class="icon-bx-wraper bx-style-1 p-a10 left m-b30">
                                <div class="icon-bx-md text-primary bg-white m-b5"> <a href="#" class="icon-cell"><i class="fa fa-user"></i></a> </div>
                                <div class="icon-content">
                                    <h5 class="dez-tilte text-uppercase">Pemesan</h5>
                                    <div class="row">
                                        <div class="table-responsive">
                                            <div class="col-sm-12">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                        <tr>
                                                            <td>Nama</td>
                                                            <td>
                                                                <strong><?php echo ucwords($var_data_bookdetail['user_detail_firstname']) . " " . ucwords($var_data_bookdetail['user_detail_lastname']) ?></strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Telepon</td>
                                                            <td>
                                                                <?php echo ucwords($var_data_bookdetail['user_detail_phone']) ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Alamat</td>
                                                            <td>
                                                                <?php echo ucwords($var_data_bookdetail['user_detail_address']) ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="icon-bx-wraper bx-style-1 p-a10 left m-b30">
                                <div class="icon-bx-md text-primary bg-white m-b5"> <a href="#" class="icon-cell"><i class="fa fa-paper-plane"></i></a> </div>
                                <div class="icon-content">
                                    <h5 class="dez-tilte text-uppercase">Detail</h5>
                                    <div class="row">
                                        <?php
                                        $awal  = strtotime($_SESSION['book']['date_from']);
                                        $akhir = strtotime($_SESSION['book']['date_to']);
                                        $diff  = $akhir - $awal;

                                        $jam   = floor($diff / (60 * 60));
                                        $menit = $diff - $jam * (60 * 60);
                                        ?>
                                        <div class="table-responsive">
                                            <div class="col-sm-12">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <th>Berangkat</th>
                                                        <th>Pulang</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><?php echo dateFormat($_SESSION['book']['date_from']) ?></td>
                                                            <td><?php echo dateFormat($_SESSION['book']['date_to']) ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                                <table class="table table-bordered">
                                                    <thead>
                                                        <th>Lama</th>
                                                        <th>Total Bayar</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><?php echo $jam . " Jam " . $menit . " Menit" ?></td>
                                                            <td>
                                                                <?php
                                                                    if ($jam <= 12) {
                                                                        $price = $var_price;
                                                                        // $price += $price;
                                                                        $_SESSION['book']['price'] = $price;
                                                                        echo "Rp " . number_format($price, 2, ',', '.');
                                                                    }

                                                                    if ($jam > 12) {
                                                                        $count = round($jam / 12);
                                                                        $price = $count * $var_price;
                                                                        // $price += $price;
                                                                        $_SESSION['book']['price'] = $price;
                                                                        echo "Rp " . number_format($price, 2, ',', '.');
                                                                    }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Left part start END -->
                        <?php
                        //retrive armada temp
                        $var_sql_book = "
                        SELECT ot.temp_id, ot.temp_data, ot.temp_sessionid, u.user_detail_firstname, u.user_detail_lastname, u.user_detail_phone, u.user_detail_address, u.user_detail_avatar, am.model_name, am.model_image, am.model_fuel, am.model_fuelmax, at.type_seat, at.type_model, at.type_price
                        FROM sp_order_temp ot
                        JOIN sp_user_detail u ON u.user_id = ot.temp_id
                        JOIN sp_armada_model am ON am.model_id = ot.temp_data
                        JOIN sp_armada_type at ON at.type_id = am.type_id
                        WHERE ot.temp_sessionid = '{$var_cart_id}'
                        ";
                        // debug($var_sql_book);
                        $var_query_book = mysqli_query($var_con, $var_sql_book);
                        while($var_data_book = mysqli_fetch_array($var_query_book)):
                            ?>
                            <!-- Right part start -->
                            <div class="col-sm-8">
                                <div class="panel panel-primary bx-style-1 p-a30 left">
                                    <!-- Armada Cart -->
                                    <div class="blog-post blog-md clearfix date-style-1">
                                        <div class="dez-post-media dez-img-effect"> <a href="blog-half-img.html#"><img src="../images/armada/<?php echo $var_data_book['model_image']; ?>" alt="<?php echo $var_data_book['model_image']; ?>" class="img img-responsive"></a> </div>
                                        <div class="dez-post-info">
                                            <div class="dez-post-title ">
                                                <h3 class="post-title"><?php echo $var_data_book['model_name'] ?>
                                                </h3>
                                            </div>
                                            <div class="dez-post-text">
                                                <p>
                                                    Jenis : <?php echo ucwords($var_data_book['type_model']) ?> <br>
                                                    Maks. kursi : <?php echo ucwords($var_data_book['type_seat']) ?> <br>
                                                    Harga : <?php echo number_format($var_data_book['type_price'], 2, ',', '.') ?> / 12 Jam<br>
                                                    Jenis bahan bakar: <?php echo ucwords($var_data_book['model_fuel']) ?> <br>
                                                    Kapasitas tangki : <?php echo ucwords($var_data_book['model_fuelmax']) ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Right part start END -->
                        <?php endwhile; ?>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <a href="index.php?p=armada-cart.php&amp;act=del&amp;item=<?php echo $var_data_book['temp_data']; ?>"class="site-button orange"><i class="fa fa-chevron-left"></i> Batal</a>

                                    <div class="pull-right">
                                        <a href="index.php?p=customer-placedbook.php&amp;act=book" class="site-button right"><i class="fa fa-check-circle"> Proses</i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- END ARMADA CART -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.sectoin-full -->
    </div>
    <!-- /.content-area -->
</div>
<!-- /.page-content -->

<?php require_once 'inc-template/inc-template-footer.php'; ?>
</body>
</html>

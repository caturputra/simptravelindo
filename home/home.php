<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman Beranda pengguna
*/

$var_title = "Beranda";

require_once '../core/init.php';

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>

<!-- Content -->
<div class="page-content">
    <!-- inner page banner -->
	<div class="dez-bnr-inr overlay-black-middle" style="background-image:url(../assets/global/home/images/background/bg4.jpg);">
		<div class="container">
			<div class="dez-bnr-inr-entry">
				<h1 class="text-white"><?= $var_title; ?></h1>
			</div>
		</div>
	</div>
	<!-- inner page banner END -->
	<!-- Breadcrumb row -->
	<div class="breadcrumb-row">
		<div class="container">
			<ul class="list-inline">
				<li><?= $var_title; ?></li>
			</ul>
		</div>
	</div>

<!-- Our Awesome Services -->
<div class="section-full box-shadow bg-white p-t70 p-b40" >
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-md-12 text-center section-head">
                    <h2 class="h2"><span class="text-primary">Halaman Media Sosial</span></h2>
                    <div class="dez-separator-outer"><div class="dez-separator bg-primary style-liner"></div></div>
                    <div class="clear"></div>
                </div>
                <div class="icon-bx-wraper center col-md-4 col-sm-4 bg-green icon-box p-a0">
                    <div class="icon-bx-lg text-white"> <a href="index-3.html#" class="icon-cell"><i class="fa fa-facebook"></i></a> </div>
                    <div class="icon-content text-white p-lr15 p-tb30">
                        <h2 class="dez-tilte font-weight-900 m-b10">Facebook</h2>
                        <p><a href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=5&cad=rja&uact=8&ved=0ahUKEwisrqCzm7_SAhVElZQKHcyrAYYQFgg7MAQ&url=https%3A%2F%2Fwww.facebook.com%2Fpages%2FMitra-Persada-Travelindo%2F1412721125608003&usg=AFQjCNEtvLeEjJb1LVuMW3ExgokfxWymSQ" target="_blank">Pergi Ke Halaman!</a></p>
                    </div>
                </div>
                <div class="icon-bx-wraper center col-md-4 col-sm-4 bg-yello icon-box p-a0">
                    <div class="icon-bx-lg text-white"> <a href="index-3.html#" class="icon-cell"><i class="fa fa-twitter"></i></a> </div>
                    <div class="icon-content text-white p-lr15 p-tb30">
                        <h2 class="dez-tilte font-weight-900 m-b10">Twitter</h2>
                        <p><a href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=5&cad=rja&uact=8&ved=0ahUKEwisrqCzm7_SAhVElZQKHcyrAYYQFgg7MAQ&url=https%3A%2F%2Fwww.facebook.com%2Fpages%2FMitra-Persada-Travelindo%2F1412721125608003&usg=AFQjCNEtvLeEjJb1LVuMW3ExgokfxWymSQ" target="_blank">Pergi Ke Halaman!</a></p>
                    </div>
                </div>
                <div class="icon-bx-wraper center col-md-4 col-sm-4 bg-red icon-box p-a0">
                    <div class="icon-bx-lg text-white"> <a href="index-3.html#" class="icon-cell"><i class="fa fa-google-plus"></i></a> </div>
                    <div class="icon-content text-white p-lr15 p-tb30">
                        <h2 class="dez-tilte font-weight-900 m-b10">Google Plus</h2>
                        <p><a href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=5&cad=rja&uact=8&ved=0ahUKEwisrqCzm7_SAhVElZQKHcyrAYYQFgg7MAQ&url=https%3A%2F%2Fwww.facebook.com%2Fpages%2FMitra-Persada-Travelindo%2F1412721125608003&usg=AFQjCNEtvLeEjJb1LVuMW3ExgokfxWymSQ" target="_blank">Pergi Ke Halaman!</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Our Awesome Services END -->
<!-- Our Armada -->
<div class="section-full text-white bg-img-fix p-t70 p-b40 bg-primary choose-us" style="background-image:url(../assets/global/home/images/background/patter-bg.png);">
    <div class="container">
        <div class="section-head  text-center text-white">
            <h2 class="h2">Armada Kami</h2>
            <div class="dez-separator-outer "><div class="dez-separator bg-white style-liner"></div></div>
        </div>
        <div class="row">
            <?php
            //retrieve armada
            $var_sql_armada = "
			SELECT am.model_image, am.model_name, at.type_seat
			FROM sp_armada a
			JOIN sp_model_type mt ON mt.armada_regnumber = a.armada_regnumber
			JOIN sp_armada_model am ON am.model_id = mt.model_id
			JOIN sp_armada_type at ON at.type_id = am.type_id
			WHERE a. armada_status = 'approved'
			GROUP BY mt.model_id
            ";

            $var_query_armada= mysqli_query($var_con, $var_sql_armada);
            while ($var_data_armada = mysqli_fetch_row($var_query_armada)) :
                ?>
                <div class="col-md-3 col-sm-6 m-b30">
                    <div class="icon-bx-wraper bx-style-1 p-a20 center radius-sm">
                        <span class="icon-cell text-primary thumbnail"><img class="img-responsive img-rounded" style="height: 7em" src="./../images/armada/<?= $var_data_armada[0]?>" alt="<?= $var_data_armada[0]?>"></span>
                        <div class="icon-content m-t10">
                            <h4 class="dez-tilte"><?= $var_data_armada[1]?> </h4>
                            <p>Total kursi:  <?= $var_data_armada[2] ?></p>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</div>
<!-- Our Armada End -->

<!-- Latest News -->
<div class="section-full p-t70 p-b10 box-shadow bg-img-fix">
    <div class="container">
        <div class="row">
            <div class="section-head text-center">
                <h2  class="h2"><span class="text-primary">Testimoni</span></h2>
                <div class="dez-separator-outer"><div class="dez-separator bg-primary style-liner"></div></div>
            </div>
        </div>
        <div class="section-content">
            <div>
                <div class="tab-content client-think">
                    <div role="tabpanel" class="tab-pane active p-a40 fade in" id="testmonial1">
                        <p>Pelayanan di PT. Mitra Persada Travelindo sangat bagus, cepat dan ramah.</p>
                        <div class="dez-separator-outer"><div class="dez-separator bg-primary style-liner"></div></div>
                        <h5>Sherly</h5>
                    </div>
                    <div role="tabpanel" class="tab-pane p-a40 fade" id="testmonial2">
                        <p>Well Done</p>
                        <div class="dez-separator-outer"><div class="dez-separator bg-primary style-liner"></div></div>
                        <h5>Jackson Hernandez</h5>
                    </div>
                    <div role="tabpanel" class="tab-pane p-a40 fade" id="testmonial3">
                        <p>Pelayanan yang sekarang sudah baik, alangkah lebih baik ditingkatkan lagi :D .</p>
                        <div class="dez-separator-outer"><div class="dez-separator bg-primary style-liner"></div></div>
                        <h5>Sasono</h5>
                    </div>
                </div>
                <!-- Nav tabs -->
				<style media="screen">
					img.testimonial{
						height: 6em !important;
						width: 7em;
						image-resolution: from-image;
					}
				</style>
                <ul class="client-pic text-center m-t40" role="tablist">
                    <li role="presentation" class="active">
                        <a href="index-3.html#testmonial1" aria-controls="testmonial1" role="tab" data-toggle="tab">
                            <img src="https://cdn1.iconfinder.com/data/icons/user-pictures/100/female1-512.png" class="img testimonial"/>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="index-3.html#testmonial2" aria-controls="testmonial2" role="tab" data-toggle="tab">
                            <img src="https://cdn0.iconfinder.com/data/icons/user-pictures/100/matureman2-512.png" class="img testimonial"/>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="index-3.html#testmonial3" aria-controls="testmonial3" role="tab" data-toggle="tab">
                            <img src="https://www.instantassignmenthelp.com.au/images/data/img/1483015988man5.png" class="img testimonial"/>
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
            </div>
        </div>
    </div>
</div>
<!-- Latest News END -->

</div>
<!-- Content END-->

<?php require_once 'inc-template/inc-template-footer.php'; ?>

<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman yang memuat tentang travelindo
*/

$var_title = "Tentang Kami";

require_once __DIR__ . '/../core/init.php';

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>

<!-- Content -->
<div class="page-content">
    <!-- inner page banner -->
    <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(../assets/global/home/images/background/bg4.jpg);">
        <div class="container">
            <div class="dez-bnr-inr-entry">
                <h1 class="text-white"><?php echo $var_title; ?></h1>
                <p class="text-white">
                    About us
                </p>
            </div>
        </div>
    </div>
    <!-- inner page banner END -->
    <!-- Breadcrumb row -->
    <div class="breadcrumb-row">
        <div class="container">
            <ul class="list-inline">
                <li><a href="?p=home.php">Beranda</a></li>
                <li><?php echo $var_title; ?></li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb row END -->

    <!-- contact area -->
    <div class="content-area">
        <!-- Our Awesome Services -->
        <div class="section-full awesome-services bg-white p-t70 p-b70" >
            <div class="container">
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12 text-center section-head">
                            <h2 class="h2"> Apa itu <span class="text-primary">Travelindo</span>?</h2>
                            <div class="dez-separator-outer "><div class="dez-separator bg-primary style-liner"></div></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-8" style="text-align: justify;">
                            <div class="icon-bx-wraper bx-style-2 m-b30 p-a20 left">
                                <div class="icon-bx-md radius bg-primary"> <a href="about-1.html#" class="icon-cell"><i class="fa fa-info"></i></a> </div>
                                <div class="icon-content p-l40">
                                    <h5 class="dez-tilte ">Introduction</h5>
                                    <div class="dez-separator-outer ">
                                        <div class="dez-separator bg-primary style-liner"></div>
                                    </div>
                                    <p>Founded in 1995, Travelindo is a travel agent serves Indonesian domestic and international flight reservation and ticketing. Along with ticketing services, Travelindo is a tour operator serving all the tourism need such as tour packages, transportation, hotel information and reservation in Jakarta, Bali, Yogyakarta and other places in Indonesia. As the member of ASITA (Association of Indonesia Tours & Travel Agencies) and within more than fifteen years experience in travel accommodation service business of the online travel services provider then Travelindo will simplify your journey arrangement throughout Indonesia.</p>
                                </div>
                            </div>

                            <div class="icon-bx-wraper bx-style-2 m-b30 p-a20 left">
                                <div class="icon-bx-md radius bg-primary"> <a href="about-1.html#" class="icon-cell"><i class="fa fa-info"></i></a> </div>
                                <div class="icon-content p-l40">
                                    <h5 class="dez-tilte ">All For Your Journey Plan</h5>
                                    <div class="dez-separator-outer ">
                                        <div class="dez-separator bg-primary style-liner"></div>
                                    </div>
                                    <p>From the very first time, Travelindo realizes the importance of a successful travel plan and always ensures customers get the best possible service to create an unforgettable travel experience. Travelindo is a tour operator, a ground handler, a travel agent, as well as the only agency in Yogyakarta that offers regular tours, special interests, sports and hobbies, MICE, outbound training with specialization in custom holidays; organize customers� liberated self-arrangement tour itineraries suitable to individual and groups of travelers. Hence, travelers may compromise what they want to 'see and do' which is delivered at both a pace and a budget matching to the travelers need. Travelindo is located in Yogyakarta - speaks the language; knows the customs, the cultures, and the laws. With experiences and local network, all ground-handling requirements are in the safe hands, from transport to accommodation and local guides. Consequently, costumers will perfectly enjoy their precious moment during the vacation. Travelindo is an authorized travel agent for major airlines operated in Indonesia and member of IATA (International Air Transport Association) with full liability on providing both domestic and international flight ticketing connecting most of major cities to other cities in Indonesia and to overseas cities.</p>
                                </div>
                            </div>

                            <div class="icon-bx-wraper bx-style-2 m-b30 p-a20 left">
                                <div class="icon-bx-md radius bg-primary"><i class="fa fa-info"></i></div>
                                <div class="icon-content p-l40">
                                    <h5 class="dez-tilte ">Make Things Easy</h5>
                                    <div class="dez-separator-outer ">
                                        <div class="dez-separator bg-primary style-liner"></div>
                                    </div>
                                    <p>Travelindo has built a well-organized widespread network, provides hotel information and flight ticketing all over Indonesia such as Bali, Bandung, Jakarta, Yogyakarta, Medan and much more cities to support the scheduled tour market. There is no more reason to be fatigued in planning a trip before the trip itself is started as sometimes the plan needs a plenty of effort, time and more particular cost. Nowadays you may arrange your journey, adventure or just sight seeing in such of a simple action; especially, since some domestic airlines apply electronic ticketing, your journey arrangement is purely easy. You can find all of the services in one box namely Travelindo.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-4">
                            <div class="icon-bx-wraper bx-style-2 m-b30 p-a20 right">
                                <div class="icon-bx-md radius bg-primary"><i class="fa fa-institution"></i></div>
                                <div class="icon-content p-r40">
                                    <h5 class="dez-tilte">Kantor</h5>
                                    <div class="dez-separator-outer ">
                                        <div class="dez-separator bg-primary style-liner"></div>
                                    </div>
                                    <p>
                                        <div class="table-responsive table-striped">
                                            <table class="table text-left">
                                                <tr>
                                                    <tr>
                                                        <td>Nama perusahaan</td>
                                                        <td><strong>MITRA PERSADA TRAVELINDO</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Berdiri</td>
                                                        <td>Yogyakarta , 9th January 1995</td>
                                                    </tr>
                                                </tr>
                                            </table>
                                        </div>
                                    </p>
                                </div>
                            </div>
                            <div class="icon-bx-wraper bx-style-2 m-b30 p-a20  right">
                                <div class="icon-bx-md radius bg-primary"><i class="fa fa-institution"></i></div>
                                <div class="icon-content p-r40">
                                    <h5 class="dez-tilte">Kantor pusat</h5>
                                    <div class="dez-separator-outer ">
                                        <div class="dez-separator bg-primary style-liner"></div>
                                    </div>
                                    <p>
                                        <div class="table-responsive table-striped">
                                            <table class="table text-left">
                                                <tr>
                                                    <tr>
                                                        <td>Alamat</td>
                                                        <td>Yogyakarta    :  Jl. Pringgodani No. 12, Mrican Baru, Yogyakarta 55281 , Indonesia</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pemesanan tiket</td>
                                                        <td>+62 274 511100 (hunting 6 lines)</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tur</td>
                                                        <td>+62 274 541409 (hunting 4 lines)</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fax</td>
                                                        <td>+62 274 541402</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Surel</td>
                                                        <td>info@travelindo.com</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Situs web</td>
                                                        <td>www.travelindo.com</td>
                                                    </tr>
                                                </tr>
                                            </table>
                                        </div>
                                    </p>
                                </div>
                            </div>
                            <div class="icon-bx-wraper bx-style-2 m-b30 p-a20 right">
                                <div class="icon-bx-md radius bg-primary"><i class="fa fa-institution"></i></div>
                                <div class="icon-content p-r40">
                                    <h5 class="dez-tilte ">Kantor cabang</h5>
                                    <div class="dez-separator-outer ">
                                        <div class="dez-separator bg-primary style-liner"></div>
                                    </div>
                                    <p>
                                        <h4 class="text-left font-16 font-weight-500">Purwokerto</h4>

                                        <div class="table-responsive table-striped">
                                            <table class="table text-left">
                                                <tr>
                                                    <tr>
                                                        <td>Purwokerto</td>
                                                        <td>Tel. 0281-7611826</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Surel</td>
                                                        <td>pwl@travelindo.com</td>
                                                    </tr>
                                                </tr>
                                            </table>
                                        </div>

                                        <h4 class="text-left font-16 font-weight-500">Bali</h4>

                                        <div class="table-responsive table-striped">
                                            <table class="table text-left">
                                                <tr>
                                                    <tr>
                                                        <td>Bali</td>
                                                        <td>Balinirwana Tour and Travel
                                                            Kori Nuansa Utama Timur, FF-2 Jimbaran, Kuta, Bali-Indonesia</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Surel</td>
                                                            <td>balinirwana@travelindo.com</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Surel</td>
                                                            <td>balinirwana@travelindo.com</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Telepon</td>
                                                            <td>+62 361 8959207</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Fax</td>
                                                            <td>+62 361 8959206</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Registrasi</td>
                                                            <td>Dinas pariwisata dan telekomunikasi No. 625/D.2/BPW/III/96</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Managemen</td>
                                                            <td>Direktur utama- Mr. Andreas Perdana Putra | Direktur pemasaran- Mr. A. Radix Atmono</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Keanggotaan</td>
                                                            <td>ASITA - Association of Indonesia Tours & Travel Agencies No. 096/XI/DPP/97.
                                                                IATA � International Air Transport Association No. 15-3 0708 4.</td>
                                                            </tr>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Our Awesome Services END -->
            </div>
            <!-- contact area END -->
        </div>
        <!-- Content END-->

    <?php require_once 'inc-template/inc-template-footer.php'; ?>
    </body>
</html>

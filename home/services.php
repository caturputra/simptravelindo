<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman yang memuat layanan yang disediakan oleh travelindo
*/

$var_title = "layanan Kami";

require_once __DIR__ . '/../core/init.php';

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>

<!-- Content -->
<div class="page-content">
    <!-- inner page banner -->
    <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(../assets/global/home/images/background/bg4.jpg);">
        <div class="container">
            <div class="dez-bnr-inr-entry">
                <h1 class="text-white"><?php echo $var_title; ?></h1>
                <p class="text-white">
                    Our services
                </p>
            </div>
        </div>
    </div>
    <!-- inner page banner END -->
    <!-- Breadcrumb row -->
    <div class="breadcrumb-row">
        <div class="container">
            <ul class="list-inline">
                <li><a href="?p=home.php">Beranda</a></li>
                <li><?php echo $var_title; ?></li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb row END -->

    <!-- contact area -->
    <div class="content-area">
        <!-- Our Awesome Services -->
        <div class="section-full awesome-services bg-white p-t70 p-b70" >
            <div class="container">
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12 text-center section-head">
                            <h2 class="h2"><span class="text-primary">Layanan</span> Kami</h2>
                            <div class="dez-separator-outer "><div class="dez-separator bg-primary style-liner"></div></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-8" style="text-align: justify;">
                            <div class="icon-bx-wraper bx-style-2 m-b30 p-a20 left">
                                <div class="icon-bx-md radius bg-primary"> <a href="about-1.html#" class="icon-cell"><i class="fa fa-info"></i></a> </div>
                                <div class="icon-content p-l40">
                                    <h5 class="dez-tilte ">In / Out Airport</h5>
                                    <div class="dez-separator-outer ">
                                        <div class="dez-separator bg-primary style-liner"></div>
                                    </div>
                                    <p>Mitra Persada Travelindo melayani penjemputan untuk rombongan wisatawan atau perorangan, juga pemanduan pada saat wisatawan yang akan pulang ke tempat asalnya tentunya dengan pelayanan yang memuaskan dari Travelindo.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-8 col-md-offset-1" style="text-align: justify;">
                            <div class="icon-bx-wraper bx-style-2 m-b30 p-a20 left">
                                <div class="icon-bx-md radius bg-primary"> <a href="about-1.html#" class="icon-cell"><i class="fa fa-info"></i></a> </div>
                                <div class="icon-content p-l40">
                                    <h5 class="dez-tilte ">City Tour</h5>
                                    <div class="dez-separator-outer ">
                                        <div class="dez-separator bg-primary style-liner"></div>
                                    </div>
                                    <p>Mitra Persada Travelindo melayani penyewaan bus untuk menemani anda beriwisata di pulau jawa dan sekitarnya, kualitas dan pelayanan yang kami tawarkan tentunya akan membuat Anda merasa aman dan nyaman menggunakan jasa Travelindo.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-8 col-md-offset-2" style="text-align: justify;">
                            <div class="icon-bx-wraper bx-style-2 m-b30 p-a20 left">
                                <div class="icon-bx-md radius bg-primary"><i class="fa fa-info"></i></div>
                                <div class="icon-content p-l40">
                                    <h5 class="dez-tilte ">Tour Package</h5>
                                    <div class="dez-separator-outer ">
                                        <div class="dez-separator bg-primary style-liner"></div>
                                    </div>
                                    <p>Mitra Persada Travelindo melayani penyewaan bus untuk menemani anda beriwisata di pulau jawa dan sekitarnya, kualitas dan pelayanan yang kami tawarkan tentunya akan membuat Anda merasa aman dan nyaman menggunakan jasa Travelindo.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Our Awesome Services END -->
    </div>
    <!-- contact area END -->
</div>
<!-- Content END-->

<?php require_once 'inc-template/inc-template-footer.php'; ?>
</body>
</html>

<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman yang menampilkan daftar cart armada yang tersedia
** untuk tanggal yang ditentukan pengguna
*/

$var_title = "Pemesanan armada";

require_once __DIR__ . '/../core/init.php';

if (checkSid() === false) {
    setNotif(['warning' => 'Silahkan \'login\' terlebih dahulu. <a href="../dashboard/login.php">Masuk!</a>']);
    routeUrl('index.php?p=reservation');
    die();
}

//update 17/12/2017: membatasi pemesanan hanya oleh member atau agen
if ($_SESSION['levelid'] != '4' && $_SESSION['levelid'] != '5') {
    setNotif(['warning' => 'Anda tidak diperbolehkan melakukan pemesanan.']);
    routeUrl('index.php?p=reservation');
    die();
}

$var_sid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)): "";

$var_sql_user = "
SELECT user_detail_firstname, user_detail_lastname, user_detail_birthdate, user_detail_gender, user_detail_address, user_detail_phone, user_detail_avatar, user_id
FROM sp_user_detail
WHERE user_id = '{$var_sid}'
LIMIT 1
";
$var_query_user = mysqli_query($var_con, $var_sql_user);
$var_numrows_user = mysqli_num_rows($var_query_user);
$var_data_user = mysqli_fetch_array($var_query_user);
if ($var_numrows_user > 0) :
    if ($var_data_user[0] == null || $var_data_user[1] == null || $var_data_user[2] == null || $var_data_user[3] == null || $var_data_user[4] == null || $var_data_user[5] == null || $var_data_user[6] == null) :
        routeUrl('../dashboard/index.php?p=home-' . trim(strtolower($_SESSION['levelname'])));
    endif;
endif;

$var_sesfrom = isset($_SESSION['book']['date_from']) ? $_SESSION['book']['date_from'] : "";
$var_sesto = isset($_SESSION['book']['date_to']) ? $_SESSION['book']['date_to'] : "";

if (isset($_POST['btn_cancel'])) {
    unset($_SESSION['book']);
    routeUrl('customer-booking-armada');
}

//tanggal Reservation
if (isset($_POST['btn_submit_date'])) {
    $var_book_date = isset($_POST['frm_date_book']) ? mysqli_escape_string($var_con, $_POST['frm_date_book']) : "";
    $var_service_type = isset($_POST['frm_service_book']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_service_book'], FILTER_SANITIZE_STRING)) : "";
    $var_request = isset($_POST['frm_request']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_request'], FILTER_SANITIZE_STRING)) : "";
    $var_dropship = isset($_POST['frm_dropship']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_dropship'], FILTER_SANITIZE_STRING)) : "";
    $var_err = [];

    if (empty($var_book_date)) {
        $var_err['date'] = "Mohon pilih tanggal pemesanan.";
    }

    if (empty(trim($var_service_type))) {
        $var_err['service'] = 'Mohon pilih jenis layanan.';
    }

    //update 17/12/2017: tambah form lokasi pemjemputan
    if (empty(trim($var_dropship))) {
        $var_err['dropship'] = 'Mohon masukkan detail penjemputan.';
    }

    // update 08/12/2017: batas waktu pemesanan hanya jam 8 - 22
    // $var_timenow = date('H:i:s');
    // if (strtotime($var_timenow) < strtotime(date('H:i:s', mktime(date('08'), 0, 0))) || strtotime($var_timenow) > strtotime(date('H:i:s', mktime(date('20'), 0, 0)))) {
    //     $var_err['date'] = "Pemesanan hanya dibuka pukul 08:00 - 22:00 WIB.";
    // }

    if (empty($var_err)) {
        $var_xdate = explode("-", trim($var_book_date));

        $var_xdate_timefrom = explode(" ", trim($var_xdate[0]));
        $var_xdate_from = explode("/", trim($var_xdate_timefrom[0]));
        $var_book_from = $var_xdate_from[2]."-".$var_xdate_from[1]."-".$var_xdate_from[0] . " " . $var_xdate_timefrom[1];

        $var_xdate_timeto = explode(" ", trim($var_xdate[1]));
        $var_xdate_to = explode("/", trim($var_xdate_timeto[0]));
        $var_book_to = $var_xdate_to[2] . "-" . $var_xdate_to[1] . "-" . $var_xdate_to[0] . " " . $var_xdate_timeto[1];

        $_SESSION['book']['date_from'] = $var_book_from;
        $_SESSION['book']['date_to'] = $var_book_to;
        $_SESSION['book']['service'] = $var_service_type;
        $_SESSION['book']['request'] = $var_request;
        $_SESSION['book']['dropship'] = $var_dropship;

        routeUrl('index.php?p=customer-booking-armada');
    }
}

$head_component = [
    'bootstrap-daterangepicker' => '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css',
    'datepickercss'=>'../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>

<!-- Content -->
<div class="page-content">
    <!-- inner page banner -->
    <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(../assets/global/home/images/background/bg4.jpg);">
        <div class="container">
            <div class="dez-bnr-inr-entry">
                <h1 class="text-white"><?php echo $var_title; ?></h1>
                <p class="text-white">
                    Vehicle Reservation
                </p>
            </div>
        </div>
    </div>
    <!-- inner page banner END -->
    <!-- Breadcrumb row -->
    <div class="breadcrumb-row">
        <div class="container">
            <ul class="list-inline">
                <li><a href="?p=home.php">Beranda</a></li>
                <li><?php echo $var_title; ?></li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb row END -->

    <!-- contact area -->
    <div class="content-area">
        <!-- Our Awesome Services -->
        <div class="section-full awesome-services bg-grey p-t70 p-b70" >
            <div class="container">
                <div class="section-content">
                    <!-- MESSAGE -->
                    <div class="row">
                        <div class="col-lg-6 col-md-9 col-sm-8 col-xs-12 m-b0 col-sm-offset-3">
                            <?php if (isset($_SESSION['notif']['success'])) :  ?>
                                <div class="alert alert-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
                                <?php unsetNotif() ?>
                            <?php elseif (isset($_SESSION['notif']['info'])) : ?>
                                <div class="alert alert-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
                                <?php unsetNotif() ?>
                            <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
                                <div class="alert alert-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
                                <?php unsetNotif() ?>
                            <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
                                <div class="alert alert-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
                                <?php unsetNotif() ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- END MESSAGE -->

                    <!-- INFORMASI -->
                    <?php if (empty($_SESSION['book']['date_from'])): ?>
                        <div class="row">
                            <div class="col-lg-6 col-md-9 col-sm-8 col-xs-12 m-b0 col-sm-offset-3">
                                <div class="well" style="text-align: justify;">
                                    <ol>
                                        <li>Untuk pemesanan akhir pekan harus melakukan pemesanan minimal 2 (hari), yaitu sabtu dan minggu dan pemesanan dilakukan <strong>seminggu</strong> sebelumnya.</li>
                                        <li>Untuk pemesanan high season (liburan dan/atau cuti hari-hari perayaan) pemesanan harus dilakukan minimal <strong>sebulan</strong> sebelumnya.</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <!-- END INFORMASI -->

                    <div class="row">
                        <div id="reservation" class="post card-container col-lg-6 col-md-9 col-sm-8 col-xs-12 m-b0 col-sm-offset-3">
                            <div class="widget bg-white p-a20">
                                <h4 class="widget-title">Pemesanan</h4>
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <form action="index.php?p=customer-booking-armada.php" class="form-horizontal" method="post">
                                            <div class="form-group">
                                                <label for="defaultrange" class="control-label col-sm-3">Tanggal <span class="required">*</span></label>
                                                <div class="col-sm-9">
                                                    <div class="input-group" id="defaultrange">
                                                        <input type="text" class="form-control" name="frm_date_book" readonly required>
                                                        <span class="input-group-btn">
                                                            <button class="site-button date-range-toggle" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                                    </div>
                                                    <span class="help-block warning-text">
                                                        <?php
                                                        if (isset($var_err['date'])) {
                                                            echo $var_err['date'];
                                                        }
                                                        ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="frm_service_book" class="control-label col-sm-3">Jenis layanan <span class="required">*</span></label>
                                                <div class="col-sm-9">
                                                    <select class="form-control" name="frm_service_book" id="frm_service_book" required>
                                                        <option value="">Pilih</option>
                                                        <option value="2">Transfer In/Out</option>
                                                        <option value="1">City Tour</option>
                                                        <!-- <option value="3">Tour Package</option> -->
                                                    </select>
                                                    <span class="help-block warning-text">
                                                        <?php
                                                        if (isset($var_err['service'])) {
                                                            echo $var_err['service'];
                                                        }
                                                        ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="frm_dropship" class="control-label col-sm-3">Lokasi Penjemputan <span class="required">*</span></label>
                                                <div class="col-sm-9">
                                                    <textarea name="frm_dropship" id="frm_dropship" class="form-control" rows="4" cols="80" placeholder="Permintaan lain (Contoh: Jemput di lokasi xxx jam xxx)" required maxlength="300" autofocus></textarea>
                                                    <span class="help-block warning-text">
                                                        <?php
                                                        if (isset($var_err['dropship'])) {
                                                            echo $var_err['dropship'];
                                                        }
                                                        ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="frm_request" class="control-label col-sm-3">Permintaan</label>
                                                <div class="col-sm-9">
                                                    <textarea name="frm_request" id="frm_request" class="form-control" rows="4" cols="80" placeholder="Permintaan lain" maxlength="300"></textarea>
                                                    <span class="help-block warning-text">
                                                        <?php
                                                        if (isset($var_err['request'])) {
                                                            echo $var_err['request'];
                                                        }
                                                        ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="pull-right">
                                                        <button type="submit" name="btn_submit_date" class="site-button green">Selanjutnya  <i class="fa fa-chevron-right"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- available data -->
                    <div class="row">
                        <!-- widgets grid start -->
                        <div id="vehicle" class="dez-blog-grid-2">
                            <!-- Sort -->
                            <div class="post card-container col-lg-3 col-sm-3 col-sm-4 col-xs-12">
                                <div class="widget bg-white p-a20">
                                    <h4 class="widget-title">Pemesanan</h4>
                                    Berangkat: <?php echo isset($_SESSION['book']['date_from']) ? dateFormat($_SESSION['book']['date_from']) : "" ?><br>
                                    Pulang: <?php echo isset($_SESSION['book']['date_to']) ? dateFormat($_SESSION['book']['date_to']) : "" ?><br>
                                    Lokasi penjemputan: <?php echo isset($_SESSION['book']['dropship']) ? $_SESSION['book']['dropship'] : "" ?><hr>
                                <!-- </div>
                                <div class="widget bg-white p-a20"> -->
                                    <form class="" action="customer-booking-armada.php" method="post">
                                        <button type="submit" name="btn_cancel" class="btn-success btn btn-block"> Batalkan</button>
                                    </form>
                                </div>

                                <div class="widget bg-white p-a20">
                                    <h4 class="widget-title">Pengurutan</h4>
                                    <div class="list-group">
                                        <a href="<?php echo "?" . http_build_query(array_merge($_GET, array('sort' => 'seathilo'))); ?>" class="list-group-item">
                                            Kursi (Banyak Sedikit)
                                        </a>
                                        <a href="<?php echo "?" . http_build_query(array_merge($_GET, array('sort' => 'seatlohi'))); ?>" class="list-group-item">
                                            Kursi (Sedikit Banyak)
                                        </a>
                                        <a href="<?php echo "?" . http_build_query(array_merge($_GET, array('sort' => 'type'))); ?>" class="list-group-item">
                                            type
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Sort end-->


                            <!-- List -->
                            <div class="post card-container col-lg-9 col-sm-9 col-sm-8 col-xs-12 m-b0">
                                <div class="widget bg-white p-a20">
                                    <div class="search-bx">
                                        <form action="index.php?p=customer-booking-armada.php" method="post">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Pencarian berdasarkan merk armada" name="frm_armada_search">
                                                <span class="input-group-btn">
                                                    <button type="submit" class="site-button" name="btn_search"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- List end-->

                            <!-- Armada -->
                            <div class="post card-container col-lg-9 col-sm-9 col-sm-12 col-xs-12 m-t0">
                                <div class="widget bg-white p-a20">
                                    <h4 class="widget-title">Armada</h4>
                                    <!-- data armada -->
                                    <div class="row">
                                        <?php
                                        $var_result_per_page = 6;
                                        $var_current_page = !empty($_GET['page']) && ctype_digit($_GET['page']) ? $_GET['page'] : 1;
                                        $var_limit = ($var_current_page * $var_result_per_page) - $var_result_per_page;

                                        $var_search = isset($_POST['frm_armada_search']) ? $_POST['frm_armada_search'] : "" ;

                                        $var_sql_armada = "
                                            SELECT mt.*, count(am.model_id) as modelid, am.*, at.*
                                            FROM sp_model_type mt
                                            JOIN sp_armada_model am ON am.model_id = mt.model_id
                                            JOIN sp_armada_type at ON at.type_id = am.type_id
                                            GROUP BY am.model_id
                                        ";

                                            //searching
                                            // if (isset($_POST['btn_search'])) {
                                            //     //$var_query_string = array('search' => $var_search);
                                            //     $var_sql_armada = "
                                            //     SELECT armada_regnumber, concat(armada_brand, ' ', armada_typevariant) armada_brand, armada_image, armada_color, armada_seat, armada_typevariant
                                            //     FROM sp_armada
                                            //     WHERE armada_status = 'approved' && armada_regnumber
                                            //     NOT IN
                                            //     (
                                            //         SELECT a.armada_regnumber
                                            //         FROM sp_order_transaction ot
                                            //         LEFT JOIN sp_armada a ON a.armada_regnumber = ot.armada_regnumber
                                            //         WHERE ((ot.od_from BETWEEN '{$var_sesfrom}' AND '{$var_sesto}') OR (ot.od_to BETWEEN '{$var_sesfrom}' AND '{$var_sesto}' )) AND (ot.od_status IN ('0', '1', '4', '5'))
                                            //         )
                                            //         && (armada_brand LIKE '%{$var_search}%' || armada_typevariant LIKE '%{$var_search}%') LIMIT ".$var_limit.",". $var_result_per_page."
                                            //         ";
                                            //     }
                                            //
                                            //     //sorting
                                            //     $var_sort = isset($_GET['sort']) ? filter_var($_GET['sort'], FILTER_SANITIZE_STRING) : "";
                                            //     //$var_query_string = array('sort' => $var_sort);
                                            //     if (isset($var_sort)) {
                                            //         switch ($var_sort) {
                                            //             case 'seathilo':
                                            //             $var_sql_armada = "
                                            //             SELECT armada_regnumber, armada_brand, armada_typevariant, armada_image, armada_color, armada_seat
                                            //             FROM sp_armada
                                            //             WHERE armada_status = 'approved' && armada_regnumber
                                            //             NOT IN
                                            //             (
                                            //                 SELECT a.armada_regnumber
                                            //                 FROM sp_order_transaction ot
                                            //                 LEFT JOIN sp_armada a ON a.armada_regnumber = ot.armada_regnumber
                                            //                 WHERE ((ot.od_from BETWEEN '{$var_sesfrom}' AND '{$var_sesto}') OR (ot.od_to BETWEEN '{$var_sesfrom}' AND '{$var_sesto}' )) AND (ot.od_status IN ('0', '1', '4', '5'))
                                            //                 )
                                            //                 ORDER BY armada_seat DESC LIMIT ".$var_limit.",". $var_result_per_page."";
                                            //                 break;
                                            //
                                            //                 case 'seatlohi':
                                            //                 $var_sql_armada = "
                                            //                 SELECT armada_regnumber, armada_brand, armada_typevariant, armada_image, armada_color, armada_seat
                                            //                 FROM sp_armada
                                            //                 WHERE armada_status = 'approved' && armada_regnumber
                                            //                 NOT IN
                                            //                 (
                                            //                     SELECT a.armada_regnumber
                                            //                     FROM sp_order_transaction ot
                                            //                     LEFT JOIN sp_armada a ON a.armada_regnumber = ot.armada_regnumber
                                            //                     WHERE ((ot.od_from BETWEEN '{$var_sesfrom}' AND '{$var_sesto}') OR (ot.od_to BETWEEN '{$var_sesfrom}' AND '{$var_sesto}' )) AND (ot.od_status IN ('0', '1', '4', '5'))
                                            //                     )
                                            //                     ORDER BY armada_seat ASC LIMIT ".$var_limit.",". $var_result_per_page."";
                                            //                 break;
                                            //
                                            //                 case 'type':
                                            //                 $var_sql_armada = "
                                            //                 SELECT armada_regnumber, armada_brand, armada_typevariant, armada_image, armada_color, armada_seat
                                            //                 FROM sp_armada
                                            //                 WHERE armada_status = 'approved' && armada_regnumber
                                            //                 NOT IN
                                            //                 (
                                            //                     SELECT a.armada_regnumber
                                            //                     FROM sp_order_transaction ot
                                            //                     LEFT JOIN sp_armada a ON a.armada_regnumber = ot.armada_regnumber
                                            //                     WHERE ((ot.od_from BETWEEN '{$var_sesfrom}' AND '{$var_sesto}') OR (ot.od_to BETWEEN '{$var_sesfrom}' AND '{$var_sesto}' )) AND (ot.od_status IN ('0', '1', '4', '5'))
                                            //                     )
                                            //                     ORDER BY armada_model DESC LIMIT ".$var_limit.",". $var_result_per_page."";
                                            //                 break;
                                            //             }
                                            //         }

                                                    $var_query_armada = mysqli_query($var_con, $var_sql_armada);
                                                    // $var_base_url = "customer-booking-armada.php";
                                                    //
                                                    // //hitung jumlah dta yang ditampilkan
                                                    // $var_sql_count_armada = "
                                                    // SELECT count(armada_regnumber) as total
                                                    // FROM sp_armada
                                                    // WHERE armada_status = 'approved' && armada_regnumber
                                                    // NOT IN
                                                    // (
                                                    //     SELECT a.armada_regnumber
                                                    //     FROM sp_order_transaction ot
                                                    //     LEFT JOIN sp_armada a ON a.armada_regnumber = ot.armada_regnumber
                                                    //     WHERE ((ot.od_from BETWEEN '{$var_sesfrom}' AND '{$var_sesto}') OR (ot.od_to BETWEEN '{$var_sesfrom}' AND '{$var_sesto}' )) AND (ot.od_status IN ('0', '1', '4', '5'))
                                                    //     )
                                                    //     ";
                                                    //     $var_query_count_armada = mysqli_fetch_array(mysqli_query($var_con, $var_sql_count_armada));
                                                    //     $totalResults = $var_query_count_armada['total'];
                                                        if (mysqli_num_rows($var_query_armada) > 0) :
                                                            while ($var_data_armada = mysqli_fetch_array($var_query_armada)) :
                                                                $var_sql_temp = "
                                                                    SELECT temp_data
                                                                    FROM sp_order_temp WHERE temp_data = '{$var_data_armada['model_id']}'
                                                                ";
                                                                $var_query_temp = mysqli_query($var_con, $var_sql_temp);
                                                                $var_count_temp = mysqli_num_rows($var_query_temp);
                                                                $var_countcart = $var_data_armada['modelid'] - $var_count_temp;
                                                                if ($var_countcart > 0) {
                                                                $var_sql = "
                                                                SELECT ot.*, count(otd.model_id) as jumlah
                                                                FROM sp_order_transaction ot
                                                                JOIN sp_order_transaction_detail otd ON ot.od_id = otd.od_id
                                                                WHERE (ot.od_status NOT IN ('2', '3', '5')) AND (otd.model_id = '{$var_data_armada['model_id']}') AND ((ot.od_from  BETWEEN '{$var_sesfrom}' AND '{$var_sesto}') OR (ot.od_to BETWEEN '{$var_sesfrom}' AND '{$var_sesto}'))
                                                                ";
                                                                $var_query = mysqli_query($var_con, $var_sql);
                                                                $var_data = mysqli_fetch_array($var_query);
                                                                $var_count = $var_data_armada['modelid'] - $var_data['jumlah'];

                                                                if ($var_count > 0) :
                                                                ?>
                                                                <style>
                                                                .thumbnail{
                                                                    min-width: 100%;
                                                                    height: 10em;
                                                                }
                                                                .thumbnail img {
                                                                    object-fit: cover;
                                                                }
                                                                </style>
                                                                <div class="col-sm-4 col-sm-4 m-b30">
                                                                    <div class="dez-box p-a20 border-1 bg-gray">
                                                                        <div class="thumbnail dez-thum-bx dez-img-overlay1 dez-img-effect zoom">
                                                                            <img class="img-rounded" src="../images/armada/<?= $var_data_armada['model_image']; ?>">
                                                                        </div>
                                                                        Spesifikasi:
                                                                        <div class="dez-info p-t5 text-center">
                                                                            <h4 class="dez-title m-t0 text-uppercase"><a href="#" class="font-14 open_modal" id="<?= $var_data_armada['model_id']; ?>"><?= $var_data_armada['model_name']; ?> (<?= $var_data_armada['type_seat']; ?> Kursi)</a></h4>
                                                                            <p>Harga: <label class="label label-info">Rp <?= number_format($var_data_armada['type_price'], 2, ',', '.') ?>/ 12 Jam</label></p>
                                                                            <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <a href="#" class="site-button-secondry btn-block open_modal" id="<?= $var_data_armada['model_id']; ?>" role="button" title="Detail Armada"><i class="fa fa-eye"></i></a>
                                                                                </div>
                                                                                <div class="col-sm-8">
                                                                                    <a href="index.php?p=armada-cart.php&amp;act=add&amp;item=<?= $var_data_armada['model_id']; ?>" class="site-button btn-block"><i class="fa fa-shopping-cart"></i> Pesan</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            <?php endif; } ?>
                                                            <?php endwhile; ?>
                                                        <?php else: ?>
                                                            <div class="col-sm-4 col-sm-4 m-b30">
                                                                Data tidak ditemukan
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                    <!-- /.row -->
                                                    <?php
                                                    // if (isset($_GET['sort'])) {
                                                    //     $var_query_string = array('sort' => $_GET['sort']);
                                                    //     echo pagination($var_base_url, $totalResults, $var_result_per_page, $var_current_page, $var_query_string);
                                                    // } else {
                                                    //     echo pagination($var_base_url, $totalResults, $var_result_per_page, $var_current_page);
                                                    // }
                                                    ?>
                                                </div>
                                            </div>
                                            <!-- Armada end-->
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- modal detail armada -->
                <div class="update-modal">
                    <div id="ModalUpdate" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                    </div>
                    <!-- /.modal -->
                </div>

                <?php

                $footer_component = [
                    'momentdate' => '../assets/global/plugins/moment.js',
                    'datepicker' => '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                    'daterangepicker' => '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js',
                ];

                require_once 'inc-template/inc-template-footer.php';
                ?>
                <script>
                $(document).ready(function () {
                    $('#defaultrange').daterangepicker(
                        {
                            "locale": {
                                "format": "DD/MM/YYYY",
                                "separator": " - ",
                                "applyLabel": "Pilih",
                                "cancelLabel": "Batal",
                                "fromLabel": "Dari",
                                "toLabel": "Sampai",
                                "customRangeLabel": "Kustom",
                                "weekLabel": "W",
                                "daysOfWeek": [
                                    "Min",
                                    "Sen",
                                    "Sel",
                                    "Rab",
                                    "Kam",
                                    "Jum",
                                    "Sab"
                                ],
                                "monthNames": [
                                    "Januari",
                                    "Februari",
                                    "Maret",
                                    "April",
                                    "Mei",
                                    "Juni",
                                    "Juli",
                                    "Augustus",
                                    "September",
                                    "Oktober",
                                    "November",
                                    "Desember"
                                ],
                                "firstDay": 1
                            },
                            timePicker: true,
                            timePicker24Hour: true,
                            setStartDate: moment(),
                            setEndDate: moment().endOf('year'),
                            // moment().endOf('year')moment().add(1, 'year')
                            minDate: moment().startOf('day'),
                            maxDate: moment().add(1, 'year'),
                            opens: 'left',
                            autoApply: true,
                        },
                        function(start, end, label) {
                            $('#defaultrange input').val(start.format('DD/MM/YYYY H:mm') + ' - ' + end.format('DD/MM/YYYY H:mm'));
                        });

                        $(".open_modal").click(function(e) {
                            var id = $(this).attr("id");
                            $.ajax({
                                url: "armada-detail.php",
                                type: "GET",
                                data : {id: id,},
                                success: function (ajaxData){
                                    $("#ModalUpdate").html(ajaxData);
                                    $("#ModalUpdate").modal('show',{backdrop: 'true'});
                                }
                            });
                        });

                        var sess = "<?php echo $var_sesfrom; ?>";
                        if (sess != '') {
                            $("#reservation").hide();
                            $("#vehicle").show();
                        } else {
                            $("#reservation").show();
                            $("#vehicle").hide();
                        }
                    });
                    </script>

                </body>
                </html>

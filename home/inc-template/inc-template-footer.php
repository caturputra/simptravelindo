<!-- Footer -->
<footer class="site-footer" >
    <div class="footer-top bg-primary text-white" style="background-image: url(../assets/global/home/images/footer-border-top.png); background-size: auto auto; background-position: center top; background-repeat: repeat-x;">
        <div class="container  p-t50">
            <div class="row">
                <div class="col-md-3 col-sm-6 footer-col-4">
                    <div class="widget widget_about text-justify">
                        <h4 class="m-b15 ">Tentang kami</h4>
                        <div class="dez-separator-outer"><div class="dez-separator bg-white style-liner"></div></div>
                        <p class="m-t15"> PT. Mitra Persada Travelindo adalah perusahaan yang bergerak dibidang jasa yaitu jasa Transportasi yang menyediakan berbagai armada.</p>
                        <ul class="dez-social-icon border">
                            <li><a class="fa fa-facebook" href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=5&cad=rja&uact=8&ved=0ahUKEwisrqCzm7_SAhVElZQKHcyrAYYQFgg7MAQ&url=https%3A%2F%2Fwww.facebook.com%2Fpages%2FMitra-Persada-Travelindo%2F1412721125608003&usg=AFQjCNEtvLeEjJb1LVuMW3ExgokfxWymSQ" target="_blank"></a></li>
                            <li><a class="fa fa-twitter" href="javascript:void(0);"></a></li>
                            <li><a class="fa fa-google-plus" href="javascript:void(0);"></a></li>
                        </ul>
                    </div>
                </div>
                <!-- <div class="col-md-3 col-sm-6 footer-col-4"> -->
                    <!-- <div class="widget widget_services">
                        <h4 class="m-b15 ">Usefull Links</h4>
                        <div class="dez-separator-outer"><div class="dez-separator bg-white style-liner"></div></div>
                        <ul>
                            <li><a href="#">Course Price Lists</a></li>
                            <li><a href="#">Drive School İnfo</a></li>
                            <li><a href="#">Our Vehicle Lİst</a></li>
                            <li><a href="#">Usefull Link 01</a></li>
                            <li><a href="#">Our Support Team</a></li>
                            <li><a href="#">FAQS </a></li>
                        </ul>
                    </div> -->
                <!-- </div> -->
                <div class="col-md-3 col-sm-6 footer-col-4">
                    <div class="widget ">
                        <h4 class="m-b15">Hubungi kami</h4>
                        <div class="dez-separator-outer"><div class="dez-separator bg-white style-liner"></div></div>
                        <ul class="widget_getintuch">
                            <li><i class="fa fa-map-marker"></i><strong class="text-white">address</strong> Jalan Pringgodani 12 Mrican baru, Yogyakarta 55281.</li>
                            <li><i class="fa fa-phone"></i><strong class="text-white">phone</strong>+62 274 511 100</li>
                            <li><i class="fa fa-whatsapp"></i><strong class="text-white">Whatsapp</strong>+62 813 251 221 90</li>
                            <li><i class="fa fa-envelope"></i><strong class="text-white">email</strong><a href="mailto:info@travelindo.com">info@travelindo.com</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 footer-col-4"></div>
                <div class="col-md-3 col-sm-6 footer-col-4">
                    <div class="widget ">
                        <h4 class="m-b15">Langganan</h4>
                        <div class="dez-separator-outer"><div class="dez-separator bg-white style-liner"></div></div>
                        <ul>
                            <li>
                                <form role="search" method="post" class="newsletter">
                                    <div class="input-group">
                                        <input name="text" class="form-control" placeholder="Alamat surel" type="text">
                                        <span class="input-group-btn">
                                            <button type="submit" class="site-button">Berlangganan</button>
                                        </span>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer END-->
<!-- scroll top button -->
<button class="scroltop fa fa-caret-up" ></button>

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9547885;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->

<!-- BEGIN CORE PLUGINS -->
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<!-- <script src="../assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.min.js" type="text/javascript"></script> -->
<!-- <script src="../assets/global/plugins/owl-carousel/owl.carousel.js" type="text/javascript"></script> -->
<script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>

<script src="../assets/global/home/js/magnific-popup.js" type="text/javascript"></script>
<script src="../assets/global/home/js/waypoints-min.js" type="text/javascript"></script>
<script src="../assets/global/home/js/counterup.min.js" type="text/javascript"></script>
<script src="../assets/global/home/js/imagesloaded.js" type="text/javascript"></script>
<script src="../assets/global/home/js/masonry-3.1.4.js" type="text/javascript"></script>
<script src="../assets/global/home/js/masonry.filter.js" type="text/javascript"></script>
<!-- <script src="../assets/global/home/js/dz.carousel.js" type="text/javascript"></script> -->
<script src="../assets/global/home/js/custom.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<!-- END THEME LAYOUT SCRIPTS -->

<!-- BEGIN PLUGINS -->
<?php
if (isset($footer_component)) {
    foreach ($footer_component as $key => $component) {
        echo"<script src=\"". $component ."\" type=\"text/javascript\"></script>";
    }
}
?>
<!-- END PLUGINS -->

<script type="text/javascript">
jQuery(document).ready(function($) {
    var pgurl = window.location.href.substr(window.location.href
        .lastIndexOf("?"));

        $(".navbar-collapse ul li a").each(function(){
            if($(this).attr("href") == pgurl || $(this).attr("href") == '' )
            $(this).parents("li").addClass("active");
        });
    });
</script>

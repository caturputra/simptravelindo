<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="#">
    <meta name="author" content="#">
    <!-- Favicons Icon -->
    <link rel="icon" href="../assets/global/home/images/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" type="image/x-icon" href="../assets/global/home/images/favicon.png" />

    <title><?php echo $var_title; ?></title>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- <link href="../assets/global/plugins/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css" /> -->
    <link href="../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />

    <!-- <link href="../assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" /> -->
    <!-- <link href="../assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="../assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" /> -->

    <link href="../assets/global/home/css/magnific-popup.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/home/css/style.css" rel="stylesheet" type="text/css" />
    <link class="skin" href="../assets/global/home/css/skin-3.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/home/css/templete.css" rel="stylesheet" type="text/css" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,300italic,400italic,500,500italic,700,700italic,900italic,900' rel='stylesheet' type='text/css'>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->

    <!-- END THEME LAYOUT STYLES -->
    <!--[if lt IE 9]>
    <script src="..assets/global/plugins/mapplic/js/html5shiv.min.js"></script>
    <script src="../assets/global/plugins/respond.min.js"></script>
    <![endif]-->
    <?php
    if (isset($head_component)) {
        foreach ($head_component as $key => $component) {
            echo "<link href=\"". $component ."\" rel=\"stylesheet\" type=\"text/css\" />";
        }
    }
    ?>
    <style media="screen">
        .warning-text, .required {
            color: red;
        }
    </style>
</head><!--/head-->

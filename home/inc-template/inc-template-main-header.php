<body id="bg">
    <div class="page-wraper">
        <!-- header -->
        <header class="site-header header-style-6">
            <!-- top bar -->
            <div class="top-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9 col-sm-12 col-xs-12 col-md-offset-3 col-sm-offset-0 col-xs-offset-0 top-bar-curve">
                            <div class="dez-topbar-left list-unstyled e-p-bx ">
                                <ul>
                                    <li><i class="fa fa-envelope"></i><a href="mailto:info@travelindo.com">info@travelindo.com</a></li>
                                    <li><i class="fa fa-phone"></i>+62 274 511 100</li>
                                </ul>
                            </div>
                            <div class="dez-topbar-right">
                                <ul class="social-bx list-inline pull-right">
                                    <li><a href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=5&cad=rja&uact=8&ved=0ahUKEwisrqCzm7_SAhVElZQKHcyrAYYQFgg7MAQ&url=https%3A%2F%2Fwww.facebook.com%2Fpages%2FMitra-Persada-Travelindo%2F1412721125608003&usg=AFQjCNEtvLeEjJb1LVuMW3ExgokfxWymSQ" target="_blank" class="fa fa-facebook"></a></li>
                                    <li><a href="javascript:void(0);" class="fa fa-twitter"></a></li>
                                    <li><a href="javascript:void(0);" class="fa fa-google-plus"></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- top bar END-->
            <!-- main header -->
            <div class="sticky-header main-bar-wraper">
                <div class="main-bar clearfix ">
                    <div class="container clearfix">
                        <!-- website logo -->
                        <div class="logo-header mostion"><a href="../"><img src="../assets/global/home/images/logo.png" width="193" height="89" alt=""></a></div>
                        <!-- nav toggle button -->
                        <button data-target=".header-nav" data-toggle="collapse" type="button" class="navbar-toggle collapsed"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                        <!-- extra nav -->
                        <div class="extra-nav">
                            <div class="extra-cell">
                                <button id="quik-search-btn" type="button" class="site-button"><i class="fa fa-search"></i></button>
                                <?php
                                if (!isset($_SESSION['userid'])) :
                                ?>
                                <a href="./../dashboard/login.php" class="site-button sign-in">Masuk/Daftar</a>
                            <?php else: ?>
                                <a href="./../dashboard/<?php echo 'index.php?p=home-'.trim(strtolower($_SESSION['levelname']) . '.php'); ?>" class="site-button sign-in">Ke Dashboard</a>
                                <a href="./../dashboard/logout.php" class="site-button red" title="Keluar"><i class="fa fa-power-off"></i></a>
                                <!-- <a href="index.php?p=armada-cart.php" class="site-button yellow sign-in" title="Keranjang"><i class="fa fa-shopping-cart"></i></a> -->
                            <?php endif; ?>
                            </div>
                        </div>
                        <!-- Quik search -->
                        <div class="dez-quik-search bg-primary bg-primary-dark">
                            <form action="#">
                                <input name="search" value="search" type="text" class="form-control" placeholder="Type to search">
                                <span  id="quik-search-remove"><i class="fa fa-remove"></i></span>
                            </form>
                        </div>
                        <!-- main nav -->
                        <div class="header-nav navbar-collapse collapse">
                            <ul class=" nav navbar-nav">
                            <?php
                            $var_sql_menu = "
                            SELECT
                                m.menu_id, m.menu_name, m.menu_url, m.menu_parent, m.menu_icon FROM sp_previllages p
                            JOIN
                                sp_menu m
                            ON
                                m.menu_id = p.menu_id
                            JOIN
                                sp_level l
                            ON
                                l.level_id = p.level_id
                            WHERE
                                p.level_id = '11'
                            AND
                                m.menu_parent = 0
                            GROUP BY
                                m.menu_name
                            ORDER BY
                                m.menu_order";
                            $var_query_menu = mysqli_query($var_con, $var_sql_menu);

                            foreach ($var_query_menu as $var_data_menu) :
                                $var_submenu = $var_data_menu['menu_id'];
                                $var_sql_menu2 = "
                                SELECT
                                    m.menu_id, m.menu_name, m.menu_url, m.menu_parent, m.menu_icon FROM sp_previllages p
                                JOIN
                                    sp_menu m
                                ON
                                    m.menu_id = p.menu_id
                                JOIN
                                    sp_level l
                                ON
                                    l.level_id = p.level_id
                                WHERE
                                    m.menu_parent = '{$var_submenu}'

                                GROUP BY
                                    m.menu_name
                                ORDER BY
                                    m.menu_order";
                                ;
                                $var_menu2 = mysqli_query($var_con, $var_sql_menu2);
                                $var_query_menu2 = mysqli_fetch_array($var_menu2);
                                if (mysqli_num_rows($var_menu2) > 0) :
                                    //die(var_dump($var_query_menu2));
                                    ?>
                                    <li>
                                        <a href="javascript:;">
                                            <?php echo $var_data_menu['menu_name']; ?><i class="<?php echo $var_data_menu['menu_icon']; ?>"></i>
                                        </a>
                                        <?php
                                        echo "<ul class='sub-menu'>";
                                        foreach ($var_menu2 as $var_query_sub) :
                                            ?>
                                            <li><a href="?p=<?php echo $var_query_sub['menu_url']; ?>"><?php echo $var_query_sub['menu_name']; ?></a></li>
                                            <?php
                                        endforeach;
                                        echo"</ul></li>";
                                        else :
                                            ?>
                                            <li>
                                                <a href="?p=<?php echo $var_data_menu['menu_url']; ?>">
                                                    <?php echo $var_data_menu['menu_name']; ?>
                                                </a>
                                            </li>
                                            <?php
                                        endif;
                            endforeach;
                                    ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- main header END -->
        </header>
        <!-- header END -->

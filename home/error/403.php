<?php

$var_title = "4 0 4";

require 'inc-template/inc-template-header.php';
require 'inc-template/inc-template-main-header.php';

?>

<!-- Content -->
<div class="page-content">
    <!-- Breadcrumb row -->
    <div class="breadcrumb-row">
        <div class="container">
            <ul class="list-inline">
                <li><a href="?p=home.php">Beranda</a></li>
                <li><?php echo $var_title; ?></li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb row END -->

    <!-- contact area -->
    <div class="container">
        <!-- 404 Page -->
        <div class="section-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-notfound text-center">
                        <form method="post">
                            <strong>4<i class="fa fa-frown-o text-primary"></i>3</strong>
                            <br>
                            <h5 class="m-b20 text-uppercase">Tidak memiliki akses!</h5>
                            <a href="?p=home.php" class="site-button "><span>Kembali ke beranda</span></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- 404 Page END -->
    </div>
    <!-- contact area END -->
</div>
<!-- Content END-->



<?php require 'inc-template/inc-template-footer.php'; ?>
</body>
</html>

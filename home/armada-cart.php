<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman yang memuat daftar pesanan
*/

$var_title = "Keranjang";

require_once __DIR__ . '/../core/init.php';

//action untuk add cart
//proses store item
$var_cart_id = session_id();

$var_get_item = isset($_GET['item']) ? mysqli_escape_string($var_con, $_GET['item']) : "";
$var_get_action = isset($_GET['act']) ? mysqli_escape_string($var_con, $_GET['act']) : "";

if ($var_get_action) {
    switch ($var_get_action) {
        case 'add':
        //cek apakah sudah dibook atau belum
        $var_sql_book = "
        SELECT count(ot.temp_data) as jumlah
        FROM sp_order_temp ot
        WHERE ot.temp_sessionid = '{$var_cart_id}'
        GROUP BY ot.temp_data
        ";
        $var_query_book = mysqli_query($var_con, $var_sql_book);
        $var_numrows_book = mysqli_num_rows($var_query_book);
        $var_data_book = mysqli_fetch_row($var_query_book);
        // debug($var_sql_book);
        // if ($var_numrows_book > 0) {
        //     setNotif(['warning' => 'item telah dipesan.']);
        // } else {
            $var_data_order = [
                'temp_id' => $_SESSION['userid'],
                'temp_data' => $var_get_item,
                'temp_sessionid' => $var_cart_id
            ];

            if (insertMulti($var_con, "sp_order_temp", $var_data_order)) {
                setNotif(['success' => 'item berhasil dipesan.']);
                routeUrl('index.php?p=armada-cart');
                die();
            }
        // }
        break;
        case 'del':
        $var_data_id = ['temp_data' => $var_get_item];

        if (delete($var_con, "sp_order_temp", $var_data_id)) {
            setNotif(['danger' => 'item berhasil dihapus.']);
            routeUrl('index.php?p=armada-cart');
            die();
        }
        break;
    }
}

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>

<!-- Content -->
<div class="page-content">
    <!-- inner page banner -->
    <div class="dez-bnr-inr overlay-black-middle" style="background-image:url(../assets/global/home/images/background/bg4.jpg);">
        <div class="container">
            <div class="dez-bnr-inr-entry">
                <h1 class="text-white"><?php echo $var_title; ?></h1>
                <p class="text-white">
                    Vehicle Reservation
                </p>
            </div>
        </div>
    </div>
    <!-- inner page banner END -->
    <!-- Breadcrumb row -->
    <div class="breadcrumb-row">
        <div class="container">
            <ul class="list-inline">
                <li><a href="?p=home.php">Beranda</a></li>
                <li><?php echo $var_title; ?></li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb row END -->

    <!-- contact area -->
    <div class="content-area">
        <!-- Our Awesome Services -->
        <div class="section-full awesome-services bg-white p-t70 p-b70" >
            <div class="container">
                <div class="section-content">
                    <!-- MESSAGE -->
                    <div class="row">
                        <div class="col-md-12">
                            <?php if (isset($_SESSION['notif']['success'])) :  ?>
                                <div class="alert alert-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
                                <?php unsetNotif() ?>
                            <?php elseif (isset($_SESSION['notif']['info'])) : ?>
                                <div class="alert alert-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
                                <?php unsetNotif() ?>
                            <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
                                <div class="alert alert-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
                                <?php unsetNotif() ?>
                            <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
                                <div class="alert alert-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
                                <?php unsetNotif() ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- END MESSAGE -->
                    <div class="row">
                        <?php
                        //retrive armada temp
                        $var_sql_book = "
                        SELECT ot.temp_id, ot.temp_data, ot.temp_sessionid, am.model_image, am.model_name, am.model_fuel, am.model_fuelmax
                        FROM sp_order_temp ot
                        LEFT JOIN sp_armada_model am ON am.model_id = ot.temp_data
                        LEFT JOIN sp_armada_type at ON at.type_id = am.type_id
                        WHERE ot.temp_sessionid = '{$var_cart_id}'
                        ";
                        // debug($var_sql_book);
                        $var_query_book = mysqli_query($var_con, $var_sql_book);
                        if (mysqli_num_rows($var_query_book) > 0) :
                            while($var_data_book = mysqli_fetch_array($var_query_book)):
                                ?>
                                <!-- Armada Cart -->
                                <div class="blog-post blog-md clearfix date-style-2">
                                    <div class="dez-post-media dez-img-effect"> <a href="blog-half-img.html#"><img src="../images/armada/<?php echo $var_data_book['model_image']; ?>" alt="<?php echo $var_data_book['model_image']; ?>"></a> </div>
                                    <div class="dez-post-info">
                                        <div class="dez-post-title ">
                                            <h3 class="post-title"><?php echo $var_data_book['model_name'] ?>
                                            </h3>
                                        </div>
                                        <div class="dez-post-text">
                                            <p>
                                                Jenis : <?php echo ucwords($var_data_book['model_name']) ?> <br>
                                                Jenis Bahan Bakar: <?php echo ucwords($var_data_book['model_fuel']) ?> <br>
                                                Kap. Bahan Bakar : <?php echo ucwords($var_data_book['model_fuelmax']) ?>
                                            </p>
                                            <a href="index.php?p=armada-cart.php&amp;act=del&amp;item=<?php echo $var_data_book['temp_data']; ?>"class="site-button orange"><i class="fa fa-trash-o"></i> Hapus item</a>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        <?php else: ?>
                            <p class="h2 text-center">Keranjang anda <span class="text-success">kosong.</span></p>
                        <?php endif; ?>
                        <a href="index.php?p=customer-booking-armada.php" class="site-button right"><i class="fa fa-chevron-left"> Tambah Armada</i></a>

                        <div class="pull-right">
                            <a href="index.php?p=customer-checkout.php" class="site-button right"><i class="fa fa-check-circle"> Selesaikan Pesanan</i></a>
                        </div>
                    </div>
                    <!-- /.row -->
                    <!-- END ARMADA CART -->
                </div>
            </div>
        </div>
        <!-- /.sectoin-full -->
    </div>
    <!-- /.content-area -->
</div>
<!-- /.page-content -->

<?php require_once 'inc-template/inc-template-footer.php'; ?>
</body>
</html>

<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Modal untuk menampilkan detail armada
*/

require_once '../core/init.php';

$var_armada_id = mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING));
$var_sql_armada = "
SELECT a.armada_regnumber, concat(a.armada_brand, ' ', a.armada_typevariant) as armada_brand, a.armada_color, a.armada_seat, a.armada_image
FROM sp_armada a
WHERE a.armada_regnumber = '{$var_armada_id}'";
$var_query_armada = mysqli_query($var_con, $var_sql_armada);
while ($var_data_armada = mysqli_fetch_array($var_query_armada)) :
    ?>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Armada Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="box box-success box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Gambar Armada</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <img src="../images/armada/<?= $var_data_armada['armada_image']; ?>" alt="<?= $var_data_armada['armada_image']; ?>" class="img img-responsive img-rounded" />
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->

                        <div class="col-md-8">
                            <div class="box box-success box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Detail Armada</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label for="frm_armada_id" class=" control-label">No. Plat</label>
                                            <input type="text" class="form-control" id="frm_armada_id" name="frm_armada_id" placeholder="No Plat" value="<?= $var_data_armada['armada_regnumber']; ?>" readonly>
                                        </div>
                                        <!-- /form-group -->

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="frm_name" class=" control-label">Brand</label>
                                                <input type="text" class="form-control" id="frm_name" name="frm_name" placeholder="Nama" value="<?= $var_data_armada['armada_brand']; ?>" readonly>
                                            </div>
                                        </div>
                                        <!-- /form-group -->

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="frm_total_seat" class=" control-label">Jumlah Kursi</label>
                                                <input type="number" class="form-control" id="frm_total_seat" name="frm_total_seat" placeholder="Jumlah Kursi" value="<?= $var_data_armada['armada_seat']; ?>" readonly>
                                            </div>
                                        </div>
                                        <!-- /form-group -->

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="frm_color" class=" control-label">Warna</label>
                                                <input type="text" class="form-control" id="frm_color" name="frm_color" placeholder="Warna" value="<?= $var_data_armada['armada_color']; ?>" readonly>
                                            </div>
                                        </div>
                                        <!-- /form-group -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.modal-body -->

                <div class="modal-footer">
                    <button type="button" class="site-button white" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                    <a href="index.php?p=armada-cart.php&amp;act=add&amp;item=<?= encUrl($var_data_armada['armada_regnumber']); ?>"class="site-button"><i class="fa fa-shopping-cart"></i> Pesan</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    <?php endwhile; ?>

<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman yang memuat aksi terakhir untuk konfirmasi pelanggan
** akan pesanan armadanya
*/

require_once '../core/init.php';
require_once '../inc/helper/Mail.php';

if (checkSid() === false) {
    setNotif(['warning' => 'Silahkan \'login\' terlebih dahulu. <a href="../dashboard/login.php">Masuk!</a>']);
    routeUrl('index.php?p=reservation');
}

$var_temp_sid = session_id();

$var_get_act = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var($_GET['act'], FILTER_SANITIZE_STRING)) : "";

if (strtolower($var_get_act) === "book") {
    $var_sql_temp = "
    SELECT t.temp_data, t.temp_sessionid
    FROM sp_order_temp t
    WHERE temp_sessionid = '{$var_temp_sid}'
    ";
    $var_query_temp = mysqli_query($var_con, $var_sql_temp);
    $var_rows = mysqli_num_rows($var_query_temp);
    $var_data_temp = mysqli_fetch_row($var_query_temp);

    if ($var_rows > 0) {
        mysqli_autocommit($var_con, FALSE);
        mysqli_begin_transaction($var_con);

        $var_gen_orderid = autonumber($var_con, "sp_order_transaction", "od_id", 4, "TR");

        $var_get_id = substr($var_gen_orderid, -4);
        $var_get_date = date('m');
        $var_gen_invoice = autonumber($var_con, "sp_order_transaction", "od_invoice", 0, "ITR".$var_get_date.$var_get_id);

        $var_insert_order = [
            'od_id' => $var_gen_orderid,
            // 'model_id' => $var_data_temp[0],
            'od_orderdate' => date('Y-m-d H:i:s'),
            'od_service' => $_SESSION['book']['service'],
            'od_request' => $_SESSION['book']['request'],
            'od_from' => $_SESSION['book']['date_from'],
            'od_to' => $_SESSION['book']['date_to'],
            'od_dropship' => $_SESSION['book']['dropship'],
            'user_id' => $_SESSION['userid'],
            'od_status' => '1',
            'od_selling' => $_SESSION['book']['price'],
            'od_invoice' => substr($var_gen_invoice, 0, 9),
        ];

        if (insert($var_con, "sp_order_transaction", $var_insert_order)) {

            $var_insert_schedule = ['od_id' => $var_gen_orderid, 'schedule_status' => '1'];
            $var_insert = insert($var_con, "sp_schedule", $var_insert_schedule);

            $var_sql_temp2 = "
            SELECT t.temp_data, t.temp_sessionid
            FROM sp_order_temp t
            WHERE temp_sessionid = '{$var_temp_sid}'
            ";
            $var_query_temp2 = mysqli_query($var_con, $var_sql_temp2);

            while($var_data_temp2 = mysqli_fetch_row($var_query_temp2)) :
                $var_insert_detail = ['model_id' => $var_data_temp2[0], 'od_id' => $var_gen_orderid];
                $var_insert = insert($var_con, "sp_order_transaction_detail", $var_insert_detail);
            endwhile;

            $var_del_temp = [ 'temp_sessionid' => $var_temp_sid ];
            if (delete($var_con, "sp_order_temp", $var_del_temp)) {
                mysqli_commit($var_con);
                unset($_SESSION['book']);

                $var_sql_getdata = "
                SELECT ot.od_id, ot.od_orderdate, ot.od_from, ot.od_to, concat(ud.user_detail_firstname, ' ', ud.user_detail_lastname) as member, u.user_email,ud.user_detail_phone, am.model_name, am.model_image, am.model_fuel,  CASE WHEN ot.od_service = '2' THEN 'Layanan in/out' WHEN ot.od_service = '1' THEN 'City tour' END
                FROM sp_order_transaction ot
                JOIN sp_user u ON u.user_id = ot.user_id
                JOIN sp_user_detail ud ON ud.user_id = u.user_id
                JOIN sp_armada_model am ON am.model_id = ot.model_id
                JOIN sp_armada_type at on at.type_id = am.type_id
                WHERE ot.od_id = '{$var_gen_orderid}'
                LIMIT 1
                ";
                $var_query_getdata = mysqli_query($var_con, $var_sql_getdata);
                $var_data_getdata = mysqli_fetch_row($var_query_getdata);

                //send email
                $mail = new Mail();
                $from = "noreply@travelindo.com";
                $subject = "E-Ticket Travelindo";
                $dir = "content-mail";
                $template = "e-ticket";
                $var_opt = [
                    'wrapper_orderid' => $var_data_getdata[0],
                    'wrapper_orderdate' => dateFormat($var_data_getdata[1]),
                    'wrapper_membername' => $var_data_getdata[4],
                    'wrapper_memberemail' => $var_data_getdata[5],
                    'wrapper_memberphone' => $var_data_getdata[6],
                    'wrapper_armadaregnumber' => $var_data_getdata[7],
                    'wrapper_service' => $var_data_getdata[10],
                    'wrapper_from' => dateFormat($var_data_getdata[2]),
                    'wrapper_to' => dateFormat($var_data_getdata[3]),
                    'footer_year' => date('Y')
                ];
                // $var_ = $mail->sendMail($from, $var_data_getdata[5], $subject, $template, $dir, $var_opt);

                try {

                    $var_sql_getadm = "
                    SELECT u.user_email, ud.user_detail_phone
                    FROM sp_user u
                    LEFT JOIN sp_user_detail ud ON ud.user_id = u.user_id
                    WHERE u.user_level = '1'
                    ";
                    $var_query_getadm = mysqli_query($var_con, $var_sql_getadm);

                    //kirim pesan ke semua akun yang berlevel admin transport
                    while ($var_data_getadm = mysqli_fetch_row($var_query_getadm)) {
                        sendPhone($var_con, ['phone' => $var_data_getadm[1], 'message' => 'Notifikasi! Pemesanan ' . $var_data_getdata[0] . ' mohon segera ditindak lanjuti.']);
                    }
                } catch(\Exception $e) {
                    routeUrl('index.php?p=customer-booking-armada');
                }

                setNotif(['info' => 'Pesanan anda telah diproses, silahkan ke halaman dashboard untuk melihat status pesanan anda. Terima kasih.']);
                routeUrl('index.php?p=customer-booking-armada');
                die();
            } else { mysqli_rollback($var_con); }
        } else {
            mysqli_rollback($var_con);
            setNotif(['danger' => 'Galat! Pesanan anda gagal diproses. Cek kembali data anda.']);
            routeUrl('index.php?p=customer-booking-armada');
            die();
        }
    }
}

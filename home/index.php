<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman yang digunakan sebagai router halaman
*/

require_once '../core/init.php';
?>

<?php
$pages_dir = '.';
if(!empty($_GET['p'])){
    $pages = scandir($pages_dir, 0);
    unset($pages[0], $pages[1]);

    $p = $_GET['p'];
    if(in_array($p, $pages)){
            include($p);
    } else {
        include 'error/404.php';
    }
} else {
    header('location: index.php?p=home.php');
}
?>

<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 10/12/2017
** Halaman untuk mengirim pesan kepada member atau agen
*/

$var_title = "Kirim Pesan";

require_once '../core/init.php';
require_once '../inc/helper/Mail.php';

$var_goid = isset($_GET['id']) ? base64_decode($_GET['id']) : "";
$var_oid = isset($_POST['frm_order_id']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_order_id'], FILTER_SANITIZE_STRING)) : "";
$var_subject = isset($_POST['frm_subject']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_subject'], FILTER_SANITIZE_STRING)) : "";
$var_message = isset($_POST['frm_message']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_message'], FILTER_SANITIZE_STRING)) : "";
$var_err = [];

if (isset($_POST['btn_send_message'])) {
    $var_sql_getusr = "
    SELECT u.user_email, ud.user_detail_phone
    FROM sp_order_transaction ot
    JOIN sp_user u ON u.user_id = ot.user_id
    JOIN sp_user_detail ud ON ud.user_id = u.user_id
    WHERE ot.od_id = '{$var_oid}'
    LIMIT 1
    ";
    $var_query_getusr = mysqli_query($var_con, $var_sql_getusr);
    $var_data_getusr = mysqli_fetch_row($var_query_getusr);

    if (empty(trim($var_subject))) {
        $var_err['frm_subject'] = 'Subjek tidak boleh kosong';
    }

    if (empty(trim($var_message))) {
        $var_err['frm_message'] = 'Pesan tidak boleh kosong.';
    }

    if (!empty($var_subject) && !empty($var_message)) {
        //send email confirmation
        $mail = new Mail();
        $from = "noreply@travelindo.com";
        $subject = "Pesan dari travelindo (" . $var_subject . ")";
        $dir = "content-mail";
        $template = "message";
        $var_opt = [
            'wrapper_message' => $var_message,
            'footer_year' => date('Y')
        ];

        //kirim ke admin transport yang melakukan konfirmasi
        if($mail->sendMail($from, $var_data_getusr[0], $subject, $template, $dir, $var_opt)) {
            sendPhone($var_con, ['phone' => $var_data_getusr[1], 'message' => 'Notifikasi! Mohon cek email: ' . $var_data_getusr[0] .', ada pesan dari travelindo.']);
            setNotif(['success' => 'Pesan anda berhasil dikirim.']);
            routeUrl('index.php?p=payment-claim');
        }
    }
}

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<div class="sendmes_form">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-money font-blue"></i>
                        <span class="caption-subject font-blue bold uppercase">Kirim Pesan</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <form class="form-horizontal" action="payment-claim-message.php" method="post">
                        <input type="hidden" name="frm_order_id" value="<?= $var_goid ?>">
                        <div class="form-group">
                            <label for="frm_subject" class="control-label col-sm-2 col-xs-4">Subjek <span class="required">*</span></label>
                            <div class="col-sm-10 col-xs-8">
                                <input type="text" name="frm_subject" id="frm_subject" class="form-control" placeholder="Subjek" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="frm_message" class="control-label col-sm-2 col-xs-4">Pesan <span class="required">*</span></label>
                            <div class="col-sm-10 col-xs-8">
                                <textarea type="text" class="form-control" name="frm_message" id="frm_message" placeholder="Isi Pesan" required cols="4" rows="15"></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="pull-right">
                                    <a href="index.php?p=payment-claim.php" class="btn dark btn-outline" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Kembali</a>
                                    <button type="submit" class="btn green" name="btn_send_message"><i class="fa fa-paper-plane"></i> Kirim</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT -->

<?php
require_once 'inc-template/inc-template-footer.php';
?>

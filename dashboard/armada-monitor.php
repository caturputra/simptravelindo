<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk monitoring armada
*/

require_once '../core/init.php';

$var_title = "Monitor Perjalanan";

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN CONTENT -->
<div class="row">
    <?php
    $var_sql_going = "
    SELECT ot.od_from, ot.od_to, am.model_name as armada_brand, a.armada_regnumber, concat(udm.user_detail_firstname, ' ', udm.user_detail_lastname) as member, ot.od_service, a.armada_image, s.schedule_id
    FROM sp_going g
    #JOIN sp_going_log l ON l.going_id = g.going_id
    JOIN sp_schedule s ON s.schedule_id = g.schedule_id
    JOIN sp_order_transaction ot ON ot.od_id = s.od_id
    JOIN sp_armada a ON ot.armada_regnumber = a.armada_regnumber
    JOIN sp_armada_model am ON am.model_id = ot.model_id
    JOIN sp_user_detail udm ON udm.user_id = ot.user_id
    #WHERE l.log_createat = (SELECT MAX(log_createat) FROM sp_going_log)
    WHERE s.schedule_status != '4'
    GROUP BY s.schedule_id
    ORDER BY g.going_id
    ";
    $var_query_going = mysqli_query($var_con, $var_sql_going);
    $var_numrows_going = mysqli_num_rows($var_query_going);
    ?>
    <?php if ($var_numrows_going > 0) : ?>
        <?php while ($var_data_going = mysqli_fetch_array($var_query_going)) : ?>
            <div class="col-sm-4 col-sm-6 col-xs-12">
                <div class="thumbnail">
                    <div class="mt-element-ribbon bg-grey-steel">
                        <a href="javascript:;">
                            <img src="../images/armada/<?php echo $var_data_going['armada_image']; ?>" class="img img-responsive bg-blue-oleo" alt="<?php echo $var_data_going['armada_regnumber'] ?>" />
                        </a>
                    </div>
                    <div class="caption">
                        <ul class="list-group">
                            <li class="list-group-item"> Armada <span class="pull-right"><?= $var_data_going['armada_brand'] ?> ( <?= $var_data_going['armada_regnumber'] ?> )</span></li>
                            <li class="list-group-item">Penyewa <span class="pull-right"><?php echo ucwords($var_data_going['member']); ?></span></li>
                            <li class="list-group-item">Berangkat <span class="pull-right"><?php echo dateFormat($var_data_going['od_from']) ?> - <?php echo dateFormat($var_data_going['od_to'])?></span></li>
                            <li class="list-group-item">Tipe <span class="pull-right">
                                <?php
                                switch ($var_data_going['od_service']) {
                                    case '1':
                                    echo ucwords("city tour");
                                    break;

                                    case '2':
                                    echo ucwords("transfer in/out");
                                    break;

                                    case '3':
                                    echo ucwords("tour package");
                                    break;
                                }
                                ?></span>
                            </li>
                            <?php
                            $var_sql_crews = "
                            SELECT cd.schedule_id, GROUP_CONCAT(crew_armadaid) As Names FROM sp_crew_detail cd GROUP BY cd.schedule_id
                            ";
                            $var_query_crews = mysqli_query($var_con, $var_sql_crews);
                            $var_data_crews = mysqli_fetch_row($var_query_crews);
                            $var_driver = explode(',', $var_data_crews[1]);
                            $var_sql_crew = "
                            SELECT concat(udd.user_detail_firstname, ' ', udd.user_detail_lastname) as crew
                            FROM sp_crew_armada ca
                            JOIN sp_user_detail udd ON udd.user_id = ca.user_id
                            WHERE crew_armadaid = '{$var_driver[0]}'
                            ";
                            $var_query_crew = mysqli_query($var_con, $var_sql_crew);
                            $var_data_crew = mysqli_fetch_row($var_query_crew);

                            if (!empty($var_driver[1])) {
                                $var_sql_crew2 = "
                                SELECT concat(udd.user_detail_firstname, ' ', udd.user_detail_lastname) as crew
                                FROM sp_crew_armada ca
                                JOIN sp_user_detail udd ON udd.user_id = ca.user_id
                                WHERE crew_armadaid = '{$var_driver[1]}'";
                                $var_query_crew2 = mysqli_query($var_con, $var_sql_crew2);
                                $var_data_crew2 = mysqli_fetch_row($var_query_crew2);
                            }
                            ?>
                            <li class="list-group-item">Sopir <span class="pull-right"><?php echo ucwords($var_data_crew[0]) ?></span>
                            </li>
                            <li class="list-group-item">Kernet <span class="pull-right"><?php if (isset($var_data_crew2[0])) { echo ucwords($var_data_crew2[0]); } ?></span>
                            </li>
                        </ul>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <a href="index.php?p=armada-monitor-timeline.php&amp;id=<?= base64_encode($var_data_going['schedule_id']) ?>" class="btn purple"><i class="fa fa-eye"> Lihat Monitoring</i> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.blog-post-content -->
                </div>
                <!-- /.blog-container -->
            </div>
            <!-- /.col-sm-6 -->
        <?php endwhile; ?>
    <?php else: ?>
        <div class="col-sm-12">
            <div class="note note-info">
                <p>Tidak ada data perjalanan</p>
            </div>
        </div>
    <?php endif; ?>
</div>
<!-- END CONTENT -->

<?php
require_once 'inc-template/inc-template-footer.php';
?>

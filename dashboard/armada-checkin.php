<?php
require_once '../core/init.php';
require_once '../inc/helper/Mail.php';

$var_action = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var($_GET['act'], FILTER_SANITIZE_STRING)) : "";

switch ($var_action) {
    case 'set':

    if (isset($_POST['btn_set'])) {
        $var_oid = mysqli_escape_string($var_con, $_POST['frm_oid']);
        $var_armada = mysqli_escape_string($var_con, $_POST['frm_arn']);
        $var_mid = mysqli_escape_string($var_con, $_POST['frm_mid']);
        // $var_armada = base64_encode($var_armada);
        //
        $var_sql_detail = "
        SELECT ot.od_from, ot.od_to, concat(ud.user_detail_firstname, ' ', ud.user_detail_lastname) as member, CASE ot.od_service WHEN '1' THEN 'City Tour' WHEN '2' THEN 'Transfer In/Out' END as service, s.schedule_id
        FROM sp_order_transaction ot
        JOIN sp_user_detail ud ON ud.user_id = ot.user_id
        LEFT JOIN sp_schedule s ON s.od_id = ot.od_id
        WHERE ot.od_id = '{$var_oid}'
        LIMIT 1
        ";
        $var_query_detail = mysqli_query($var_con, $var_sql_detail);
        $var_data_detail = mysqli_fetch_array($var_query_detail);

        $_SESSION['datefrom'] = $var_data_detail['od_from'];
        $_SESSION['dateto'] = $var_data_detail['od_to'];
        $_SESSION['schedule_id'] = $var_data_detail['schedule_id'];
        $_SESSION['armada_regnumber'] =  $var_armada;
        $_SESSION['order_id'] =  $var_oid;
        $_SESSION['model_id'] =  $var_mid;

        header('location: booking-schedule.php');
        //
        // $var_update_data = ['armada_regnumber' => $var_armada, 'od_confirmedby' => $_SESSION['userid'], 'od_createat' => date('Y-m-d')];
        // $var_update_cond = ['od_id' => $var_oid];
        //
        // if (update($var_con, "sp_order_transaction", $var_update_data, $var_update_cond)) {
        //     setNotif(['success' => 'Armada untuk pemesanan' . $var_oid . ' berhasil diset.']);
        //     routeUrl('booking-armada');
        //     die();
        // }
    }

    break;
}

$var_title = "Cek In";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
    'scroll' => 'https://cdn.datatables.net/scroller/1.4.4/css/scroller.dataTables.css',
    'blog_style' => '../assets/pages/css/blog.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN CONTENT -->
<div class="blog-page blog-content-1">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <?php
                $var_sql_trid = "
                SELECT otd.model_id, ot.od_from, ot.od_to, otd.armada_regnumber, ot.od_id
                FROM sp_order_transaction ot
                JOIN sp_order_transaction_detail otd ON otd.od_id = ot.od_id
                WHERE ot.od_id = '{$_GET['id']}'
                ";
                // echo $var_sql_trid;
                // echo "<br>";
                $var_query_trid = mysqli_query($var_con, $var_sql_trid);
                while($var_data_trid = mysqli_fetch_array($var_query_trid)){

                // echo $var_data_trid['model_id'] . "<br>";

                $var_sql_vh = "
                SELECT mt.armada_regnumber, a.armada_image, am.model_name, am.model_fuel, am.model_fuelmax, at.type_seat, at.type_model, mt.model_id
                FROM sp_model_type mt
                JOIN sp_armada a ON a.armada_regnumber = mt.armada_regnumber
                JOIN sp_armada_model am ON am.model_id = mt.model_id
                JOIN sp_armada_type at ON at.type_id = am.type_id
                WHERE mt.model_id = '{$var_data_trid['model_id']}'
                ";
                // debug($var_sql_vh);
                $var_query_vh = mysqli_query($var_con, $var_sql_vh);
                ?>

                <?php while ($var_data_vh = mysqli_fetch_array($var_query_vh)) : ?>
                    <?php
                    $var_sql_armada = "
                    SELECT a.armada_regnumber
                    FROM sp_order_transaction ot
                    LEFT JOIN sp_order_transaction_detail otd ON otd.od_id = ot.od_id
                    LEFT JOIN sp_armada a ON a.armada_regnumber = otd.armada_regnumber
                    WHERE ((ot.od_from BETWEEN '{$var_data_trid['od_from']}' AND '{$var_data_trid['od_to']}') OR (ot.od_to BETWEEN '{$var_data_trid['od_from']}' AND '{$var_data_trid['od_to']}' )) AND (ot.od_status IN ('0', '1', '4')) AND a.armada_regnumber = '{$var_data_vh['armada_regnumber']}'
                    LIMIT 1
                    ";
                    $var_query_armada = mysqli_query($var_con, $var_sql_armada);
                    $var_data_armada = mysqli_fetch_row($var_query_armada);
                    // echo $var_sql_armada;;
                    ?>
                    <?php if ($var_data_armada == NULL): ?>
                        <?php
                        $var_sql_condition = "
                            SELECT ac.*
                            FROM sp_armada_condition ac
                            WHERE ac.armada_regnumber = '{$var_data_vh['armada_regnumber']}' AND condition_checkdate = (SELECT MAX(condition_checkdate) FROM sp_armada_condition WHERE armada_regnumber = '{$var_data_vh['armada_regnumber']}')
                        ";
                        $var_query_condition = mysqli_query($var_con, $var_sql_condition);
                        $var_data_condition = mysqli_fetch_array($var_query_condition);
                        ?>
                        <?php if ($var_data_condition['condition_meter'] >= 70): ?>
                            <div class="col-sm-4 col-xs-12">
                                <div class="blog-post-sm bordered blog-container">
                                    <div class="blog-img-thumb">
                                        <div class="mt-element-ribbon bg-grey-steel">
                                            <div class="ribbon ribbon-right ribbon-shadow ribbon-color-primary uppercase">
                                                <?php echo $var_data_vh['armada_regnumber'] ?>
                                            </div>
                                        </div>
                                        <a href="javascript:;">
                                            <img src="../images/armada/<?php echo $var_data_vh['armada_image']; ?>" class="img img-responsive bg-blue-oleo" alt="<?php echo $var_data_vh['armada_regnumber'] ?>" />
                                        </a>
                                    </div>
                                    <div class="blog-post-content">
                                        <h2 class="blog-title blog-post-title">
                                            <a href="javascript:;"></a>
                                            <div class="pull-right">

                                            </div>
                                        </h2>
                                        <!-- List group -->
                                            <ul class="list-group">
                                                <li class="list-group-item"> Model
                                                    <span class="badge badge-info"> <?php echo ucwords($var_data_vh['type_model']) ?> </span>
                                                </li>
                                                <li class="list-group-item"> Jenis
                                                    <span class="badge badge-success"> <?php echo $var_data_vh['model_name'] ?> </span>
                                                </li>
                                                <li class="list-group-item"> Kursi
                                                    <span class="badge badge-danger"> <?php echo $var_data_vh['type_seat'] ?> </span>
                                                </li>
                                            </ul>
                                        <div class="blog-post-foot">
                                            <form action="armada-checkin.php?act=set" method="post">
                                                <input type="hidden" name="frm_oid" value="<?php echo $var_data_trid['od_id'] ?>">
                                                <input type="hidden" name="frm_arn" value="<?php echo $var_data_vh['armada_regnumber'] ?>">
                                                <input type="hidden" name="frm_mid" value="<?php echo $var_data_vh['model_id'] ?>">
                                                <button type="submit" name="btn_set" class="btn green btn-sm"><i class="fa fa-check"></i> Pilih</button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.blog-post-content -->
                                </div>
                                <!-- /.blog-container -->
                            </div>
                            <!-- /.col-sm-6 -->
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endwhile; } ?>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT -->

<!-- modal untuk persetujuan data -->
<div class="approve-modal">
    <div id="ModalApprove" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">

    </div>
    <!-- /.modal -->
</div>
<!-- /.approve-modal -->

<!-- modal untuk penolakan data -->
<div class="reject-modal">
    <div id="ModalReject" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">

    </div>
    <!-- /.modal -->
</div>
<!-- /.reject-modal -->

<!-- modal untuk hapus data -->
<div class="delete-modal">
    <div id="ModalDelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Yakin membatalkan pesanan?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
                    <a href="#" class="btn red" id="delete_link">Batal</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.hapus-modal -->

<!-- modal untuk view detail data -->
<div class="detail-modal">
    <div id="ModalDetail" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    </div>
    <!-- /.modal -->
</div>
<!-- /.view detail-modal -->
<?php
$footer_component = [
    'datatableglobalscript' => '../assets/global/scripts/datatable.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'datableplugin' => '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
    'datatblescroll' => 'https://cdn.datatables.net/scroller/1.4.4/js/dataTables.scroller.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
require_once 'inc-template/inc-template-footer.php';
?>
<script type="text/javascript">
function confirm_modal(delete_url) {
    $('#ModalDelete').modal('show', {
        backdrop: 'static'
    });
    document.getElementById('delete_link').setAttribute('href', delete_url);
}

$(document).ready(function() {
    $(".open_modal").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "booking-approval.php",
            type: "GET",
            data: {
                orderid: id,
            },
            success: function(ajaxData) {
                $("#ModalApprove").html(ajaxData);
                $("#ModalApprove").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $(".open_modal_reject").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "booking-rejected.php",
            type: "GET",
            data: {
                orderid: id,
            },
            success: function(ajaxData) {
                $("#ModalReject").html(ajaxData);
                $("#ModalReject").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $(".open_modal_detail").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "booking-detail.php",
            type: "GET",
            data: {
                orderid: id,
            },
            success: function(ajaxData) {
                $("#ModalDetail").html(ajaxData);
                $("#ModalDetail").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $("#tableListOrder").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "deferRender": true,
        "scroller": true,
        "scrollX": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        },
    });
});
</script>

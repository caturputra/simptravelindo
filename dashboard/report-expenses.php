<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk daftar expenses yang bisa dilaporkan
*/

require_once '../core/init.php';

$var_title = "Laporan";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<div class="mt-bootstrap-tables">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bars font-blue"></i>
                        <span class="caption-subject font-blue bold uppercase">Daftar Laporan Expenses</span>
                    </div>
                    <div class="actions">
                        <!-- actions print dll -->
                    </div>
                </div>
                <div class="portlet-body flip-scroll">
                    <div class="row">
                        <div class="table-responsive">
                            <div class="col-sm-12">
                                <table id="tableListpayments" class="table table-hover table-striped table-bordered flip-content">
                                    <thead class="flip-content">
                                        <th class="text-center">No.</th>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Tanggal Dibuat</th>
                                        <th class="text-center">Armada</th>
                                        <th class="text-center">Berangkat - Pulang</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center"></th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        //menampilkan payments
                                        $var_sql_payment = "
                                        SELECT e.expenses_id, e.expenses_status, e.expenses_createat, ot.od_from, ot.od_to, ot.armada_regnumber
                                        FROM sp_expenses e
                                        JOIN sp_schedule s ON s.schedule_id = e.schedule_id
                                        JOIN sp_order_transaction ot ON ot.od_id = s.od_id
                                        ORDER BY expenses_id DESC
                                        ";
                                        $var_query_payment = mysqli_query($var_con, $var_sql_payment);
                                        $num = (int) 1;
                                        while ($var_data_payment = mysqli_fetch_array($var_query_payment)) :
                                            ?>
                                            <tr>
                                                <td class="text-center">
                                                    <?php echo $num++; ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php echo ucwords($var_data_payment['expenses_id']); ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php echo dateFormat($var_data_payment['expenses_createat']); ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php echo $var_data_payment['armada_regnumber'] ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php echo dateFormat($var_data_payment['od_from']) ?> - <?php echo dateFormat($var_data_payment['od_to']) ?>
                                                </td>
                                                <td class="text-center" >
                                                    <?php
                                                    switch ($var_data_payment['expenses_status']) {
                                                        case 0:
                                                        echo '<span class="label label-warning label-sm">' . ucwords('ditunda') . '</span>';
                                                        break;

                                                        case 1:
                                                        echo '<span class="label label-success label-sm">' . ucwords('disetujui') . '</span>';
                                                        break;

                                                        case 2:
                                                        echo '<span class="label label-danger label-sm">' . ucwords('ditolak') . '</span>';
                                                        break;
                                                    }
                                                    ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php if ($var_data_payment['expenses_status'] != 0 && $var_data_payment['expenses_status'] != 2): ?>
                                                        <a href="print-report.php?act=er&amp;id=<?php echo base64_encode($var_data_payment['expenses_id']); ?>" data-toggle="tooltip" data-placement="bottom" title="Print Expenses" class="btn btn-sm green-meadow"><i class="fa fa-print"></i> Expenses </a>
                                                    <?php else: ?>
                                                        <label class="label label-default label-sm">Tidak ada aksi</label>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endwhile; ?>
                                    </tbody>
                                </table>
                                <!-- /.tableListpayments -->
                            </div>
                            <!-- /.col-sm-12 -->
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.porlet-body -->
            </div>
            <!-- /.portlet -->
        </div>
        <!-- /.col-sm-6 -->
    </div>
    <!-- /.mt-bootstrap-tables -->
    <!-- END CONTENT -->

    <!-- modal untuk penolakan data -->
    <div class="detail-modal">
        <div id="ModalDetail" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        </div>
        <!-- /.modal -->
    </div>
    <!-- /.reject-modal -->

</div>
<!-- /.content-wrapper -->
<?php
$footer_component = [
    'datatableglobalscript' => '../assets/global/scripts/datatable.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'datableplugin' => '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    //'sample' => '../assets/custom/form-validation-user-add.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
?>
<?php require_once 'inc-template/inc-template-footer.php'; ?>
<script type="text/javascript">
$(document).ready(function() {
    $("#tableListpayments").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autowidth": false,
        "lengthChange": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        }
    });
});
</script>

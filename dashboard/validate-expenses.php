<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk validasi expenses
*/

$var_title = "Validasi Expenses";

require_once '../core/init.php';

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

$var_eid = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : "";
$var_action = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var($_GET['act'], FILTER_SANITIZE_NUMBER_INT)) : "";
$var_userid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "";
$var_eid = base64_decode($var_eid);

//get phone number
$var_get_phone = mysqli_fetch_row(mysqli_query($var_con, "SELECT ud.user_detail_phone, e.user_id FROM sp_user_detail ud JOIN sp_expenses e ON e.user_id = ud.user_id WHERE e.expenses_id = '{$var_eid}' LIMIT 1"));

switch ($var_action) {
    case 1:
    $var_data_update = ['expenses_status' => '1', 'expenses_confirmedby' => $var_userid];
    $var_cond_update = ['expenses_id' => $var_eid];
    $var_update = update($var_con, "sp_expenses", $var_data_update, $var_cond_update);

    if ($var_update) {
        notification($var_con, [
            'from' => $_SESSION['userid'],
            'to' => $var_get_phone[1],
            'description' => 'Expenses '. $var_eid .' telah disetujui.',
            'type' => 'konfirmasi',
        ]);
        setNotif(['success' => 'validasi dengan kode ' . $var_eid . ' berhasil diproses.']);
        sendPhone($var_con, ['phone' => $var_get_phone[0], 'message' => 'Expenses telah divalidasi, masuk sistem untuk detail validasi.']);
        routeUrl('index.php?p=validate-expenses');
        die();
    } else {
        setNotif(['danger' => 'validasi dengan kode ' . $var_eid . ' gagal diproses.']);
        routeUrl('index.php?p=validate-expenses');
        die();
    }
    break;

    case 2:
    $var_data_update = ['expenses_status' => '2', 'expenses_confirmedby' => $var_userid];
    $var_cond_update = ['expenses_id' => $var_eid];
    $var_update = update($var_con, "sp_expenses", $var_data_update, $var_cond_update);

    if ($var_update) {
        notification($var_con, [
            'from' => $_SESSION['userid'],
            'to' => $var_get_phone[1],
            'description' => 'Expenses '. $var_eid .' telah ditolak.',
            'type' => 'konfirmasi',
        ]);
        setNotif(['success' => 'validasi dengan kode ' . $var_eid . ' berhasil diproses.']);
        sendPhone($var_con, ['phone' => $var_get_phone[0], 'message' => 'Expenses telah divalidasi, masuk sistem untuk detail validasi.']);
        routeUrl('index.php?p=validate-expenses');
        die();
    } else {
        setNotif(['danger' => 'validasi dengan kode ' . $var_eid . ' gagal diproses.']);
        routeUrl('index.php?p=validate-expenses');
        die();
    }
    break;
}

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<div class="mt-bootstrap-tables">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bars font-blue"></i>
                        <span class="caption-subject font-blue bold uppercase">Daftar Expenses</span>
                    </div>
                    <div class="actions">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                                <a class="btn green" href="javascript:;" data-toggle="dropdown">
                                    <i class="fa fa-gear"></i> Aksi
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a id="approve_bulk"><i class="fa fa-check"></i> Setujui <span class="rows_selected select_count" >0 Terpilih</span></a></li>
                                    <li><a id="reject_bulk"><i class="fa fa-exclamation-triangle"></i> Tolak <span class="rows_selected select_count" >0 Terpilih</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body flip-scroll">
                    <div class="row">
                        <div class="table-responsive">
                            <div class="col-sm-12">
                                <table id="tableListpayments" class="table table-hover table-striped table-bordered flip-content">
                                    <thead class="flip-content">
                                        <th class="text-center"><label class="mt-checkbox mt-checkbox-outline"><input type="checkbox" id="select_all"><span></span></label></th>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Tanggal Dibuat</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center"></th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        //menampilkan payments
                                        $var_sql_payment = "
                                        SELECT e.expenses_id, e.expenses_status, e.expenses_createat, e.schedule_id, e.user_id
                                        FROM sp_expenses e
                                        WHERE e.expenses_status = '0'
                                        ORDER BY expenses_id DESC
                                        ";
                                        $var_query_payment = mysqli_query($var_con, $var_sql_payment);
                                        $num = (int) 1;
                                        while ($var_data_payment = mysqli_fetch_array($var_query_payment)) :
                                            ?>
                                            <tr>
                                                <td class="text-center" style="width: 10px">
                                                    <label class="mt-checkbox mt-checkbox-outline"><input type="checkbox" class="emp_checkbox" data-emp-id="<?php echo $var_data_payment['expenses_id']; ?>"><span></span></label>
                                                </td>
                                                <td class="text-center" style="width: 2em;">
                                                    <?php echo $num++; ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php echo ucwords($var_data_payment['expenses_id']); ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php echo dateFormat($var_data_payment['expenses_createat']); ?>
                                                </td>
                                                <td class="text-center" >
                                                    <?php
                                                    switch ($var_data_payment['expenses_status']) {
                                                        case 0:
                                                        echo '<span class="label label-warning label-sm">' . ucwords('ditunda') . '</span>';
                                                        break;

                                                        case 1:
                                                        echo '<span class="label label-success label-sm">' . ucwords('disetujui') . '</span>';
                                                        break;

                                                        case 2:
                                                        echo '<span class="label label-danger label-sm">' . ucwords('ditolak') . '</span>';
                                                        break;
                                                    }
                                                    ?>
                                                </td>
                                                <td class="text-center">
                                                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Detail Armada" class="btn btn-sm green open_modal_detail" id="<?php echo $var_data_payment['expenses_id'] ?>"><i class="fa fa-check"></i> </a>
                                                </td>
                                            </tr>
                                        <?php endwhile; ?>
                                    </tbody>
                                </table>
                                <!-- /.tableListpayments -->
                            </div>
                            <!-- /.col-sm-12 -->
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.porlet-body -->
            </div>
            <!-- /.portlet -->
        </div>
        <!-- /.col-sm-6 -->
    </div>
    <!-- /.mt-bootstrap-tables -->
    <!-- END CONTENT -->

    <!-- modal untuk penolakan data -->
    <div class="detail-modal">
        <div id="ModalDetail" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        </div>
        <!-- /.modal -->
    </div>
    <!-- /.reject-modal -->

</div>
<!-- /.content-wrapper -->
<?php
$footer_component = [
    'datatableglobalscript' => '../assets/global/scripts/datatable.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'datableplugin' => '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    //'sample' => '../assets/custom/form-validation-user-add.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
?>
<?php require_once 'inc-template/inc-template-footer.php'; ?>
<script type="text/javascript">
$(document).ready(function() {
    $(".open_modal_detail").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "manageexpenses-detail.php",
            type: "GET",
            data: {
                id: id,
            },
            success: function(ajaxData) {
                $("#ModalDetail").html(ajaxData);
                $("#ModalDetail").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $("#tableListpayments").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autowidth": false,
        "lengthChange": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        }
    });

    // select all checkbox
    $('#select_all').on('click', function(e) {
        if($(this).is(':checked',true)) {
            $(".emp_checkbox").prop('checked', true);
        }
        else {
            $(".emp_checkbox").prop('checked',false);
        }
        // set all checked checkbox count
        $(".select_count").html($("input.emp_checkbox:checked").length+" Selected");
    });

    // set particular checked checkbox count
    $(".emp_checkbox").on('click', function(e) {
        $(".select_count").html($("input.emp_checkbox:checked").length+" Selected");
    });

    $('#approve_bulk').on('click', function(e) {
        var employee = [];
        $(".emp_checkbox:checked").each(function() {
            employee.push($(this).data('emp-id'));
        });

        if(employee.length <=0) {
            alert("Pilih item terlebih dahulu.");
        } else {
            WRN_PROFILE_APPROVE = "Yakin akan menyetujui "+(employee.length>1?"banyak":"ini")+" item?";

            var checked = confirm(WRN_PROFILE_APPROVE);

            if(checked == true) {
                var selected_values = employee;

                $.ajax({
                    type: "POST",
                    url: "ajax-bulk-actionexpenses.php?a=1",
                    cache:false,
                    data: {id: selected_values},
                    dataType: "html",
                    success: function(response) {
                        location.reload();
                    }
                });
            }
        }
    });

    $('#reject_bulk').on('click', function(e) {
        var employee = [];
        $(".emp_checkbox:checked").each(function() {
            employee.push($(this).data('emp-id'));
        });

        if(employee.length <=0) {
            alert("Pilih item terlebih dahulu.");
        } else {
            WRN_PROFILE_APPROVE = "Yakin akan menolak "+(employee.length>1?"banyak":"ini")+" item?";

            var checked = confirm(WRN_PROFILE_APPROVE);

            if(checked == true) {
                var selected_values = employee;
                $.ajax({
                    type: "POST",
                    url: "ajax-bulk-actionexpenses.php?a=2",
                    cache:false,
                    data: {id: selected_values},
                    dataType: "html",
                    success: function(response) {
                        location.reload();
                    }
                });
            }
        }
    });
});
</script>

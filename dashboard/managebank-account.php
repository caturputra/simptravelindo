<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk menambah data akun bank
*/

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';
require_once '../inc/function/inc-func-users.php';

$var_action = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var($_GET['act'], FILTER_SANITIZE_STRING)) : "";

switch (strtolower($var_action)) {
    case 'add':
    $var_bankname = isset($_POST['frm_bankname']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_bankname'], FILTER_SANITIZE_STRING)) : "";
    $var_accountname = isset($_POST['frm_accountname']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_accountname'], FILTER_SANITIZE_STRING)) : "";
    $var_accountnumber = isset($_POST['frm_accountnumber']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_accountnumber'], FILTER_SANITIZE_STRING)) : "";
    $var_error = [];

    if (isset($_POST['btn_add_account'])) {
        if (empty(trim($var_bankname))) {
            $var_error['err']['bankname'] = 'Mohon pilih nama bank.';
        }

        if (empty(trim($var_accountnumber))) {
            $var_error['err']['accountnumber'] = 'Mohon masukkkan nomor rekening.';
        }

        if (empty(trim($var_accountname))) {
            $var_error['err']['accountname'] = 'Mohon masukkkan atas nama rekening.';
        }

        if (!is_numeric($var_accountnumber)) {
            $var_error['err']['accountnumber'] = 'Nomor rekening harus dalam format angka.';
        }

        if (empty($var_error)) {
            $var_data_account = [
                'bank_id' => trim($var_bankname),
                'account_name' => ucwords($var_accountname),
                'account_number' => trim($var_accountnumber),
                'account_status' => '1'
            ];
            $var_insert_account = insert($var_con, "sp_bank_account", $var_data_account);

            if ($var_insert_account) {
                setNotif(['success' => 'Nomor rekening baru berhasil ditambahkan.']);
                routeUrl('index.php?p=managebank-account');
                die();
            } else {
                setNotif(['warning' => 'Nomor rekening baru gagal ditambahkan. Mohon cek kembali data rekening']);
                routeUrl('index.php?p=managebank-account');
                die();
            }
        }
    }
    break;

    case 'update':
    $var_bankname = isset($_POST['frm_bankname']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_bankname'], FILTER_SANITIZE_STRING)) : "";
    $var_accountname = isset($_POST['frm_accountname']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_accountname'], FILTER_SANITIZE_STRING)) : "";
    $var_accountid = isset($_POST['frm_accountid']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_accountid'], FILTER_SANITIZE_STRING)) : "";
    $var_status = isset($_POST['frm_status']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_status'], FILTER_SANITIZE_STRING)) : "";
    $var_error = [];

    if (isset($_POST['btn_update_account'])) {
        if (empty(trim($var_bankname))) {
            $var_error['err']['bankname'] = 'Mohon pilih nama bank.';
        }

        if (empty(trim($var_accountname))) {
            $var_error['err']['accountname'] = 'Mohon masukkkan atas nama rekening.';
        }

        if (empty($var_error)) {
            $var_data_account = [
                'bank_id' => trim($var_bankname),
                'account_name' => ucwords($var_accountname),
                'account_status' => $var_status
            ];
            $var_data_cond = ['account_number' => $var_accountid];
            $var_update_account = update($var_con, "sp_bank_account", $var_data_account, $var_data_cond);

            if ($var_update_account) {
                setNotif(['success' => 'Data rekening berhasil diubah.']);
                routeUrl('index.php?p=managebank-account');
                die();
            } else {
                setNotif(['warning' => 'Data rekening gagal diubah. Mohon cek kembali data rekening']);
                routeUrl('index.php?p=managebank-account');
                die();
            }
        }
    }
    break;

    case 'delete':
    $var_rid = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT)) :"";
    $var_delete_cond = ['account_number' => $var_rid];

    $var_delete_account = delete($var_con, "sp_bank_account", $var_delete_cond);

    if ($var_delete_account) {
        setNotif(['success' => 'Data rekening berhasil dihapus.']);
        routeUrl('index.php?p=managebank-account');
        die();
    } else {
        setNotif(['success' => 'Data rekening gagal dihapus.']);
        routeUrl('index.php?p=managebank-account');
        die();
    }
    break;
}

$var_title = "Manajemen Nomor Rekening";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-sm-8">
        <!-- BEGIN TAB PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="fa fa-bars font-green"></i>
                    <span class="caption-subject font-green bold uppercase">Daftar Rekening Bank</span>
                </div>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="table-responsive">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-striped table-hover" id="tableListBank">
                                <thead>
                                    <th class="text-center" style="width: 10px;">No.</th>
                                    <th class="text-center">Nama Bank</th>
                                    <th class="text-center">Nomor Rekening</th>
                                    <th class="text-center">Nama</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center"></th>
                                </thead>
                                <tbody>

                                    <?php
                                    $var_sql_account = "
                                    SELECT a.account_name, a.account_number, a.account_status, b.bank_name
                                    FROM sp_bank_account a
                                    JOIN sp_bank b ON b.bank_id = a.bank_id
                                    ";

                                    $var_query_account = mysqli_query($var_con, $var_sql_account);
                                    $no= (int) 1;

                                    while ($var_data_account = mysqli_fetch_array($var_query_account)) :
                                        ?>
                                        <tr>
                                            <td class="text-center">
                                                <?php echo $no++; ?>
                                            </td>
                                            <td>
                                                <?php echo $var_data_account['bank_name']; ?>
                                            </td>
                                            <td>
                                                <?php echo $var_data_account['account_number']; ?>
                                            </td>
                                            <td>
                                                <?php echo $var_data_account['account_name']; ?>
                                            </td>
                                            <td class="text-center">
                                                <?php switch($var_data_account['account_status']) : case '1': ?>
                                                <label class="label label-success label-sm"><?= ucwords('aktif'); ?></label>
                                                <?php break; case '0': ?>
                                                <label class="label label-warning label-sm"><?= ucwords('nonaktif'); ?></label>
                                                <?php break; endswitch; ?>
                                            </td>
                                            <td class="text-center">
                                                <!-- <a href="#" data-toggle="tooltip" data-placement="bottom" title="Detail Pengguna" class="btn blue btn-sm open_modal_detail" id="<?php echo  $var_data_account['user_id']; ?>"><i class="fa fa-eye"></i></a> -->

                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Ubah" class="btn yellow btn-xs open_modal" id="<?php echo  $var_data_account['account_number']; ?>"><i class="fa fa-edit"></i></a>

                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Hapus" class="btn red btn-xs" onclick="confirm_modal('index.php?p=managebank-account.php&amp;act=delete&amp;id=<?php echo $var_data_account['account_number']; ?>');"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    <?php endwhile; ?>
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.portlet-body -->
        </div>
        <!-- /.portlet -->
    </div>
    <!-- /.col -->

    <div class="col-sm-4">
        <!-- BEGIN TAB PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="fa fa-bank font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Tambah Rekening Bank</span>
                </div>
            </div>
            <div class="portlet-body">
                <form action="index.php?p=managebank-account.php&amp;act=add" method="post">
                    <div class="form-group">
                        <label for="frm_bankname" class="control-label">Nama bank <span class="required">*</span></label>
                        <select class="form-control select2" name="frm_bankname" id="frm_bankname">
                            <option value="">Pilih</option>
                            <?php
                            $var_sql_bank = "SELECT bank_id, bank_name FROM sp_bank ORDER BY bank_name";
                            $var_query_bank = mysqli_query($var_con, $var_sql_bank);
                            $var_numrows_bank = mysqli_num_rows($var_query_bank);
                            ?>
                            <?php if ($var_numrows_bank > 0) : ?>
                                <?php while ($var_data_bank = mysqli_fetch_row($var_query_bank)) : ?>
                                    <option value="<?= $var_data_bank[0] ?>"><?= $var_data_bank[1] ?></option>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </select>
                        <span class="help-block warning-text"><?php if (isset($var_error['err']['bankname'])) { echo $var_error['err']['bankname']; } ?></span>
                    </div>

                    <div class="form-group">
                        <label for="frm_accountnumber" class="control-label">Nomor rekening <span class="required">*</span></label>
                        <input type="text" class="form-control" name="frm_accountnumber" id="frm_accountnumber" placeholder="Nomor rekening" value="<?php if (isset($var_accountnumber)) { echo $var_accountnumber; } ?>">
                        <span class="help-block warning-text"><?php if (isset($var_error['err']['accountnumber'])) { echo $var_error['err']['accountnumber']; } ?></span>
                    </div>

                    <div class="form-group">
                        <label for="frm_accountname" class="control-label">Nama <span class="required">*</span></label>
                        <input type="text" class="form-control" name="frm_accountname" id="frm_accountname" placeholder="Atas nama nomor rekening" value="<?php if (isset($var_accountname)) { echo $var_accountname; } ?>">
                        <span class="help-block warning-text"><?php if (isset($var_error['err']['accountname'])) { echo $var_error['err']['accountname']; } ?></span>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pull-right">
                                <button type="submit" class="btn green" name="btn_add_account"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.col-sm-4 -->
</div>
<!-- /.row -->

<!-- END CONTENT -->

<!-- modal lihat detail data -->
<div class="detail-modal">
    <div id="ModalDetail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Detail Pengguna</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center"></h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.detail-modal -->

<!-- modal untuk edit data -->
<div class="update-modal">
    <div id="ModalUpdate" class="modal bs-modal-lg fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">

    </div>
    <!-- /.modal -->
</div>
<!-- /.update-modal -->

<!-- modal untuk hapus data -->
<div class="delete-modal">
    <div id="ModalDelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Yakin menghapus data rekening ini?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
                    <a href="#" class="btn red" id="delete_link">Hapus</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.hapus-modal -->


<?php
$footer_component = [
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'sample' => '../assets/custom/form-validation-user-add.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];

require_once 'inc-template/inc-template-footer.php';
?>

<script type="text/javascript">
function confirm_modal(delete_url) {
    $('#ModalDelete').modal('show', {
        backdrop: 'static'
    });
    document.getElementById('delete_link').setAttribute('href', delete_url);
}

$(document).ready(function() {
    $(".open_modal").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "managebank-account-update.php",
            type: "GET",
            data: {
                id: id,
            },
            success: function(ajaxData) {
                $("#ModalUpdate").html(ajaxData);
                $("#ModalUpdate").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $(".open_modal_detail").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "manageuser-detail.php",
            type: "GET",
            data: {
                id: id,
            },
            success: function(ajaxData) {
                $("#ModalDetail").html(ajaxData);
                $("#ModalDetail").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $("#tableListBank").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autowidth": true,
        "lengthChange": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        }
    });
});
</script>

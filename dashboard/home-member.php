<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk beranda member
*/

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';

$var_sid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "";
$var_action = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var($_GET['act'], FILTER_SANITIZE_STRING)) : "";

switch (strtolower($var_action)) {
    case 'complete':
    $var_firstname = isset($_POST['frm_firstname']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_firstname'], FILTER_SANITIZE_STRING)) : "";
    $var_lastname = isset($_POST['frm_lastname']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_lastname'], FILTER_SANITIZE_STRING)) : "";
    $var_birthdate = isset($_POST['frm_birthdate']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_birthdate'], FILTER_SANITIZE_STRING)) : "";
    $var_gender = isset($_POST['frm_gender']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_gender'], FILTER_SANITIZE_STRING)) : "";
    $var_phone = isset($_POST['frm_phone']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_phone'], FILTER_SANITIZE_STRING)) : "";
    $var_address = isset($_POST['frm_address']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_address'], FILTER_SANITIZE_STRING)) : "";
    $var_photo = isset($_FILES['frm_photo']['name']) ? addslashes($_FILES['frm_photo']['name']) : "";
    $var_error = [];

    if (isset($_POST['btn_complete'])) {
        if (empty(trim($var_firstname))) {
            $var_error['err']['firstname'] = 'Mohon masukkan nama depan.';
        }

        if (empty(trim($var_lastname))) {
            $var_error['err']['lastname'] = 'Mohon masukkan nama belakang.';
        }

        if (empty(trim($var_birthdate))) {
            $var_error['err']['birthdate'] = 'Mohon masukkan tanggal lahir.';
        }

        if (empty(trim($var_gender))) {
            $var_error['err']['gender'] = 'Mohon masukkan jenis kelamin.';
        }

        if (empty(trim($var_phone))) {
            $var_error['err']['phone'] = 'Mohon masukkan nomor telepon.';
        }

        if (empty(trim($var_address))) {
            $var_error['err']['address'] = 'Mohon masukkan alamat.';
        }

        if (empty(trim($var_photo))) {
            $var_error['err']['photo'] = 'Mohon masukkan foto.';
        } else {
            $var_ava_tmp = addslashes($_FILES['frm_photo']['tmp_name']);
            $var_ava_size = $_FILES['frm_photo']['size'];
            $var_ava_err = $_FILES['frm_photo']['error'];
            $var_ava_dir = "../images/ava/";
            $var_ava_type = strtolower(pathinfo(isset($var_photo) ? $var_photo : "", PATHINFO_EXTENSION));
            $var_ava_ext = array("jpeg", "png", "jpg", "JPG", "PNG", "JPEG");
            $var_ava_newName = "img-ava-" . $_SESSION['username'] .".jpg";

            if ($var_ava_size > 1024000) {
                $var_error['err']['photosize'] = 'Maximum ukuran foto sebesar 1 MB.';
            }

            // cek apakah yang diupload adalah file gambar
            $check = getimagesize($var_ava_tmp);
            if ($check === false) {
                $var_error['err']['photoext'] = 'Hanya gambar berformat .jpeg, .jpg, and .png.';
            }
        }

        //jika lolos Validasi
        if (empty($var_error)) {
            $var_data_user = [
                'user_detail_firstname' => $var_firstname,
                'user_detail_lastname' => $var_lastname,
                'user_detail_birthdate' => dateDb($var_birthdate),
                'user_detail_phone' => '+62'.$var_phone,
                'user_detail_gender' => $var_gender,
                'user_detail_address' => $var_address,
                'user_detail_avatar' => $var_ava_newName,
            ];
            $var_cond_user = ['user_id' => $var_sid];
            $var_update_user = update($var_con, "sp_user_detail", $var_data_user, $var_cond_user);
            if (move_uploaded_file($var_ava_tmp, $var_ava_dir.$var_ava_newName) && $var_update_user) {
                setNotif(['success' => 'Terima kasih telah menyelesaikan registrasi akun.']);
                routeUrl('../home/index');
                die();
            } else {
                setNotif(['warning' => 'Mohon cek kembali data registrasi.']);
                routeUrl('index.php?p=home-member');
                die();
            }
        }
    }

break;
}

$var_title = "Dashboard Member";
$head_component = [
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
    'morris' => '../assets/global/plugins/morris/morris.css',
];
require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>

<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<?php
$var_sid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)): "";

$var_sql_user = "
SELECT user_detail_firstname, user_detail_lastname, user_detail_birthdate, user_detail_gender, user_detail_address, user_detail_phone, user_detail_avatar, user_id
FROM sp_user_detail
WHERE user_id = '{$var_sid}'
LIMIT 1
";
$var_query_user = mysqli_query($var_con, $var_sql_user);
$var_numrows_user = mysqli_num_rows($var_query_user);
$var_data_user = mysqli_fetch_array($var_query_user);
if ($var_numrows_user > 0) :
    if ($var_data_user[0] == null || $var_data_user[1] == null || $var_data_user[2] == null || $var_data_user[3] == null || $var_data_user[4] == null || $var_data_user[5] == null || $var_data_user[6] == null) :
        ?>
        <div class="profile-modal">
            <div id="ModalProfile" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Profil</h4>
                        </div>
                        <div class="modal-body">
                            <?php if (!empty($var_error['err'])) : ?>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="alert alert-danger">
                                            <ol>
                                                <?php foreach ($var_error['err'] as $key => $value) : ?>
                                                    <li><?php echo $value; ?></li>
                                                <?php endforeach; ?>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <form class="form-horizontal" action="index.php?p=home-member.php&amp;act=complete" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="frm_firstname" class="control-label col-sm-4">Nama depan <span class="required">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="frm_firstname" id="frm_firstname" placeholder="Nama depan" value="<?php if(isset($var_firstname)) { echo $var_firstname; }?>" autofocus>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="frm_lastname" class="control-label col-sm-4">Nama Belakang <span class="required">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="frm_lastname" id="frm_lastname" placeholder="Nama Belakang" value="<?php if(isset($var_lastname)) { echo $var_lastname; }?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="frm_birthdate" class="control-label col-sm-4">Tanggal Lahir <span class="required">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control mask_date2" name="frm_birthdate" id="frm_birthdate" placeholder="Tanggal Lahir"value="<?php if(isset($var_birthdate)) { echo $var_birthdate; }?>">
                                                <span class="help-block">e.g. 1 Januari 1999 = 01/01/1999</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="frm_phone" class="control-label col-sm-4">Telepon <span class="required">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control mask_phone" name="frm_phone" id="frm_phone" placeholder="Telepon"value="<?php if(isset($var_phone)) { echo $var_phone; }?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="frm_gender" class="control-label col-sm-4">Gender <span class="required">*</span></label>
                                            <div class="col-sm-8">
                                                <div class="mt-radio-inline">
                                                    <label class="mt-radio">
                                                        <input type="radio" name="frm_gender" id="frm_gender" value="M" checked> Laki-laki
                                                        <span></span>
                                                    </label>
                                                    <label class="mt-radio">
                                                        <input type="radio" name="frm_gender" id="frm_gender" value="F"> Perempuan
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="frm_address" class="control-label col-sm-4">Alamat <span class="required">*</span></label>
                                            <div class="col-sm-8">
                                                <textarea type="text" class="form-control" name="frm_address" id="frm_address" placeholder="Alamat"><?php if(isset($var_address)) { echo $var_address; }?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="frm_photo" class="control-label col-sm-4">Foto <span class="required">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="file" class="form-control" name="frm_photo" id="frm_image" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="pull-right">
                                            <button type="submit" class="btn green" name="btn_complete"><i class="fa fa-save"></i> Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>
    <?php endif; ?>
<?php endif; ?>

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN DASHBOARD STATS 1-->
<div class="row">
    <div class="col-lg-3 col-sm-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-money"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $var_sql_countorder = "SELECT count(*) as count_order FROM sp_order_transaction WHERE user_id = '{$_SESSION['userid']}'";
                    $var_query_countorder = mysqli_query($var_con, $var_sql_countorder);
                    $var_numrows_countorder = mysqli_num_rows($var_query_countorder);
                    $var_data_countorder = mysqli_fetch_row($var_query_countorder);
                    if ($var_numrows_countorder > 0) :
                        ?>
                        <span><?php echo $var_data_countorder[0] ?></span>
                    <?php else: ?>
                        <span>0</span>
                    <?php endif; ?>
                </div>
                <div class="desc"> Total Pemesanan <a href="#" class="font-white"><span class="fa fa-external-link"> </span></a></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat dashboard-stat-v2 purple" href="#">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $var_sql_countorder = "SELECT count(*) as counthowto FROM sp_payment WHERE payment_howto='0'";
                    $var_query_countorder = mysqli_query($var_con, $var_sql_countorder);
                    $var_numrows_countorder = mysqli_num_rows($var_query_countorder);
                    $var_data_countorder = mysqli_fetch_row($var_query_countorder);
                    if ($var_numrows_countorder > 0) :
                        ?>
                        <span><?php echo $var_data_countorder[0] ?></span>
                    <?php else: ?>
                        <span>0</span>
                    <?php endif; ?>
                </div>
                <div class="desc"> Pembayaran Via Transfer <a href="index.php?p=manageuser-setting.php" class="font-white"><span class="fa fa-external-link"> </span></a></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat dashboard-stat-v2 red" href="#">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $var_sql_countorder = "SELECT count(*) as counthowto FROM sp_payment WHERE payment_howto='1'";
                    $var_query_countorder = mysqli_query($var_con, $var_sql_countorder);
                    $var_numrows_countorder = mysqli_num_rows($var_query_countorder);
                    $var_data_countorder = mysqli_fetch_row($var_query_countorder);
                    if ($var_numrows_countorder > 0) :
                        ?>
                        <span><?php echo $var_data_countorder[0] ?></span>
                    <?php else: ?>
                        <span>0</span>
                    <?php endif; ?>
                </div>
                <div class="desc"> Pembayaran via kasir <a href="index.php?p=booking-armada.php" class="font-white"><span class="fa fa-external-link"> </span></a></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat dashboard-stat-v2 green" href="#">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $var_sql_countorder = "SELECT count(*) as count FROM sp_payment";
                    $var_query_countorder = mysqli_query($var_con, $var_sql_countorder);
                    $var_numrows_countorder = mysqli_num_rows($var_query_countorder);
                    $var_data_countorder = mysqli_fetch_row($var_query_countorder);
                    if ($var_numrows_countorder > 0) :
                        ?>
                        <span><?php echo $var_data_countorder[0] ?></span>
                    <?php else: ?>
                        <span>0</span>
                    <?php endif; ?>
                </div>
                <div class="desc"> Total Pembayaran <a href="index.php?p=booking-armada.php" class="font-white"><span class="fa fa-external-link"> </span></a></div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<!-- END DASHBOARD STATS 1-->

<?php
//FOOTER PAGE BASE LEVEL
$footer_component = [
    'momentdate' => '../assets/global/plugins/moment.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
require 'inc-template/inc-template-footer.php';
?>
<script type="text/javascript">
$(document).ready(function () {
    $('#ModalProfile').modal('show', {
        backdrop: 'static'
    });
});
</script>

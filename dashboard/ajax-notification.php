<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Ajax untuk operasi notifikasi
*/

require_once '../core/init.php';

$var_uid = isset($_SESSION['userid']) ? filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT) : "";
$cek = isset($_POST['view']) ? filter_var($_POST['view'], FILTER_SANITIZE_STRING) : "";

if ($cek != '') {
    $data = ['notif_isread' => 1];
    $update = ['notif_to' => $var_uid];

    update($var_con, "sp_notification", $data, $update);
}

$var_sql_getnotif = "SELECT notif_desc, notif_type FROM sp_notification WHERE notif_to = '{$var_uid}' ORDER BY notif_id DESC LIMIT 10";
$var_query_getnotif = mysqli_query($var_con, $var_sql_getnotif);
$output = '';
$output_count = '';
if (mysqli_num_rows($var_query_getnotif) > 0) {
    while ($var_data_getnotif = mysqli_fetch_array($var_query_getnotif)) {
        $output .= '
        <li>
            <a href="#">
                <span class="details">
                    <strong>'. $var_data_getnotif['notif_type'] .'</strong><br>
                    '.$var_data_getnotif['notif_desc'].'
                </span>
            </a>
        </li>
        ';
    }
} else {
    $output .=  '<li><a href="#"><span class="details"><strong>Tidak ada notifikasi</strong><br></span></a></li>';
}

$var_sql_getnotifc = "SELECT notif_id FROM sp_notification WHERE notif_to = '{$var_uid}' AND notif_isread = '0'";
$var_query_getnotifc = mysqli_query($var_con, $var_sql_getnotifc);
$count = mysqli_num_rows($var_query_getnotifc);
if ($count > 0) {
    $output_count .= '<span class="badge badge-success count">'. $count .'</span>';
}
$data = [
    'notification' => $output,
    'unseen_notification' => $output_count
];
echo json_encode($data);

<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk generate laporan bentuk pdf
*/

require_once '../core/init.php';

//proses cetak transport order
$var_action = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var($_GET['act'], FILTER_SANITIZE_STRING)) : routeUrl('index') ;
$var_order_id = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : '' ;
$var_order_id = base64_decode($var_order_id);

$logoaddress = 'Mitra Persada Travelindo PT. Jalan Pringgondani 1 Mrican Yogyakarta, Ph. +62 274 541409 Email: info@travelindo.com';
$font = [
    'font_logo' => [
        'type' => 'Arial',
        'thin' => '',
        'size' => '5',
    ],
    'font_header' => [
        'type' => 'Arial',
        'thin' => 'B',
        'size' => '16',
    ],
    'font_content' => [
        'type' => 'Arial',
        'thin' => '',
        'size' => '8',
    ],
    'font_footer' => [
        'type' => 'Arial',
        'thin' => '',
        'size' => '8',
    ],
];

switch (strtolower($var_action)) {
    case 'pa':
    $var_type = isset($_POST['frm_armada']) ? mysqli_escape_string($var_con, $_POST['frm_armada']) : "";
    $var_datefrom = isset($_POST['frm_datefrom']) ? mysqli_escape_string($var_con, $_POST['frm_datefrom']) : "";
    $var_dateto = isset($_POST['frm_dateto']) ? mysqli_escape_string($var_con, $_POST['frm_dateto']) : "";

    $var_year = isset($_POST['frm_year']) ? mysqli_escape_string($var_con, $_POST['frm_year']) : "";
    $var_month = isset($_POST['frm_month']) ? mysqli_escape_string($var_con, $_POST['frm_month']) : "";

    require_once '../inc/lib/fpdf/fpdf.php';
    require_once '../inc/lib/fpdf-easytable/exfpdf.php';
    require_once '../inc/lib/fpdf-easytable/easyTable.php';

    $fpdf = new exFPDF('P', 'mm', 'A4');
    $title = strtoupper('Laporan Pemesanan');

    $fpdf->AddPage();

    //add image logo
    $fpdf->Image('../assets/global/home/images/logo.png', 10, 10, -145);
    $fpdf->SetFont($font['font_logo']['type'], $font['font_logo']['thin'], $font['font_logo']['size']);
    $fpdf->Ln(10);
    $fpdf->Cell(10, 10, $logoaddress, 0, 1, 'L');
    $fpdf->Ln(5);

    //add header
    $fpdf->SetFont($font['font_header']['type'], $font['font_header']['thin'], $font['font_header']['size']);
    $fpdf->Cell(0,0, $title, 0, 1, 'C');
    $fpdf->Ln(10);

    $fpdf->SetFont($font['font_content']['type'], $font['font_content']['thin'], $font['font_content']['size']);
    $table_detail=new easyTable($fpdf, 6);

    $table_detail->easyCell('No.');
    $table_detail->easyCell('Kode Pesan');
    $table_detail->easyCell('Pemesan');
    $table_detail->easyCell('Armada');
    $table_detail->easyCell('Jenis Layanan');
    $table_detail->easyCell('Status');
    $table_detail->printRow();

    switch ($var_type) {
        case '1':
            if (empty(trim($var_datefrom))) {
                die('Tanggal dari kosong, Kembali');
            }

            if (empty(trim($var_dateto))) {
                die('Tanggal sampai kosong, kembali');
            }

            $var_sql_report = "
            SELECT od_id,concat(sp_user_detail.user_detail_firstname, ' ', sp_user_detail.user_detail_lastname), armada_regnumber, CASE od_service
            WHEN '1' THEN 'City Tour'
            WHEN '2' THEN 'Transfer In/Out'
            END,
            CASE od_status
            WHEN '0' THEN 'Ditunda'
            WHEN '1' THEN 'Disetujui'
            WHEN '2' THEN 'Ditolak'
            WHEN '3' THEN 'Dibatalkan'
            WHEN '4' THEN 'Definit'
            END
            FROM sp_order_transaction
            JOIN sp_user_detail ON sp_user_detail.user_id = sp_order_transaction.user_id
            WHERE (od_from BETWEEN '". dateDb($var_datefrom) ."' AND '". dateDb($var_dateto) ."') OR (od_to BETWEEN '". dateDb($var_datefrom) ."' AND '". dateDb($var_dateto) ."')
            ";
            // debug($var_sql_report);
            $var_query_report = mysqli_query($var_con, $var_sql_report);
            $num = 1;
            while ($var_data_report = mysqli_fetch_row($var_query_report)) {
                $fpdf->Ln(4);
                $fpdf->SetFont($font['font_footer']['type'], $font['font_footer']['thin'], $font['font_footer']['size']);
                $table_detail->easyCell($num);
                $table_detail->easyCell($var_data_report[0]);
                $table_detail->easyCell($var_data_report[1]);
                $table_detail->easyCell($var_data_report[2]);
                $table_detail->easyCell($var_data_report[3]);
                $table_detail->easyCell($var_data_report[4]);
                $table_detail->printRow();
                $num++;
            }
            $table_detail->endTable(0);
            $fpdf->Output('I', 'report-booking-'.date('Ymd') .'.pdf');
            break;

        case '2':
            if (empty(trim($var_year))) {
                die('Tahun kosong, kembali');
            }

            if (empty(trim($var_month))) {
                die('Bulan kosong, kembali');
            }

            $var_sql_report = "
            SELECT od_id,concat(sp_user_detail.user_detail_firstname, ' ', sp_user_detail.user_detail_lastname), armada_regnumber, CASE od_service
            WHEN '1' THEN 'City Tour'
            WHEN '2' THEN 'Transfer In/Out'
            END,
            CASE od_status
            WHEN '0' THEN 'Ditunda'
            WHEN '1' THEN 'Disetujui'
            WHEN '2' THEN 'Ditolak'
            WHEN '3' THEN 'Dibatalkan'
            WHEN '4' THEN 'Definit'
            END
            FROM sp_order_transaction
            JOIN sp_user_detail ON sp_user_detail.user_id = sp_order_transaction.user_id
            WHERE (MONTH(od_from) = '{$var_month}' AND YEAR(od_from) = '{$var_year}') OR (MONTH(od_to) = '{$var_month}' AND YEAR(od_to) = '{$var_year}')
            ";
            $var_query_report = mysqli_query($var_con, $var_sql_report);

            $var_query_report = mysqli_query($var_con, $var_sql_report);
            $num = 1;
            while ($var_data_report = mysqli_fetch_row($var_query_report)) {
                $fpdf->Ln(4);
                $fpdf->SetFont($font['font_footer']['type'], $font['font_footer']['thin'], $font['font_footer']['size']);
                $table_detail->easyCell($num);
                $table_detail->easyCell($var_data_report[0]);
                $table_detail->easyCell($var_data_report[1]);
                $table_detail->easyCell($var_data_report[2]);
                $table_detail->easyCell($var_data_report[3]);
                $table_detail->easyCell($var_data_report[4]);
                $table_detail->printRow();
                $num++;
            }
            $table_detail->endTable(0);
            $fpdf->Output('I', 'report-booking'.'-'.date('Ymd') .'.pdf');
        break;
    }
    break;

    case 'vh':
    require_once '../inc/lib/fpdf/fpdf.php';
    require_once '../inc/lib/fpdf-easytable/exfpdf.php';
    require_once '../inc/lib/fpdf-easytable/easyTable.php';

    $fpdf = new exFPDF('P', 'mm', 'A4');
    $title = strtoupper('Laporan Armada');

    $fpdf->AddPage();

    //add image logo
    $fpdf->Image('../assets/global/home/images/logo.png', 10, 10, -145);
    $fpdf->SetFont($font['font_logo']['type'], $font['font_logo']['thin'], $font['font_logo']['size']);
    $fpdf->Ln(10);
    $fpdf->Cell(10, 10, $logoaddress, 0, 1, 'L');
    $fpdf->Ln(5);

    //add header
    $fpdf->SetFont($font['font_header']['type'], $font['font_header']['thin'], $font['font_header']['size']);
    $fpdf->Cell(0,0, $title, 0, 1, 'C');
    $fpdf->Ln(10);

    $fpdf->SetFont($font['font_content']['type'], $font['font_content']['thin'], $font['font_content']['size']);
    $table_detail=new easyTable($fpdf, '%{5,35, 10, 15, 35}');

    $table_detail->easyCell('No.');
    $table_detail->easyCell('Gambar');
    $table_detail->easyCell('Nomor Plat');
    $table_detail->easyCell('Nama');
    $table_detail->easyCell('Keterangan');
    $table_detail->printRow();

    $var_sql_report = "
    SELECT a.armada_regnumber, am.model_name as armada_brand, a.armada_productionyear, a.armada_chassisnumber, a.armada_enginenumber, a.armada_image, at.type_model, at.type_seat, at.type_price, am.model_fuel, am.model_fuelmax
    FROM sp_model_type mt
    JOIN sp_armada a ON a.armada_regnumber = mt.armada_regnumber
    JOIN sp_armada_model am ON am.model_id = mt.model_id
    JOIN sp_armada_type at ON at.type_id = am.type_id
    WHERE a.armada_status = 'approved'
    ORDER BY a.armada_regnumber ASC
    ";
    $var_query_report = mysqli_query($var_con, $var_sql_report);
    $num = 1;
    while ($var_data_report = mysqli_fetch_array($var_query_report)) {
        $fpdf->Ln(4);
        $fpdf->SetFont($font['font_footer']['type'], $font['font_footer']['thin'], $font['font_footer']['size']);
        $table_detail->easyCell($num);
        $table_detail->easyCell('', 'img:../images/armada/'.$var_data_report['armada_image'] . ', w40;');
        //$fpdf->Image('../images/armada/'.$var_data_report['armada_image'], 10, 10, 'jpg');
        $table_detail->easyCell($var_data_report[0]);
        $table_detail->easyCell($var_data_report[1]);
        $table_detail->easyCell(
            'Jenis: ' . ucwords($var_data_report['type_model']) . "\n" .
            'Tahun Produksi: ' . $var_data_report['armada_productionyear'] . "\n" .
            'Nomor rangka: ' . $var_data_report['armada_chassisnumber'] . "\n" .
            'Nomor mesin: ' . $var_data_report['armada_enginenumber'] . "\n" .
            'Jenis bahan bakar: ' . ucwords($var_data_report['model_fuel']) . ", Kapasitas: " . $var_data_report['model_fuelmax'] . "L\n"
        );
        $table_detail->printRow();
        $num++;
    }
    $table_detail->endTable(0);
    $fpdf->Output('I', 'report-vehicles'.'-'.date('Ymd') .'.pdf');
    break;

    case 'ca':
    require_once '../inc/lib/fpdf/fpdf.php';
    require_once '../inc/lib/fpdf-easytable/exfpdf.php';
    require_once '../inc/lib/fpdf-easytable/easyTable.php';

    $fpdf = new exFPDF('P', 'mm', 'A4');
    $title = strtoupper('Laporan Kru Armada');

    $fpdf->AddPage();

    //add image logo
    $fpdf->Image('../assets/global/home/images/logo.png', 10, 10, -145);
    $fpdf->SetFont($font['font_logo']['type'], $font['font_logo']['thin'], $font['font_logo']['size']);
    $fpdf->Ln(10);
    $fpdf->Cell(10, 10, $logoaddress, 0, 1, 'L');
    $fpdf->Ln(5);

    //add header
    $fpdf->SetFont($font['font_header']['type'], $font['font_header']['thin'], $font['font_header']['size']);
    $fpdf->Cell(0,0, $title, 0, 1, 'C');
    $fpdf->Ln(10);

    $fpdf->SetFont($font['font_content']['type'], $font['font_content']['thin'], $font['font_content']['size']);
    $table_detail=new easyTable($fpdf, '%{5,10, 25, 25, 35}', 'width: 300');

    $table_detail->easyCell('No.');
    $table_detail->easyCell('ID Kru');
    $table_detail->easyCell('Nama');
    $table_detail->easyCell('Posisi');
    $table_detail->easyCell('Status');
    $table_detail->printRow();

    $var_sql_report = "
    SELECT ca.crew_armadaid, concat(ud.user_detail_firstname, ' ', ud.user_detail_lastname) as crew_name, l.level_name,
    CASE ca.crew_status
    WHEN '0' THEN 'Ditunda'
    WHEN '1' THEN 'Diterima'
    WHEN '2' THEN 'Ditolak'
    END
    FROM sp_crew_armada ca
    JOIN sp_user u ON u.user_id = ca.user_id
    JOIN sp_level l ON l.level_id = u.user_level
    JOIN sp_user_detail ud ON ud.user_id = u.user_id
    JOIN sp_crew_armada_detail cad ON ca.crew_armadaid = cad.crew_armadaid
    ";
    $var_query_report = mysqli_query($var_con, $var_sql_report);

    $var_query_report = mysqli_query($var_con, $var_sql_report);
    $num = 1;
    while ($var_data_report = mysqli_fetch_row($var_query_report)) {
        $fpdf->Ln(4);
        $fpdf->SetFont($font['font_footer']['type'], $font['font_footer']['thin'], $font['font_footer']['size']);
        $table_detail->easyCell($num);
        $table_detail->easyCell($var_data_report[0]);
        $table_detail->easyCell($var_data_report[1]);
        $table_detail->easyCell($var_data_report[2]);
        $table_detail->easyCell($var_data_report[3]);
        $table_detail->printRow();
        $num++;
    }
    $table_detail->endTable(0);
    $fpdf->Output('I', 'report-armadacrew'.'-'.date('Ymd') .'.pdf');
    break;

    //cetak laporan untuk transport order
    case 'to':
        $var_sql_to = "
        SELECT
        concat(udc.user_detail_firstname, ' ', udc.user_detail_lastname) as customer,
        concat(udbc.user_detail_firstname, ' ', udbc.user_detail_lastname) as confirmedby,
        a.armada_regnumber,
        concat(a.armada_model, ' ',a.armada_brand, ' ', a.armada_typevariant, ' ', a.armada_color) as armadatype
        FROM sp_order_transaction od
        JOIN sp_user_detail udc ON udc.user_id = od.user_id
        JOIN sp_user_detail udbc ON udbc.user_id = od.od_confirmedby
        JOIN sp_armada a ON a.armada_regnumber = od.armada_regnumber
        WHERE od.od_id = '{$var_order_id}'
        LIMIT 1
        ";
        $var_query_to = mysqli_query($var_con, $var_sql_to);
        $var_data_to = mysqli_fetch_array($var_query_to);

        $var_sql_driver = "
        SELECT concat(ud.user_detail_firstname, ' ',ud.user_detail_lastname) as driver, ud.user_detail_phone
        FROM sp_crew_detail cd
        JOIN sp_crew_armada ca ON ca.crew_armadaid = cd.crew_armadaid
        JOIN sp_user u ON u.user_id = ca.user_id
        JOIN sp_user_detail ud ON ud.user_id = u.user_id
        JOIN sp_level l ON l.level_id = u.user_level
        JOIN sp_schedule s ON s.schedule_id = cd.schedule_id
        JOIN sp_order_transaction ot ON ot.od_id = s.od_id
        WHERE ot.od_id = '{$var_order_id}' && l.level_name = 'sopir'
        ";
        $var_query_driver = mysqli_query($var_con, $var_sql_driver);
        $var_data_driver = mysqli_fetch_array($var_query_driver);

        $var_sql_codriver = "
        SELECT concat(ud.user_detail_firstname, ' ',ud.user_detail_lastname) as codriver, ud.user_detail_phone
        FROM sp_crew_detail cd
        JOIN sp_crew_armada ca ON ca.crew_armadaid = cd.crew_armadaid
        JOIN sp_user u ON u.user_id = ca.user_id
        JOIN sp_user_detail ud ON ud.user_id = u.user_id
        JOIN sp_level l ON l.level_id = u.user_level
        JOIN sp_schedule s ON s.schedule_id = cd.schedule_id
        JOIN sp_order_transaction ot ON ot.od_id = s.od_id
        WHERE ot.od_id = '{$var_order_id}' && l.level_name = 'kernet'
        ";
        $var_query_codriver = mysqli_query($var_con, $var_sql_codriver);
        $var_data_codriver = mysqli_fetch_array($var_query_codriver);

        $var_html = '<link rel="stylesheet" href="../assets/global/css/print-report.css">
        <body>
        <div class="header">
        <div class="main-header">
        <img src="../assets/global/home/images/logo.png">
        </div>
        <div class="address">
        <p class="camel font-bold">Mitra Persada Travelindo PT. Jalan Pringgondani 1 Mrican Yogyakarta, Ph. +62 274 541409 Email: info@travelindo.com</p>
        </div>
        </div>

        <div class="wrapper">
        <h4 class="title uppercase font-bold text-center font-20">transport order</h4>
        <div class="order_detail">
        <table class="identity">
        <tr>
        <td class="camel">no <td>:</td><td class="row"><span class="font-bold">'. $var_order_id .'</span></td></td>
        </tr>
        <tr>
        <td class="camel">to <td>:</td><td class="row"><span class="font-bold camel">mitra persada travelindo</span></td></td>
        </tr>
        </table>
        </div>

        <div class="order_to">
        <span class="title text-left camel m-b-10">please provide transportation services for</span>
        <h5 class="order_by font-15">'. $var_data_to['customer'] .'</h5>
        </div>

        <hr>

        <table class="vehicle">
        <tr>
        <td class="camel row">car type<td>:</td> <td>'. ucwords($var_data_to['armadatype']) .'</td></td>
        </tr>
        <tr>
        <td class="camel row">car id<td>:</td> <td>'. $var_data_to['armada_regnumber'] .'</td></td>
        </tr>
        </table>

        <div class="itinerary">
        <h4 class="title uppercase text-center font-bold font-10">itinerary</h4>
        <div class="description camel">
        <table class="itinerary">
        <tr>
        <td class="row-10">driver <td>:</td><td class="row-full">'. $var_data_driver['driver'] .' + '. $var_data_codriver['codriver'] .'</td></td>
        </tr>
        <tr>
        <td class="row-10">local contact <td>:</td><td class="row-full">0274 5111022</td></td>
        </tr>
        <tr>
        <td>confirmed by <td>:</td><td class="row-full">'. $var_data_to['confirmedby'] .'</td></td>
        </tr>
        </table>
        </div>
        </div>
        </div>

        <div class="footer">
        <div class="confirmed_by">
        <p class="text-center"><span class="camel">yogyakarta, </span>' . dateFormat(date('Y m d')) . '</p>
        <p class="m-t-25 text-center camel">'. $var_data_to['confirmedby'] .'</p>
        </div>
        </div>
        </body>
        ';

        require_once '../inc/lib/dompdf/autoload.inc.php';

        $dompdf = new Dompdf\Dompdf();
        $dompdf->loadHtml($var_html);
        $dompdf->setpaper('A4', 'Potrait');
        $dompdf->render();
        $dompdf->stream('transport-order-'. $var_order_id.'-'.date('Ymd') .'.pdf',array('Attachment'=>0));
    break;

    //cetak laporan untuk expenses report
    case 'er':
    $var_sql_expenses = "
    SELECT e.expenses_id, e.expenses_status, e.expenses_createat, e.schedule_id, concat(ud.user_detail_firstname, ' ', ud.user_detail_lastname) as creator, s.od_id, concat(udc.user_detail_firstname, ' ', udc.user_detail_lastname) as confirmedby
    FROM sp_expenses e
    JOIN sp_schedule s ON s.schedule_id = e.schedule_id
    JOIN sp_user_detail ud ON ud.user_id = e.user_id
    JOIN sp_user_detail udc ON udc.user_id = e.expenses_confirmedby
    WHERE expenses_id = '{$var_order_id}'
    LIMIT 1
    ";
    $var_query_expenses = mysqli_query($var_con, $var_sql_expenses);
    $var_data_expenses = mysqli_fetch_array($var_query_expenses);

    $var_sql_to = "
    SELECT
    concat(udc.user_detail_firstname, ' ', udc.user_detail_lastname) as customer,
    concat(udbc.user_detail_firstname, ' ', udbc.user_detail_lastname) as confirmedby,
    a.armada_regnumber,
    concat(a.armada_brand, ' ', a.armada_typevariant) as armada_brand, od.od_from
    FROM sp_order_transaction od
    JOIN sp_user_detail udc ON udc.user_id = od.user_id
    JOIN sp_user_detail udbc ON udbc.user_id = od.od_confirmedby
    JOIN sp_armada a ON a.armada_regnumber = od.armada_regnumber
    WHERE od.od_id = '".$var_data_expenses['od_id']."'
    LIMIT 1
    ";
    $var_query_to = mysqli_query($var_con, $var_sql_to);
    $var_data_to = mysqli_fetch_array($var_query_to);

    $var_sql_item = "
    SELECT SUM(d.detail_amount) as total
    FROM sp_expenses_detail d
    WHERE expenses_id = '{$var_order_id}'
    ";
    $var_query_item = mysqli_query($var_con, $var_sql_item);
    $var_data_item = mysqli_fetch_row($var_query_item);

    $var_sql_driver = "
    SELECT concat(ud.user_detail_firstname, ' ',ud.user_detail_lastname) as driver, ud.user_detail_phone
    FROM sp_crew_detail cd
    JOIN sp_crew_armada ca ON ca.crew_armadaid = cd.crew_armadaid
    JOIN sp_user u ON u.user_id = ca.user_id
    JOIN sp_user_detail ud ON ud.user_id = u.user_id
    JOIN sp_level l ON l.level_id = u.user_level
    JOIN sp_schedule s ON s.schedule_id = cd.schedule_id
    JOIN sp_order_transaction ot ON ot.od_id = s.od_id
    WHERE ot.od_id = '".$var_data_expenses['od_id']."' && l.level_name = 'Driver'
    ";
    $var_query_driver = mysqli_query($var_con, $var_sql_driver);
    $var_data_driver = mysqli_fetch_row($var_query_driver);

    $var_sql_codriver = "
    SELECT concat(ud.user_detail_firstname, ' ',ud.user_detail_lastname) as codriver, ud.user_detail_phone
    FROM sp_crew_detail cd
    JOIN sp_crew_armada ca ON ca.crew_armadaid = cd.crew_armadaid
    JOIN sp_user u ON u.user_id = ca.user_id
    JOIN sp_user_detail ud ON ud.user_id = u.user_id
    JOIN sp_level l ON l.level_id = u.user_level
    JOIN sp_schedule s ON s.schedule_id = cd.schedule_id
    JOIN sp_order_transaction ot ON ot.od_id = s.od_id
    WHERE ot.od_id = '".$var_data_expenses['od_id']."' && l.level_name = 'Co-Driver'
    ";
    $var_query_codriver = mysqli_query($var_con, $var_sql_codriver);
    $var_data_codriver = mysqli_fetch_row($var_query_codriver);

    $var_sql_item = "
    SELECT d.detail_peritem, d.detail_amountperitem, d.detail_countitem, (d.detail_amountperitem * d.detail_countitem) as amountperitem
    FROM sp_expenses_detail d
    WHERE expenses_id = '{$var_order_id}'
    ";
    $var_query_item = mysqli_query($var_con, $var_sql_item);

    require_once '../inc/lib/fpdf/fpdf.php';
    require_once '../inc/lib/fpdf-easytable/exfpdf.php';
    require_once '../inc/lib/fpdf-easytable/easyTable.php';

    $fpdf = new exFPDF('P', 'mm', 'A4');
    $title = strtoupper('expenses report');
    $logoaddress = 'Mitra Persada Travelindo PT. Jalan Pringgondani 1 Mrican Yogyakarta, Ph. +62 274 541409 Email: info@travelindo.com';
    $font = [
        'font_logo' => [
            'type' => 'Arial',
            'thin' => '',
            'size' => '5',
        ],
        'font_header' => [
            'type' => 'Arial',
            'thin' => 'B',
            'size' => '16',
        ],
        'font_content' => [
            'type' => 'Arial',
            'thin' => '',
            'size' => '8',
        ],
        'font_footer' => [
            'type' => 'Arial',
            'thin' => '',
            'size' => '8',
        ],
    ];
    $fpdf->AddPage();

    //add image logo
    $fpdf->Image('../assets/global/home/images/logo.png', 10, 10, -145);
    $fpdf->SetFont($font['font_logo']['type'], $font['font_logo']['thin'], $font['font_logo']['size']);
    $fpdf->Ln(10);
    $fpdf->Cell(10, 10, $logoaddress, 0, 1, 'L');
    $fpdf->Ln(5);

    //add header
    $fpdf->SetFont($font['font_header']['type'], $font['font_header']['thin'], $font['font_header']['size']);
    $fpdf->Cell(0,0, $title, 0, 1, 'C');
    $fpdf->Ln(10);

    //content
    $fpdf->SetFont($font['font_content']['type'], $font['font_content']['thin'], $font['font_content']['size']);
    $table_member=new easyTable($fpdf, 2);
    $table_member->easyCell('Please provide transportation services for', $font['font_content']['size'].';' .  $font['font_content']['thin'] . ';');
    $table_member->easyCell('No: <b>'. $var_order_id . '</b>', 'align:R;');
    $table_member->printRow();

    $table_member->easyCell($var_data_to['customer'], 'font-size:15; font-style:B;');
    $table_member->easyCell('Jumlah: <b> IDR '. number_format($var_data_item[0], 2, ',','.') .'</b>', $font['font_content']['size'].';' .  $font['font_content']['thin'] . ';' . 'align:R;');
    $table_member->printRow();
    $table_member->endTable(1);

    $fpdf->Cell(0,0, '', 1, 1, 'C');

    $fpdf->SetFont($font['font_content']['type'], $font['font_content']['thin'], $font['font_content']['size']);
    $table_detail=new easyTable($fpdf, 2, 'width:200;');
    $table_detail->easyCell('Periode : ' . dateFormat($var_data_to['od_from']), $font['font_content']['size'].';' .  $font['font_content']['thin'] . ';');
    $table_detail->printRow();

    $table_detail->easyCell('Vehicle  : ' . $var_data_to['armada_brand'], $font['font_content']['size'].';' .  $font['font_content']['thin'] . ';');
    $table_detail->printRow();

    $table_detail->easyCell('Crew     : ' . $var_data_driver[0] . ' + ' . $var_data_codriver[0], $font['font_content']['size'].';' .  $font['font_content']['thin'] . ';');
    $table_detail->printRow();

    $table_detail->easyCell('Needed: ', $font['font_content']['size'].';' .  $font['font_content']['thin'] . ';');
    $table_detail->printRow();
    $table_detail->endTable(0);

    $num = 1;
    $table_detail=new easyTable($fpdf, 2, 'width:400;');
    while ($var_data_item = mysqli_fetch_array($var_query_item)) {
        $fpdf->Ln(4);
        $fpdf->SetFont($font['font_footer']['type'], $font['font_footer']['thin'], $font['font_footer']['size']);
        $fpdf->Cell(10, -5, $num, 0, 0, 'C');
        $fpdf->Cell(25, -5, $var_data_item['detail_peritem'], 0, 0, 'L');
        $fpdf->Cell(3, -5, $var_data_item['detail_countitem'] .' @', 0, 0, 'L');
        $fpdf->Cell(20, -5, number_format($var_data_item['detail_amountperitem'], 2, ',', '.'), 0, 0, 'R');
        $fpdf->Cell(20, -5, number_format($var_data_item['amountperitem'], 2, ',', '.'), 0, 0, 'R');
        $num++;
    }
    $table_detail->endTable(0);
    $fpdf->Cell(0,10, '', 0, 1, 'C');
    $table_member=new easyTable($fpdf, 2, 'width:150;');
    $table_member->easyCell('Penerima', 'align:C;');
    $table_member->easyCell('Keuangan', 'align:C;');
    $table_member->printRow();
    $fpdf->Cell(0,10, '', 0, 1, 'C');
    $table_member->easyCell($var_data_expenses['creator'], 'align:C;');
    $table_member->easyCell($var_data_expenses['confirmedby'], 'align:C;');
    $table_member->printRow();
    $table_member->endTable(1);
    $fpdf->Output('I', 'report-expenses-'. $var_order_id.'-'.date('Ymd') .'.pdf');
    break;

    //cetak laporan untuk armada order
    case 'ao':
    break;

    case 'inv':
    $var_sql_inv = "
    SELECT concat(u.user_detail_firstname,' ',u.user_detail_lastname) as member, u.user_detail_phone, ot.od_invoice, concat(uc.user_detail_firstname,' ',uc.user_detail_lastname) as confirmed_by, b.bank_name, ba.account_number, ba.account_name, ot.od_id, ot.od_selling
    FROM sp_order_transaction ot
    JOIN sp_user_detail u ON ot.user_id = u.user_id
    JOIN sp_payment p ON p.payment_invoice = ot.od_invoice
    JOIN sp_bank_account ba ON ba.account_number = p.payment_accountto
    JOIN sp_bank b ON b.bank_id = ba.bank_id
    JOIN sp_user_detail uc ON uc.user_id = p.payment_confirmedby
    WHERE od_invoice = '{$var_order_id}'";
    $var_query_inv = mysqli_query($var_con, $var_sql_inv);
    $var_rows_inv = mysqli_num_rows($var_query_inv);
    $var_data_inv = mysqli_fetch_array($var_query_inv);

    // debug($var_sql_inv);

    $var_html = '<link rel="stylesheet" href="../assets/global/css/print-report.css">
        <body>
            <div class="header">
                <div class="main-header">
                    <img src="../assets/global/home/images/logo.png">
                </div>
                <div class="address">
                    <p class="camel font-bold">Mitra Persada Travelindo PT. Jalan Pringgondani 1 Mrican Yogyakarta, Ph. +62 274 541409 Email: info@travelindo.com</p>
                </div>
            </div>

            <div class="wrapper">
                <h4 class="title uppercase font-bold text-center font-20">invoice</h4>
                <div class="order_detail">
                    <table class="identity">
                        <tr>
                            <td class="camel">no <td>:</td><td class="row"><span class="font-bold">'. $var_data_inv['od_invoice'] .'</span></td></td>
                        </tr>
                        <tr>
                            <td class="camel">to <td>:</td><td class="row"><span class="font-bold camel">' . $var_data_inv['member'] . ' </span><br><small>( ' . $var_data_inv['user_detail_phone'] . ' )</small></td></td>
                        </tr>
                    </table>
                </div>

                <div class="order_to">
                    <span class="title text-left camel m-b-10">transport service on behalf of ' . $var_data_inv['member'] . '</span>
                    <h5 class="order_by font-15"></h5>
                </div>

                <hr>

                <div class="left-col">
                    <div class="description camel">
                        <table class="itinerary">
                            <tr>
                                <td class="row-10"><p class="uppercase font-bold">' . $var_data_inv['bank_name'] . '</p></td>
                            </tr>
                            <tr>
                                <td class="row-10">On beneficiary of <td>:</td><td class="row-full">' . $var_data_inv['account_name'] . '</td></td>
                            </tr>
                            <tr>
                                <td class="row-10">Account Number <td>:</td><td class="row-full">' . $var_data_inv['account_number'] . '</td></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="right-col">
                <div class="description">
                    <table class="itinerary">
                        <tr>
                            <td class="row-10">Total <td>:</td><td class="row-full">Rp ' . number_format($var_data_inv['od_selling'],2,',','.') . '</td></td>
                        </tr>
                        <tr>
                            <td class="row-10">Deposit <td>:</td><td class="row-full">-</td></td>
                        </tr>
                        <tr class="font-bold">
                            <td class="row-10">Grand total <td>:</td><td class="row-full">Rp ' . number_format($var_data_inv['od_selling'],2,',','.') . '</td></td>
                        </tr>
                        <tr class="font-bold">
                            <td class="row-10">Reff <td>:</td><td class="row-full">' . $var_data_inv['od_id'] . '</td></td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="footer">
            <table class="signature">
                <tr>
                <td class="left">
                    <div class="confirmed_by_left">
                        <p class="m-t-25 text-center camel confirm">Accouting</p>
                        <p class="m-t-25 text-center camel">'. $var_data_inv['confirmed_by'] .'</p>
                    </div>
                </td>
                    <td class="right">
                        <div class="confirmed_by_right">
                            <p class="text-center"><span class="camel">yogyakarta, </span>' . dateFormat(date('Y m d')) . '</p>
                            <p class="m-t-10 text-center camel">Guest</p>
                            <p class="m-t-25 text-center camel">'. $var_data_inv['member'] .'</p>
                        </div>
                    </td>
                </tr>
            </table>
            </div>
        </body>
    ';

        require_once '../inc/lib/dompdf/autoload.inc.php';

        $dompdf = new Dompdf\Dompdf();
        $dompdf->loadHtml($var_html);
        $dompdf->setpaper('A4', 'Potrait');
        $dompdf->render();
        $dompdf->stream('invoice--'. $var_inv_num.'-'.date('Ymd') .'.pdf',array('Attachment'=>0));

    break;

    case 'lp';
    require_once '../inc/lib/fpdf/fpdf.php';
    require_once '../inc/lib/fpdf-easytable/exfpdf.php';
    require_once '../inc/lib/fpdf-easytable/easyTable.php';

    $fpdf = new exFPDF('P', 'mm', 'A4');
    $title = strtoupper('Laporan laba Kotor');

    $fpdf->AddPage();

    //add image logo
    $fpdf->Image('../assets/global/home/images/logo.png', 10, 10, -145);
    $fpdf->SetFont($font['font_logo']['type'], $font['font_logo']['thin'], $font['font_logo']['size']);
    $fpdf->Ln(10);
    $fpdf->Cell(10, 10, $logoaddress, 0, 1, 'L');
    $fpdf->Ln(5);

    //add header
    $fpdf->SetFont($font['font_header']['type'], $font['font_header']['thin'], $font['font_header']['size']);
    $fpdf->Cell(0,0, $title, 0, 1, 'C');
    $fpdf->Ln(10);

    $fpdf->SetFont($font['font_content']['type'], $font['font_content']['thin'], $font['font_content']['size']);
    $table_detail=new easyTable($fpdf, '%{5,45, 50}');

    $table_detail->easyCell('No.');
    $table_detail->easyCell('Tanggal');
    $table_detail->easyCell('Nominal');
    $table_detail->printRow();

    $var_sql_lp = "
    SELECT *
    FROM sp_payment
    WHERE payment_date
    IN (SELECT MAX(payment_date) FROM sp_payment GROUP BY schedule_id ORDER BY payment_date DESC) AND payment_status = '1' GROUP BY schedule_id;
    ";
    $var_query_lp = mysqli_query($var_con, $var_sql_lp);
    $num = 1;
    while ($var_data_report = mysqli_fetch_array($var_query_lp)) {
        $fpdf->Ln(4);
        $fpdf->SetFont($font['font_footer']['type'], $font['font_footer']['thin'], $font['font_footer']['size']);
        $table_detail->easyCell($num);
        $table_detail->easyCell(dateFormat($var_data_report[1]));
        $table_detail->easyCell($var_data_report[8]);
        $table_detail->printRow();
        $num++;
    }
    $table_detail->endTable(0);
    $fpdf->Output('I', 'report-margin'.'-'.date('Ymd') .'.pdf');
    break;
}
?>

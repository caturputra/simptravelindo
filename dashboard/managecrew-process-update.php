<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk memperbaharui data kru armada
*/

require_once '../core/init.php';

//prosedur add crew
$var_crew_id = isset($_POST['frm_crew_id']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_crew_id']), FILTER_SANITIZE_STRING)) : "" ;
$var_crew_name = isset($_POST['frm_driver_name']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_driver_name']), FILTER_SANITIZE_STRING)) : "" ;
$var_crew_co_driver = isset($_POST['frm_co_driver']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_co_driver']), FILTER_SANITIZE_STRING)) : "" ;
$var_crew_status = isset($_POST['frm_status']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_status']), FILTER_SANITIZE_STRING)) : "" ;
$var_error = array();

if (isset($_POST['btn_update_crew'])) {

    //validasi
    if (empty(trim($var_crew_name))) {
        $var_error['name'] = "Please fill in crew name.";
    }

    if (empty(trim($var_crew_co_driver))) {
        $var_error['co_driver'] = "Please fill in co-crew name.";
    }

    if (empty(trim($var_crew_status))) {
        $var_error['status'] = "Please choose status of crew.";
    }

    // Mengambil data dari form gambar
    // $var_ava_name = addslashes($_FILES['frm_avatar']['name']);
    // $var_ava_tmp = addslashes($_FILES['frm_avatar']['tmp_name']);
    // $var_ava_size = $_FILES['frm_avatar']['size'];
    // $var_ava_err = $_FILES['frm_avatar']['error'];
    // $var_ava_dir = "../images/";
    // $var_ava_type = strtolower(pathinfo($var_ava_name, PATHINFO_EXTENSION));
    // $var_ava_ext = array("jpeg", "png", "jpg");
    // $var_ava_newName = "img-ava-" . $var_crew_id .".jpg";
    // $var_init = true;

    // cek apakah sudah memilih gambar?
    // if (empty($var_ava_name)) {
    //     $var_error['ava'] = "We need crew image.";
    // }

    //jika lolos validasi
    // if ($var_ava_err === 0) {
    // cek apakah ukuran file tidak melebihi 700KB (716800 byte)
    // if ($ukuran_file > 1024000) {
    //     $var_message = "Image's size is bigger than 1MB.";
    //     $var_init = false;
    // }

    // cek apakah yang diupload adalah file gambar
    // $check = getimagesize($var_ava_tmp);
    // if ($check === false) {
    //     $var_message = "Please upload avatar in (jpeg, png, jpg) format.";
    //     $var_init = false;
    // }
    // if (empty($var_error) && $var_init === true) {
    if (empty($var_error)) {
        $var_sql_check_crew = "SELECT crew_id FROM sp_crew";
        $var_query_check_crew = mysqli_query($var_con, $var_sql_check_crew);
        $var_data_check_crew = mysqli_fetch_row($var_query_check_crew);
        // if (in_array($var_ava_type, $var_ava_ext) == true) {
            $var_data_crew = array(
                'crew_driver' => $var_crew_name,
                'crew_co_driver' => $var_crew_co_driver,
                'crew_status' => $var_crew_status
            );

            $var_data_crew_condition = array(
                'crew_id' => $var_crew_id,
            );

            // if (mysqli_num_rows($var_query_check_crew) < 0) {
            // if (move_uploaded_file($var_ava_tmp, $var_ava_dir.$var_ava_newName)) {
            if(update($var_con, "sp_crew", $var_data_crew, $var_data_crew_condition)) {
                header('location: index.php?p=managecrew.php&init=2');
            } else {
                header('location: index.php?p=managecrew.php&init=3');
            }
            // } else {
            //     $var_message = "This $var_crew_id has on the database.";
            // }
        } else {
            header('location: index.php?p=managecrew.php&init=4');
        }
        // } else {
        //     $var_message = "This $var_crew_id has on the database.";
        // }
    }
//}
//}
//}

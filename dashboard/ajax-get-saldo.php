<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Ajax untuk ambil histori rekening
*/

require_once '../core/init.php';

$var_rekeningid = isset($_GET['rekening_id']) ? mysqli_escape_string($var_con, filter_var($_GET['rekening_id'], FILTER_SANITIZE_STRING)) : "";
$var_data = [];

$var_sql_saldo = "
SELECT d.deposit_id, d.deposit_balance, l.log_id, l.log_type, l.log_amount, l.log_createdate,
IF (l.log_type = '1', l.log_amount, 0) as debet,
IF (l.log_type = '2', l.log_amount, 0) as kredit
FROM sp_deposit d
JOIN sp_saldo_log l ON l.deposit_id = d.deposit_id
JOIN sp_rekening r ON r.rekening_id = d.rekening_id
WHERE r.rekening_id = '{$var_rekeningid}'
";

$var_query_saldo = mysqli_query($var_con, $var_sql_saldo);
$var_numrows_saldo = mysqli_num_rows($var_query_saldo);

if ($var_numrows_saldo > 0) {
    while ($var_data_saldo = mysqli_fetch_array($var_query_saldo)) {
        $temp = [];
        $temp['deposit_debit'] = $var_data_saldo['debet'];
        $temp['deposit_credit'] = $var_data_saldo['kredit'];
        $temp['log_type'] = $var_data_saldo['log_type'];
        $temp['log_createdate'] = $var_data_saldo['log_createdate'];

        array_push($var_data, $temp);
    }
    echo json_encode($var_data);
}
?>

<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** CRUD data master untuk Parameter pengecekan armada
*/

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';

//set variabel
$var_userid = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT)) : "";
$var_actiondel = isset($_GET['action']) ? mysqli_escape_string($var_con, filter_var($_GET['action'], FILTER_SANITIZE_STRING)) : "";
$var_params = isset($_POST['frm_params_name']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_params_name'], FILTER_SANITIZE_STRING)) : "";

//proses add params
if (isset($_POST['btn_submit_params'])) {
    $var_error = [];

    if (empty(trim($var_params))) {
        $var_error['params'] = "Mohon masukan nama parameter.";
    }

    if (empty($var_error)) {
        $var_insert_params = [
            'params_name' => ucwords($var_params),
        ];

        if (insert($var_con, "sp_check_params", $var_insert_params)) {
            setNotif(['success' => 'parameter berhasil ditambahkan.']);
            routeUrl('index.php?p=armada-check-params');
            die();
        } else {
            setNotif(['danger' => 'parameter gagal ditambahkan.']);
            routeUrl('index.php?p=armada-check-params');
            die();
        }
    }
}

//proses del params
if ($var_actiondel === "del") {
    $var_params_delid = [
        'params_id' => (int) $var_userid
    ];

    if (delete($var_con, "sp_check_params", $var_params_delid)) {
        setNotif(['success' => 'parameter berhasil dihapus.']);
        routeUrl('index.php?p=armada-check-params');
        die();
    } else {
        setNotif(['danger' => 'parameter gagal dihapus.']);
        routeUrl('index.php?p=armada-check-params');
        die();
    }
}

//proses update params
$var_name = $var_params = isset($_POST['name']) ? mysqli_escape_string($var_con, filter_var($_POST['name'], FILTER_SANITIZE_STRING)) : "";
$var_pk = $var_params = isset($_POST['pk']) ? mysqli_escape_string($var_con, filter_var($_POST['pk'], FILTER_SANITIZE_NUMBER_INT)) : "";
$var_value = $var_params = isset($_POST['value']) ? mysqli_escape_string($var_con, filter_var($_POST['value'], FILTER_SANITIZE_STRING)) : "";
if (empty($var_value)) {
    header('HTTP/1.0 400 Bad Request', true, 400);
}
if (!empty($var_pk) && !empty($var_value)) {
    $var_edit_id = ['params_id' => (int) $var_pk];
    $var_edit = ['params_name' => $var_value];
    if (update($var_con, "sp_check_params", $var_edit, $var_edit_id)) {
        //setNotif(['success' => 'parameter successfully updated.'], true);
    } else {
        header('HTTP/1.0 400 Bad Request', true, 400);
    }
}

$var_title = "Parameter cek kondisi armada";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'bt' => '../assets/global/plugins/bootstrap-table/bootstrap-table.min.css',
    'xe' => '../assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>

<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-sm-8">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="icon-speech font-green-sharp"></i>
                    <span class="caption-subject bold uppercase">Parameter</span>
                    <span class="caption-helper">Manajemen pengecekan</span>
                </div>
                <div class="actions">

                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="note note-info"> <p>Klik nama parameter untuk mengubah data </p></div>
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="scroller" style="" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">
                        <table class="table table-striped table-hover table-bordered" id="tableListParams">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Parameter</th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $var_sql_params = "
                                SELECT params_id, params_name
                                FROM sp_check_params
                                ORDER BY params_name
                                ";
                                $var_query_params = mysqli_query($var_con, $var_sql_params);
                                $var_num = (int) 1;
                                if (mysqli_num_rows($var_query_params)) :
                                    while ($var_data_params = mysqli_fetch_array($var_query_params)):
                                        ?>
                                        <tr>
                                            <td class="text-center" style="width: 5em;">
                                                <?php echo $var_num++; ?>
                                            </td>
                                            <td>
                                                <a href="#" class="xupdate" data-type="text" data-pk="<?php echo $var_data_params['params_id'] ?>" data-url="armada-check-params.php" data-title="Parameter Name"><?php echo $var_data_params['params_name'] ?></a>
                                            </td>
                                            <td class="text-center" style="width: 10em;">
                                                <a href="#" onclick="confirm_modal('armada-check-params.php?action=del&amp;id=<?php echo $var_data_params['params_id'] ?>');" class="btn red btn-sm">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endwhile; ?>
                                <?php else : ?>
                                    <?php setNotif(['danger' => 'Data tidak ditemukan.']); ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Portlet PORTLET-->
    </div>
    <!-- /.col-sm-8 -->
    <div class="col-sm-4">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-dark">
                    <i class="icon-speech font-green-dark"></i>
                    <span class="caption-subject bold uppercase">Tambah Parameter</span>
                </div>
                <div class="actions">

                </div>
            </div>
            <div class="portlet-body">
                <form class="armada-check-params.php" action="armada-check-params.php" method="post">
                    <input type="hidden" class="form-control" id="frm_params_id" name="frm_params_id">

                    <div class="form-group">
                        <label for="frm_params_name" class="control-label">Nama <span class="required">*</span></label>
                        <input type="text" class="form-control" id="frm_params_name" name="frm_params_name" placeholder="Nama Parameter" required autofocus>
                        <span class="help-block warning-text"><?php if (isset($var_error['params'])) { echo $var_error['params']; } ?> </span>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <button type="submit" class="btn green btn-block" name="btn_submit_params" id="btn_submit_params"><i class="fa fa-save"></i> Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.form-group -->
                </form>
                <!-- /.form check -->
            </div>
        </div>
        <!-- END Portlet PORTLET-->
    </div>
    <!-- /.col-sm-4 -->
</div>
<!-- /.row -->
<!-- END PAGE BASE CONTENT -->

<!-- modal untuk hapus data -->
<div class="delete-modal">
    <div id="ModalDelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Konfirmasi</h4>
                    </div>
                    <div class="modal-body">
                        <h4 class="text-center">Yakin menghapus parameter ini?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                        <a href="#" class="btn red" id="delete_link"><i class="fa fa-trash"></i> Hapus</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!-- /.hapus-modal -->

    <?php
    $footer_component = [
        'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
        'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
        'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
        'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
        'select2 script' => '../assets/pages/scripts/components-select2.min.js',
        'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
        'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
        'bs' => '../assets/pages/scripts/components-bootstrap-switch.min.js',
        'bt' => '../assets/pages/scripts/table-bootstrap.min.js',
        'xe' => '../assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js',
    ];
    require_once 'inc-template/inc-template-footer.php';
    ?>
    <script type="text/javascript">
    function confirm_modal(delete_url) {
        $('#ModalDelete').modal('show', {
            backdrop: 'static'
        });
        document.getElementById('delete_link').setAttribute('href', delete_url);
    }

    $(document).ready(function() {
        $.fn.editable.defaults.mode = 'inline';
        $('.xupdate').editable();

        $("#tableListParams").DataTable({
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autowidth": true,
            "lengthChange": true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
            }
        });
    });
    </script>

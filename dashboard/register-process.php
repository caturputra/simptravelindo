<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk proses register akun baru
*/

require_once '../core/init.php';
require_once '../inc/function/inc-func-users.php';
require_once '../inc/helper/Mail.php';

// Menerima nilai dari form markup
$var_role = isset($_POST['frm_role']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_role'], FILTER_SANITIZE_STRING)) : "4";
$var_username = isset($_POST['frm_username']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_username'], FILTER_SANITIZE_STRING)) : "";
$var_email = isset($_POST['frm_email']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_email'], FILTER_SANITIZE_EMAIL)) : "";
$var_password = isset($_POST['frm_password']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_password'], FILTER_SANITIZE_STRING)) : "";
$var_confirm_password = isset($_POST['frm_confirm_password']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_confirm_password'], FILTER_SANITIZE_STRING)) : "";
$var_term = isset($_POST['frm_term']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_term'], FILTER_SANITIZE_STRING)) : "";
$var_init = true;

if (isset($_POST['btn_register'])) {
    if (trim(empty($var_username))) {
        $var_init = false;
        setNotif(['warning' => 'Mohon isikan nama pengguna.']);
        routeUrl('register');
        die();
    }

    $var_sql_username = "SELECT user_username FROM sp_user WHERE user_username = '{$var_username}' LIMIT 1";
    $var_query_username = mysqli_query($var_con, $var_sql_username);
    if (mysqli_num_rows($var_query_username) > 0) {
        $var_init = false;
        setNotif(['warning' => '<strong>'. $var_username .' </strong> pengguna sudah digunakan.']);
        routeUrl('register');
        die();
    }

    if (strlen($var_username) < 6 || strlen($var_username) > 32) {
        $var_init = false;
        setNotif(['warning' => 'Nama pengguna harus memiliki panjang 6 - 32 Karakter.']);
        routeUrl('register');
        die();
    }

    if (trim(empty($var_email))) {
        $var_init = false;
        setNotif(['warning' => 'Mohon isikan alamat surel.']);
        routeUrl('register');
        die();
    }

    if (!checkEmail($var_email)) {
        $var_init = false;
        setNotif(['warning' => 'Mohon isikan alamat surel yang valid.']);
        routeUrl('register');
        die();
    }

    $var_sql_email = "SELECT user_email FROM sp_user WHERE user_email = '{$var_email}' LIMIT 1";
    $var_query_email = mysqli_query($var_con, $var_sql_email);
    if (mysqli_num_rows($var_query_email) > 0) {
        $var_init = false;
        setNotif(['warning' =>  '<strong>'. $var_email .' </strong>sudah digunakan.']);
        routeUrl('register');
        die();
    }

    if (trim(empty($var_password))) {
        $var_init = false;
        setNotif(['warning' => 'mohon isikan kata sandi.']);
        routeUrl('register');
        die();
    }

    if (strlen($var_password) < 8 || strlen($var_password) > 32) {
        $var_init = false;
        setNotif(['warning' => 'panjang kata sandi 8 - 32 karakter.']);
        routeUrl('register');
        die();
    }

    if (trim(empty($var_confirm_password))) {
        $var_init = false;
        setNotif(['warning' => 'Mohon isikan konfirmasi kata sandi.']);
        routeUrl('register');
        die();
    }

    if ($var_confirm_password != $var_password) {
        $var_init = false;
        setNotif(['warning' => 'kata sandi tidak sama.']);
        routeUrl('register');
        die();
    }

    if (trim(empty($var_term))) {
        $var_init = false;
        setNotif(['warning' => 'mohon cek kebijakan kami.']);
        routeUrl('register');
        die();
    }

    if (trim(empty($var_role))) {
        $var_init = false;
        setNotif(['warning' => 'mohon pilih peran anda.']);
        routeUrl('register');
        die();
    }


    //recaptcha
    $var_captcha = isset($_POST['g-recaptcha-response']) ? filter_var($_POST['g-recaptcha-response'], FILTER_SANITIZE_STRING):'';

    /* Validasi config recaptcha */
    $var_data = recaptcha($var_captcha);

    if ($var_data->success === false) {
        $var_init = false;
        setNotif(['warning' => 'Mohon konfirmasi jika anda bukan robot.']);
        routeUrl('register');
        die();
    }

    if ($var_init = true && $var_term === "on" && $var_data->success === true) {
        $var_confirm_password = password_hash($var_confirm_password, PASSWORD_BCRYPT);
        $var_status = "suspend";

        //session untuk encode username
        $_SESSION['reg']['encusername'] = base64_encode($var_username);

        //send email confirmation
        $mail = new Mail();
        $from = "noreply@travelindo.com";
        $subject = "Konfirmasi Akun";
        $dir = "content-mail";
        $template = "register";
        $var_opt = [
            'wrapper_linkverify' => $_SERVER['SERVER_NAME'] . '/dashboard/register-confirm.php?id=' . $_SESSION['reg']['encusername'],
            'footer_year' => date('Y')
        ];

        if ($mail->sendMail($from, $var_email, $subject, $template, $dir, $var_opt)) {
            mysqli_autocommit($var_con, FALSE);
            mysqli_begin_transaction($var_con);
            $var_insert_register = userAdd($var_con, $var_username, $var_confirm_password, $var_email, $var_role, $var_status);
            if ($var_insert_register) {
                $var_user_lastid = mysqli_insert_id($var_con);
                $var_data_detail = ['user_id' => $var_user_lastid];

                if ($var_role == '5') {
                    $var_generate_rekening = 'TRV'.date('ymd').strtoupper(randpass(6));
                    $var_data_rekening = [
                        'rekening_id' => $var_generate_rekening,
                        'user_id' => $var_user_lastid
                    ];
                    $var_insert_rekening = insert($var_con, "sp_rekening", $var_data_rekening);

                    $var_data_deposit = [
                        'rekening_id' => $var_generate_rekening,
                    ];
                    $var_inseer_deposit = insert($var_con, "sp_deposit", $var_data_deposit);
                }

                if (insert($var_con, "sp_user_detail", $var_data_detail)) {
                    mysqli_commit($var_con);
                    setNotif(['success' => 'pendaftaran registrasi akun berhasil, mohon cek email <strong>'. $var_email .' </strong> dan segera konfirmasi akun anda.'], true);
                    routeUrl('register');
                    die();
                } else {
                    mysqli_rollback($var_con);
                    setNotif(['danger' => 'Galat! Mohon cek kembali data akun anda.']);
                    routeUrl('register');
                    die();
                }
            } else {
                mysqli_rollback($var_con);
                setNotif(['danger' => 'Galat! mohon cek kembali data registrasi anda.']);
                routeUrl('register');
                die();
            }
        } else {
            setNotif(['warning' => 'Galat! gagal membuat akun.']);
            routeUrl('register');
            die();
        }
    }
}

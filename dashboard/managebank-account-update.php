<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk memperbaharui data rekening bank untuk penerima pembayaran
*/

require_once '../core/init.php';
require_once '../inc/function/inc-func-users.php';

$var_rid = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT)) :"";
?>
<div class="modal-dialog modal-xs">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah rekening : <?php echo $var_rid ?></h4>
            </div>
            <form action="index.php?p=managebank-account.php&amp;act=update" method="post">
                <div class="modal-body">
                    <?php
                    $var_sql_account = "SELECT a.account_name, a.account_number, a.account_status, b.bank_name, b.bank_id FROM sp_bank_account a JOIN sp_bank b ON b.bank_id = a.bank_id LIMIT 1";
                    $var_query_account = mysqli_query($var_con, $var_sql_account);
                    $var_numrows_account = mysqli_num_rows($var_query_account);
                    $var_data_account = mysqli_fetch_array($var_query_account);
                    ?>
                    <?php if ($var_numrows_account > 0) : ?>
                        <input type="hidden" name="frm_accountid" value="<?php echo $var_rid?>">
                        <div class="form-group">
                            <label for="frm_bankname" class="control-label">Nama bank <span class="required">*</span></label>
                            <select class="form-control select2" name="frm_bankname" id="frm_bankname">
                                <option value="<?= $var_data_account['bank_id'] ?>" selected><?= $var_data_account['bank_name'] ?></option>
                                <?php
                                $var_sql_bank = "SELECT bank_id, bank_name FROM sp_bank ORDER BY bank_name";
                                $var_query_bank = mysqli_query($var_con, $var_sql_bank);
                                $var_numrows_bank = mysqli_num_rows($var_query_bank);
                                ?>
                                <?php if ($var_numrows_bank > 0) : ?>
                                    <?php while ($var_data_bank = mysqli_fetch_row($var_query_bank)) : ?>
                                        <option value="<?= $var_data_bank[0] ?>"><?= $var_data_bank[1] ?></option>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </select>
                            <span class="help-block warning-text"><?php if (isset($var_error['err']['bankname'])) { echo $var_error['err']['bankname']; } ?></span>
                        </div>

                        <div class="form-group">
                            <label for="frm_accountnumber" class="control-label">Nomor rekening <span class="required">*</span></label>
                            <input type="text" class="form-control" name="frm_accountnumber" id="frm_accountnumber" placeholder="Nomor rekening" value="<?php if (isset($var_data_account['account_number'])) { echo $var_data_account['account_number']; } ?>">
                            <span class="help-block warning-text"><?php if (isset($var_error['err']['accountnumber'])) { echo $var_error['err']['accountnumber']; } ?></span>
                        </div>

                        <div class="form-group">
                            <label for="frm_accountname" class="control-label">Nama <span class="required">*</span></label>
                            <input type="text" class="form-control" name="frm_accountname" id="frm_accountname" placeholder="Atas nama nomor rekening" value="<?php if (isset($var_data_account['account_name'])) { echo $var_data_account['account_name']; } ?>">
                            <span class="help-block warning-text"><?php if (isset($var_error['err']['accountname'])) { echo $var_error['err']['accountname']; } ?></span>
                        </div>

                        <div class="form-group">
                            <label for="frm_status" class=" control-label">Status <span class="required">*</span></label>
                            <?php switch ($var_data_account['account_status']) : case '1': ?>
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="frm_status" id="optionsRadios1" value="1" checked> Aktif
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="frm_status" id="optionsRadios2" value="0"> Nonaktif
                                    <span></span>
                                </label>
                            </div>
                            <?php break; case '0': ?>
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="frm_status" id="optionsRadios1" value="1"> Aktif
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="frm_status" id="optionsRadios2" value="0" checked> Nonaktif
                                    <span></span>
                                </label>
                            </div>
                            <?php break; endswitch; ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn green" name="btn_update_account"><i class="fa fa-save"></i> Ubah</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->

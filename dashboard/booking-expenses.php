<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk input kebutuhan pengeluaran
*/

require_once '../core/init.php';

$var_uid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "";
$var_action = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var($_GET['act'], FILTER_SANITIZE_STRING)) : "";
$var_sidb = isset($_SESSION['temp']['sid']) ? base64_decode($_SESSION['temp']['sid'])  : "";
$var_sid = mysqli_escape_string($var_con, filter_var($_SESSION['temp']['sid'], FILTER_SANITIZE_STRING));

switch (strtolower($var_action)) {
    case 'exnew':
    $var_groups = isset($_POST['group-'.$var_sid]) ? $_POST['group-'.$var_sid] : "";
    $var_error = [];

    if (isset($_POST['btn_generate'])) {
        mysqli_autocommit($var_con, FALSE);
        mysqli_begin_transaction($var_con);
        $var_eidauto = autonumber($var_con, "sp_expenses", "expenses_id", $width = 4, $start = 'ER'.date('Ym'));
        $var_data_expenses = [
            'expenses_id' => $var_eidauto,
            'expenses_createat' => date('Y-m-d H:i:s'),
            'user_id' => $var_uid,
            'schedule_id' => $var_sid
        ];

        $var_insert_expenses = insert($var_con, "sp_expenses", $var_data_expenses);

        $var_sql_getid = "SELECT expenses_id FROM sp_expenses WHERE user_id = '{$var_uid}' ORDER BY expenses_id DESC LIMIT 1";
        $var_query_getid = mysqli_query($var_con, $var_sql_getid);
        $var_data_getid = mysqli_fetch_row($var_query_getid);

        for ($i=0; $i < sizeof($var_groups); $i++) {
            $var_name = isset($var_groups[$i]['frm_name']) ? mysqli_escape_string($var_con, filter_var($var_groups[$i]['frm_name'], FILTER_SANITIZE_STRING)) : "";
            $var_amount = isset($var_groups[$i]['frm_amount']) ? mysqli_escape_string($var_con, filter_var($var_groups[$i]['frm_amount'], FILTER_SANITIZE_STRING)) : "";
            $var_count = isset($var_groups[$i]['frm_count']) ? mysqli_escape_string($var_con, filter_var($var_groups[$i]['frm_count'], FILTER_SANITIZE_STRING)) : "1";

            if (empty(trim($var_name))) {
                $var_error['err']['name'] = 'Mohon masukkan nama kebutuhan.';
            }

            if (empty(trim($var_amount))) {
                $var_error['err']['amount'] = 'Mohon masukkan nominal kebutuhan.';
            }

            //jika lolos Validasi
            if (empty($var_error)) {
                foreach ($var_groups as $key => $var_group) {
                    $data[] = $var_group;
                }

                 $var_totalamount[$i] = (int) $var_groups[$i]['frm_amount'] * $var_groups[$i]['frm_count'];
                $var_data_detail[$i] = [
                    'detail_amount' => $var_totalamount[$i],
                    'detail_peritem' => $data[$i]['frm_name'],
                    'detail_amountperitem' => $data[$i]['frm_amount'],
                    'detail_countitem' => isset($data[$i]['frm_count']) ?  $data[$i]['frm_count'] : 1,
                    'expenses_id' => $var_data_getid[0],
                ];
                $var_insert_detail = insert($var_con, "sp_expenses_detail", $var_data_detail[$i]);
            }
        }

        if ($var_insert_expenses && $var_insert_detail) {
            mysqli_commit($var_con);
            setNotif(['success' => 'data kebutuhan perjalanan berhasil diproses.']);
            routeUrl('index.php?p=booking-schedule-list');
            die();
        } else {
            mysqli_rollback($var_con);
            setNotif(['warning' => 'data kebutuhan perjalanan berhasil diproses.']);
            routeUrl('index.php?p=booking-schedule-list');
            die();
        }
    }
    break;
}

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

$var_title = "Expenses";

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-docs font-green"></i>
                    <span class="caption-subject font-green bold uppercase">Buat Kebutuhan Pengeluaran</span>
                </div>
                <div class="actions">
                    <!-- actions print dll -->
                </div>
            </div>
            <div class="portlet-body flip-scroll">
                <div class="row">
                    <div class="col-sm-12">
                        <form action="index.php?p=booking-expenses.php&amp;act=exnew" class="mt-repeater form-horizontal" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="frm_sid" value="<?= $var_sid ?>">
                            <a href="javascript:;" data-repeater-create class="btn blue mt-repeater-add"><i class="fa fa-plus"></i> Tambah</a>
                            <br><br>
                            <div data-repeater-list="group-<?= $var_sid ?>">
                                <div data-repeater-item class="mt-repeater-item">
                                    <!-- jQuery Repeater Container -->
                                    <div class="mt-repeater-input">
                                        <label for="frm_name" class="control-label">Nama <span class="required">*</span></label>
                                        <br/>
                                        <input type="text" name="frm_name" id="frm_name" class="form-control" autofocus>
                                        <span class="help-block warning-text"><?php if (isset($var_error['err']['name'])) { echo $var_error['err']['name']; }?></span>
                                    </div>

                                    <div class="mt-repeater-input">
                                        <label for="frm_amount" class="control-label">Nominal @ <span class="required">*</span></label>
                                        <br/>
                                        <input type="text" name="frm_amount" id="frm_amount" class="form-control">
                                        <span class="help-block warning-text"><?php if (isset($var_error['err']['amount'])) { echo $var_error['err']['amount']; }?></span>
                                    </div>

                                    <div class="mt-repeater-input">
                                        <label for="frm_count" class="control-label">Jumlah <span class="required">*</span></label>
                                        <br/>
                                        <input type="number" name="frm_count" id="frm_count" class="form-control" pattern="[0-9]" min="1" value="1">
                                        <span class="help-block warning-text"><?php if (isset($var_error['err']['count'])) { echo $var_error['err']['count']; }?></span>
                                    </div>

                                    <div class="mt-repeater-input">
                                        <a href="javascript:;" data-repeater-delete class="btn red mt-repeater-delete"><i class="fa fa-close"></i> Hapus</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <a href="index.php?p=booking-schedule-list.php" class="btn default"><i class="fa fa-exclamation-triangle"></i> Batal</a>
                                    <div class="pull-right">
                                        <button type="submit" class="btn green" name="btn_generate"><i class="fa fa-save"></i> Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.porlet-body -->
        </div>
        <!-- /.portlet -->
    </div>
    <!-- /.col-sm-6 -->
</div>
<!-- END CONTENT -->

<?php
$footer_component = [
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    //'sample' => '../assets/custom/form-validation-expenses-add.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
    'jsfr' => '../assets/global/plugins/jquery-repeater/jquery.repeater.js',
    'fr' => '../assets/pages/scripts/form-repeater.min.js'
];

require_once 'inc-template/inc-template-footer.php';
?>

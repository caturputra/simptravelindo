<?php
    require_once '../core/init.php';

    $var_action = isset($_GET['a']) ? filter_var($_GET['a'], FILTER_SANITIZE_NUMBER_INT) : "";
    $var_eid = isset($_POST['id']) ? $_POST['id'] : "";
    $var_userid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "";

    switch ($var_action) {
        case 1:

        foreach ($var_eid as $key => $value) {
            $var_get_phone = mysqli_fetch_row(mysqli_query($var_con, "SELECT ud.user_detail_phone, e.user_id FROM sp_user_detail ud JOIN sp_expenses e ON e.user_id = ud.user_id WHERE e.expenses_id = '{$value}' LIMIT 1"));

            $var_data_update = ['expenses_status' => '1', 'expenses_confirmedby' => $var_userid];
            $var_cond_update = ['expenses_id' => $value];
            $var_update = update($var_con, "sp_expenses", $var_data_update, $var_cond_update);

            notification($var_con, [
                'from' => $var_userid,
                'to' => $var_get_phone[1],
                'description' => 'Deposit '. $value .' telah disetujui.',
                'type' => 'konfirmasi',
            ]);

            sendPhone($var_con, ['phone' => $var_get_phone[0], 'message' => 'Expenses telah divalidasi, masuk sistem untuk detail validasi.']);
        }

        if ($var_update) {
            return true;
        }
        break;

        case 2:

        foreach ($var_eid as $key => $value) {
            $var_get_phone = mysqli_fetch_row(mysqli_query($var_con, "SELECT ud.user_detail_phone, e.user_id FROM sp_user_detail ud JOIN sp_expenses e ON e.user_id = ud.user_id WHERE e.expenses_id = '{$value}' LIMIT 1"));

            $var_data_update = ['expenses_status' => '2', 'expenses_confirmedby' => $var_userid];
            $var_cond_update = ['expenses_id' => $value];
            $var_update = update($var_con, "sp_expenses", $var_data_update, $var_cond_update);

            notification($var_con, [
                'from' => $var_userid,
                'to' => $var_get_phone[1],
                'description' => 'Expenses '. $value .' tidak disetujui.',
                'type' => 'konfirmasi',
            ]);

            sendPhone($var_con, ['phone' => $var_get_phone[0], 'message' => 'Expenses telah divalidasi, masuk sistem untuk detail validasi.']);
        }

        if ($var_update) {
            return true;
        }
        break;
    }
?>

<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk melakukan penjadwalan
*/

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';

$var_action = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var(strtolower($_GET['act']), FILTER_SANITIZE_STRING)) : "";
$var_uid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'])) : "";
$var_oid = isset($_SESSION['order_id']) ? mysqli_escape_string($var_con, filter_var($_SESSION['order_id'])) : "";
$var_ida = isset($_SESSION['armada_regnumber']) ? mysqli_escape_string($var_con, filter_var($_SESSION['armada_regnumber'])) : "";
$var_mid = isset($_SESSION['model_id']) ? mysqli_escape_string($var_con, filter_var($_SESSION['model_id'])) : "";

switch ($var_action) {
    case 'setcrew':
    $var_crews = isset($_POST['frm_crew']) ? $_POST['frm_crew'] : "";
    $var_error = [];

    if (empty($var_crews)) {
        $var_error['err']['crews'] = "Mohon pilih minimal satu crew.";
    }

    mysqli_autocommit($var_con, FALSE);
    mysqli_begin_transaction($var_con);

    foreach ($var_crews as $key => $value) {
        $var_crew = isset($value) ? mysqli_escape_string($var_con, filter_var($value, FILTER_SANITIZE_STRING)) : "";
        $var_datefrom = isset($_SESSION['datefrom']) ? mysqli_escape_string($var_con, filter_var($_SESSION['datefrom'], FILTER_SANITIZE_STRING)) : "";
        $var_dateto = isset($_SESSION['dateto']) ? mysqli_escape_string($var_con, filter_var($_SESSION['dateto'], FILTER_SANITIZE_STRING)) : "";
        $var_sid = isset($_SESSION['schedule_id']) ? mysqli_escape_string($var_con, filter_var($_SESSION['schedule_id'], FILTER_SANITIZE_STRING)) : "";

        $var_sql_check = "
        SELECT cd.crew_armadaid
        FROM sp_crew_detail cd
        WHERE ((cd.crew_datefrom BETWEEN '{$var_datefrom}' AND '{$var_dateto}') OR (cd.crew_dateto BETWEEN '{$var_datefrom}' AND '{$var_dateto}')) AND (cd.crew_action IN ('0', '1'))
        ";
        $var_query_check = mysqli_query($var_con, $var_sql_check);
        $var_data_crew = [
            'crew_armadaid' => $var_crew,
            'crew_datefrom' => $var_datefrom,
            'crew_dateto' => $var_dateto,
            'schedule_id' => $var_sid,
            'crew_createdby' => $var_uid,
        ];

        $var_insert_crew = insert($var_con, "sp_crew_detail", $var_data_crew);

        // get createdby who?
        $var_sql_creator = "
        SELECT ca.user_id, ud.user_detail_phone
        FROM sp_crew_armada ca
        JOIN sp_user_detail ud ON ud.user_id = ca.user_id
        WHERE ca.crew_armadaid = '{$var_crew}'";

        $var_get_creator = mysqli_fetch_row(mysqli_query($var_con, $var_sql_creator));
        notification($var_con, [
            'from' => $var_uid,
            'to' => $var_get_creator[0],
            'description' => 'Anda mendapat penugasan, segera konfirmasi.',
            'type' => 'penugasan',
        ]);
        sendPhone($var_con, [
            'phone' => $var_get_creator[1],
            'message' => 'Anda mendapat penugasan. Segara konfirmasi 1x24 jam. http://' . $_SERVER['SERVER_NAME']
        ]);
    }

    // $var_update_data = ['od_confirmedby' => $var_uid, 'od_createat' => date('Y-m-d')];
    // $var_update_cond = ['od_id' => $var_oid];
    //
    // $var_update_armada = update($var_con, "sp_order_transaction", $var_update_data, $var_update_cond);

    $var_update_data_detail = ['armada_regnumber' => $var_ida];
    $var_update_cond_detail = ['od_id' => $var_oid, 'model_id' => $var_mid];
    $var_update_armada_detail = update($var_con, "sp_order_transaction_detail", $var_update_data_detail, $var_update_cond_detail);

    if ($var_insert_crew && $var_update_armada_detail) {
        mysqli_commit($var_con);
        setNotif(['success' => 'Penjadwalan kru berhasil. Tunggu konfirmasi dari kru.']);
        routeUrl('index.php?p=booking-armada');
        unset($_SESSION['datefrom']);
        unset($_SESSION['dateto']);
        unset($_SESSION['schedule_id']);
        unset($_SESSION['armada_regnumber']);
        unset($_SESSION['order_id']);
        unset($_SESSION['model_id']);
        die();
    } else {
        mysqli_rollback($var_con);
        setNotif(['warning' => 'Penjadwalan kru gagal, periksa data Penjadwalan']);
        routeUrl('index.php?p=booking-armada');
        die();
    }

    break;

    case 'del':
    $var_sid = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : "";

    $var_delete_cond = ['schedule_id' => $var_sid];

    if (delete($var_con, 'sp_crew_detail', $var_delete_cond)) {
        setNotif(['warning' => 'Penjadwalan kru berhasil dihapus.']);
        routeUrl('index.php?p=booking-schedule-list');
        die();
    }

    break;
}

$var_title = "Penjadwalan";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
    'bsselect' => '../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css',
    'multiselect' => '../assets/global/plugins/jquery-multi-select/css/multi-select.css',
    // 'multiselect2' => '../assets/global/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css',
    'datepickercss'=>'../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
    'datetimepickercss' => '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-sm-4 col-xs-12 col-sm-4">
        <!-- <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-calendar font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">ID</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <select class="form-control select2" name="frm_order_id" id="frm_order_id">
                    <option value="">Pilih</option>
                    <?php
                    // $var_sql_selectid = "
                    // SELECT ot.od_id, s.schedule_id, ot.od_from, ot.od_to, ot.armada_regnumber,  DATEDIFF(ot.od_from, CURRENT_TIMESTAMP()) as date_dispute
                    // FROM sp_order_transaction ot
                    // JOIN sp_schedule s ON s.od_id = ot.od_id
                    // WHERE (ot.od_status = '4') AND s.schedule_id NOT IN (SELECT schedule_id FROM sp_crew_detail)
                    // ";
                    // $var_query_selectid = mysqli_query($var_con, $var_sql_selectid);
                    // $var_numrows_selectid = mysqli_num_rows($var_query_selectid);
                    ?>
                    <?php //if ($var_numrows_selectid > 0) :?>
                        <?php //while ($var_data_selectid = mysqli_fetch_array($var_query_selectid)) : ?>
                            <?php //if ($var_data_selectid[5] >= 0 && $var_data_selectid[5] <= 3) :?>
                                <?php// $_SESSION['schedule_id']  = $var_data_selectid['1'] ?>
                                <?php //$_SESSION['datefrom']  = $var_data_selectid['2'] ?>
                                <?php// $_SESSION['dateto']  = $var_data_selectid['3'] ?>
                                <option value="<?//= $var_data_selectid['od_id'] ?>"><?//= $var_data_selectid['od_id'] ?></option>
                            <?php //endif; ?>
                        <?php// endwhile; ?>
                    <?php// endif; ?>
                </select>
            </div>
        </div> -->

        <div class="portlet light bordered" id="detailOrder">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-calendar font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Detail</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
            <style media="screen">
                .head-left-data {
                    font-weight: 600;
                }
            </style>
            <?php
            $var_sql_detail = "
            SELECT ot.od_from, ot.od_to, concat(ud.user_detail_firstname, ' ', ud.user_detail_lastname) as member, CASE ot.od_service WHEN '1' THEN 'City Tour' WHEN '2' THEN 'Transfer In/Out' END as service
            FROM sp_order_transaction ot
            JOIN sp_user_detail ud ON ud.user_id = ot.user_id
            WHERE ot.od_id = '{$var_oid}'
            LIMIT 1
            ";
            $var_query_detail = mysqli_query($var_con, $var_sql_detail);
            $var_data_detail = mysqli_fetch_array($var_query_detail);
            ?>
            <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered">
                    <tbody>
                        <?php if (mysqli_num_rows($var_query_detail) > 0): ?>
                                <tr>
                                    <td class="head-left-data">Pemesan</td>
                                    <td id="member_detail"><?= $var_data_detail['member'] ?></td>
                                </tr>
                                <tr>
                                    <td class="head-left-data">Berangkat</td>
                                    <td id="from_detail"><?= $var_data_detail['od_from'] ?></td>
                                </tr>
                                <tr>
                                    <td class="head-left-data">Pulang</td>
                                    <td id="to_detail"><?= $var_data_detail['od_to'] ?></td>
                                </tr>
                                <tr>
                                    <td class="head-left-data">Layanan</td>
                                    <td id="service_detail"><?= $var_data_detail['service'] ?></td>
                                </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
    <!-- col-sm-4 -->

    <div class="col-sm-8 col-xs-12 col-sm-7" id="setcrew_box">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-calendar font-blue-steel"></i>
                    <span class="caption-subject font-blue-steel bold uppercase">jadwalkan sopir dan kernet</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <form class="" action="index.php?p=booking-schedule.php&amp;act=setcrew" method="post" id="form_crew">
                    <input type="hidden" name="frm_order_id" value="<?php echo $var_oid ?>" class="frm_order_id">
                    <div class="row">
                        <div class="col-sm-12 col-sm-12">
                            <div class="form-group">
                                <span class="help-block warning-text"><?php if(!empty($var_error['err']['crews'])) {echo $var_error['err']['crews'];} ?></span>
                                <select multiple="multiple" class="multi-select" id="my_multi_select2" name="frm_crew[]" required>
                                    <optgroup label="SOPIR">
                                        <?php
                                        $var_sql_driver = "
                                        SELECT ca.crew_armadaid, u.user_level, concat(ud.user_detail_firstname, ' ', ud.user_detail_lastname) as crew_name
                                        FROM sp_crew_armada ca
                                        JOIN sp_user u ON u.user_id = ca.user_id
                                        JOIN sp_user_detail ud ON ud.user_id = u.user_id
                                        JOIN sp_crew_armada_detail cad ON ca.crew_armadaid = cad.crew_armadaid
                                        WHERE (u.user_level = '8' && ca.crew_status = '1') && (ca.crew_armadaid
                                        NOT IN ( SELECT cd.crew_armadaid FROM sp_schedule s JOIN sp_crew_detail cd ON cd.schedule_id = s.schedule_id WHERE cd.crew_action != '1' AND ((cd.crew_datefrom BETWEEN '{$_SESSION['datefrom']}' AND '{$_SESSION['dateto']}') OR (cd.crew_dateto BETWEEN '{$_SESSION['datefrom']}' AND '{$_SESSION['dateto']}'))))
                                        GROUP BY ca.crew_armadaid
                                        ";

                                        $var_query_driver = mysqli_query($var_con, $var_sql_driver);
                                        $var_num_rows = mysqli_num_rows($var_query_driver);
                                        ?>
                                        <?php if ($var_num_rows > 0) : ?>
                                            <?php while ($var_data_driver = mysqli_fetch_array($var_query_driver)) : ?>
                                                <option value="<?= $var_data_driver['crew_armadaid']; ?>"><?= $var_data_driver['crew_name']; ?></option>
                                            <?php endwhile; ?>
                                        <?php else: ?>
                                            <option value="" disabled>Data tidak ditemukan</option>
                                        <?php endif; ?>
                                    </optgroup>
                                    <optgroup label="KERNET">
                                        <?php
                                        $var_sql_codriver = "
                                        SELECT ca.crew_armadaid, u.user_level, concat(ud.user_detail_firstname, ' ', ud.user_detail_lastname) as crew_name
                                        FROM sp_crew_armada ca
                                        JOIN sp_user u ON u.user_id = ca.user_id
                                        JOIN sp_user_detail ud ON ud.user_id = u.user_id
                                        JOIN sp_crew_armada_detail cad ON ca.crew_armadaid = cad.crew_armadaid
                                        WHERE (u.user_level = '12' && ca.crew_status = '1') && (ca.crew_armadaid
                                        NOT IN ( SELECT cd.crew_armadaid FROM sp_schedule s JOIN sp_crew_detail cd ON cd.schedule_id = s.schedule_id  WHERE cd.crew_action != '1' AND ((cd.crew_datefrom BETWEEN '{$_SESSION['datefrom']}' AND '{$_SESSION['dateto']}') OR (cd.crew_dateto BETWEEN '{$_SESSION['datefrom']}' AND '{$_SESSION['dateto']}'))))
                                        GROUP BY ca.crew_armadaid
                                        ";

                                        $var_query_codriver = mysqli_query($var_con, $var_sql_codriver);
                                        $var_num_rows = mysqli_num_rows($var_query_codriver);
                                        ?>
                                        <?php if ($var_num_rows > 0) : ?>
                                            <?php while ($var_data_codriver = mysqli_fetch_array($var_query_codriver)) : ?>
                                                <option value="<?= $var_data_codriver['crew_armadaid']; ?>"><?= $var_data_codriver['crew_name']; ?></option>
                                            <?php endwhile; ?>
                                        <?php else: ?>
                                            <option value="" disabled>Data tidak ditemukan</option>
                                        <?php endif; ?>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <a href="index.php?p=booking-schedule.php" class="btn default"><i class="fa fa-exclamation-triangle"></i> Batal</a>
                            <div class="pull-right">
                                <button type="submit" name="btn_setcrew" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END CONTENT -->

</div>
<!-- /.content-wrapper -->
<?php
$footer_component = [
    'momentdate' => '../assets/global/plugins/moment.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'bsselect' => '../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js',
    'multiselect' => '../assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js',
    'multi-select' => '../assets/pages/scripts/components-multi-select.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
    'datepicker' => '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
    'datetimepicker' => '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
    'datescript' => '../assets/pages/scripts/components-date-time-pickers.js',
];
require_once 'inc-template/inc-template-footer.php';
?>
<script type="text/javascript">
$(document).ready(function () {
    // $('#frm_order_id').change( function() {
    //     var seletedid = $(this).find("option:selected").text();
    //     var selectedvalue = $(this).val();
    //
    //     if (selectedvalue != ' ') {
    //         var data = {
    //             "id" : selectedvalue
    //         };
    //         $.get('ajax-get-detailschedule.php', data, function (var_data_detail) {
    //             $('#from_detail').empty().html(var_data_detail.od_from);
    //             $('#to_detail').empty().html(var_data_detail.od_to);
    //             $('#member_detail').empty().html(var_data_detail.member);
    //             $('#service_detail').empty().html(var_data_detail.service);
    //         }, 'json')
    //         $('.frm_order_id').val(selectedvalue);
            // $('#setcrew_box').show();
            // $('#detailOrder').show();
        // } else {
            // $('#setcrew_box').hide();
            // $('#detailOrder').hide();
    //     }
    // });

    // $('#detailOrder').hide();
    // $('#setcrew_box').hide();
});
</script>

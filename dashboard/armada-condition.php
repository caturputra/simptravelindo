<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk menampilkan data armada yang siap dicek
*/

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';

$var_title = "Monitor Kondisi Armada";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Daftar Kondisi</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="table-responsive">
                        <div class="col-sm-12 col-xs-12 col-lg-12">
                            <table class="table table-bordered table-striped table-hover" id="tableListCond">
                                <thead>
                                    <tr>
                                        <th>Gambar</th>
                                        <th>Armada</th>
                                        <th>No. Plat</th>
                                        <th>Kondisi</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $var_sql_armada = "
                                    SELECT a.armada_regnumber, a.armada_odometer, a.armada_image, a.armada_status, a.armada_createat, am.model_name as armada_brand, ac.condition_meter
                                    FROM sp_model_type mt
                                    LEFT JOIN sp_armada a ON mt.armada_regnumber = a.armada_regnumber
                                    LEFT JOIN sp_armada_model am ON am.model_id = mt.model_id
                                    LEFT JOIN sp_armada_condition ac ON ac.armada_regnumber = a.armada_regnumber
                                    WHERE a.armada_status = 'approved' AND ac.condition_checkdate IN
                                    (SELECT MAX(condition_checkdate) FROM sp_armada_condition GROUP BY armada_regnumber)
                                    ORDER BY armada_brand DESC
                                    ";

                                    $var_query_armada = mysqli_query($var_con, $var_sql_armada);
                                    $var_numrows_armada = mysqli_num_rows($var_query_armada);
                                    ?>
                                    <?php while ($var_data_armada = mysqli_fetch_array($var_query_armada)): ?>
                                        <tr>
                                            <td class="text-center"><img src="../images/armada/<?= $var_data_armada['armada_image' ] ?>" alt="" class="img img-responsive" style="width: 100px;"></td>
                                            <td><?php echo ucwords($var_data_armada['armada_brand']) ?></td>
                                            <td><span class="label label-primary bg-purple"><?php echo ucwords($var_data_armada['armada_regnumber']) ?></span></td>
                                            <td><?php echo ucwords($var_data_armada['condition_meter']) ?></td>
                                            <td class="text-center"><a href="index.php?p=armada-condition-check.php&amp;id=<?= base64_encode($var_data_armada['armada_regnumber']) ?>" class="btn yellow-crusta btn-sm"><i class="fa fa-edit"> Cek</i> </a></td>
                                        </tr>
                                    <?php endwhile; ?>
                                </tbody>
                            </table>
                            <!-- /.tableListOrder -->
                        </div>
                        <!-- /.col-sm-12 -->
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.porlet-body -->
        </div>
        <!-- /.portlet -->
    </div>
    <!-- /.col-sm-6 -->
</div>
<!-- END PAGE BASE CONTENT -->

<?php
$footer_component = [
    'datatableglobalscript' => '../assets/global/scripts/datatable.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'datableplugin' => '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
];

require_once 'inc-template/inc-template-footer.php';
?>
<script type="text/javascript">
$(document).ready(function() {
    $("#tableListCond").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "deferRender": true,
        // "scroller": true,
        // "scrollX": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        },
    });
});
</script>

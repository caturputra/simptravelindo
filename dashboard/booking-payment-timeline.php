<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman yang memuat histori pembayaran
*/

require_once '../core/init.php';

$var_title = "Histori Pembayaran";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-microphone font-green"></i>
                    <span class="caption-subject bold font-green uppercase"> Histori Pembayaran </span>
                </div>
                <div class="actions">

                </div>
            </div>
            <div class="portlet-body">
                <div class="mt-timeline-2">
                    <div class="mt-timeline-line border-grey-steel"></div>
                    <ul class="mt-container">
                        <?php
                        $var_sid = isset($_GET['invoice']) ? mysqli_escape_string($var_con, $_GET['invoice']) : "";
                        $var_sid = base64_decode($var_sid);
                        $var_sid = filter_var($var_sid, FILTER_SANITIZE_STRING);

                        // echo "<pre>";
                        // $var_get_monitor = file_get_contents('../log/monitor/log-20171006.txt');
                        // $rows = explode("\n", trim($var_get_monitor));
                        // array_shift($rows);
                        // $info = [];
                        // foreach (new SplFileObject('../log/monitor/log-20171006.txt') as $key => $value) {
                        //     $data = explode('##', trim($value));
                        //     var_dump($data);
                        //     $info[$key]['pesan'] = $data[5];
                        //     echo $info[$key]['pesan'];
                        // }
                        // // debug($info[$key]['pesan']);
                        // die();

                        $var_sql_monitor = "
                        SELECT p.payment_date, p.payment_accountfrom, p.payment_accountto, p.payment_balance, p.payment_status, ba.account_name, p.payment_accountname
                        FROM sp_payment p
                        JOIN sp_schedule s ON s.schedule_id = p.schedule_id
                        JOIN sp_order_transaction ot ON ot.od_id = s.od_id
                        JOIN sp_bank_account ba ON ba.account_number = p.payment_accountto
                        WHERE ot.od_invoice = '{$var_sid}'
                        ORDER BY payment_date DESC
                        ";
                        $var_query_monitor = mysqli_query($var_con, $var_sql_monitor);
                        $var_numrows_monitor = mysqli_num_rows($var_query_monitor);
                        ?>
                        <?php if ($var_numrows_monitor > 0) : ?>
                            <?php while ($var_data_monitor = mysqli_fetch_array($var_query_monitor)) : ?>

                                <li class="mt-item">
                                    <div class="mt-timeline-icon bg-blue bg-font-red border-grey-steel">
                                        <i class="icon-credit-card"></i>
                                    </div>
                                    <div class="mt-timeline-content">
                                        <div class="mt-content-container">
                                            <div class="mt-title">
                                                <h3 class="mt-content-title">Detail</h3>
                                            </div>
                                            <div class="mt-author">
                                                <div class="mt-author-name">
                                                    <a href="javascript:;" class="font-blue-madison">Rekening Pengirim: <?= substr_replace($var_data_monitor['payment_accountfrom'], '****', 5) ?> <br>a/n <?= $var_data_monitor['payment_accountname'] ?></a>
                                                </div>
                                                <div class="mt-author-notes font-grey-mint"><?= $var_data_monitor['payment_date'] ?></div>
                                            </div>
                                            <div class="mt-content border-grey-salt">
                                                <p>Rekening Penerima: <?= $var_data_monitor['payment_accountto'] ?> a/n <?= $var_data_monitor['account_name'] ?></p>
                                                <p>Nominal: IDR <?= number_format($var_data_monitor['payment_balance'], 2, ',', '.') ?></p>
                                                <p>Status: <?php if ($var_data_monitor['payment_status'] == 1) echo 'Tervalidasi'; else echo 'Belum divalidasi'; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endwhile; ?>
                        <?php else: ?>
                            No data
                        <?php endif; ?>
                    </ul>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pull-right">
                                <a href="index.php?p=booking-list-payment.php" class="btn blue"><i class="fa fa-arrow-left"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT -->

<?php
$footer_component = [
    'datatableglobalscript' => '../assets/global/scripts/datatable.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'datableplugin' => '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
require_once 'inc-template/inc-template-footer.php';
?>

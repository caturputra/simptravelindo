<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk beranda Direktur
*/

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';

$var_title = "Dashboard Direktur";
$head_component = [
    'fccss' => '../assets/global/plugins/fullcalendar/fullcalendar.min.css',
    'morris' => '../assets/global/plugins/morris/morris.css',
];
require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>

<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- BEGIN DASHBOARD STATS 1-->
<div class="row">
    <div class="col-lg-3 col-sm-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $var_sql_countorder = "SELECT count(*) as count_odvehicles FROM sp_order_transaction";
                    $var_query_countorder = mysqli_query($var_con, $var_sql_countorder);
                    $var_numrows_countorder = mysqli_num_rows($var_query_countorder);
                    $var_data_countorder = mysqli_fetch_row($var_query_countorder);
                    if ($var_numrows_countorder > 0) :
                        ?>
                        <span><?php echo $var_data_countorder[0] ?></span>
                    <?php else: ?>
                        <span>0</span>
                    <?php endif; ?>
                </div>
                <div class="desc"> Total Pemesanan Armada <a href="#" class="font-white"><span class="fa fa-external-link"> </span></a></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat dashboard-stat-v2 purple" href="#">
            <div class="visual">
                <i class="fa fa-bus"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $var_sql_countorder = "SELECT count(*) as count_armada FROM sp_armada WHERE armada_status = 'approved'";
                    $var_query_countorder = mysqli_query($var_con, $var_sql_countorder);
                    $var_numrows_countorder = mysqli_num_rows($var_query_countorder);
                    $var_data_countorder = mysqli_fetch_row($var_query_countorder);
                    if ($var_numrows_countorder > 0) :
                        ?>
                        <span><?php echo $var_data_countorder[0] ?></span>
                    <?php else: ?>
                        <span>0</span>
                    <?php endif; ?>
                </div>
                <div class="desc"> Total Armada <a href="#" class="font-white"><span class="fa fa-external-link"> </span></a></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat dashboard-stat-v2 blue-madison" href="#">
            <div class="visual">
                <i class="fa fa-users"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $var_sql_countorder = "SELECT count(*) as count_armada FROM sp_crew_armada WHERE crew_status = '1'";
                    $var_query_countorder = mysqli_query($var_con, $var_sql_countorder);
                    $var_numrows_countorder = mysqli_num_rows($var_query_countorder);
                    $var_data_countorder = mysqli_fetch_row($var_query_countorder);
                    if ($var_numrows_countorder > 0) :
                        ?>
                        <span><?php echo $var_data_countorder[0] ?></span>
                    <?php else: ?>
                        <span>0</span>
                    <?php endif; ?>
                </div>
                <div class="desc"> Total Kru <a href="#" class="font-white"><span class="fa fa-external-link"> </span></a></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat dashboard-stat-v2 yellow" href="#">
            <div class="visual">
                <i class="fa fa-users"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $var_sql_countorder = "SELECT count(*) as count_armada FROM sp_crew_armada WHERE crew_status = '0'";
                    $var_query_countorder = mysqli_query($var_con, $var_sql_countorder);
                    $var_numrows_countorder = mysqli_num_rows($var_query_countorder);
                    $var_data_countorder = mysqli_fetch_row($var_query_countorder);
                    if ($var_numrows_countorder > 0) :
                        ?>
                        <span><?php echo $var_data_countorder[0] ?></span>
                    <?php else: ?>
                        <span>0</span>
                    <?php endif; ?>
                </div>
                <div class="desc"> Validasi Kru <a href="#" class="font-white"><span class="fa fa-external-link"> </span></a></div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<!-- END DASHBOARD STATS 1-->

<!-- AGENDA PESANAN -->
<div class="row">
    <div class="col-lg-12 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light calendar bordered">
            <div class="portlet-title ">
                <div class="caption">
                    <i class="icon-calendar font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Agenda Pesanan</span>
                </div>
            </div>
            <div class="portlet-body">
                <div id="calendar"> </div>
                <div class="clearfix"></div><br>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <span class="btn btn-sm purple-seance"><label class="label">Ditunda</label></span>
                        <span class="btn btn-sm green-jungle"><label class="label">Disetujui</label></span>
                        <span class="btn btn-sm red-thunderbird"><label class="label">Ditolak</label></span>
                        <span class="btn btn-sm yellow-lemon"><label class="label">Dibatalkan</label></span>
                        <span class="btn btn-sm grey-salsa"><label class="label">Dikunci</label></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
<!-- END AGENDA PESANAN -->

<?php
//FOOTER PAGE BASE LEVEL
$footer_component = [
    'momentdate' => '../assets/global/plugins/moment.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'dashboardscript' => '../assets/pages/scripts/dashboard.js',
    'fcjs' => '../assets/global/plugins/fullcalendar/fullcalendar.min.js',
    'morrisjs' => '../assets/global/plugins/morris/morris.min.js'
];
require_once 'inc-template/inc-template-footer.php';
?>

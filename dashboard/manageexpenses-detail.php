<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk memperbaharui data armada
*/

require_once '../core/init.php';
require_once '../inc/function/inc-func-users.php';

$var_iid = isset($_GET['id']) ? filter_var($_GET['id'], FILTER_SANITIZE_STRING) :"";

?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Detail Expenses : <?php echo $var_iid ?></h4>
        </div>
        <form action="booking-action-approve.php" method="post" class="form-horizontal">
            <div class="modal-body">
                <?php
                $var_sql_detail = "
                SELECT e.expenses_id, e.expenses_status, e.expenses_createat, e.schedule_id, concat(ud.user_detail_firstname, ' ', ud.user_detail_lastname) as creator
                FROM sp_expenses e
                JOIN sp_user_detail ud ON ud.user_id = e.user_id
                WHERE expenses_id = '{$var_iid}'
                LIMIT 1
                ";
                // debug($var_sql_detail);
                $var_query_detail = mysqli_query($var_con, $var_sql_detail);
                while ($var_data_detail = mysqli_fetch_array($var_query_detail)) :
                    ?>
                    <form class="form-horizontal" role="form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="" class="control-label col-sm-4">Item </label>
                                            <div class="col-sm-8">
                                                <?php
                                                $var_sql_item = "
                                                SELECT d.detail_amount, d.detail_peritem, d.detail_amountperitem, d.detail_countitem
                                                FROM sp_expenses_detail d
                                                WHERE expenses_id = '{$var_iid}'
                                                ";
                                                $var_query_item = mysqli_query($var_con, $var_sql_item);
                                                $num = 1;
                                                ?>
                                                <?php while ($var_data_item = mysqli_fetch_array($var_query_item)) : ?>
                                                    <p class="form-control-static">
                                                        <?php echo $num++ ?>. <?php echo $var_data_item['detail_peritem']; ?> @ <?php echo $var_data_item['detail_amountperitem'] ?> x <?php echo $var_data_item['detail_countitem'] ?>
                                                    </p>
                                                <?php endwhile; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="" class="control-label col-sm-4">Tanggal Dibuat: </label>
                                            <div class="col-sm-8">
                                                <p class="form-control-static"><?php echo dateFormat($var_data_detail['expenses_createat']); ?></p>
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="" class="control-label col-sm-4">Total: </label>
                                        <div class="col-sm-8">
                                            <?php
                                            $var_sql_item = "
                                            SELECT SUM(d.detail_amount)
                                            FROM sp_expenses_detail d
                                            WHERE expenses_id = '{$var_iid}'
                                            ";
                                            $var_query_item = mysqli_query($var_con, $var_sql_item);
                                            $var_data_item = mysqli_fetch_row($var_query_item);
                                            ?>
                                            <p class="form-control-static">Rp <?php echo number_format($var_data_item[0], 2, ',', '.'); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="" class="control-label col-sm-4">Pembuat: </label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static"><?php echo $var_data_detail['creator']; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <div class="margiv-top-10 col-sm-offset-2">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                            <a class="btn red" href="index.php?p=validate-expenses.php&amp;act=2&amp;id=<?php echo base64_encode($var_data_detail['expenses_id']) ?>"> <i class="fa fa-ban"></i> Tolak</a>
                            <a class="btn green" href="index.php?p=validate-expenses.php&amp;act=1&amp;id=<?php echo base64_encode($var_data_detail['expenses_id']) ?>"> <i class="fa fa-check"></i> Setujui</a>
                        </div>
                    </div>
                </form>
                <!-- /.form-horizontal -->
                <?php endwhile; ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->

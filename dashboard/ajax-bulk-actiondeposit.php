<?php
    require_once '../core/init.php';

    $var_action = isset($_GET['a']) ? filter_var($_GET['a'], FILTER_SANITIZE_NUMBER_INT) : "";
    $var_eid = isset($_POST['id']) ? $_POST['id'] : "";
    $var_userid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "";

    switch ($var_action) {
        case 1:

        foreach ($var_eid as $key => $value) {
            $var_get_phone = mysqli_fetch_row(mysqli_query($var_con, "SELECT ud.user_detail_phone, r.user_id FROM sp_user_detail ud JOIN sp_rekening r ON r.user_id = ud.user_id WHERE r.rekening_id = '{$value}' LIMIT 1"));

            $var_data_update = ['temp_status' => '1'];
            $var_cond_update = ['rekening_id' => $value];
            $var_update = update($var_con, "sp_deposit_temp", $var_data_update, $var_cond_update);

            if ($var_update) {
                $var_sql_getrek = "
                SELECT temp_id, temp_amount, temp_status, temp_type, rekening_id
                FROM sp_deposit_temp
                WHERE temp_createdat = (SELECT MAX(temp_createdat) FROM sp_deposit_temp WHERE rekening_id =  '{$value}' LIMIT 1)
                ";
                $var_query_getrek = mysqli_query($var_con, $var_sql_getrek);
                $var_data_getrek = mysqli_fetch_row($var_query_getrek);

                $var_sql_getdepositid = "SELECT deposit_id FROM sp_deposit WHERE rekening_id = '{$var_data_getrek[4]}' LIMIT 1";
                $var_query_getdepositid = mysqli_query($var_con, $var_sql_getdepositid);
                $var_data_getdepositid = mysqli_fetch_row($var_query_getdepositid);

                $var_data_insert = [
                    'log_type' => $var_data_getrek[3],
                    'log_amount' => $var_data_getrek[1],
                    'deposit_id' => $var_data_getdepositid[0]
                ];

                $var_insert = insert($var_con, "sp_saldo_log", $var_data_insert);

                notification($var_con, [
                    'from' => $var_userid,
                    'to' => $var_get_phone[1],
                    'description' => 'Deposit '. $value .' berhasil ditambahkan.',
                    'type' => 'konfirmasi',
                ]);
                sendPhone($var_con, ['phone' => $var_get_phone[0], 'message' => 'Topup anda telah divalidasi, masuk sistem untuk detail histori.']);
            }
        }

        break;

        case 2:
        foreach ($var_eid as $key => $value) {
            $var_get_phone = mysqli_fetch_row(mysqli_query($var_con, "SELECT ud.user_detail_phone, r.user_id FROM sp_user_detail ud JOIN sp_rekening r ON r.user_id = ud.user_id WHERE r.rekening_id = '{$value}' LIMIT 1"));

            $var_data_update = ['temp_status' => '2'];
            $var_cond_update = ['rekening_id' => $value];
            $var_update = update($var_con, "sp_deposit_temp", $var_data_update, $var_cond_update);

            if ($var_update) {
                $var_sql_getrek = "
                SELECT temp_id, temp_amount, temp_status, temp_type, rekening_id
                FROM sp_deposit_temp
                WHERE temp_createdat = (SELECT MAX(temp_createdat) FROM sp_deposit_temp WHERE rekening_id =  '{$value}' LIMIT 1)
                ";
                $var_query_getrek = mysqli_query($var_con, $var_sql_getrek);
                $var_data_getrek = mysqli_fetch_row($var_query_getrek);

                $var_sql_getdepositid = "SELECT deposit_id FROM sp_deposit WHERE rekening_id = '{$var_data_getrek[4]}' LIMIT 1";
                $var_query_getdepositid = mysqli_query($var_con, $var_sql_getdepositid);
                $var_data_getdepositid = mysqli_fetch_row($var_query_getdepositid);

                $var_data_insert = [
                    'log_type' => $var_data_getrek[3],
                    'log_amount' => $var_data_getrek[1],
                    'deposit_id' => $var_data_getdepositid[0]
                ];

                $var_insert = insert($var_con, "sp_saldo_log", $var_data_insert);

                notification($var_con, [
                    'from' => $var_userid,
                    'to' => $var_get_phones[1],
                    'description' => 'Deposit '. $value .' gagal ditambahkan.',
                    'type' => 'konfirmasi',
                ]);
                sendPhone($var_con, ['phone' => $var_get_phone[0], 'message' => 'Topup anda telah divalidasi, masuk sistem untuk detail histori.']);
            }
        }

        break;
    }
?>

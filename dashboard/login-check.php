<?php

require_once '../core/init.php';

// Menerima nilai dari form markup
$var_username = isset($_POST['frm_username']) ? mysqli_real_escape_string($var_con, validateSecurity($_POST['frm_username'])) : "";
$var_email = isset($_POST['frm_username']) ? mysqli_real_escape_string($var_con, validateSecurity($_POST['frm_username'])) : "";
$var_password = isset($_POST['frm_password']) ? mysqli_real_escape_string($var_con, validateSecurity($_POST['frm_password'])) : "";
$var_remember = isset($_POST['frm_remember']) ? mysqli_real_escape_string($var_con, validateSecurity($_POST['frm_remember'])) : "";
$var_init = true;

//jika tombol login ditekan
if (isset($_POST['btn_login'])) {

    //validasi username atau email
    if (trim(empty($var_username))) {
        $var_init = false;
        setNotif(['warning' => 'mohon isikan nama pengguna yang valid.']);
        routeUrl('login');


        if (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $var_email)) {
            $var_init = false;
            setNotif(['warning' => 'mohon isikan alamat surel yang valid.']);
            routeUrl('login');
        }
    }

    //validasi email
    if (trim(empty($var_email))) {
        $var_init = false;
        setNotif(['warning' => 'mohon isikan nama pangguna atau email dan kata sandi anda.']);
        routeUrl('login');
    }

    //validasi password
    if (trim(empty($var_password))) {
        $var_init = false;
        setNotif(['warning' => 'mohon isikan nama pangguna atau email dan kata sandi anda.']);
        routeUrl('login');
    }

    //jika lolos validasi
    if ($var_init = true) {
        if ((!trim(empty($var_username)) or !trim(empty($var_email))) and !trim(empty($var_password))) {
            //ambil data dari tabel user dan customer
            $var_sql_user = "
            SELECT u.user_id, u.user_username, u.user_password, u.user_email, u.user_level, u.user_status, l.level_name
            FROM sp_user u
            JOIN sp_level l ON l.level_id = u.user_level
            WHERE (u.user_username='{$var_username}' OR u.user_email = '{$var_username}') AND u.user_status = 'active'
            LIMIT 1
            ";
            $var_query_user = mysqli_query($var_con, $var_sql_user);
            $var_data_user = mysqli_fetch_array($var_query_user);

            // jika password tidak sesuai dengan database
            $var_passwd_check = password_verify($var_password, $var_data_user['user_password']);

            //jika data ditemukan
            if (mysqli_num_rows($var_query_user) > 0) {
                if ($var_passwd_check) {
                    // cek level username
                    if ($var_data_user['user_status'] === 'active') {
                        switch ($var_data_user['userlevel']) {
                            case $var_data_user['userlevel']:
                            // set session untuk username dan level
                            $_SESSION['userid'] = $var_data_user['user_id'];
                            $_SESSION['username'] = $var_data_user['user_username'];
                            $_SESSION['levelid'] = $var_data_user['user_level'];
                            $_SESSION['levelname'] = $var_data_user['level_name'];
                            $_SESSION['userLoggedin_time'] = time();

                            if (isLoginSessionExpired()) {
                                routeUrl('login');
                            }

                            if (!trim(empty($var_remember))) {
                                setcookie("user_login", $var_username, time()+ (10 * 365 * 24 * 60 * 60));
                            } else {
                                if (isset($_COOKIE["user_login"]) && isset($_COOKIE["password_login"])) {
                                    setcookie("user_login", "");
                                }
                            }
                            // arahkan ke dashboard untuk user selain member dan agen
                            routeUrl('index.php?p=home-'. trim(strtolower($var_data_user['level_name'])));
                            break;
                        }
                    } else {
                        setNotif(['info' => 'akun anda telah dinonaktikan. silahkan hubungi admin.']);
                        routeUrl('login');
                    }
                } else {
                    setNotif(['warning' => 'mohon isikan kata sandi yang valid.']);
                    routeUrl('login');
                }
            } else {
                setNotif(['warning' => 'akun anda tidak tersedia dalam gudang data kami.']);
                routeUrl('login');
            }
        }

        // bebaskan memory
        mysqli_free_result($var_query_user);

        // tutup koneksi database
        mysqli_close($var_con);
    }
}

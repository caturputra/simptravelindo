<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Modal untuk memasukkan harga tiap pesanan
*/

require_once '../core/init.php';
require_once '../inc/function/inc-func-users.php';

$var_order_id = isset($_GET['orderid']) ? filter_var($_GET['orderid'], FILTER_SANITIZE_STRING) :"";

?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Validasi : <?php echo $var_order_id ?></h4>
        </div>
        <form action="booking-approval-process.php?s=approved" method="post" class="form-horizontal">
            <div class="modal-body">
                <?php
                $var_sql_approval = "
                SELECT a.armada_regnumber, a.armada_image, at.type_seat as armada_seat ,am.model_name as armada_brand, ot.od_from, ot.od_to, l.level_name, concat(u.user_detail_firstname, ' ' ,u.user_detail_lastname) as member, CASE ot.od_service WHEN '1' THEN 'City Tour' WHEN '2' THEN 'Transfer In/Out' END as service
                FROM sp_order_transaction ot
                JOIN sp_model_type mt ON mt.model_id = ot.model_id
                JOIN sp_armada_model am ON am.model_id = mt.model_id
                JOIN sp_armada_type at ON at.type_id = am.type_id
                JOIN sp_armada a ON a.armada_regnumber = ot.armada_regnumber
                JOIN sp_user_detail u ON u.user_id = ot.user_id
                JOIN sp_user us ON us.user_id = ot.user_id
                JOIN sp_level l ON l.level_id = us.user_level
                WHERE ot.od_id = '{$var_order_id}'
                LIMIT 1
                ";
                $var_query_approval = mysqli_query($var_con, $var_sql_approval);
                while ($var_data_approval = mysqli_fetch_array($var_query_approval)) :
                    ?>
                    <input type="hidden" name="frm_aid" value="<?= $var_data_approval['armada_regnumber'] ?>">
                    <input type="hidden" name="frm_datefrom" value="<?= $var_data_approval['od_from'] ?>">
                    <input type="hidden" name="frm_dateto" value="<?= $var_data_approval['od_to'] ?>">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="portlet light bordered">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <h5 class="text-center bold"><?php echo $var_data_approval['armada_brand'] ?> (<?= $var_data_approval['armada_seat']?> Kursi)</h5>
                                            </div>
                                        </div>
                                        <div class="portlet-body form">
                                            <img class="img img-responsive" alt="<?php echo $var_data_approval['armada_regnumber']; ?>" src="../images/armada/<?php echo $var_data_approval['armada_image']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /armada -->

                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="note note-info">
                                        <p class="text-justify text-info">Masukan harga untuk pesanan <strong><?= $var_order_id ?></strong>, sistem akan memberikan pemberitahuan kepada member bahwa pesanan telah dikonfirmasi.</p>
                                    </div>
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <h5 class="text-center bold">Detail Pemesanan</h5>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <style media="screen">
                                            .head-left-data {
                                                font-weight: 600;
                                            }
                                        </style>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td class="head-left-data">Pemesan</td>
                                                        <td><?= $var_data_approval['member'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="head-left-data">Tipe</td>
                                                        <td><?= $var_data_approval['level_name'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="head-left-data">Berangkat</td>
                                                        <td><?= dateFormat($var_data_approval['od_from']) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="head-left-data">Pulang</td>
                                                        <td><?= dateFormat($var_data_approval['od_to']) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="head-left-data">Layanan</td>
                                                        <td><?= $var_data_approval['service'] ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <th class="text-center">Tipe</th>
                                                <th class="text-center">6 Jam (IDR)</th>
                                                <th class="text-center">12 Jam (IDR)</th>
                                                <th class="text-center">24 Jam (IDR)</th>
                                            </thead>
                                            <tr>
                                                <td>City car</td>
                                                <td class="text-right">300.000</td>
                                                <td class="text-right">475.000</td>
                                                <td class="text-right">560.000</td>
                                            </tr>
                                            <tr>
                                                <td>Minibus</td>
                                                <td class="text-right">350.000</td>
                                                <td class="text-right">475.000</td>
                                                <td class="text-right">675.000</td>
                                            </tr>
                                            <tr>
                                                <td>Microbus</td>
                                                <td class="text-right">550.000</td>
                                                <td class="text-right">675.000</td>
                                                <td class="text-right">1.000.000</td>
                                            </tr>
                                            <tr>
                                                <td>Bus</td>
                                                <td class="text-right">675.000</td>
                                                <td class="text-right">800.000</td>
                                                <td class="text-right">1.200.000</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="clearfix"></div> -->
                                    <input type="hidden" name="frm_orderid" value="<?php echo $var_order_id; ?>">
                                    <div class="form-group">
                                        <label for="frm_approval" class="control-label col-sm-3">Harga <span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="frm_approval" id="frm_approval" pattern="^[0-9]{1,10}$" min="0" maxlength="11" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /update-selling -->
                    </div>
                <?php endwhile; ?>
                <div class="modal-footer">
                    <div class="margiv-top-10 col-sm-offset-2">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                        <button type="submit" class="btn green" name="btn_order_approve"><i class="fa fa-check"></i> Konfirmasi</button>
                    </div>
                </div>
            </form>
            <!-- /.form-horizontal -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->

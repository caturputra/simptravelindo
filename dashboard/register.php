<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk form register
*/

$var_title="Register";

require_once '../core/init.php';

$head_component = array(
    'logincss' => '../assets/pages/css/login-3.min.css',
);

require_once 'inc-template/inc-template-header.php';
?>
<style media="screen">
    body {
        background-image:url(../assets/pages/img/login/bg3.jpg);
        background-position: center;
        background-size: cover;
    }
</style>
<body class="login">
    <!-- BEGIN LOGO -->
    <div class="logo" style="width: 125px;">
        <a href="login.php">
            <img src="../images/logo-travelindo.png" class="img img-responsive img-logo">
        </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <div class="content">
        <p class="login-box-msg">Registrasi akun baru</p>
        <!-- MESSAGE -->
        <div class="row">
            <div class="col-sm-12">
                <?php if (isset($_SESSION['notif']['success'])) :  ?>
                    <div class="alert alert-success"><p><i class="fa fa-check"></i> Sukses<br> <?php echo $_SESSION['notif']['success'] ?></p></div>
                    <?php unsetNotif() ?>
                <?php elseif (isset($_SESSION['notif']['info'])) : ?>
                    <div class="alert alert-info"><p><i class="fa fa-info"></i> Info<br> <?php echo $_SESSION['notif']['info'] ?></p></div>
                    <?php unsetNotif() ?>
                <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
                    <div class="alert alert-warning"><p><i class="fa fa-exclamation"></i> Peringatan<br> <?php echo $_SESSION['notif']['warning'] ?></p></div>
                    <?php unsetNotif() ?>
                <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
                    <div class="alert alert-danger"><p><i class="fa fa-ban"></i> Bahaya<br> <?php echo $_SESSION['notif']['danger'] ?></p></div>
                    <?php unsetNotif() ?>
                <?php endif; ?>
            </div>
        </div>
        <!-- END MESSAGE -->

        <form action="register-process.php" method="post">
            <div class="form-group">
                <div class="input-icon">
                    <i class="fa fa-user"></i>
                    <input type="text" class="form-control" placeholder="Nama Pengguna" name="frm_username" autofocus required pattern="[a-z]{6,32}.*" title="Panjang karakter 6 - 32 karakter, hanya boleh menggunakan huruf kecil" maxlength="32" min="6">
                </div>
            </div>
            <div class="form-group">
                <div class="input-icon">
                    <i class="fa fa-envelope"></i>
                    <input type="email" class="form-control" placeholder="Alamat surel" name="frm_email" required>
                </div>
            </div>
            <div class="form-group">
                <div class="input-icon input-group">
                    <i class="fa fa-lock"></i>
                    <input type="password" class="form-control password" placeholder="Kata sandi" name="frm_password" id="frm_password" required pattern="(?=^.{8,32}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*" title="Kata sandi harus kombinasi minimal 1 huruf besar, 1 huruf kecil, dan 1 angka. Panjang 8 - 32 Karakter" maxlength="32" min="8">
                    <span class="input-group-btn">
                        <button type="button" name="button" class="btn default visible-password"><i class="fa fa-eye"></i></button>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-icon input-group">
                    <i class="fa fa-repeat"></i>
                    <input type="password" class="form-control password-confirm" placeholder="Konfirmasi kata sandi" name="frm_confirm_password" id="frm_confirm_password" required>
                    <span class="input-group-btn">
                        <button type="button" name="button" class="btn default visible-password-confirm"><i class="fa fa-eye"></i></button>
                    </span>
                </div>
            </div>
            <!-- <div class="form-group">
                <div class="input-icon">
                    <i class="fa fa-key"></i>
                    <select class="form-control" name="frm_role">
                        <option value="">Pilih peran</option>
                        <option value="4">Member</option>
                        <option value="5">Agen</option>
                    </select>
                </div>
            </div> -->
            <div class="form-group">
                <div class="g-recaptcha" data-sitekey="6Lfy3CkUAAAAADoyBTd4-t27Wy90DPoGpTrKDwhC"></div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-sm-8">
                        <label class="rememberme mt-checkbox mt-checkbox-outline">
                            <input type="checkbox" name="frm_term"> Setuju dengan <a href="#" onclick="openKebijakan()">syarat dan ketentuan</a>
                            <span></span>
                        </label>
                    </div>
                    <div class="col-sm-4">
                        <button type="submit" class="btn blue pull-right" name="btn_register"><i class="fa fa-sign-in"></i> Daftar</button>
                    </div>
                </div>
            </div>
        </form>

        <div class="forget-password">
            <p class="text-center">Sudah mempunyai akun?<a href="login.php"> Masuk!</a></p>
        </div>
    </div>
    <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- BEGIN MODAL -->
<div class="kebijakan-modal">
    <div id="ModalKebijakan" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Syarat dan Ketentuan</h4>
                </div>
                <div class="modal-body">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFirst" aria-expanded="true" aria-controls="collapseFirst">
                                        Pendahuluan
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFirst" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <li>
                                            Terima kasih atas kunjungan Anda ke Website Kami, www.tiket.com. Kami berharap agar kunjungan Anda dapat bermanfaat dan memberi kenyamanan dalam mengakses dan menggunakan seluruh Layanan yang tersedia di Website Kami. Kami terus menerus berupaya memperbaiki dan meningkatan mutu pelayanan Kami, dan sangat menghargai segala kritik, saran dan masukan dari Anda; Silakan Anda menyampaikannya ke Kami melalu admin@travelindo.com atau telepon di +62 274 511 100.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Pemesanan
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <li>Pemesanan untuk keberangkatan di akhir pekan (sabtu dan minggu) harus dilakukan pemesanan seminggu sebelumnya.</li>
                                        <li>Pemesanan akhir pekan minimal 2 hari.</li>
                                        <li>Pemesanan untuk <em>High Season</em> liburan dan/atau cuti hari-hari perayaan (hari libur nasional) harus dilakukan minimal <strong>sebulan</strong> sebelumnya.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Pembayaran
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <ul>
                                        <li>
                                            Pelunasan atas harga pembelian merupakan syarat untuk melakukan pembelian. Kami menerima pembayaran dengan sistem pembayaran menggunakan kartu kredit (VISA, Master Card dan BCA Card), transfer antar rekening serta antar bank ke rekening travelindo.com.
                                        </li>
                                        <li>
                                            Atas setiap pemesanan yang dapat Kami konfirmasi, Kami akan mengirim Anda Pemberitahuan Konfirmasi via sms.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        Pembatalan
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <ul>
                                        <li>
                                            Kecuali secara tegas dinyatakan lain dalam Syarat dan Ketentuan Penggunaan tiket.com ini, semua pembelian Produk di travelindo.com tidak dapat diubah, dibatalkan, dikembalikan uang, ditukar atau dialihkan ke orang/pihak lain.
                                        </li>
                                        <li>
                                            Pembatalan dapat dilakukan seminggu sebelumnya dari jadwal keberangkatan pada pemesanan anda.
                                            <ul>
                                                <li>
                                                    Apabila pesanan anda dilakukan pembayaran/pelunasan dan dibatalkan kurang seminggu sebelumnya atau lebih maka anda melakukan klaim pembayaran dengan datang langsung ke kantor travelindo.com.
                                                </li>
                                                <li>
                                                    Apabila pesanan dibatalkan kurang dari seminggu maka pembayaran sejumlah 30 persen tidak dapat diklaim (hangus).
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            Walaupun sangat kecil kemungkinan Kami membatalkan atau mengubah pemesanan yang sudah Kami Konfirmasi, namun jika diperlukan, Kami akan memberitahu Anda secepat mungkin.
                                        </li>
                                        <li>
                                            Kami tidak bertanggung-jawab ataupun menanggung kerugian Anda dalam hal Kami tidak dapat menyerahkan Layanan kepada Anda, akibat dari hal-hal yang terjadi akibat keadaan memaksa atau yang diluar kekuasaan Kami seperti, tapi tidak terbatas pada: perang, kerusuhan, teroris, perselisihan industrial, tindakan pemerintah, bencana alam, kebakaran atau banjir, cuaca ekstrim, dan lain sebagainya.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- END MODAL -->

<!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script>
<script src="../assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script>

<script>
function openKebijakan() {
    $('#ModalKebijakan').modal('show', {
        backdrop: 'static'
    });
}

$(document).ready(function() {
    var password = document.getElementById("frm_password")
    , confirm_password = document.getElementById("frm_confirm_password");

    function validatePassword(){
        if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Password Doesn't Match");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    $('.visible-password').hover(function () {
       $('.password').attr('type', 'text');
    }, function () {
       $('.password').attr('type', 'password');
    });

    $('.visible-password-confirm').hover(function () {
       $('.password-confirm').attr('type', 'text');
    }, function () {
       $('.password-confirm').attr('type', 'password');
    });

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
});
</script>

</body>
</html>

<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk melakukan konfirmasi penolakan terhadap pesanan
*/

require_once '../core/init.php';
require_once '../inc/function/inc-func-users.php';

$var_order_id = isset($_GET['orderid']) ? filter_var($_GET['orderid'], FILTER_SANITIZE_STRING) :"";

?>
<div class="modal-dialog modal-xs">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Penolakan : <?php echo $var_order_id ?></h4>
        </div>
        <form action="booking-approval-process.php?s=rejected" method="post" class="form-horizontal">
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <input type="hidden" name="frm_orderid" value="<?php echo $var_order_id; ?>">
                        <div class="form-group">
                            <label for="" class="control-label col-sm-2">Alasan <span class="required">*</span></label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="frm_reject" rows="4" cols="40" placeholder="Alasan Penolakan" required></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-body -->
            <div class="modal-footer">
                <div class="margiv-top-10 col-sm-offset-2">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                    <button type="submit" class="btn green" name="btn_order_reject"><i class="fa fa-ban"></i> Konfirmasi</button>
                </div>
            </div>
        </form>
        <!-- /.form-horizontal -->
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->

<?php
require_once '../core/init.php';
require_once '../inc/helper/Mail.php';

$var_title = "Pemesanan Armada";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
    'scroll' => 'https://cdn.datatables.net/scroller/1.4.4/css/scroller.dataTables.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Daftar Pesanan</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="table-responsive">
                        <div class="col-sm-12 col-xs-12 col-lg-12">
                            <table class="table table-hover table-striped table-bordered" id="tableListOrder">
                                <thead>
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">Pemesan</th>
                                        <th class="text-center">Tanggal Pesan</th>
                                        <th class="text-center">Armada</th>
                                        <th class="text-center">No. Plat</th>
                                        <th class="text-center">Berangkat-Pulang</th>
                                        <th class="text-center">Tipe</th>
                                        <!-- <th class="text-center">Permintaan</th> -->
                                        <th class="text-center"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    //menampilkan order
                                    $var_sql_order = "
                                    SELECT ot.od_id, otd.armada_regnumber, ot.od_orderdate, ot.od_invoice, ot.od_service, ot.od_selling, ot.od_from, ot.od_to, ot.od_request, ot.od_status, ot.od_confirmedby, concat(u.user_detail_firstname, ' ' ,u.user_detail_lastname) as member, u.user_detail_phone, am.model_name,  datediff(ot.od_from, CURRENT_TIMESTAMP()) as date_dispute
                                    FROM sp_order_transaction ot
                                    JOIN sp_order_transaction_detail otd ON otd.od_id = ot.od_id
                                    JOIN sp_user_detail u ON u.user_id = ot.user_id
                                    JOIN sp_armada_model am ON am.model_id = otd.model_id
                                    JOIN sp_armada_type at ON at.type_id = am.type_id
                                    ORDER BY ot.od_id DESC
                                    ";
                                    $var_query_order = mysqli_query($var_con, $var_sql_order);
                                    $num = (int) 1;
                                    while ($var_data_order = mysqli_fetch_array($var_query_order)) :
                                        ?>
                                        <tr>
                                            <td class="text-center">
                                                <?php echo $num++; ?>
                                            </td>
                                            <td class="text-center">
                                                <?php echo $var_data_order['member']; ?>
                                            </td>
                                            <td class="text-center">
                                                <?php echo dateFormat($var_data_order['od_orderdate']) ?>
                                            </td>
                                            <td class="text-center">
                                                <?php echo $var_data_order['model_name'] ?>
                                            </td>
                                            <td class="text-center">
                                                <?php echo isset($var_data_order['armada_regnumber']) ? $var_data_order['armada_regnumber'] : 'Belum diset' ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo dateFormat($var_data_order['od_from'])?> - <?php echo dateFormat($var_data_order['od_to']); ?>
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                switch ($var_data_order['od_service']) {
                                                    case '1':
                                                    echo ucwords("city tour");
                                                    break;

                                                    case '2':
                                                    echo ucwords("transfer in/out");
                                                    break;

                                                    case '3':
                                                    echo ucwords("tour package");
                                                    break;
                                                }
                                                ?>
                                            </td>
                                            <td class="text-center">
                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Detail Pemesanan" class="btn btn-sm blue-madison open_modal_detail" id="<?php echo $var_data_order['od_id']; ?>"><i class="fa fa-eye"></i> </a>

                                                <?php switch ($var_data_order['od_status']) : case '1': ?>

                                                <a href="print-report.php?act=to&amp;id=<?php echo base64_encode($var_data_order['od_id']); ?>" data-toggle="tooltip" data-placement="bottom" title="Print Transport Order" class="btn btn-sm green-meadow"><i class="fa fa-print"></i> </a>

                                                <?php break; case '3': ?>
                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Setujui" class="btn btn-sm blue open_modal" id="<?php echo $var_data_order['od_id']; ?>"><i class="fa fa-check"></i> </a>

                                                <?php break; case '4': ?>
                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Batalkan" class="btn btn-sm red" onclick="confirm_modal('booking-approval-process.php?s=canceled&amp;id=<?php echo base64_encode($var_data_order['od_id']); ?>');"><i class="fa fa-close"></i> </a>

                                                <?php if ($var_data_order['date_dispute'] <= '3' && $var_data_order['armada_regnumber'] == null): ?>
                                                    <a href="armada-checkin.php?id=<?php echo $var_data_order['od_id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Check In" class="btn btn-sm purple"><i class="fa fa-clock-o"></i> </a>
                                                <?php endif; ?>

                                                <a href="print-report.php?act=to&amp;id=<?php echo base64_encode($var_data_order['od_id']); ?>" data-toggle="tooltip" data-placement="bottom" title="Print Transport Order" class="btn btn-sm green-meadow"><i class="fa fa-print"></i> </a>
                                                <?php break; endswitch; ?>
                                            </td>
                                        </tr>
                                    <?php endwhile; ?>
                                </tbody>
                            </table>
                            <!-- /.tableListOrder -->
                        </div>
                        <!-- /.col-sm-12 -->
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.porlet-body -->
        </div>
        <!-- /.portlet -->
    </div>
    <!-- /.col-sm-6 -->
</div>
<!-- END CONTENT -->

<!-- modal untuk persetujuan data -->
<div class="approve-modal">
    <div id="ModalApprove" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">

    </div>
    <!-- /.modal -->
</div>
<!-- /.approve-modal -->

<!-- modal untuk penolakan data -->
<div class="reject-modal">
    <div id="ModalReject" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">

    </div>
    <!-- /.modal -->
</div>
<!-- /.reject-modal -->

<!-- modal untuk hapus data -->
<div class="delete-modal">
    <div id="ModalDelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Yakin membatalkan pesanan?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
                    <a href="#" class="btn red" id="delete_link">Batal</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.hapus-modal -->

<!-- modal untuk view detail data -->
<div class="detail-modal">
    <div id="ModalDetail" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    </div>
    <!-- /.modal -->
</div>
<!-- /.view detail-modal -->
<?php
$footer_component = [
    'datatableglobalscript' => '../assets/global/scripts/datatable.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'datableplugin' => '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
    'datatblescroll' => 'https://cdn.datatables.net/scroller/1.4.4/js/dataTables.scroller.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
require_once 'inc-template/inc-template-footer.php';
?>
<script type="text/javascript">
function confirm_modal(delete_url) {
    $('#ModalDelete').modal('show', {
        backdrop: 'static'
    });
    document.getElementById('delete_link').setAttribute('href', delete_url);
}

$(document).ready(function() {
    $(".open_modal").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "booking-approval.php",
            type: "GET",
            data: {
                orderid: id,
            },
            success: function(ajaxData) {
                $("#ModalApprove").html(ajaxData);
                $("#ModalApprove").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $(".open_modal_reject").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "booking-rejected.php",
            type: "GET",
            data: {
                orderid: id,
            },
            success: function(ajaxData) {
                $("#ModalReject").html(ajaxData);
                $("#ModalReject").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $(".open_modal_detail").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "booking-detail.php",
            type: "GET",
            data: {
                orderid: id,
            },
            success: function(ajaxData) {
                $("#ModalDetail").html(ajaxData);
                $("#ModalDetail").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $("#tableListOrder").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "deferRender": true,
        "scroller": true,
        "scrollX": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        },
    });
});
</script>

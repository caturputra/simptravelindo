<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Pengecekan armada untuk setiap penjadwalan setiap keberangkatan
*/

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';

$var_title = "Monitor Kondisi Armada";

$head_component = [
    'blog_style' => '../assets/pages/css/blog.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN PAGE BASE CONTENT -->
<div class="blog-page blog-content-1">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <?php
                $var_userid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "";

                /* UPDATE ON 25/11/2017 by Caturputra */
                $var_sql_checkdata = "
                SELECT acd.acd_id, acd.schedule_id, acd.condition_id
                FROM sp_armada_conditiononday acd
                GROUP BY acd.schedule_id
                ";
                $var_query_checkdata = mysqli_query($var_con, $var_sql_checkdata);
                $var_numrows_checkdata = mysqli_num_rows($var_query_checkdata);
                $var_data_checkdate = mysqli_fetch_row($var_query_checkdata);

                //if ($var_data_checkdate[2] == null) {
                    $var_sql_armada = "
                    SELECT acd.acd_id, acd.schedule_id, acd.condition_id, a.armada_regnumber, a.armada_image, am.model_name as armada_brand, ac.user_id, ac.condition_checkdate, ac.condition_meter, IF(ac.condition_meter > 70, 'BAIK', 'BURUK') as condition_status
                    FROM sp_armada_conditiononday acd
                    LEFT JOIN sp_armada_condition ac ON ac.condition_id = acd.condition_id
                    LEFT JOIN sp_schedule s ON s.schedule_id = acd.schedule_id
                    LEFT JOIN sp_order_transaction ot ON ot.od_id = s.od_id
                    LEFT JOIN sp_armada a ON a.armada_regnumber = ot.armada_regnumber
                    JOIN sp_armada_model am ON am.model_id = ot.model_id
                    GROUP BY acd.schedule_id
                    ";
                //} else {
                //     $var_sql_armada = "
                //     SELECT acd.acd_id, acd.schedule_id, acd.condition_id, a.armada_regnumber, a.armada_image, concat(a.armada_brand, ' ', a.armada_typevariant) as armada_brand, ac.user_id, ac.condition_checkdate, ac.condition_meter, IF(ac.condition_meter > 70, 'BAIK', 'BURUK') as condition_status
                //     FROM sp_armada_conditiononday acd
                //     LEFT JOIN sp_armada_condition ac ON ac.condition_id = acd.condition_id
                //     LEFT JOIN sp_schedule s ON s.schedule_id = acd.schedule_id
                //     LEFT JOIN sp_order_transaction ot ON ot.od_id = s.od_id
                //     LEFT JOIN sp_armada a ON a.armada_regnumber = ot.armada_regnumber
                //     WHERE condition_checkdate = (SELECT MAX(condition_checkdate) FROM sp_armada_condition)
                //     GROUP BY acd.schedule_id
                //     ";
                // }
                // $var_sql_armada = "
                // SELECT acd.acd_id, acd.schedule_id, acd.condition_id, a.armada_regnumber, a.armada_image, concat(a.armada_brand, ' ', a.armada_typevariant) as armada_brand, ac.user_id, ac.condition_checkdate, ac.condition_meter, IF(ac.condition_meter > 70, 'BAIK', 'BURUK') as condition_status
                // FROM sp_armada_conditiononday acd
                // LEFT JOIN sp_armada_condition ac ON ac.condition_id = acd.condition_id
                // LEFT JOIN sp_schedule s ON s.schedule_id = acd.schedule_id
                // LEFT JOIN sp_order_transaction ot ON ot.od_id = s.od_id
                // LEFT JOIN sp_armada a ON a.armada_regnumber = ot.armada_regnumber
                // WHERE condition_checkdate = (SELECT MAX(condition_checkdate) FROM sp_armada_condition)
                // GROUP BY acd.schedule_id
                // ";

                $var_query_armada = mysqli_query($var_con, $var_sql_armada);
                $var_numrows_armada = mysqli_num_rows($var_query_armada);
                ?>
                <?php if ($var_numrows_armada > 0) : ?>
                    <?php while ($var_data_armada = mysqli_fetch_array($var_query_armada)): ?>
                        <div class="col-sm-6 col-xs-12">
                            <div class="blog-post-sm bordered blog-container">
                                <div class="blog-img-thumb">
                                    <div class="mt-element-ribbon bg-grey-steel">
                                        <div class="ribbon ribbon-right ribbon-shadow ribbon-color-primary uppercase">
                                            <?= $var_data_armada['condition_status'] ?> (<?= $var_data_armada['condition_meter'] ?> %)
                                        </div>
                                    </div>
                                    <a href="javascript:;">
                                        <img src="../images/armada/<?php echo $var_data_armada['armada_image']; ?>" class="img img-responsive bg-blue-oleo" alt="<?php echo $var_data_armada['armada_regnumber'] ?>" />
                                    </a>
                                </div>
                                <div class="blog-post-content">
                                    <h2 class="blog-title blog-post-title">
                                        <a href="javascript:;"><?php echo ucwords($var_data_armada['armada_brand']); ?></a>
                                        <div class="pull-right">
                                            <a href="index.php?p=armada-condition-check.php&amp;id=<?= base64_encode($var_data_armada['armada_regnumber']) ?>&amp;sid=<?= base64_encode($var_data_armada['schedule_id']) ?>&amp;act=day" class="btn yellow-crusta"><i class="fa fa-edit"> Cek Armada</i> </a>
                                        </div>
                                    </h2>
                                    <p class="blog-post-desc">
                                        Cheker: <?php
                                        if ($var_data_armada['user_id'] == null) {
                                            echo "Belum ada pengecekan oleh petugas.";
                                        } else {
                                            $var_sql_cheker = "SELECT concat(ud.user_detail_firstname, ' ', ud.user_detail_lastname) as cheker_name FROM sp_user_detail ud WHERE user_id = '" . $var_data_armada['user_id'] . "' LIMIT 1";
                                            $var_query_cheker = mysqli_query($var_con, $var_sql_cheker);
                                            $var_data_cheker = mysqli_fetch_row($var_query_cheker);
                                            echo ucwords($var_data_cheker[0]);
                                        } ?>
                                    </p>
                                    <div class="blog-post-foot">
                                        <div class="blog-post-meta">
                                            <i class="icon-calendar font-blue"></i>
                                            <a href="javascript:;"><?php echo $var_data_armada['condition_checkdate'] ?></a>
                                        </div>
                                        <div class="blog-post-meta">
                                            <i class="icon-bubble font-blue"></i>
                                            <a href="javascript:;"><?php //echo $var_data_armada[''] ?></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.blog-post-content -->
                            </div>
                            <!-- /.blog-container -->
                        </div>
                        <!-- /.col-sm-6 -->
                    <?php endwhile; ?>
                <?php else: ?>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="note note-info">
                            <p>Tidak ada armada yang sudah dicek kondisinya</p>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- END PAGE BASE CONTENT -->

<?php require_once 'inc-template/inc-template-footer.php'; ?>

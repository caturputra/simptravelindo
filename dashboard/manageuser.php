<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk CRUD data pengguna
*/

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';
require_once '../inc/function/inc-func-users.php';

$var_action = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var($_GET['act'], FILTER_SANITIZE_STRING)) : "";

switch (strtolower($var_action)) {
    case 'add':
    // jika button add ditekan
    if (isset($_POST['btn_add'])) {
        // deklarasi variabel untuk nilai form
        $var_firstname = isset($_POST['frm_firstname']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_firstname'], FILTER_SANITIZE_STRING)) : "" ;
        $var_lastname = isset($_POST['frm_lastname']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_lastname'], FILTER_SANITIZE_STRING)) : "" ;
        $var_username = isset($_POST['frm_username']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_username'], FILTER_SANITIZE_STRING)) : "" ;
        $var_password = isset($_POST['frm_password']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_password'], FILTER_SANITIZE_STRING)) : "" ;
        $var_confirm_password = isset($_POST['frm_confirm_password']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_confirm_password'], FILTER_SANITIZE_STRING)) : "" ;
        $var_role = isset($_POST['frm_role']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_role'], FILTER_SANITIZE_STRING)) : "" ;
        $var_email = isset($_POST['frm_email']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_email'], FILTER_SANITIZE_EMAIL)) : "" ;
        $var_status = isset($_POST['frm_status']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_status'], FILTER_SANITIZE_STRING)) : "" ;
        $var_address = isset($_POST['frm_address']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_address'], FILTER_SANITIZE_STRING)) : "" ;
        $var_phone = isset($_POST['frm_phone']) ? mysqli_real_escape_string($var_con, filter_var(trim($_POST['frm_phone']), FILTER_SANITIZE_STRING)) : "" ;
        $var_gender = isset($_POST['frm_gender']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_gender'], FILTER_SANITIZE_STRING)) : "" ;
        $var_birthdate = isset($_POST['frm_birthdate']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_birthdate'], FILTER_SANITIZE_STRING)) : "" ;
        $var_error = array();

        if (empty($var_firstname)) {
            $var_error['firstname'] = "Mohon masukkan nama depan.";
        }

        if (empty($var_lastname)) {
            $var_error['lastname'] = "Mohon masukkan nama belakang.";
        }

        //ambil data
        $var_sql_user = "
        SELECT user_email
        FROM sp_user
        WHERE user_email = '{$var_email}'
        ";
        $var_query_user = mysqli_query($var_con, $var_sql_user);

        //validasi Email
        if (empty($var_email)) {
            $var_error['email'] = "Mohon masukkan alamat surel.";
        }

        if (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $var_email)) {
            $var_error['email'] .= "Mohon masukkan alamat surel yang valid.";
        }

        //validasi email registered
        if (mysqli_num_rows($var_query_user) > 0) {
            $var_error['email'] = "Alamat surel sudah digunakan.";
        }

        $var_sql_user = "
        SELECT user_username
        FROM sp_user
        WHERE user_username = '{$var_username}'
        ";
        $var_query_user = mysqli_query($var_con, $var_sql_user);

        //validasi username
        if (empty($var_username)) {
            $var_error['username'] = "Mohon masukkan nama pengguna.";
        }

        if (strlen($var_username) < 4) {
            $var_error['username'] = "Minimum panjang nama pengguna 4 karakter.";
        }

        //validasi user registered
        if (mysqli_num_rows($var_query_user) > 0) {
            $var_error['username'] = "Nama pengguna sudah digunakan.";
        }

        //validasi password
        if (strlen($var_password) < 4) {
            $var_error['password'] = "Panjang kata sandi minimal 4 karakter.";
        }

        //validasi confirm password
        if ($var_confirm_password !== $var_password || empty($var_confirm_password)) {
            $var_error['passConfirm'] = "Kata sandi tidak sama.";
        }

        //validasi role
        if (empty($var_role)) {
            $var_error['role'] = "Mohon pilih peran pengguna.";
        }

        //validasi Gender
        if (empty($var_gender)) {
            $var_error['gender'] = "Mohon pilih jenis kelamin.";
        }

        //validasi phone number
        if (empty($var_phone)) {
            $var_error['phone'] = "Mohon masukkan telepon.";
        }

        //validasi address
        if (empty($var_address)) {
            $var_error['address'] = "Mohon masukkan alamat.";
        }

        //validasi birthdate
        if (empty($var_birthdate)) {
            $var_error['birthdate'] = "Mohon masukkan tanggal lahir.";
        }

        //$var_ava_err = $_FILES['frm_avatar']['error'];
        //$var_ava_name = $_FILES['frm_avatar']['tmp_name'];
        $var_ava_dir = "../images/ava/";
        $var_ava_type = strtolower(pathinfo(isset($var_ava_name) ? $var_ava_name : "", PATHINFO_EXTENSION));
        $var_ava_ext = array("jpeg", "png", "jpg");
        $var_ava_newName = "img-ava-" . $var_username .".jpg";
        $var_ava_default = "img-ava-default.png";
        $var_init = true;

        if (file_exists($var_ava_dir.$var_ava_default)) {
            $var_ava_newName = "img-ava-" . $var_username .".png";
        }

        if (empty($var_error) && $var_init === true) {
            // set password ke md5 dan sha1
            $var_confirm_password = password_hash($var_password, PASSWORD_BCRYPT);

            // jika semua kondisi terpenuhi
            $var_add =  userAdd($var_con, $var_username, $var_confirm_password, $var_email, $var_role, $var_status);
            if ($var_add) {
                $var_last_userid = mysqli_insert_id($var_con);
                if (userAddDetail($var_con, $var_firstname, $var_lastname, $var_gender, $var_phone, $var_address, $var_last_userid, $var_ava_newName, dateDb($var_birthdate))) {
                    if (copy($var_ava_dir.$var_ava_default, $var_ava_dir.$var_ava_newName)) {
                        setNotif(['success'=>'Penggguna baru berhasil ditambahkan.']);
                        routeUrl('index.php?p=manageuser');
                        // die();
                    } else {
                        setNotif(['warning'=>'pengguna baru gagal ditambahkan.']);
                        routeUrl('index.php?p=manageuser');
                        // die();
                    }
                } else {
                    setNotif(['danger'=>'pengguna baru gagal ditambahkan.']);
                    routeUrl('index.php?p=manageuser');
                    // die();
                }
            } else {
                setNotif(['danger'=>'Galat!']);
                routeUrl('index.php?p=manageuser');
                // die();
            }

            switch ($var_role) {
                case '8':
                $var_autonum = autonumber($var_con, "sp_crew_armada", 'crew_armadaid', 3, "CA");
                $var_data_driver = [
                    'crew_armadaid' => $var_autonum,
                    'crew_status' => '0',
                    'user_id' => $var_last_userid
                ];
                $var_insert_driver = insert($var_con, "sp_crew_armada", $var_data_driver) or die();
                break;

                case '12':
                $var_autonum = autonumber($var_con, "sp_crew_armada", 'crew_armadaid', 3, "CA");
                $var_data_driver = [
                    'crew_armadaid' => $var_autonum,
                    'crew_status' => '0',
                    'user_id' => $var_last_userid
                ];
                $var_insert_driver = insert($var_con, "sp_crew_armada", $var_data_driver) or die();
                break;

                case '5':
                $var_generate_rekening = 'TRV'.date('ymd').strtoupper(randpass(6));
                $var_data_rekening = [
                    'rekening_id' => $var_generate_rekening,
                    'user_id' => $var_last_userid
                ];
                $var_insert_rekening = insert($var_con, "sp_rekening", $var_data_rekening);

                $var_data_deposit = [
                    'rekening_id' => $var_generate_rekening,
                ];
                $var_insert_deposit = insert($var_con, "sp_deposit", $var_data_deposit) or die();
                break;
            }
            die();
        }
    }
    break;

    case 'update':
    // jika button update ditekan
    if (isset($_POST['btn_update'])) {
        // deklarasi variabel untuk nilai form
        $var_userid = isset($_POST['frm_userid']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_userid'], FILTER_SANITIZE_STRING)) : "" ;
        $var_role = isset($_POST['frm_role']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_role'], FILTER_SANITIZE_STRING)) : "" ;
        $var_status = isset($_POST['frm_status']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_status'], FILTER_SANITIZE_STRING)) : "" ;
        $var_error = [];

        //validasi role
        if (empty($var_role)) {
            $var_error['role'] = "Mohon pilih peran pengguna.";
        }

        if (empty($var_error)) {
            // jika semua kondisi terpenuhi
            $var_update =  userUpdate($var_con, $var_userid, $var_role, $var_status);
            if ($var_update) {
                setNotif(['success' => 'data pengguna berhasil diubah.']);
                routeUrl('index.php?p=manageuser');
                die();
                $var_error = [];
            } else {
                setNotif(['danger' => 'Data pengguna gagal diubah.']);
                routeUrl('index.php?p=manageuser');
                die();
            }
        } else {
            setNotif(['danger' => 'Galat!']);
            routeUrl('index.php?p=manageuser');
            die();
        }
    }
    break;

    case 'delete':
    $var_user_id = isset($_GET['id']) ? (int) mysqli_real_escape_string($var_con, $_GET['id']):"";

    $var_sql_delete = "SELECT user_detail_avatar FROM sp_user_detail WHERE user_id='{$var_user_id}'";
    $var_query_delete = mysqli_query($var_con, $var_sql_delete);
    $var_data_delete = mysqli_fetch_row($var_query_delete);
    $var_ava_dir = "../images/ava/" . $var_data_delete[0];
    $var_ava_name = $var_data_delete[0];

    if ($var_data_delete[0] == $var_ava_name) {
        if (file_exists($var_ava_dir)) {
            unlink($var_ava_dir);
            $var_delete = userDelete($var_con, $var_user_id);

            if ($var_delete) {
                setNotif(['success' => 'Data pengguna berhasil dihapus.']);
                routeUrl('index.php?p=manageuser');
                die();
            }
        } else {
            $var_delete = userDelete($var_con, $var_user_id);
            if ($var_delete) {
                setNotif(['success' => 'Data pengguna berhasil dihapus.']);
                routeUrl('index.php?p=manageuser');
                die();
            }
        }
    } else {
        setNotif(['danger' => 'Data pengguna gagal dihapus.']);
        routeUrl('index.php?p=manageuser');
        die();
    }
    break;
}

$var_title = "Manajemen Pengguna";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- BEGIN TAB PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-user-follow font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Tambah Pengguna</span>
                </div>
            </div>
            <div class="portlet-body">
                <form action="index.php?p=manageuser.php&amp;act=add" method="post" enctype="multipart/form-data" id="form_user_adds">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="frm_email" class=" control-label">Surel <span class="required">*</span></label>
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="email" class="form-control" id="frm_email" name="frm_email" placeholder="Alamat surel" value="<?php if(isset($var_email)) { echo $var_email; }?>">
                                    <span class="help-block warning-text"><?php if (!empty($var_error['email'])) { echo $var_error['email'];} ?></span>
                                </div>
                            </div>
                            <!-- /form-group -->

                            <div class="form-group">
                                <label for="frm_username" class=" control-label">Nama pengguna <span class="required">*</span></label>
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" id="frm_username" name="frm_username" placeholder="Nama pengguna" value="<?php if(isset($var_username)) { echo $var_username; }?>" pattern="[a-z]{4, 32}.*?" title="pengguna (Min 4 Max 32 Karakter)">
                                    <span class="help-block warning-text"><?php if (!empty($var_error['username'])) { echo $var_error['username'];} ?></span>
                                </div>
                            </div>
                            <!-- /form-group -->

                            <div class="form-group">
                                <label for="frm_password" class=" control-label">Kata sandi <span class="required">*</span></label>
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="password" class="form-control" id="frm_password" name="frm_password" placeholder="Kata sandi" value="<?php if(isset($var_password)) { echo $var_password; }?>" pattern="(?=^.{4,32}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*" title="Kata sandi harus kombinasi minimal 1 huruf besar, 1 huruf kecil, dan 1 angka. Panjang 4 - 32 Karakter">
                                    <span class="help-block warning-text"><?php if (!empty($var_error['password'])) { echo $var_error['password'];} ?></span>
                                </div>
                            </div>
                            <!-- /form-group -->

                            <div class="form-group">
                                <label for="frm_confirm_password" class=" control-label">Konfirmasi kata sandi <span class="required">*</span></label>
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="password" class="form-control" id="frm_confirm_password" name="frm_confirm_password" placeholder="Konfirmasi kata sandi" value="<?php if(isset($var_confirm_password)) { echo $var_confirm_password; }?>">
                                    <span class="help-block warning-text"><?php if (!empty($var_error['passConfirm'])) { echo $var_error['passConfirm']; } ?></span>
                                </div>
                            </div>
                            <!-- /form-group -->

                            <div class="form-group">
                                <label for="frm_role" class=" control-label">Peran <span class="required">*</span></label>
                                <select class="form-control select2" name="frm_role" id="frm_role">
                                    <option value="">Pilih peran</option>
                                    <?php
                                    $var_sql = "SELECT level_id, level_name FROM sp_level WHERE level_id != '11' ORDER BY level_name ASC";
                                    $var_Level = mysqli_query($var_con, $var_sql);
                                    while ($var_data = mysqli_fetch_array($var_Level)) :
                                        ?>
                                        <option value="<?php echo $var_data['level_id']; ?>">
                                            <?php echo $var_data['level_name']; ?>
                                        </option>
                                    <?php endwhile; ?>
                                </select>
                                <span class="help-block warning-text"><?php if (!empty($var_error['role'])) { echo $var_error['role']; } ?></span>
                            </div>
                            <!-- /form-group -->

                            <div class="form-group">
                                <label for="frm_status" class=" control-label">Status <span class="required">*</span></label>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="frm_status" id="optionsRadios1" value="active" checked> Aktif
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="frm_status" id="optionsRadios2" value="suspend"> Nonaktif
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.form-group -->
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="frm_firstname" class=" control-label">Nama depan <span class="required">*</span></label>
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" id="frm_firstname" name="frm_firstname" placeholder="Nama depan" value="<?php if(isset($var_firstname)) { echo $var_firstname; }?>">
                                    <span class="help-block warning-text"><?php if (!empty($var_error['firstname'])) { echo $var_error['firstname']; } ?></span>
                                </div>
                            </div>
                            <!-- /form-group -->

                            <div class="form-group">
                                <label for="frm_lastname" class=" control-label">Nama belakang <span class="required">*</span></label>
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" id="frm_lastname" name="frm_lastname" placeholder="Nama belakang" value="<?php if(isset($var_lastname)) { echo $var_lastname; }?>">
                                    <span class="help-block warning-text"><?php if (!empty($var_error['lastname'])) { echo $var_error['lastname']; } ?></span>
                                </div>
                            </div>
                            <!-- /form-group -->

                            <div class="form-group">
                                <label for="frm_birthdate" class=" control-label">Tanggal lahir <span class="required">*</span></label>
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control mask_date2" id="frm_birthdate" name="frm_birthdate" placeholder="Tanggal lahir" value="<?php if(isset($var_birthdate)) { echo $var_birthdate; }?>">
                                    <span class="help-block">e.g. 1 Januari 1999 = 01/01/1999</span>
                                    <span class="help-block warning-text"><?php if (!empty($var_error['birthdate'])) {echo $var_error['birthdate']; } ?></span>
                                </div>
                            </div>
                            <!-- /form-group -->

                            <div class="form-group">
                                <label for="frm_address" class=" control-label">Alamat <span class="required">*</span></label>
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <textarea class="form-control" id="frm_address" name="frm_address" rows="3" cols="40" placeholder="Alamat" maxlength="250"><?php if(isset($var_address)) { echo $var_address; }?></textarea>
                                    <span class="help-block warning-text"><?php if (!empty($var_error['address'])) { echo $var_error['address']; } ?></span>
                                </div>
                            </div>
                            <!-- /form-group -->

                            <div class="form-group">
                                <label for="frm_phone" class=" control-label">Telepon <span class="required">*</span></label>
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control mask_phone" id="frm_phone" name="frm_phone" placeholder="Telepon" value="<?php if(isset($var_phone)) { echo $var_phone; }?>">
                                    <span class="help-block warning-text"><?php if (!empty($var_error['phone'])) { echo $var_error['phone']; } ?></span>
                                </div>
                            </div>
                            <!-- /form-group -->

                            <div class="form-group">
                                <label for="frm_gender" class=" control-label">Jenis kelamin <span class="required">*</span></label>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="frm_gender" id="optionsRadios1" value="M" checked> Laki-laki
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="frm_gender" id="optionsRadios2" value="F"> Perempuan
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.col-sm-6 -->
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pull-right">
                                <div class="form-group">
                                    <button type="reset" name="btn_cancel" class="btn  btn-outline dark">Reset</button>
                                    <button type="submit" name="btn_add" class="btn green"><i class="fa fa-plus-square"> Simpan</i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /form add account  -->
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.portlet-body -->
</div>
<!-- /.portlet -->

<!-- List user -->
<div class="row">
    <div class="col-sm-12">
        <!-- BEGIN TAB PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-share font-green"></i>
                    <span class="caption-subject font-green bold uppercase">Daftar akun</span>
                </div>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="table-responsive">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-striped table-hover" id="tableListUser">
                                <thead>
                                    <th class="text-center" style="width: 10px;">No.</th>
                                    <th class="text-center">Nama pengguna</th>
                                    <th class="text-center">Surel</th>
                                    <th class="text-center">Peran</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center"></th>
                                </thead>
                                <tbody>

                                    <?php
                                    $query = "
                                    SELECT u.user_id, u.user_username, u.user_email, l.level_name, u.user_status
                                    FROM sp_user u JOIN sp_level l ON l.level_id = u.user_level
                                    WHERE u.user_id != '{$_SESSION['userid']}'
                                    ORDER BY u.user_username, l.level_id ASC
                                    ";

                                    $dataUser = mysqli_query($var_con, $query);
                                    $no= (int) 1;

                                    while ($row = mysqli_fetch_array($dataUser)) :
                                        ?>
                                        <tr>
                                            <td class="text-center">
                                                <?php echo $no++; ?>
                                            </td>
                                            <td>
                                                <?php echo $row['user_username']; ?>
                                            </td>
                                            <td>
                                                <?php echo $row['user_email']; ?>
                                            </td>
                                            <td>
                                                <?php echo $row['level_name']; ?>
                                            </td>
                                            <td class="text-center">
                                                <?php switch($row['user_status']) : case 'active' : ?>
                                                <label class="label label-default label-sm bg-dark">Aktif</label>
                                                <?php break; case 'suspend' : ?>
                                                <label class="label label-default label-sm bg-dark">Tidak Aktif</label>
                                                <?php break; endswitch; ?>
                                            </td>
                                            <td class="text-center">
                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Detail Pengguna" class="btn blue btn-sm open_modal_detail" id="<?php echo  $row['user_id']; ?>"><i class="fa fa-eye"></i></a>

                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Ubah" class="btn yellow btn-sm open_modal" id="<?php echo  $row['user_id']; ?>"><i class="fa fa-edit"></i></a>

                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Hapus" class="btn red btn-sm" onclick="confirm_modal('index.php?p=manageuser.php&amp;act=delete&amp;id=<?php echo $row['user_id']; ?>');"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    <?php endwhile; ?>
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.portlet-body -->
        </div>
        <!-- /.portlet -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<!-- END CONTENT -->

<!-- modal lihat detail data -->
<div class="detail-modal">
    <div id="ModalDetail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Detail Pengguna</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center"></h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.detail-modal -->

<!-- modal untuk edit data -->
<div class="update-modal">
    <div id="ModalUpdate" class="modal bs-modal-lg fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    </div>
    <!-- /.modal -->
</div>
<!-- /.update-modal -->

<!-- modal untuk hapus data -->
<div class="delete-modal">
    <div id="ModalDelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Yakin menghapus pengguna ini?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
                    <a href="#" class="btn red" id="delete_link">Hapus</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.hapus-modal -->


<?php
$footer_component = [
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'sample' => '../assets/custom/form-validation-user-add.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];

require_once 'inc-template/inc-template-footer.php';
?>

<script type="text/javascript">
function confirm_modal(delete_url) {
    $('#ModalDelete').modal('show', {
        backdrop: 'static'
    });
    document.getElementById('delete_link').setAttribute('href', delete_url);
}

$(document).ready(function() {
    $(".open_modal").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "manageuser-update.php",
            type: "GET",
            data: {
                id: id,
            },
            success: function(ajaxData) {
                $("#ModalUpdate").html(ajaxData);
                $("#ModalUpdate").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $(".open_modal_detail").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "manageuser-detail.php",
            type: "GET",
            data: {
                id: id,
            },
            success: function(ajaxData) {
                $("#ModalDetail").html(ajaxData);
                $("#ModalDetail").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $("#tableListUser").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autowidth": true,
        "lengthChange": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        }
    });
});
</script>

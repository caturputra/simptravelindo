<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk melakukan konfirmasi pembayaran
*/

$var_title = "Pembayaran";

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';

//retrive session user_id who login
// $var_usid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "" ;

//proses payment
$var_order_id = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : "" ;
$var_invoice_url = isset($_GET['invoice']) ? mysqli_escape_string($var_con, filter_var($_GET['invoice'], FILTER_SANITIZE_STRING)) : "" ;
$_SESSION['temp']['oid'] = base64_decode($var_order_id);
$_SESSION['temp']['inv'] = base64_decode($var_invoice_url);

$var_invoice = isset($_POST['frm_invoice']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_invoice'], FILTER_SANITIZE_STRING)) : "" ;
$var_date = isset($_POST['frm_date']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_date'], FILTER_SANITIZE_STRING)) : "" ;
$var_amount = isset($_POST['frm_amount']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_amount'], FILTER_SANITIZE_NUMBER_INT)) : "" ;
$var_senderid = isset($_POST['frm_noregfrom']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_noregfrom'], FILTER_SANITIZE_NUMBER_INT)) : "";
$var_sendername = isset($_POST['frm_noregfroman']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_noregfroman'], FILTER_SANITIZE_STRING)) : "" ;
//$var_bank = isset($_POST['frm_bank']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_bank'], FILTER_SANITIZE_STRING)) : "" ;
$var_receipt = isset($_POST['frm_noregto']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_noregto'], FILTER_SANITIZE_NUMBER_INT)) : "" ;
$var_error = [];

// if (empty($_SESSION['temp']['oid']) && empty($_SESSION['temp']['inv'])) {
//     routeUrl('index.php?p=member-orders');
// }

if (isset($_POST['btn_payment'])) {
    if (empty(trim($var_date))) {
        $var_error['err']['date'] = 'Mohon masukkan tanggal bayar.';
    }

    if (empty(trim($var_senderid))) {
        $var_error['err']['sender'] = "Mohon masukkan nomor rekening pengirim.";
    }

    if (!is_numeric($var_senderid)) {
        $var_error['err']['sender'] = "nomor rekening harus dalam format angka.";
    }

    if (empty(trim($var_sendername))) {
        $var_error['err']['senderan'] = "Mohon masukkan nama pengirim.";
    }

    // if (empty(trim($var_bank))) {
    //     $var_error['err']['bank'] = "Mohon pilih bank penerima.";
    // }

    if (empty(trim($var_receipt))) {
        $var_error['err']['bank'] = "Mohon pilih bank penerima.";
    }

    if (empty(trim($var_amount))) {
        $var_error['err']['amount'] = 'Mohon masukkan nominal bayar.';
    }

    $var_sql_getid = "
    SELECT ot.od_id, ot.od_selling, p.payment_balance, p.payment_lackofpay, s.schedule_id
    FROM sp_payment p
    LEFT JOIN sp_schedule s ON p.schedule_id = s.schedule_id
    LEFT JOIN sp_order_transaction ot ON ot.od_id = s.od_id
    WHERE ot.od_invoice = '{$var_invoice}' AND p.payment_confirmdate =
    (
        SELECT MAX(payment_confirmdate)
        FROM sp_payment
        GROUP BY p.schedule_id
        )
        ";
        $var_query_getid = mysqli_query($var_con, $var_sql_getid);
        $var_data_getid = mysqli_fetch_row($var_query_getid);

        if (mysqli_num_rows($var_query_getid) == 0) {
            $var_sql_getid = "
            SELECT ot.od_id, ot.od_selling, s.schedule_id
            FROM sp_order_transaction ot
            LEFT JOIN sp_schedule s ON s.od_id = ot.od_id
            WHERE ot.od_invoice = '{$var_invoice}'
            ";
            $var_query_getid = mysqli_query($var_con, $var_sql_getid);
            $var_data_getid = mysqli_fetch_row($var_query_getid);

            //hitung minimal bayar sesuai total penjualan
            $var_minpayment = (float) $_ENV['PAYMENT_MIN'] * $var_data_getid[1];

            //validasi amount harus minimal 30% dari total tagihan
            if ($var_amount < $var_minpayment) {
                $var_error['err']['amount'] = 'Pembayaran minimal IDR '. number_format($var_minpayment, 2, ',','.') .'.';
            } else {
                $var_count_lackofpay = (int) $var_data_getid[1] - $var_amount;

                if ($var_amount > $var_data_getid[1]) {
                    $var_error['err']['amount'] = 'Total tagihan anda IDR' . number_format($var_data_getid[1], 2 ,',','.') .'.';
                } else if ($var_count_lackofpay > 0 && $var_count_lackofpay <= $var_data_getid[1]) {
                    $var_lackofpay = $var_count_lackofpay;
                    $var_type = '0'; //uangmuka
                } else if ($var_count_lackofpay === 0) {
                    $var_lackofpay .= 0;
                    $var_type = '1'; //lunas
                }
            }

            //image process
            $var_receipt_name = addslashes($_FILES['frm_img_receipt']['name']);
            $var_receipt_tmpname = addslashes($_FILES['frm_img_receipt']['tmp_name']);
            $var_receipt_err =$_FILES['frm_img_receipt']['error'];
            $var_receipt_size = $_FILES['frm_img_receipt']['size'];
            $var_receipt_dir = "../images/receipt/";
            $var_receipt_type = strtolower(pathinfo(isset($var_receipt_name) ? $var_receipt_name : "", PATHINFO_EXTENSION));
            $var_receipt_ext = array("jpeg", "png", "jpg");
            $var_receipt_newName = strtoupper("img-receipt-" . $var_invoice )."-1.jpg";

            if (empty($var_receipt_name)) {
                $var_error['err']['receipt'] = "Mohon masukkan bukti transfer.";
            }

            $check = getimagesize($var_receipt_tmpname);
            if ($check === false) {
                $var_error['err']['receipt']  = 'Bukti transfer harus berformat .jpg, .png, .jpeg.';
            }

            if (!in_array($var_receipt_type, $var_receipt_ext)) {
                $var_error['err']['receipt']  = 'Bukti transfer harus berformat .jpg, .png, .jpeg.';
            }

            if (empty($var_error)) {
                $var_date = explode("-", trim($var_date));
                $var_xdate = explode("/", trim($var_date[0]));
                $var_xdate = $var_xdate[2]."-".$var_xdate[1]."-".$var_xdate[0];

                $var_xtime = trim($var_date[1]);

                $var_date = $var_xdate .  " " . $var_xtime. ":00";

                if (move_uploaded_file($var_receipt_tmpname, $var_receipt_dir.$var_receipt_newName)) {
                    $var_insert_payment = [
                        'payment_date' => $var_date,
                        'payment_accountfrom' => $var_senderid,
                        'payment_accountname' => $var_sendername,
                        'payment_accountto' => $var_receipt,
                        'payment_confirmdate' => date('Y-m-d H:i:s'),
                        'payment_balance' => $var_amount,
                        'payment_lackofpay' => $var_lackofpay,
                        'payment_howto' => 0,
                        'payment_type' => $var_type,
                        'payment_receipt' => $var_receipt_newName,
                        'schedule_id' => $var_data_getid[2]
                    ];

                    $var_insert_process = insert($var_con, "sp_payment", $var_insert_payment);

                    if ($var_insert_process) {
                        setNotif(['success' => 'Pembayaran berhasil diproses, mohon tunggu proses validasi.']);
                        routeUrl('index.php?p=booking-list-payment');
                        unset($_SESSION['temp']);
                        die();
                    } else {
                        setNotif(['danger' => 'pembayaran gagal diproses.']);
                        routeUrl('index.php?p=member-orders');
                        die();
                    }
                } else {
                    setNotif(['danger' => 'galat! pembayaran gagal diproses.']);
                    routeUrl('index.php?p=member-orders');
                    die();
                }
            }
        } else {
            if ($var_data_getid[3] == 0 ) {
                setNotif(['info' => 'Pembayaran untuk ' . $var_invoice . ' telah dilunasi']);
                routeUrl('index.php?p=booking-payment-cash');
                die();
            }

            $var_minpayment = $var_data_getid[3];

            //validasi amount harus minimal 30% dari total tagihan
            if ($var_amount < $var_minpayment) {
                $var_error['err']['amount'] = 'Pembayaran minimal IDR '. number_format($var_minpayment, 2, ',','.') .'.';
            } else {
                $var_count_lackofpay = (int) $var_data_getid[3] - $var_amount;

                if ($var_amount > $var_data_getid[3]) {
                    $var_error['err']['amount'] = 'Total tagihan anda IDR' . number_format($var_data_getid[3], 2, ',','.') .'.';
                } else if ($var_count_lackofpay > 0 && $var_count_lackofpay <= $var_data_getid[3]) {
                    $var_lackofpay = $var_count_lackofpay;
                    $var_type = '0'; //uangmuka
                } else if ($var_count_lackofpay === 0) {
                    $var_lackofpay .= 0;
                    $var_type = '1'; //lunas
                }
            }

            //image process
            $var_receipt_name = addslashes($_FILES['frm_img_receipt']['name']);
            $var_receipt_tmpname = addslashes($_FILES['frm_img_receipt']['tmp_name']);
            $var_receipt_err =$_FILES['frm_img_receipt']['error'];
            $var_receipt_size = $_FILES['frm_img_receipt']['size'];
            $var_receipt_dir = "../images/receipt/";
            $var_receipt_type = strtolower(pathinfo(isset($var_receipt_name) ? $var_receipt_name : "", PATHINFO_EXTENSION));
            $var_receipt_ext = array("jpeg", "png", "jpg");
            $var_receipt_newName = strtoupper("img-receipt-" . $var_invoice )."-2.jpg";

            if (empty($var_receipt_name)) {
                $var_error['err']['receipt'] = "Mohon masukkan bukti transfer.";
            }

            $check = getimagesize($var_receipt_tmpname);
            if ($check === false) {
                $var_error['err']['receipt']  = 'Bukti transfer harus berformat .jpg, .png, .jpeg.';
            }

            if (!in_array($var_receipt_type, $var_receipt_ext)) {
                $var_error['err']['receipt']  = 'Bukti transfer harus berformat .jpg, .png, .jpeg.';
            }

            if (empty($var_error)) {
                $var_date = explode("-", trim($var_date));
                $var_xdate = explode("/", trim($var_date[0]));
                $var_xdate = $var_xdate[2]."-".$var_xdate[1]."-".$var_xdate[0];

                $var_xtime = trim($var_date[1]);

                $var_date = $var_xdate .  " " . $var_xtime. ":00";

                if (move_uploaded_file($var_receipt_tmpname, $var_receipt_dir.$var_receipt_newName)) {
                    $var_insert_payment = [
                        'payment_date' => $var_date,
                        'payment_accountfrom' => $var_senderid,
                        'payment_accountname' => $var_sendername,
                        'payment_accountto' => $var_receipt,
                        'payment_confirmdate' => date('Y-m-d H:i:s'),
                        'payment_balance' => $var_amount,
                        'payment_lackofpay' => $var_lackofpay,
                        'payment_howto' => 0,
                        'payment_type' => $var_type,
                        'payment_receipt' => $var_receipt_newName,
                        'schedule_id' => $var_data_getid[4]
                    ];

                    $var_insert_process = insert($var_con, "sp_payment", $var_insert_payment);

                    if ($var_insert_process) {
                        setNotif(['success' => 'Pembayaran berhasil diproses, mohon tunggu proses validasi.']);
                        routeUrl('index.php?p=booking-list-payment');
                        unset($_SESSION['temp']);
                        die();
                    } else {
                        setNotif(['danger' => 'pembayaran gagal diproses.']);
                        routeUrl('index.php?p=member-orders');
                        die();
                    }
                } else {
                    setNotif(['danger' => 'galat! pembayaran gagal diproses.']);
                    routeUrl('index.php?p=member-orders');
                    die();
                }
            }
        }

        if (empty($var_receipt_name)) {
            $var_error['err']['receipt'] = "Mohon masukkan bukti transfer.";
        }

        $check = getimagesize($var_receipt_tmpname);
        if ($check === false) {
            $var_error['err']['receipt']  = 'Bukti transfer harus berformat .jpg, .png, .jpeg.';
        }

        if (!in_array($var_receipt_type, $var_receipt_ext)) {
            $var_error['err']['receipt']  = 'Bukti transfer harus berformat .jpg, .png, .jpeg.';
        }

        if (empty($var_error)) {
            $var_date = explode("-", trim($var_date));
            $var_xdate = explode("/", trim($var_date[0]));
            $var_xdate = $var_xdate[2]."-".$var_xdate[1]."-".$var_xdate[0];

            $var_xtime = trim($var_date[1]);

            $var_date = $var_xdate .  " " . $var_xtime. ":00";

            if (move_uploaded_file($var_receipt_tmpname, $var_receipt_dir.$var_receipt_newName)) {
                $var_insert_payment = [
                    'payment_date' => $var_date,
                    'payment_accountfrom' => $var_senderid,
                    'payment_accountname' => $var_sendername,
                    'payment_accountto' => $var_receipt,
                    'payment_confirmdate' => date('Y-m-d H:i:s'),
                    'payment_balance' => $var_amount,
                    'payment_lackofpay' => $var_lackofpay,
                    'payment_howto' => 0,
                    'payment_type' => $var_type,
                    'payment_receipt' => $var_receipt_newName,
                    'schedule_id' => $var_data_getid[4]
                ];

                $var_insert_process = insert($var_con, "sp_payment", $var_insert_payment);

                if ($var_insert_process) {
                    setNotif(['success' => 'Pembayaran berhasil diproses, mohon tunggu proses validasi.']);
                    routeUrl('index.php?p=booking-list-payment');
                    unset($_SESSION['temp']);
                    die();
                } else {
                    setNotif(['danger' => 'pembayaran gagal diproses.']);
                    routeUrl('index.php?p=member-orders');
                    die();
                }
            } else {
                setNotif(['danger' => 'galat! pembayaran gagal diproses.']);
                routeUrl('index.php?p=member-orders');
                die();
            }
        }
    }
    $head_component = [
        'select2' => '../assets/global/plugins/select2/css/select2.min.css',
        'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
        'datepickercss'=>'../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
        'datetimepickercss' => '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'
    ];

    require_once 'inc-template/inc-template-header.php';
    require_once 'inc-template/inc-template-main-header.php';
    ?>
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active"><?php echo $var_title; ?></span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->

    <!-- MESSAGE -->
    <div class="row">
        <div class="col-sm-12">
            <?php if (isset($_SESSION['notif']['success'])) :  ?>
                <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
                <?php unsetNotif() ?>
            <?php elseif (isset($_SESSION['notif']['info'])) : ?>
                <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
                <?php unsetNotif() ?>
            <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
                <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
                <?php unsetNotif() ?>
            <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
                <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
                <?php unsetNotif() ?>
            <?php endif; ?>
        </div>
    </div>
    <!-- END MESSAGE -->

    <?php
    $var_sql_order = "
    SELECT ot.od_id, otd.armada_regnumber, ot.od_orderdate, ot.od_invoice, ot.od_service, ot.od_selling, ot.od_from, ot.od_to, ot.od_request, ot.od_status, ot.od_confirmedby, u.user_detail_firstname, u.user_detail_lastname, u.user_detail_phone, am.model_name as armada_brand
    FROM sp_order_transaction ot
    JOIN sp_order_transaction_detail otd ON otd.od_id = ot.od_id
    JOIN sp_armada_model am ON am.model_id = otd.model_id
    JOIN sp_user_detail u ON u.user_id = ot.user_id
    WHERE ot.od_invoice = '" . $_SESSION['temp']['inv'] . "'
    LIMIT 1
    ";

    $var_query_order = mysqli_query($var_con, $var_sql_order);
    $var_numrows_order = mysqli_num_rows($var_query_order);
    $var_data_order = mysqli_fetch_array($var_query_order);
    ?>

    <?php switch ($_SESSION['levelid']): case '4': ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-info">
                <p><i class="fa fa-info"> </i> <?php echo 'Halaman ini untuk konfirmasi pembayaran via transfer bank.' ?></p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase">Detail Pesanan</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    Tanggal pesan: <?php echo dateFormat($var_data_order['od_orderdate']) ?>
                    <br>
                    Armada: <?php echo $var_data_order['armada_brand'] ?>
                    <br>
                    Tagihan: IDR <?php echo number_format($var_data_order['od_selling'],2 , ',', '.') ?>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-setting font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Pembayaran untuk invoice: <?= $_SESSION['temp']['inv'] ?></span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <form class="form-horizontal" action="index.php?p=booking-payment.php&amp;id=<?= base64_encode($_SESSION['temp']['oid']) ?>&amp;invoice=<?= base64_encode($_SESSION['temp']['inv']) ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="frm_order_id" value="<?php echo $var_order_id ?>">
                        <div class="form-group">
                            <label for="frm_invoice" class="control-label col-sm-4">Invoice <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="frm_invoice" id="frm_invoice" placeholder="<?= $_SESSION['temp']['inv'] ?>" value="<?= $_SESSION['temp']['inv'] ?>" readonly required>
                                <p class="help-block warning-text"><?php if(isset($var_error['err']['invoice'])) { echo $var_error['err']['invoice']; }; ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="frm_date" class="control-label col-sm-4 date-picker">Tanggal bayar <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <div class="input-group date form_datetime">
                                    <input type="text" class="form-control" name="frm_date" id="frm_date" placeholder="" required readonly>
                                    <span class="input-group-btn"><button type="button" class="btn blue date-set"><i class="fa fa-calendar-o"></i></button></span>
                                </div>
                                <p class="help-block warning-text"><?php if(isset($var_error['err']['date'])) { echo  $var_error['err']['date']; }; ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="frm_noregfrom" class="control-label col-sm-4">Pengirim <span class="required">*</span></label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="frm_noregfrom" id="frm_noregfrom" placeholder="Nomor rekening pengirim" required pattern="[0-9]+" title="Nomor rekening hanya boleh angka." maxlength="20">
                                <p class="help-block warning-text"><?php if(isset($var_error['err']['sender'])) { echo $var_error['err']['sender']; }; ?></p>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="frm_noregfroman" id="frm_noregfroman" placeholder="Nama rekening pengirim" required>
                                <p class="help-block warning-text"><?php if(isset($var_error['err']['senderan'])) { echo $var_error['err']['senderan']; }; ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="frm_noregto" class="control-label col-sm-4">Penerima <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <select class="form-control select2" name="frm_noregto" id="frm_noregto" required>
                                    <option value=""></option>
                                    <?php
                                    $var_sql_bank = "
                                    SELECT account_name, account_number, bank_name
                                    FROM sp_bank_account
                                    JOIN sp_bank ON sp_bank.bank_id = sp_bank_account.bank_id
                                    WHERE account_status = '1' ORDER BY account_number
                                    ";
                                    $var_query_bank = mysqli_query($var_con, $var_sql_bank);
                                    $var_numrows_bank = mysqli_num_rows($var_query_bank);
                                    ?>
                                    <?php if ($var_numrows_bank > 0): ?>
                                        <?php while ($var_data_bank = mysqli_fetch_row($var_query_bank)) : ?>
                                            <option value="<?= $var_data_bank[1] ?>"><?= strtoupper($var_data_bank[0]) ?> - <?= $var_data_bank[2] ?></option>
                                        <?php endwhile; ?>
                                    <?php else: ?>
                                        <option value=""></option>
                                    <?php endif; ?>
                                </select>
                                <p class="help-block warning-text"><?php if(isset($var_error['err']['bank'])) { echo $var_error['err']['bank']; }; ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="frm_amount" class="control-label col-sm-4">Nominal <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-btn"><button type="button" class="btn blue">IDR</button></span>
                                    <input type="text" class="form-control mask_number" name="frm_amount" id="frm_amount" placeholder="Nominal" required pattern="^[0-9].*$">
                                </div>
                                <p class="help-block warning-text"><?php if(isset($var_error['err']['amount'])) { echo $var_error['err']['amount']; }; ?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="frm_img_receipt" class="control-label col-sm-4">Bukti transfer <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="file" class="form-control" name="frm_img_receipt" id="frm_img_receipt" placeholder="" required>
                                <p class="help-block warning-text"><?php if(isset($var_error['err']['receipt'])) { echo $var_error['err']['receipt']; }; ?></p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <a href="index.php?p=member-orders.php" class="btn btn-outline dark"><i class="fa fa-chevron-left"></i> Kembali</a>
                                    <button type="submit" class="btn green" name="btn_payment"><i class="fa fa-save"></i> Proses</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- /.portlet-body -->
                </div>
                <!-- /.col-sm-6 -->
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
    <?php break; case '5' ?>
    <div class="row">
        <div class="col-sm-12">
            <?php
            $var_sql_saldo = "
            SELECT d.deposit_balance, d.deposit_id
            FROM sp_deposit d
            JOIN sp_rekening r ON r.rekening_id = d.rekening_id
            WHERE r.user_id = '{$_SESSION['userid']}'
            ";
            $var_query_saldo = mysqli_query($var_con, $var_sql_saldo);
            $var_data_saldo = mysqli_fetch_row($var_query_saldo);
            ?>
            <?php if ($var_data_saldo[0] >= $var_data_order['od_selling']): ?>
                <?php
                $var_sql_schedule = "
                SELECT s.schedule_id, p.payment_lackofpay
                FROM sp_schedule s
                JOIN sp_order_transaction ot ON ot.od_id = s.od_id
                LEFT JOIN sp_payment p ON p.schedule_id = s.schedule_id
                WHERE s.od_id = '{$var_data_order['od_id']}'
                LIMIT 1
                ";
                $var_query_schedule = mysqli_query($var_con, $var_sql_schedule);
                $var_data_schedule = mysqli_fetch_row($var_query_schedule);

                if ($var_data_schedule[1] != 0 || $var_data_schedule[1] == null) {
                    mysqli_begin_transaction($var_con);
                    mysqli_autocommit($var_con, FALSE);
                    $var_insert_payment = [
                        'payment_date' => date('Y-m-d H:i:s'),
                        // 'payment_bank' => $var_bank,
                        // 'payment_accountfrom' => $var_senderid,
                        // 'payment_accountto' => $var_receipt,
                        'payment_confirmdate' => date('Y-m-d H:i:s'),
                        'payment_balance' => $var_data_order['od_selling'],
                        'payment_lackofpay' => 0,
                        'payment_howto' => 3,
                        'payment_type' => 1,
                        // 'payment_receipt' => $var_receipt_newName,
                        // 'user_id' => $var_usid,
                        'schedule_id' => $var_data_schedule[0]
                    ];
                    $var_insert_process = insert($var_con, "sp_payment", $var_insert_payment);

                    $var_data_insert = [
                        'log_type' => 2,
                        'log_amount' => $var_data_order['od_selling'],
                        'deposit_id' => $var_data_saldo[1]
                    ];

                    $var_insert = insert($var_con, "sp_saldo_log", $var_data_insert);

                    if ($var_insert_process && $var_insert) {
                        mysqli_commit($var_con);
                        unset($_SESSION['temp']['oid']);
                        unset($_SESSION['temp']['inv']);
                        ?>
                        <script type="text/javascript">
                        alert("Pembayaran berhasil diproses, mohon tunggu proses validasi.");
                        window.location.href = "index.php?p=member-orders.php";
                        </script>
                        <?php
                    } else {
                        mysqli_rollback($var_con);
                        ?>
                        <script type="text/javascript">
                        alert("Pembayaran berhasil diproses, mohon tunggu proses validasi.");
                        window.location.href = "index.php?p=booking-payment.php";
                        </script>
                        <?php
                    }
                } else {
                    ?>
                    <script type="text/javascript">
                    alert("Pembayaran telah dilunasi.");
                    window.location.href = "index.php?p=member-orders.php";
                    </script>
                    <?php
                }
                ?>
                <?php die(); ?>
            <?php else : ?>
                <div class="well">
                    Saldo ada tidak mencukupi untuk melajutkan proses pembayaran, Mohon <strong>topup</strong> saldo anda.<br>
                    saldo anda saat ini Rp <?php echo number_format($var_data_saldo[0], 2, ',', '.') ?> dan total pembayaran Rp <?php echo number_format($var_data_order['od_selling'], 2, ',', '.') ?><br><br>
                    <a href="index.php?p=agent-deposit.php" class="btn blue"><i class="fa fa-external-link"></i> Topup</a>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <?php endswitch; ?>

    <?php
    $footer_component = [
        'momentdate' => '../assets/global/plugins/moment.js',
        'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
        'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
        'select2 script' => '../assets/pages/scripts/components-select2.min.js',
        'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
        'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
        'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
        'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
        'datepicker' => '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
        'datetimepicker' => '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
        'datescript' => '../assets/pages/scripts/components-date-time-pickers.js',
    ];
    require_once 'inc-template/inc-template-footer.php';
    ?>

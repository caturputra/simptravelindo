<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 07/12/2017
** Ajax untuk update status pada sp_order_transaction jika
** tidak memenuhi aturan berikut:
** tidak konfirmasi bayar > 1 jam untuk pesanan hari ini
** tidak konfirmasi bayar > 2 jam untuk pesanan 3 hari kedepan
** tidak konfirmasi bayar > 4 jam untuk pesanan > 7 hari kedepan
*/

require_once '../core/init.php';

$var_oid = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : "";

$var_sql_id = "
SELECT od_id, od_modifyat, od_from, od_status
FROM sp_order_transaction
WHERE od_id = '{$var_oid}'
";

$var_query_id = mysqli_query($var_con, $var_sql_id);
$var_numrows_id = mysqli_num_rows($var_query_id);
$var_data_id = mysqli_fetch_array($var_query_id);

if ($var_data_id[3] == 1) {
    $time = strtotime($var_data_id[1]);

    //cek tanggal
    $interval1 = date_diff(date_create($var_data_id[1]), date_create(date($var_data_id[2])));
    if ($interval1->format('%d') >= 0 && $interval1->format('%d') < 3 ) {
        $var_comparedate = date('Y-m-d H:i:s', strtotime('+1 hours', $time));

        $interval = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')));
        if ($interval > $var_comparedate) {
            $var_sql_checkpayment = "
            SELECT ot.od_id, ot.od_selling, p.payment_balance, p.payment_lackofpay, s.schedule_id
            FROM sp_payment p
            LEFT JOIN sp_schedule s ON p.schedule_id = s.schedule_id
            LEFT JOIN sp_order_transaction ot ON ot.od_id = s.od_id
            WHERE ot.od_id = '{$var_oid}' AND p.payment_confirmdate =
            (SELECT MAX(payment_confirmdate) FROM sp_payment GROUP BY p.schedule_id)
            ";
            $var_query_checkpayment = mysqli_query($var_con, $var_sql_checkpayment);

            if (mysqli_num_rows($var_query_checkpayment) == 0) {
                $var_update = ['od_status' => '3'];
                $var_cond_update = ['od_id' => $var_data_id[0]];

                if (update($var_con, "sp_order_transaction", $var_update, $var_cond_update)) {
                    return true;
                }
            } else {
                return false;
            }
        }

    } else if ($interval1->format('%d') >= 3 && $interval1->format('%d') < 7) {
        $var_comparedate = date('Y-m-d H:i:s', strtotime('+2 hours', $time));

        $interval = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')));

        if ($interval > $var_comparedate) {
            $var_sql_checkpayment = "
            SELECT ot.od_id, ot.od_selling, p.payment_balance, p.payment_lackofpay, s.schedule_id
            FROM sp_payment p
            LEFT JOIN sp_schedule s ON p.schedule_id = s.schedule_id
            LEFT JOIN sp_order_transaction ot ON ot.od_id = s.od_id
            WHERE ot.od_id = '{$var_oid}' AND p.payment_confirmdate =
            (SELECT MAX(payment_confirmdate) FROM sp_payment GROUP BY p.schedule_id)
            ";
            $var_query_checkpayment = mysqli_query($var_con, $var_sql_checkpayment);

            if (mysqli_num_rows($var_query_checkpayment) == 0) {
                $var_update = ['od_status' => '3'];
                $var_cond_update = ['od_id' => $var_data_id[0]];

                if (update($var_con, "sp_order_transaction", $var_update, $var_cond_update)) {
                    return true;
                }
            } else {
                return false;
            }
        }

    } else if ($interval1->format('%d') >= 7 ) {
        $var_comparedate = date('Y-m-d H:i:s', strtotime('+4 hours', $time));

        $interval = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')));

        if ($interval > $var_comparedate) {
            $var_sql_checkpayment = "
            SELECT ot.od_id, ot.od_selling, p.payment_balance, p.payment_lackofpay, s.schedule_id
            FROM sp_payment p
            LEFT JOIN sp_schedule s ON p.schedule_id = s.schedule_id
            LEFT JOIN sp_order_transaction ot ON ot.od_id = s.od_id
            WHERE ot.od_id = '{$var_oid}' AND p.payment_confirmdate =
            (SELECT MAX(payment_confirmdate) FROM sp_payment GROUP BY p.schedule_id)
            ";
            $var_query_checkpayment = mysqli_query($var_con, $var_sql_checkpayment);

            if (mysqli_num_rows($var_query_checkpayment) == 0) {
                $var_update = ['od_status' => '3'];
                $var_cond_update = ['od_id' => $var_data_id[0]];

                if (update($var_con, "sp_order_transaction", $var_update, $var_cond_update)) {
                    return true;
                }
            } else {
                return false;
            }
        }
    }
} else {
    return false;
}
?>

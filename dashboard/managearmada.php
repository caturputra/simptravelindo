<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman CRUD data master Armada
*/

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';

$var_action = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var($_GET['act'], FILTER_SANITIZE_STRING)) : "";

switch (strtolower($var_action)) {
    case 'add':
    //prosedur add armada
    $var_armada_id = isset($_POST['frm_armada_id']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_armada_id']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_brand = isset($_POST['frm_brand']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_brand']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_variant = isset($_POST['frm_variant']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_variant']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_type = isset($_POST['frm_type']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_type']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_model = isset($_POST['frm_model']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_model']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_color = isset($_POST['frm_color']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_color']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_chassis_number = isset($_POST['frm_chassis_number']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_chassis_number']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_engine_number = isset($_POST['frm_engine_number']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_engine_number']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_year = isset($_POST['frm_prodcution_year']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_prodcution_year']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_seat = isset($_POST['frm_total_seat']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_total_seat']), FILTER_SANITIZE_NUMBER_INT)) : "" ;
    $var_armada_color = isset($_POST['frm_color']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_color']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_fuel_type = isset($_POST['frm_fuel_type']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_fuel_type']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_fuel_size = isset($_POST['frm_fuel_size']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_fuel_size']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_odometer = isset($_POST['frm_odometer']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_odometer']), FILTER_SANITIZE_NUMBER_INT)) : "" ;
    $var_error = array();

    if (isset($_POST['btn_add_armada'])) {

        //die(var_dump($_POST));

        //validasi
        if (empty(trim($var_armada_id))) {
            $var_error['id'] = "Mohon masukkan nomor plat.";
        }

        $var_sql_checkarmada = "SELECT armada_regnumber FROM sp_armada WHERE armada_regnumber = '{$var_armada_id}'";
        $var_query_checkarmada = mysqli_query($var_con, $var_sql_checkarmada);
        $var_numrows_checkarmada = mysqli_num_rows($var_query_checkarmada);
        if ($var_numrows_checkarmada > 0) {
            $var_error['id'] = "Nomor plat ". $var_armada_id ." sudah pernah ditambahkan.";
        }

        if (strlen($var_armada_id) >= 11) {
            $var_error['id'] .= " Panjang maksimum nomor plat 10 karakter.";
        }

        if (empty(trim($var_armada_brand))) {
            $var_error['brand'] = "Mohon masukkan merk armada.";
        }

        if (empty(trim($var_armada_variant))) {
            $var_error['variant'] = "Mohon masukkan varian armada.";
        }

        if (empty(trim($var_armada_type))) {
            $var_error['type'] = "Mohon pilih tipe armada.";
        }

        if (empty(trim($var_armada_model))) {
            $var_error['model'] = "Mohon pilih model armada.";
        }

        if (empty(trim($var_armada_year))) {
            $var_error['production_year'] = "Mohon masukkan tahun produksi.";
        }

        if (empty(trim($var_armada_seat))) {
            $var_error['seat'] = "Mohon masukkan jumlah kursi.";
        }

        if (!is_numeric($var_armada_seat)) {
            $var_error['seat'] .= " Jumlah kursi harus dalam format angka.";
        }

        if (empty(trim($var_armada_color))) {
            $var_error['color'] = "Mohon pilih warna armada.";
        }

        if (empty(trim($var_armada_chassis_number))) {
            $var_error['chassis'] = "Mohon masukkan nomor rangka.";
        }

        if (!is_numeric($var_armada_chassis_number)) {
            $var_error['chassis'] .= " Nomor rangka harus dalam format angka.";
        }

        if (!strlen($var_armada_chassis_number) == 17) {
            $var_error['chassis'] .= " Nomor rangka harus 17 karakter.";
        }

        if (empty(trim($var_armada_engine_number))) {
            $var_error['engine'] = "Mohon masukkan nomor mesin.";
        }

        if (!is_numeric($var_armada_engine_number)) {
            $var_error['engine'] .= " Nomor mesin harus dalam format angka.";
        }

        if (!strlen($var_armada_engine_number) == 7) {
            $var_error['chassis'] .= " Nomor mesin harus 7 karakter.";
        }

        if (empty(trim($var_armada_fuel_type))) {
            $var_error['fuel_type'] = "Mohon pilih tipe bahan bakar.";
        }

        if (empty(trim($var_armada_fuel_size))) {
            $var_error['fuel_size'] = "Mohon masukkan kapasitas bahan bakar.";
        }

        if (!is_numeric($var_armada_fuel_size)) {
            $var_error['fuel_size'] .= " bahan bakar harus dalam format angka.";
        }

        if (empty(trim($var_armada_odometer))) {
            $var_error['odometer'] = "Mohon masukkan odometer.";
        }

        if (!is_numeric($var_armada_odometer)) {
            $var_error['odometer'] .= " Odometer harus dalam format angka.";
        }


        // Mengambil data dari form gambar
        $var_ava_name = addslashes($_FILES['frm_avatar']['name']);
        $var_ava_tmp = addslashes($_FILES['frm_avatar']['tmp_name']);
        $var_ava_size = $_FILES['frm_avatar']['size'];
        $var_ava_err = $_FILES['frm_avatar']['error'];
        $var_ava_dir = "../images/armada/";
        $var_ava_type = strtolower(pathinfo($var_ava_name, PATHINFO_EXTENSION));
        $var_ava_ext = array("jpeg", "png", "jpg");
        $var_ava_newName = strtolower("img-ava-" . $var_armada_id .".jpg");
        $var_init = true;

        // cek apakah sudah memilih gambar?
        if (empty($var_ava_name)) {
            $var_error['ava'] = "Mohon pilih gambar.";
        }

        //jika lolos validasi
        if ($var_ava_err === 0) {
            // cek apakah ukuran file tidak melebihi 700KB (716800 byte)
            if ($var_ava_size > 1024000) {
                setNotif(['warning' => 'Maximum ukuran gambar 1 MB.']);
                $var_init = false;
            }

            // cek apakah yang diupload adalah file gambar
            $check = getimagesize($var_ava_tmp);
            if ($check === false) {
                setNotif(['warning' => 'Hanya gambar berformat .jpeg, .jpg, and .png yang diijinkan.']);
                $var_init = false;
            }
            if (empty($var_error) && $var_init === true) {
                if (in_array($var_ava_type, $var_ava_ext) == true) {
                    $var_data_armada = array(
                        'armada_regnumber' => strtoupper($var_armada_id),
                        'armada_brand' => ucwords($var_armada_brand),
                        'armada_typevariant' => ucwords($var_armada_variant),
                        'armada_type' => $var_armada_type,
                        'armada_model' => $var_armada_model,
                        'armada_productionyear' => $var_armada_year,
                        'armada_chassisnumber' => $var_armada_chassis_number,
                        'armada_enginenumber' => $var_armada_engine_number,
                        'armada_typefuel' => $var_armada_fuel_type,
                        'armada_fuelsize' => $var_armada_fuel_size,
                        'armada_odometer' => $var_armada_odometer,
                        'armada_image' => strtolower($var_ava_newName),
                        'armada_color' => $var_armada_color,
                        'armada_seat' => $var_armada_seat,
                        'armada_status' => 'pending',
                        'armada_createat' => date('Y-m-d H:i:s'),
                        'armada_modifyat' => date('Y-m-d H:i:s'),
                    );

                    $var_data_condition = ['armada_regnumber' => strtoupper($var_armada_id)];
                    if ($compress = compress_image($var_ava_tmp, $var_ava_dir.$var_ava_newName)) {
                        if (move_uploaded_file($var_ava_tmp, $compress)) {
                            mysqli_autocommit($var_con, FALSE);
                            mysqli_begin_transaction($var_con);
                            if(insert($var_con, "sp_armada", $var_data_armada)) {
                                //if (insert($var_con, "sp_armada_condition", $var_data_condition)) {
                                mysqli_commit($var_con);
                                setNotif(['success' => 'Armada baru berhasil ditambahkan.']);
                                routeUrl('index.php?p=managearmada');
                                die();
                                //} else {
                                //     mysqli_rollback($var_con);
                                //     setNotif(['danger' => 'Armada baru gagal ditambahkan.']);
                                //     routeUrl('index.php?p=managearmada');
                                //     die();
                                // }
                            } else {
                                mysqli_rollback($var_con);
                                setNotif(['danger' => 'Armada baru gagal ditambahkan.']);
                                routeUrl('index.php?p=managearmada');
                                die();
                            }
                        } else {
                            mysqli_rollback($var_con);
                            setNotif(['danger' => 'Galat!']);
                            routeUrl('index.php?p=managearmada');
                            die();
                        }
                    }
                }
            }
        }
    }
    break;

    case 'update':
    //prosedur update armada
    $var_armada_id = isset($_POST['frm_armada_id']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_armada_id']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_brand = isset($_POST['frm_brand']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_brand']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_variant = isset($_POST['frm_variant']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_variant']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_type = isset($_POST['frm_type']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_type']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_model = isset($_POST['frm_model']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_model']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_color = isset($_POST['frm_color']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_color']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_chassis_number = isset($_POST['frm_chassis_number']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_chassis_number']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_engine_number = isset($_POST['frm_engine_number']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_engine_number']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_year = isset($_POST['frm_prodcution_year']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_prodcution_year']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_seat = isset($_POST['frm_total_seat']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_total_seat']), FILTER_SANITIZE_NUMBER_INT)) : "" ;
    $var_armada_color = isset($_POST['frm_color']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_color']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_fuel_type = isset($_POST['frm_fuel_type']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_fuel_type']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_fuel_size = isset($_POST['frm_fuel_size']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_fuel_size']), FILTER_SANITIZE_STRING)) : "" ;
    $var_armada_odometer = isset($_POST['frm_odometer']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_odometer']), FILTER_SANITIZE_NUMBER_INT)) : "" ;
    $var_armada_status = isset($_POST['frm_status']) ? mysqli_real_escape_string($var_con, filter_var(validateSecurity($_POST['frm_status']), FILTER_SANITIZE_STRING)) : "" ;
    $var_error = array();

    if (isset($_POST['btn_update_armada'])) {
        //validasi
        if (empty(trim($var_armada_id))) {
            $var_error['id'] = "Mohon masukkan nomor plat.";
        }

        if (strlen($var_armada_id) > 9) {
            $var_error['id'] .= " Maximum nomor plat 10 karakter.";
        }

        if (empty(trim($var_armada_type))) {
            $var_error['type'] = "Mohon masukkan tipe armada.";
        }

        if (empty(trim($var_armada_seat))) {
            $var_error['seat'] = "Mohon masukkan jumlah kursi.";
        }

        if (!is_numeric($var_armada_seat)) {
            $var_error['seat'] .= " Jumlah kursi harus dalam format angka.";
        }

        if (empty(trim($var_armada_color))) {
            $var_error['color'] = "Mohon pilih warna armada.";
        }

        if (empty(trim($var_armada_status))) {
            $var_error['status'] = "Mohon pilih status armada.";
        }

        if (empty($var_error)) {
            $var_data_armada = [
                'armada_brand' => ucwords($var_armada_brand),
                'armada_typevariant' => ucwords($var_armada_variant),
                'armada_type' => $var_armada_type,
                'armada_model' => $var_armada_model,
                'armada_productionyear' => $var_armada_year,
                'armada_chassisnumber' => $var_armada_chassis_number,
                'armada_enginenumber' => $var_armada_engine_number,
                'armada_typefuel' => $var_armada_fuel_type,
                'armada_fuelsize' => $var_armada_fuel_size,
                'armada_odometer' => $var_armada_odometer,
                'armada_color' => $var_armada_color,
                'armada_seat' => $var_armada_seat,
                'armada_status' => $var_armada_status,
                'armada_modifyat' => date('Y-m-d H:i:s'),
            ];

            $var_data_condition = ['armada_regnumber' => $var_armada_id];

            // jika semua kondisi terpenuhi
            $var_update = update($var_con, "sp_armada", $var_data_armada, $var_data_condition);
            if ($var_update) {
                $var_error = array();
                setNotif(['success' => 'Data armada berhasil diubah']);
                routeUrl('index.php?p=managearmada');
                die();
            } else {
                setNotif(['warning' => 'Data armada gagal diubah']);
                routeUrl('index.php?p=managearmada');
                die();
            }
        }
    }
    break;

    case 'delete':
    $var_armada_id = isset($_GET['armada_id']) ? mysqli_escape_string($var_con, filter_var($_GET['armada_id'], FILTER_SANITIZE_STRING)): "";

    $var_sql_delete = "SELECT armada_ava FROM sp_armada WHERE armada_id='{$var_armada_id}'";
    $var_query_delete = mysqli_query($var_con, $var_sql_delete);
    $var_data_delete = mysqli_fetch_row($var_query_delete);
    $var_ava_dir = "../images/" . $var_data_delete[0];
    $var_ava_name = $var_data_delete[0];

    //kkondisi delete
    $var_data_delete_field = array('armada_id' => $var_armada_id);

    if ($var_data_delete[0] == $var_ava_name) {
        if (file_exists($var_ava_dir)) {
            unlink($var_ava_dir);
            $var_delete = delete($var_con, "sp_armada", $var_data_delete_field);
            if ($var_delete) {
                $var_delete_detail = delete($var_con, "sp_armada_detail",$var_data_delete_field);
                setNotif(['success' => 'Data armada berhasil dihapus.']);
                routeUrl('index.php?p=managearmada');
                die();
            }
        } else {
            $var_delete = delete($var_con, "sp_armada", $var_data_delete_field);
            if ($var_delete) {
                $var_delete_detail = delete($var_con, "sp_armada_detail",$var_data_delete_field);
                setNotif(['danger' => 'Data armada gagal dihapus.']);
                routeUrl('index.php?p=managearmada');
                die();
            }
        }
    }
    break;
}

$var_title = "Manajemen Armada";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="index.php?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN CONTENT -->
<?php if ($_SESSION['levelid'] != '9') : ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bus font-blue"></i>
                        <span class="caption-subject font-blue bold uppercase">Tambah Armada Baru</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="index.php?p=managearmada.php&amp;act=add" method="post" enctype="multipart/form-data" id="form_armada_add">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="frm_armada_id" class=" control-label">No. Plat <span class="required">*</span></label>
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="text" class="form-control" id="frm_armada_id" name="frm_armada_id" placeholder="No Plat" value="<?php if (isset($var_armada_id)) { echo $var_armada_id; }?>" autofocus title='contoh: AB XXXX DL' pattern="([A-Z]{1,2})[ s].*?(\d{1,4}).*?[ s]([A-Z]{1,2})">
                                            <span class="help-block warning-text"><?php if (!empty($var_error['id'])) { echo $var_error['id'];
                                            } ?></span>
                                        </div>
                                    </div>
                                    <!-- /form-group -->

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="frm_brand" class=" control-label">Merk <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control" id="frm_brand" name="frm_brand" placeholder="Merk" value="<?php if (isset($var_armada_brand)) { echo $var_armada_brand; }?>" pattern="[A-Z]{1}.*?" title="Travelindo">
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['brand'])) { echo $var_error['brand'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="frm_variant" class=" control-label">Varian <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control" id="frm_variant" name="frm_variant" placeholder="Varian" value="<?php if (isset($var_armada_variant)) { echo $var_armada_variant; }?>" pattern="[A-Z]{1}.*?" title="Travelindo">
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['variant'])) { echo $var_error['variant'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="frm_type" class=" control-label">Tipe <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <select class="form-control select2" name="frm_type" id="frm_type">
                                                        <option value="">Pilih tipe</option>
                                                        <option value="1">Kendaraan Ringan</option>
                                                        <option value="2">Kendaraan Berat</option>
                                                    </select>
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['type'])) { echo $var_error['type'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="frm_model" class=" control-label">Model <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <select class="form-control select2" name="frm_model" id="frm_model">
                                                        <option value="" selected>Pilih Model</option>
                                                        <option value="sedan">Sedan</option>
                                                        <option value="minibus">Minibus</option>
                                                        <option value="microbus">Microbus</option>
                                                        <option value="bus">Bus</option>
                                                        <option value="elf">Elf</option>
                                                    </select>
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['model'])) { echo $var_error['model'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="frm_total_seat" class=" control-label">Jumlah kursi <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control mask_seat" id="frm_total_seat" name="frm_total_seat" placeholder="Jumlah kursi" value="<?php if (isset($var_armada_seat)) { echo $var_armada_seat; }?>" min="0">
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['seat'])) { echo $var_error['seat'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->

                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="frm_color" class=" control-label">Warna <span class="required">*</span></label>
                                                <select class="form-control select2" name="frm_color" id="frm_color">
                                                    <option value="">Pilih Warna</option>
                                                    <option value="Merah">Merah</option>
                                                    <option value="Jingga">Jingga</option>
                                                    <option value="Kuning">Kuning</option>
                                                    <option value="Putih">Putih</option>
                                                    <option value="Biru">Biru</option>
                                                    <option value="Nila">Nila</option>
                                                    <option value="Ungu">Ungu</option>
                                                    <option value="Hitam">Hitam</option>
                                                </select>
                                                <span class="help-block warning-text"><?php if (!empty($var_error['color'])) { echo $var_error['color'];
                                                } ?></span>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                    </div>
                                </div>
                                <!-- /.col-sm-6 -->

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="frm_chassis_number" class=" control-label">Nomor rangka <span class="required">*</span></label>
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control mask_chassis" id="frm_chassis_number" name="frm_chassis_number" placeholder="Nomor rangka" value="<?php if (isset($var_armada_chassis_number)) { echo $var_armada_chassis_number; }?>">
                                            <span class="help-block warning-text"><?php if (!empty($var_error['chassis'])) { echo $var_error['chassis'];
                                            } ?></span>
                                        </div>
                                    </div>
                                    <!-- /form-group -->

                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="frm_prodcution_year" class=" control-label">Tahun <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control mask_year" id="frm_prodcution_year" name="frm_prodcution_year" placeholder="Tahun Produksi" value="<?php if (isset($var_armada_year)) { echo $var_armada_year; }?>">
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['production_year'])) { echo $var_error['production_year'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>

                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                <label for="frm_engine_number" class=" control-label">Nomor Mesin <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control mask_engine" id="frm_engine_number" name="frm_engine_number" placeholder="Nomor Mesin" value="<?php if (isset($var_armada_engine_number)) { echo $var_armada_engine_number; }?>">
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['seat'])) { echo $var_error['seat'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="frm_fuel_type" class=" control-label">Tipe bahan bakar <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <select class="form-control select2" name="frm_fuel_type" id="frm_fuel_type">
                                                        <option value="">Pilih tipe</option>
                                                        <option value="bensin">Bensin</option>
                                                        <option value="solar">Solar</option>
                                                    </select>
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['fuel_type'])) { echo $var_error['fuel_type'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="frm_fuel_size" class=" control-label">Kap. Tangki <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="number" class="form-control" id="frm_fuel_size" name="frm_fuel_size" placeholder="Kapasitas bahan bakar (Liter)" value="<?php if (isset($var_armada_fuel_size)) { echo $var_armada_fuel_size; }?>" maxlength="5">
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['fuel_size'])) { echo $var_error['fuel_size'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="frm_odometer" class=" control-label">Odometer <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control mask_number" id="frm_odometer" name="frm_odometer" placeholder="Odometer (KM)" value="<?php if (isset($var_armada_odometer)) { echo $var_armada_odometer; }?>" maxlength="7">
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['odometer'])) { echo $var_error['odometer'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="frm_avatar" class=" control-label">Gambar <span class="required">*</span></label>
                                        <input type="file" class="form-control" id="frm_avatar" name="frm_avatar" value="">
                                        <span class="help-block warning-text"><?php if (!empty($var_error['ava'])) { echo $var_error['ava'];
                                        } ?></span>
                                        <span class="help-block">Ukuran Maks 2 MB</span>
                                    </div>
                                    <!-- /form-group -->
                                </div>
                                <!-- /.col-sm-6 -->
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pull-right">
                                        <button class="btn btn-outline dark" type="reset">Reset</button>
                                        <button class="btn green" type="submit" name="btn_add_armada"><i class="fa fa-plus-square"></i> Simpan</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </form>
                    </div>
                    <!-- /.portlet-body -->
                </div>
                <!-- /.portlet -->
            </div>
            <!-- /.col-sm-12 -->
        </div>
        <!-- /.row -->
    <?php endif; ?>

    <div class="mt-bootstrap-tables">
        <div class="row">
            <div class="col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-share font-green"></i>
                            <span class="caption-subject font-green bold uppercase">Daftar Armada</span>
                        </div>
                        <div class="actions">
                            <!-- actions print dll -->
                        </div>
                    </div>
                    <div class="portlet-body flip-scroll">
                        <div class="row">
                            <div class="table-responsive">
                                <div class="col-sm-12">
                                    <table id="tableListArmada" class="table table-hover table-striped table-bordered flip-content">
                                        <thead class="flip-content">
                                            <th class="text-center">No.</th>
                                            <th class="text-center">Gambar</th>
                                            <th class="text-center">No. Plat</th>
                                            <th class="text-center">Jenis</th>
                                            <th class="text-center">Status</th>
                                            <?php if ($_SESSION['levelid'] == '9') : ?>
                                                <th class="text-center">Aksi</th>
                                            <?php endif; ?>
                                        </thead>
                                        <tbody>
                                            <?php
                                            //menampilkan Armada
                                            $var_sql_armada = "
                                            SELECT
                                            a.armada_regnumber, am.model_name as armada_brand, a.armada_productionyear, a.armada_chassisnumber, a.armada_enginenumber, am.model_fuel as armada_typefuel, am.model_fuelmax as armada_fuelsize, a.armada_odometer, a.armada_image, a.armada_status, a.armada_createat, a.armada_modifyat, at.type_seat as armada_seat
                                            FROM sp_armada a
                                            JOIN sp_model_type mt ON mt.armada_regnumber = a.armada_regnumber
                                            JOIN sp_armada_model am ON am.model_id = mt.model_id
                                            JOIN sp_armada_type at ON at.type_id = am.type_id
                                            ORDER BY mt.model_id
                                            ";
                                            $var_query_armada = mysqli_query($var_con, $var_sql_armada);
                                            $num = (int) 1;
                                            while ($var_data_armada = mysqli_fetch_array($var_query_armada)) :
                                                ?>
                                                <tr>
                                                    <td class="text-center">
                                                        <?php echo $num++; ?>
                                                    </td>
                                                    <td class="text-center" style="width:100px; height:100px">
                                                        <img src="../images/armada/<?= $var_data_armada['armada_image'] ?>" alt="<?= $var_data_armada['armada_image'] ?>" class="img img-responsive" style="width: 100px">
                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo $var_data_armada['armada_regnumber']; ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo ucwords($var_data_armada['armada_brand']); ?>
                                                    </td>
                                                    <td class="text-center" >
                                                        <?php
                                                        switch ($var_data_armada['armada_status']) {
                                                            case 'approved':
                                                            echo '<span class="label label-success">' . ucwords('disetujui') . '</span>';
                                                            break;

                                                            case 'rejected':
                                                            echo '<span class="label label-danger">' . ucwords('ditolak') . '</span>';
                                                            break;

                                                            default:
                                                            echo '<span class="label label-warning">' . ucwords('ditunda') . '</span>';
                                                            break;
                                                        }
                                                        ?>
                                                    </td>
                                                    <?php if ($_SESSION['levelid'] == '9') : ?>
                                                        <td class="text-center">
                                                            <a href="#" class="open_modal" id="<?php echo $var_data_armada['armada_regnumber']; ?>"><span class="fa fa-edit"></span></a>                                                        |
                                                            <a href="#" onclick="confirm_modal('managearmada-delete.php?armada_id=<?php echo $var_data_armada['armada_regnumber']; ?>');"><span class="fa fa-remove"></span></a>
                                                        </td>
                                                    <?php endif; ?>
                                                </tr>
                                            <?php endwhile; ?>
                                        </tbody>
                                    </table>
                                    <!-- /.tableListArmada -->
                                </div>
                                <!-- /.col-sm-12 -->
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.porlet-body -->
                </div>
                <!-- /.portlet -->
            </div>
            <!-- /.col-sm-6 -->
        </div>
        <!-- /.mt-bootstrap-tables -->
        <!-- END CONTENT -->

        <!-- modal untuk edit data -->
        <div class="update-modal">
            <div id="ModalUpdate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop='static'>

            </div>
            <!-- /.modal -->
        </div>
        <!-- /.update-modal -->

        <!-- modal untuk hapus data -->
        <div class="delete-modal">
            <div id="ModalDelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">Konfirmasi</h4>
                        </div>
                        <div class="modal-body">
                            <h4 class="text-center">Yakin menghapus armada ini?</h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
                            <a href="#" class="btn red" id="delete_link">Hapus</a>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>
        <!-- /.hapus-modal -->

        <?php
        $footer_component = [
            'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
            'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
            'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
            'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
            'select2 script' => '../assets/pages/scripts/components-select2.min.js',
            'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
            'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
            //'sample' => '../assets/custom/form-validation-user-add.js',
            'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
            'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
        ];

        require_once 'inc-template/inc-template-footer.php';
        ?>
        <script type="text/javascript">
        function confirm_modal(delete_url) {
            $('#ModalDelete').modal('show', {
                backdrop: 'static'
            });
            document.getElementById('delete_link').setAttribute('href', delete_url);
        }

        $(document).ready(function() {
            $(".open_modal").click(function(e) {
                var id = $(this).attr("id");
                $.ajax({
                    url: "managearmada-update.php",
                    type: "GET",
                    data: {
                        armada_regnumber: id,
                    },
                    success: function(ajaxData) {
                        $("#ModalUpdate").html(ajaxData);
                        $("#ModalUpdate").modal('show', {
                            backdrop: 'true'
                        });
                    }
                });
            });

            $("#tableListArmada").DataTable({
                "paging": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autowidth": false,
                "lengthChange": true,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
                }
            });
        });
        </script>

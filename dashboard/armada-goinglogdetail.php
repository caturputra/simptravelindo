<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk melihat log perjalanan yang telah selesai
*/

require_once '../core/init.php';

$var_oid = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : "";
$var_oid = base64_decode($var_oid);

$var_title = "Hsitori Perjalanan";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <a href="index.php?p=armada-goinglog.php" class="btn blue"><i class="fa fa-chevron-left"></i> Kembali</a>
    </div>
</div>
<br>
<div class="row">
    <div class="col-sm-6">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-microphone font-green"></i>
                    <span class="caption-subject bold font-green uppercase"> Detail Pemesanan</span>
                </div>
                <div class="actions">

                </div>
            </div>
            <div class="portlet-body">
                <?php
                $var_sql_log = "
                SELECT a.armada_regnumber, a.armada_image, a.armada_seat ,concat(a.armada_brand, ' ', a.armada_typevariant) as armada_brand, ot.od_from, ot.od_to, l.level_name, concat(u.user_detail_firstname, ' ' ,u.user_detail_lastname) as member, CASE ot.od_service WHEN '1' THEN 'City Tour' WHEN '2' THEN 'Transfer In/Out' END as service, g.going_id
                FROM sp_order_transaction ot
                JOIN sp_armada a ON a.armada_regnumber = ot.armada_regnumber
                JOIN sp_user_detail u ON u.user_id = ot.user_id
                JOIN sp_user us ON us.user_id = ot.user_id
                JOIN sp_level l ON l.level_id = us.user_level
                JOIN sp_schedule s ON s.od_id = ot.od_id
                JOIN sp_going g ON g.schedule_id = s.schedule_id
                WHERE ot.od_id = '{$var_oid}'
                LIMIT 1
                ";
                $var_query_log = mysqli_query($var_con, $var_sql_log);
                $var_data_log = mysqli_fetch_array($var_query_log);
                $no = 1;
                ?>
                <style media="screen">
                .head-left-data {
                    font-weight: 600;
                }
                </style>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <tbody>
                            <tr>
                                <td class="head-left-data">Pemesan</td>
                                <td><?= $var_data_log['member'] ?></td>
                            </tr>
                            <tr>
                                <td class="head-left-data">Tipe</td>
                                <td><?= $var_data_log['level_name'] ?></td>
                            </tr>
                            <tr>
                                <td class="head-left-data">Berangkat</td>
                                <td><?= dateFormat($var_data_log['od_from']) ?></td>
                            </tr>
                            <tr>
                                <td class="head-left-data">Pulang</td>
                                <td><?= dateFormat($var_data_log['od_to']) ?></td>
                            </tr>
                            <tr>
                                <td class="head-left-data">Layanan</td>
                                <td><?= $var_data_log['service'] ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-microphone font-green"></i>
                    <span class="caption-subject bold font-green uppercase"> Detail Armada</span>
                </div>
                <div class="actions">

                </div>
            </div>
            <div class="portlet-body">
                <div class="caption">
                    <h5 class="text-center bold"><?php echo $var_data_log['armada_brand'] ?> (<?= $var_data_log['armada_seat']?> Kursi)</h5>
                </div>
                <img class="img img-responsive" alt="<?php echo $var_data_log['armada_regnumber']; ?>" src="../images/armada/<?php echo $var_data_log['armada_image']; ?>" style="width: 12em; margin-left: 33%;">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-microphone font-green"></i>
                    <span class="caption-subject bold font-green uppercase"> Histori Perjalanan</span>
                </div>
                <div class="actions">

                </div>
            </div>
            <div class="portlet-body">
                <?php
                $var_sql_logdetail = "
                SELECT log_id, log_desc, log_status, log_createat, going_id FROM sp_going_log WHERE going_id = '{$var_data_log['going_id']}'
                ";
                $var_query_logdetail = mysqli_query($var_con, $var_sql_logdetail);
                ?>
                <div class="row">
                    <div class="table-responsive">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">Tanggal</th>
                                        <th class="text-left">Deskripsi</th>
                                        <th class="text-left">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (mysqli_num_rows($var_query_logdetail)): ?>
                                        <?php while($var_data_logdetail = mysqli_fetch_array($var_query_logdetail)) : ?>
                                            <tr>
                                                <td class="text-center" style="width: 15em;"><?= $var_data_logdetail['log_createat'] ?></td>
                                                <td class="text-left"><?= ucwords($var_data_logdetail['log_desc']) ?></td>
                                                <td class="text-left"><?= ucwords($var_data_logdetail['log_status']) ?></td>
                                            </tr>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT -->

<?php
$footer_component = [
    'datatableglobalscript' => '../assets/global/scripts/datatable.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'datableplugin' => '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
];
require_once 'inc-template/inc-template-footer.php';
?>
<script>
$(document).ready(function () {
    $("#tableListcrew").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autowidth": false,
        "lengthChange": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        }
    });
});
</script>

<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk menampilkan daftar pesanan yang dibatalkan untuk dikonfirmasi
** apabila pesanan telah melakukan pembayaran atau status definit
*/

require_once '../core/init.php';

$var_id = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : "";
$var_action = isset($_GET['s']) ? mysqli_escape_string($var_con, filter_var($_GET['s'], FILTER_SANITIZE_NUMBER_INT)) : "";
$var_userid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "";

switch ($var_action) {
    case 1:
    $var_data_update = ['canceled_status' => '1'];
    $var_cond_update = ['od_id' => $var_id];
    $var_update = update($var_con, "sp_order_canceled", $var_data_update, $var_cond_update);

    if ($var_update) {
        setNotif(['success' => 'Pembatalan pesanan ' . $var_id . ' berhasil divalidasi.']);
        routeUrl('index.php?p=booking-canceled-list');
        die();
    } else {
        setNotif(['danger' => 'Pembatalan pesanan ' . $var_id . ' gagal divalidasi.']);
        routeUrl('index.php?p=booking-canceled-list');
        die();
    }
    break;

    case 2:
    $var_data_update = ['canceled_status' => '2'];
    $var_cond_update = ['od_id' => $var_id];
    $var_update = update($var_con, "sp_order_canceled", $var_data_update, $var_cond_update);

    if ($var_update) {
        setNotif(['success' => 'Pembatalan pesanan ' . $var_id . ' berhasil divalidasi.']);
        routeUrl('index.php?p=booking-canceled-list');
        die();
    } else {
        setNotif(['danger' => 'Pembatalan pesanan ' . $var_id . ' gagal divalidasi.']);
        routeUrl('index.php?p=booking-canceled-list');
        die();
    }
    break;
}

$var_title = "Pemesanan Armada";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Daftar Pesanan</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="table-responsive">
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped table-bordered" id="tableListOrder">
                                <thead>
                                    <th class="text-center" style="width: 2em;">No.</th>
                                    <th class="text-center">Pemesan</th>
                                    <th class="text-center">Berangkat-Pulang</th>
                                    <th class="text-center">Tipe</th>
                                    <th class="text-center">Alasan</th>
                                    <th class="text-center">Status</th>
                                    <th></th>
                                </thead>
                                <tbody>
                                    <?php
                                    //menampilkan order
                                    $var_sql_order = "
                                    SELECT ot.od_id, ot.armada_regnumber, ot.od_orderdate, ot.od_invoice, ot.od_service, ot.od_selling, ot.od_from, ot.od_to, ot.od_request, ot.od_status, ot.od_confirmedby, u.user_detail_firstname, u.user_detail_lastname, u.user_detail_phone, oc.canceled_status, oc.canceled_reason
                                    FROM sp_order_transaction ot
                                    JOIN sp_user_detail u ON u.user_id = ot.user_id
                                    JOIN sp_order_canceled oc ON oc.od_id = ot.od_id
                                    WHERE canceled_status = 0
                                    ORDER BY ot.od_id DESC
                                    ";
                                    $var_query_order = mysqli_query($var_con, $var_sql_order);
                                    $num = (int) 1;
                                    while ($var_data_order = mysqli_fetch_array($var_query_order)) :
                                        ?>
                                        <tr>
                                            <td class="text-center">
                                                <?php echo $num++; ?>
                                            </td>
                                            <td class="text-center">
                                                <?php echo $var_data_order['user_detail_firstname']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo dateFormat($var_data_order['od_from'])?> - <br> <?php echo dateFormat($var_data_order['od_to']); ?>
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                switch ($var_data_order['od_service']) {
                                                    case '1':
                                                    echo ucwords("city tour");
                                                    break;

                                                    case '2':
                                                    echo ucwords("transfer in/out");
                                                    break;

                                                    case '3':
                                                    echo ucwords("tour package");
                                                    break;
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php echo $var_data_order['canceled_reason'] ?>
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                switch ($var_data_order['canceled_status']) {
                                                    case '0':
                                                    echo '<span class="label label-info label-sm">' . ucwords("ditunda") . '</span>';
                                                    break;

                                                    case '1':
                                                    echo '<span class="label label-success label-sm">' . ucwords("disetujui") . '</span>';
                                                    break;

                                                    case '2':
                                                    echo '<span class="label label-danger label-sm">' . ucwords("ditolak") . '</span>';
                                                    break;

                                                    case '3':
                                                    echo '<span class="label label-warning label-sm">' . ucwords("dibatalkan") . '</span>';
                                                    break;

                                                    case '4':
                                                    echo '<span class="label label-default label-sm">' . ucwords("definit") . '</span>';
                                                    break;
                                                }
                                                ?>
                                            </td>
                                            <td class="text-center">
                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Detail Pemesanan" class="btn btn-sm blue open_modal_detail" id="<?php echo $var_data_order['od_id']; ?>"><i class="fa fa-eye"></i> </a>

                                                <?php switch ($var_data_order['canceled_status']) : case '0': ?>

                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Setujui" class="btn btn-sm green" onclick="confirm_modal_approved('booking-canceled-list.php?s=1&amp;id=<?php echo $var_data_order['od_id']; ?>');"><i class="fa fa-check"></i> </a>
                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Tolak" class="btn btn-sm red" onclick="confirm_modal_rejected('booking-canceled-list.php?s=2&amp;id=<?php echo $var_data_order['od_id']; ?>');"><i class="fa fa-exclamation-triangle"></i> </a>

                                                <?php break; endswitch; ?>
                                            </td>
                                        </tr>
                                    <?php endwhile; ?>
                                </tbody>
                            </table>
                            <!-- /.tableListOrder -->
                        </div>
                        <!-- /.col-sm-12 -->
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.porlet-body -->
        </div>
        <!-- /.portlet -->
    </div>
    <!-- /.col-sm-6 -->
</div>
<!-- END CONTENT -->

<!-- modal untuk approve data -->
<div class="approval-modal">
    <div id="ModalApprove" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Yakin menyetujui pembatalan pesanan ini?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                    <a href="#" class="btn green" id="approve_link"><i class="fa fa-check"></i> Konfirmasi</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.approve-modal -->

<!-- modal untuk reject data -->
<div class="reject-modal">
    <div id="ModalReject" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Yakin tolak ajuan pembatalan pesanan ini?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                    <a href="#" class="btn red" id="reject_link"><i class="fa fa-exclamation-triangle"></i> Konfirmasi</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.reject-modal -->

<!-- modal untuk view detail data -->
<div class="detail-modal">
    <div id="ModalDetail" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    </div>
    <!-- /.modal -->
</div>
<!-- /.view detail-modal -->
<?php
$footer_component = [
    'datatableglobalscript' => '../assets/global/scripts/datatable.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'datableplugin' => '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
require_once 'inc-template/inc-template-footer.php';
?>
<script type="text/javascript">
function confirm_modal_approved(approve_url) {
    $('#ModalApprove').modal('show', {
        backdrop: 'static'
    });
    document.getElementById('approve_link').setAttribute('href', approve_url);
}

function confirm_modal_rejected(reject_url) {
    $('#ModalReject').modal('show', {
        backdrop: 'static'
    });
    document.getElementById('reject_link').setAttribute('href', reject_url);
}

$(document).ready(function() {
    $(".open_modal_detail").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "booking-detail.php",
            type: "GET",
            data: {
                orderid: id,
            },
            success: function(ajaxData) {
                $("#ModalDetail").html(ajaxData);
                $("#ModalDetail").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $("#tableListOrder").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autowidth": false,
        "lengthChange": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        }
    });
});
</script>

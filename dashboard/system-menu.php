<?php

$var_title = "System Menu Management";

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';

//prses add menu
if (isset($_POST['btn_submit_menu'])) {
    $var_name = isset($_POST['frm_name']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_name'], FILTER_VALIDATE_STRING)) : "";
    $var_name = isset($_POST['frm_url']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_url'], FILTER_VALIDATE_STRING)) : "";
    $var_order = isset($_POST['frm_order']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_order'], FILTER_VALIDATE_STRING)) : "";
    $var_name = isset($_POST['frm_name']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_name'], FILTER_VALIDATE_STRING)) : "";
    $var_name = isset($_POST['frm_name']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_name'], FILTER_VALIDATE_STRING)) : "";
}

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';


?>

<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE BASE CONTENT -->
<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->>


    <!-- Proses Tambah -->
    <div class="row">
        <div class="col-sm-5">
            <!-- BEGIN TAB PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <i class="fa fa-navicon font-blue"></i>
                        <span class="caption-subject font-blue bold uppercase">Menu Add</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="system-menu.php" method="post" enctype="multipart/form-data" id="form_menu_add">
                        <div class="form-group">
                          <label for="frm_name" class="control-label">Name</label>
                          <input type="text" class="form-control" id="frm_name" name="frm_name" placeholder="Menu" required autofocus>
                        </div>

                        <div class="form-group">
                          <label for="frm_url" class="control-label">URL</label>
                          <input type="text" class="form-control" id="frm_url" name="frm_url" placeholder="URL" required>
                        </div>

                        <div class="form-group">
                          <label for="frm_order" class="control-label">Order</label>
                          <input type="text" class="form-control" id="frm_order" name="frm_order" placeholder="Order" required>
                        </div>

                        <div class="form-group mt-repeater">
                            <div data-repeater-list="group-a">
                                <div data-repeater-item class="mt-repeater-item">
                                    <div class="row mt-repeater-row">
                                        <div class="col-sm-4">
                                            <label class="control-label">Submenu</label>
                                            <input type="text" placeholder="Name" class="form-control" name="frm_submenu_name"/>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="control-label">URL</label>
                                            <input type="text" placeholder="Url" class="form-control" name="frm_submenu_url"/>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="control-label">Order</label>
                                            <input type="text" placeholder="0" class="form-control" name="frm_submenu_order"/>
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete">
                                                <i class="fa fa-close"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                <i class="fa fa-plus"></i> Add Submenu</a>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pull-right">
                                        <button type="submit" name="btn_submit_menu" class="btn green-dark">Save <i class="fa fa-plus-square"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.portlet-body -->
            </div>
            <!-- /.portlet -->
        </div>
    </div>


<!-- modal untuk edit data -->
<div class="update-modal">
    <div id="ModalUpdate" class="modal bs-modal-lg fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    </div>
    <!-- /.modal -->
</div>
<!-- /.update-modal -->

<!-- modal untuk hapus data -->
<div class="delete-modal">
    <div id="ModalDelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">User Delete</h4>
                    </div>
                    <div class="modal-body">
                        <h4 class="text-center">Are you sure to delete this user?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close</button>
                        <a href="#" class="btn btn-flat btn-primary" id="delete_link">Delete</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!-- /.hapus-modal -->

    <?php
    $footer_component = [
        'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
        'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
        'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
        'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
        'select2 script' => '../assets/pages/scripts/components-select2.min.js',
        'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
        'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
        'sample' => '../assets/custom/form-validation-user-add.js',
        'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
        'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
        'frjq' => '../assets/global/plugins/jquery-repeater/jquery.repeater.js',
        'fr' => '../assets/pages/scripts/form-repeater.min.js',
    ];
    require 'inc-template/inc-template-footer.php';
    ?>

    <script type="text/javascript">
    $(document).ready(function() {
        $(".open_modal").click(function(e) {
            var id = $(this).attr("id");
            $.ajax({
                url: "manageuser-update.php",
                type: "GET",
                data: {
                    id: id,
                },
                success: function(ajaxData) {
                    $("#ModalUpdate").html(ajaxData);
                    $("#ModalUpdate").modal('show', {
                        backdrop: 'true'
                    });
                }
            });
        });

        $("#tableListUser").DataTable({
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autowidth": true,
            "lengthChange": true
        });
    });
    </script>

    <!-- Script konfirmasi modal -->
    <script type="text/javascript">
    function confirm_modal(delete_url) {
        $('#ModalDelete').modal('show', {
            backdrop: 'static'
        });
        document.getElementById('delete_link').setAttribute('href', delete_url);
    }
    </script>

<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk memperbaharui kondisi atau lokasi armada
*/

require_once '../core/init.php';

$var_uid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "";
$var_action = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var($_GET['act'], FILTER_SANITIZE_STRING)) : "";

$var_gid = isset($_POST['frm_goingid']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_goingid'], FILTER_SANITIZE_NUMBER_INT)) : "";
$var_desc = isset($_POST['frm_desc']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_desc'], FILTER_SANITIZE_STRING)) : "";
$var_status = isset($_POST['frm_status']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_status'], FILTER_SANITIZE_STRING)) : "";
$var_garage = isset($_POST['frm_garage']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_garage'], FILTER_SANITIZE_STRING)) : "";
$var_error = [];

// switch (strtolower($var_action)) {
//     case 'update':
    if (isset($_POST['btn_update'])) {
        if (empty(trim($var_desc))) {
            $var_error['err']['desc'] = 'Mohon isikan posisi sekarang.';
        }

        if (empty(trim($var_status))) {
            $var_error['err']['status'] = 'Mohon pilih status armada sekarang.';
        }

        if (empty($var_error)) {
            if ($var_garage == 1) {
                $var_sql_getsid = "
                SELECT schedule_id
                FROM sp_going
                WHERE going_id = '{$var_gid}'
                LIMIT 1
                ";
                $var_query_sid = mysqli_query($var_con, $var_sql_getsid);
                $var_data_sid = mysqli_fetch_row($var_query_sid);
                if (mysqli_num_rows($var_query_sid) > 0) {
                    $var_update_sstatus = ['schedule_status' => '4'];
                    $var_cond_update = ['schedule_id' => $var_data_sid[0]];
                    update($var_con, "sp_schedule", $var_update_sstatus, $var_cond_update);
                }
            }

            $var_data_position = [
                'log_desc' => $var_desc,
                'log_status' => $var_status,
                'log_createat' => date('Y-m-d H:i:s'),
                'going_id' => $var_gid
            ];
            $var_insert_position = insert($var_con, "sp_going_log", $var_data_position);

            if ($var_insert_position) {
                setNotif(['success' => 'Posisi armada berhasil diperbaharui.']);
                routeUrl('index.php?p=armada-monitor-update');
                die();
            } else {
                setNotif(['success' => 'Posisi armada gagal diperbaharui.']);
                routeUrl('index.php?p=armada-monitor-update');
                die();
            }
        }
    }
    // break;
// }

$var_title = "Perbaharui Posisi";

$head_component = [
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN CONTENT -->
<?php
$var_sql_assign = "
SELECT d.crew_armadaid, d.crew_datefrom, d.crew_dateto, d.schedule_id, a.armada_image, concat(a.armada_brand, ' ', a.armada_typevariant) as armada_brand, d.crew_action, s.schedule_status, s.schedule_id, ot.od_id, ot.od_confirmedby
FROM sp_crew_detail d
JOIN sp_schedule s ON s.schedule_id = d.schedule_id
JOIN sp_order_transaction ot ON ot.od_id = s.od_id
JOIN sp_armada a ON a.armada_regnumber = ot.armada_regnumber
JOIN sp_crew_armada ca ON ca.crew_armadaid = d.crew_armadaid
JOIN sp_going g ON g.schedule_id = s.schedule_id
WHERE ca.user_id = '{$var_uid}' and d.crew_action = '1' and s.schedule_status != '4'
";
$var_query_assign = mysqli_query($var_con, $var_sql_assign);
$var_numrows_assign = mysqli_num_rows($var_query_assign);
?>
<?php if ($var_numrows_assign > 0): ?>
    <?php while ($var_data_assign = mysqli_fetch_array($var_query_assign)) : ?>
        <?php if ($var_data_assign['schedule_status' == '3']): ?>
            <?php
                $var_sql_checkexpenses = "
                SELECT expenses_id, expenses_status FROM sp_expenses WHERE schedule_id = '{$var_data_assign['schedule_id']}' LIMIT 1
                ";
                $var_query_checkexpenses = mysqli_query($var_con, $var_sql_checkexpenses);
                $var_data_checkexpenses = mysqli_fetch_row($var_query_checkexpenses);
            ?>
            <?php if (mysqli_num_rows($var_query_checkexpenses) == 0): ?>
                <?php
                $var_sql_getphone = "
                SELECT ud.user_detail_phone
                FROM sp_order_transaction ot
                JOIN sp_user_detail ud ON ud.user_id = ot.od_confirmedby
                WHERE ot.od_id = '{$var_data_assign['od_id']}'
                LIMIT 1
                ";

                $var_query_getphone = mysqli_query($var_con, $var_sql_getphone);
                $var_data_getphone = mysqli_fetch_row($var_query_getphone);
                sendPhone($var_con, [
                    'phone' => $var_data_getphone[0],
                    'message' => 'Mohon cek daftar penjadwalan, terdapat pesanan yang belum dibuat expensesnya.',
                ]);
                notification($var_con, [
                    'from' => $var_uid,
                    'to' => $var_data_assign['od_confirmedby'],
                    'description' => 'Mohon cek daftar penjadwalan, terdapat pesanan yang belum dibuat expensesnya.',
                    'type' => 'Permintaan',
                ]);
                ?>
                <div class="note note-info font-blue"><i class="fa fa-info"></i> Sistem sedang menginformasikan admin untuk membuat Expenses. Mohon Tunggu.</div>
            <?php elseif ($var_data_checkexpenses[1] == '1'): ?>
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="thumbnail">
                            <style>
                            /* Large Devices, Wide Screens */
                            @media only screen and (min-width : 1200px) {
                                img.img-thumbnails {
                                    width: 100%; height: 15em; width: 22em; margin-top: 2em;margin-left: 7em;
                                }
                            }

                            /* Medium Devices, Desktops */
                            @media only screen and (max-width : 992px) {
                                img.img-thumbnails {
                                    width: 100%; height: 15em; width: 22em; margin-top: 2em;margin-left: 4em;
                                }
                            }

                            /* Small Devices, Tablets */
                            @media only screen and (max-width : 768px) {
                                img.img-thumbnails {
                                    width: 100%; height: 15em; width: 22em; margin-top: 2em;margin-left: 10em;
                                }
                            }

                            /* Extra Small Devices, Phones */
                            @media only screen and (max-width : 480px) {
                                img.img-thumbnails {
                                    width: 100%; height: 15em; width: 22em; margin-top: 2em;margin-left: 2em;
                                }
                            }

                            /* Custom, iPhone Retina */
                            @media only screen and (max-width : 320px) {
                                img.img-thumbnails {
                                    width: 100%; height: 15em; width: 22em; margin-top: 2em;margin-left: 0.1em;
                                }
                            }
                            </style>
                            <div>
                                <img src="../images/armada/<?= $var_data_assign['armada_image'] ?>" alt="<?= $var_data_assign['armada_brand'] ?>" class="img img-responsive img-thumbnails">
                            </div>
                            <div class="caption">
                                <h3>
                                    <?= $var_data_assign['armada_brand'] ?>
                                    <?php switch($var_data_assign['crew_action']): case '1': ?>
                                    <label class="label label-success label-sm">Diterima</label>
                                    <?php break; case '2': ?>
                                    <label class="label label-danger label-sm">Ditolak</label>
                                    <?php endswitch; ?>
                                </h3>
                                <ul class="list-group">
                                    <li class="list-group-item"> Tanggal Berangkat
                                        <span class="badge badge-default"> <?= dateFormat($var_data_assign['crew_datefrom']) ?> </span>
                                    </li>
                                    <li class="list-group-item"> Tanggal Pulang
                                        <span class="badge badge-success"> <?= dateFormat($var_data_assign['crew_dateto']) ?> </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-list font-blue"></i>
                                    <span class="caption-subject font-blue bold uppercase">Perbaharui Posisi</span>
                                </div>
                                <div class="tools"> </div>
                            </div>
                            <div class="portlet-body">
                                <?php
                                $var_sql_going = "
                                SELECT going_id FROM sp_going WHERE schedule_id = '{$var_data_assign['schedule_id']}' LIMIT 1
                                ";
                                $var_query_going = mysqli_query($var_con, $var_sql_going);
                                $var_data_going = mysqli_fetch_row($var_query_going);
                                ?>
                                <form class="form-horizontal" action="index.php?p=armada-monitor-update.php" method="post">
                                    <input type="hidden" name="frm_goingid" value="<?= $var_data_going[0] ?>">
                                    <div class="form-group">
                                        <label for="frm_desc" class="control-label col-sm-3">Posisi/kondisi <span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <textarea type="text" class="form-control" id="frm_desc" name="frm_desc" placeholder="Posisi/kondisi sekarang"><?php if (isset($var_desc)) { echo $var_desc; }?></textarea>
                                            <p class="help-block warning-text"><?php if (isset($var_error['err']['desc'])) { echo $var_error['err']['desc']; } ?></p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="frm_status" class="control-label col-sm-3">Status <span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="frm_status" name="frm_status" placeholder="contoh: jalan">
                                            <p class="help-block warning-text"><?php if (isset($var_error['err']['status'])) { echo $var_error['err']['status']; } ?></p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="frm_garage" class="col-sm-2 col-sm-offset-1">Garasi?</label>
                                        <div class="col-sm-9">
                                            <div class="mt-checkbox-list">
                                                <label class="mt-checkbox mt-checkbox-outline" style="text-transform: lowercase;">
                                                    <input type="checkbox" class="form-control" name="frm_garage" value="1" id="frm_garage"> Check apabila armada sudah kembali ke garasi.
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="pull-right">
                                                <button type="submit" name="btn_update" class="btn green"><i class="fa fa-pencil"></i> Perbaharui</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else: ?>
                <div class="note note-info font-blue"><i class="fa fa-info"></i> Expenses belum divalidasi.</div>
            <?php endif; ?>
        <?php endif; ?>
    <?php endwhile; ?>
<?php else: ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="note note-info">
                <span class="fa fa-signal"></span> Tidak ada penugasan untuk anda!
            </div>
        </div>
    </div>
<?php endif; ?>

<!-- END CONTENT -->

<?php
$footer_component = [
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
];
require_once 'inc-template/inc-template-footer.php';
?>

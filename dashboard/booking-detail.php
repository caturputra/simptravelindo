<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Modal untuk menampilkan detail dari setiap pesanan
*/

require_once '../core/init.php';
require_once '../inc/function/inc-func-users.php';

$var_order_id = isset($_GET['orderid']) ? filter_var($_GET['orderid'], FILTER_SANITIZE_STRING) :"";

?>
<style media="screen">
    .detail-body {
        height: 17em;
        margin: .9em;
    }
</style>
<div class="modal-dialog modal-lg long">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Detail Pesanan : <?php echo $var_order_id ?></h4>
        </div>
        <form action="booking-action-approve.php" method="post" class="form-horizontal">
            <div class="modal-body">
                <?php
                $var_sql_detail = "
                SELECT a.armada_regnumber, a.armada_image, am.model_name as armada_brand
                FROM sp_order_transaction ot
                JOIN sp_armada a ON a.armada_regnumber = ot.armada_regnumber
                JOIN sp_armada_model am ON am.model_id = ot.model_id
                JOIN sp_armada_type at ON at.type_id = am.type_id
                WHERE ot.od_id = '{$var_order_id}'
                LIMIT 1
                ";
                // debug($var_sql_detail);
                $var_query_detail = mysqli_query($var_con, $var_sql_detail);
                $var_data_detail = mysqli_fetch_array($var_query_detail);
                    ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>
                                        <span class="caption-subject bold uppercase"> Detail Pemesan</span>
                                    </div>
                                </div>
                                <div class="portlet-body form detail-body">
                                    <?php
                                    $var_sql_buyer = "
                                    SELECT u.user_detail_firstname, u.user_detail_lastname, u.user_detail_phone, u.user_detail_address
                                    FROM sp_order_transaction ot
                                    JOIN sp_user_detail u ON u.user_id = ot.user_id
                                    WHERE ot.od_id = '{$var_order_id}'
                                    LIMIT 1
                                    ";
                                    $var_query_buyer = mysqli_query($var_con, $var_sql_buyer);
                                    $var_data_buyer = mysqli_fetch_array($var_query_buyer);
                                    ?>
                                    <form class="form-horizontal" role="form">
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="" class="control-label col-sm-4">Nama: </label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static"><?php echo $var_data_buyer['user_detail_firstname'] . " " . $var_data_buyer['user_detail_lastname']; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="" class="control-label col-sm-4">Telepon: </label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static"><?php echo $var_data_buyer['user_detail_phone']; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="" class="control-label col-sm-4">Alamat: </label>
                                                        <div class="col-sm-8">
                                                            <p class="form-control-static"><?php echo $var_data_buyer['user_detail_address']; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- customer -->

                        <div class="col-sm-4">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-bus"></i>
                                        <span class="caption-subject bold uppercase">Detail Armada</span>
                                    </div>
                                </div>
                                <div class="portlet-body form detail-body">
                                    <?php if (isset($var_data_detail['armada_regnumber'])): ?>
                                        <img class="img img-responsive" alt="<?php echo $var_data_detail['armada_regnumber']; ?>" src="../images/armada/<?php echo $var_data_detail['armada_image']; ?>" style="height: 7em;">
                                        <br>
                                        <p class="text-center"><?php echo $var_data_detail['armada_regnumber'] ?><br><?php echo " (" . $var_data_detail['armada_brand'] .")" ?></p>
                                    <?php else: ?>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                        <p class="form-control-static">
                                                            Armada belum diset.
                                                        </p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <!-- /armada -->

                    <div class="col-sm-4">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-users"></i>
                                    <span class="caption-subject bold uppercase">Detail crew</span>
                                </div>
                            </div>
                            <div class="portlet-body form detail-body">
                                <form class="form-horizontal" role="form">
                                    <div class="form-body">
                                        <?php
                                        $var_sql_detailcrew = "
                                        SELECT ud.user_detail_firstname, ud.user_detail_lastname, ud.user_detail_phone
                                        FROM sp_crew_detail cd
                                        JOIN sp_crew_armada ca ON ca.crew_armadaid = cd.crew_armadaid
                                        JOIN sp_user u ON u.user_id = ca.user_id
                                        JOIN sp_user_detail ud ON ud.user_id = u.user_id
                                        JOIN sp_level l ON l.level_id = u.user_level
                                        JOIN sp_schedule s ON s.schedule_id = cd.schedule_id
                                        JOIN sp_order_transaction ot ON ot.od_id = s.od_id
                                        WHERE l.level_name = 'Driver' && ot.od_id = '{$var_order_id}'
                                        LIMIT 1
                                        ";
                                        $var_query_detailcrew = mysqli_query($var_con, $var_sql_detailcrew);
                                        $var_rows_detail_crew = mysqli_num_rows($var_query_detailcrew);
                                        if ($var_rows_detail_crew > 0) :
                                            while ($var_crew_detailcrew = mysqli_fetch_array($var_query_detailcrew)) :
                                                ?>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label for="" class="control-label col-sm-5">Sopir: </label>
                                                            <div class="col-sm-7">
                                                                <p class="form-control-static">
                                                                    <?php
                                                                    echo $var_crew_detailcrew['user_detail_firstname'] . " " . $var_crew_detailcrew['user_detail_lastname'];
                                                                    ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            endwhile;
                                            else :
                                                ?>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label for="" class="control-label col-sm-5">Sopir: </label>
                                                            <div class="col-sm-7">
                                                                <p class="form-control-static">
                                                                    Sopir belum dijadwalkan.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <?php
                                            $var_sql_detailcrew2 = "
                                            SELECT ud.user_detail_firstname, user_detail_lastname, user_detail_phone
                                            FROM sp_crew_detail cd
                                            JOIN sp_crew_armada ca ON ca.crew_armadaid = cd.crew_armadaid
                                            JOIN sp_user u ON u.user_id = ca.user_id
                                            JOIN sp_user_detail ud ON ud.user_id = u.user_id
                                            JOIN sp_level l ON l.level_id = u.user_level
                                            JOIN sp_schedule s ON s.schedule_id = cd.schedule_id
                                            JOIN sp_order_transaction ot ON ot.od_id = s.od_id
                                            WHERE l.level_name = 'Co-Driver' && ot.od_id = '{$var_order_id}'
                                            LIMIT 1
                                            ";
                                            $var_query_detailcrew2 = mysqli_query($var_con, $var_sql_detailcrew2);
                                            $var_rows_detail_crew2 = mysqli_num_rows($var_query_detailcrew2);
                                            if ($var_rows_detail_crew2 > 0) :
                                                while ($var_crew_detailcrew2 = mysqli_fetch_array($var_query_detailcrew2)) :
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="" class="control-label col-sm-5">Kernet: </label>
                                                                <div class="col-sm-7">
                                                                    <p class="form-control-static"><?php echo $var_crew_detailcrew2['user_detail_firstname'] . " " . $var_crew_detailcrew2['user_detail_lastname']; ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            endwhile;
                                            else :
                                                ?>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label for="" class="control-label col-sm-5">Kernet: </label>
                                                            <div class="col-sm-7">
                                                                <p class="form-control-static">
                                                                    Kernet belum dijadwalkan.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /who reserved -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="margiv-top-10 col-sm-offset-2">
                            <button type="button" class="btn blue" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                        </div>
                    </div>
                </form>
                <!-- /.form-horizontal -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->

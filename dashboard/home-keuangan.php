<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk beranda akunting
*/

$var_title = "Dashboard Akunting";

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';

$head_component = [
    'morris' => '../assets/global/plugins/morris/morris.css',
];
require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>

<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- BEGIN DASHBOARD STATS 1-->
<div class="row">
    <div class="col-lg-3 col-sm-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-money"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $var_sql_countorder = "SELECT count(*) as count_payment FROM sp_payment WHERE payment_status = 0";
                    $var_query_countorder = mysqli_query($var_con, $var_sql_countorder);
                    $var_numrows_countorder = mysqli_num_rows($var_query_countorder);
                    $var_data_countorder = mysqli_fetch_row($var_query_countorder);
                    if ($var_numrows_countorder > 0) :
                        ?>
                        <span><?php echo $var_data_countorder[0] ?></span>
                    <?php else: ?>
                        <span>0</span>
                    <?php endif; ?>
                </div>
                <div class="desc"> Validasi Pending <a href="index.php?p=validate-payments.php" class="font-white"><span class="fa fa-external-link"> </span></a></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat dashboard-stat-v2 purple" href="#">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $var_sql_countorder = "SELECT count(*) as counthowto FROM sp_payment WHERE payment_howto='0'";
                    $var_query_countorder = mysqli_query($var_con, $var_sql_countorder);
                    $var_numrows_countorder = mysqli_num_rows($var_query_countorder);
                    $var_data_countorder = mysqli_fetch_row($var_query_countorder);
                    if ($var_numrows_countorder > 0) :
                        ?>
                        <span><?php echo $var_data_countorder[0] ?></span>
                    <?php else: ?>
                        <span>0</span>
                    <?php endif; ?>
                </div>
                <div class="desc"> Pembayaran via transfer <a href="index.php?p=manageuser-setting.php" class="font-white"><span class="fa fa-external-link"> </span></a></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat dashboard-stat-v2 red" href="#">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $var_sql_countorder = "SELECT count(*) as counthowto FROM sp_payment WHERE payment_howto='1'";
                    $var_query_countorder = mysqli_query($var_con, $var_sql_countorder);
                    $var_numrows_countorder = mysqli_num_rows($var_query_countorder);
                    $var_data_countorder = mysqli_fetch_row($var_query_countorder);
                    if ($var_numrows_countorder > 0) :
                        ?>
                        <span><?php echo $var_data_countorder[0] ?></span>
                    <?php else: ?>
                        <span>0</span>
                    <?php endif; ?>
                </div>
                <div class="desc"> Pembayaran via kasir <a href="index.php?p=booking-armada.php" class="font-white"><span class="fa fa-external-link"> </span></a></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat dashboard-stat-v2 green" href="#">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $var_sql_countorder = "SELECT count(*) as count FROM sp_payment";
                    $var_query_countorder = mysqli_query($var_con, $var_sql_countorder);
                    $var_numrows_countorder = mysqli_num_rows($var_query_countorder);
                    $var_data_countorder = mysqli_fetch_row($var_query_countorder);
                    if ($var_numrows_countorder > 0) :
                        ?>
                        <span><?php echo $var_data_countorder[0] ?></span>
                    <?php else: ?>
                        <span>0</span>
                    <?php endif; ?>
                </div>
                <div class="desc"> Total Pembayaran <a href="index.php?p=booking-armada.php" class="font-white"><span class="fa fa-external-link"> </span></a></div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<!-- END DASHBOARD STATS 1-->

<?php
//FOOTER PAGE BASE LEVEL
$footer_component = [
    'momentdate' => '../assets/global/plugins/moment.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'dashboardscript' => '../assets/pages/scripts/dashboard.js',
    'morrisjs' => '../assets/global/plugins/morris/morris.min.js'
];
require_once 'inc-template/inc-template-footer.php';
?>

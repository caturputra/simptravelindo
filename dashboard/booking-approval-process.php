<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk proses konfirmasi pemesanan
*/

require_once __DIR__ . '/../core/init.php';
require_once __DIR__ . '/../inc/inc-session-user.php';
require_once __DIR__ . '/../inc/helper/Mail.php';

//proses update status order dan selling
$var_uid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "";
$var_action = isset($_GET['s']) ? mysqli_escape_string($var_con, filter_var($_GET['s'], FILTER_SANITIZE_STRING)) : "";
$var_datefrom = isset($_POST['frm_datefrom']) ? mysqli_escape_string($var_con, $_POST['frm_datefrom']) : "";
$var_dateto = isset($_POST['frm_dateto']) ? mysqli_escape_string($var_con, $_POST['frm_dateto']) : "";
$var_aid = isset($_POST['frm_aid']) ? mysqli_escape_string($var_con, $_POST['frm_aid']) : "";

switch (strtolower($var_action)) {
    case 'approved':
    if (isset($_POST['btn_order_approve'])) {
        $var_sql_schedule = "
            SELECT a.armada_regnumber
            FROM sp_order_transaction ot
            LEFT JOIN sp_armada a ON a.armada_regnumber = ot.armada_regnumber
            WHERE ((ot.od_from BETWEEN '{$var_datefrom}' AND '{$var_dateto}') OR (ot.od_to BETWEEN '{$var_datefrom}' AND '{$var_dateto}' )) AND (ot.od_status IN ('1','4')) AND a.armada_regnumber = '{$var_aid}'
        ";
        $var_query_schedule = mysqli_query($var_con, $var_sql_schedule);
        $var_numrows_schedule = mysqli_num_rows($var_query_schedule);
        // debug($var_sql_schedule);

        if ($var_numrows_schedule > 0) {
            setNotif(['danger' => 'Tanggal tersebut sedang digunakan.']);
            routeUrl('index.php?p=booking-armada');
            die();
        }

        $var_numrows_armada =
        $var_id = isset($_POST['frm_orderid']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_orderid'], FILTER_SANITIZE_STRING)) : "";
        $var_selling = isset($_POST['frm_approval']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_approval'], FILTER_SANITIZE_STRING)) : "";
        $var_driver = isset($_POST['frm_driver']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_driver'], FILTER_SANITIZE_STRING)) : "";
        $var_codriver = isset($_POST['frm_codriver']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_codriver'], FILTER_SANITIZE_STRING)) : "";

        //retrive data order
        $var_sql_order = "
        SELECT ot.od_id, ot.od_from, ot.od_to, concat(udm.user_detail_firstname, ' ', udm.user_detail_lastname) as member, u.user_email, ot.od_selling, udm.user_detail_phone, ot.user_id
        FROM sp_order_transaction ot
        JOIN sp_user u ON u.user_id = ot.user_id
        JOIN sp_user_detail udm ON udm.user_id = u.user_id
        WHERE ot.od_id = '{$var_id}'
        ";
        $var_query_order = mysqli_query($var_con, $var_sql_order);
        $var_num_rows = mysqli_num_rows($var_query_order);
        $var_data_order = mysqli_fetch_row($var_query_order);

        if ($var_num_rows > 0) {
            $var_get_id = substr($var_data_order[0], -4);
            $var_get_date = date('m');
            $var_gen_invoice = autonumber($var_con, "sp_order_transaction", "od_invoice", 0, "ITR".$var_get_date.$var_get_id);

            mysqli_autocommit($var_con, FALSE);
            mysqli_begin_transaction($var_con);
            $var_data_approve = [
                'od_invoice' => substr($var_gen_invoice, 0, 9),
                'od_selling' => $var_selling,
                'od_confirmedby' => $var_uid,
                'od_status' => 1, //approve,
                'od_createat' => date('Y-m-d H:i:s')
            ];
            $var_update_cond = ['od_id' => $var_id];
            $var_update = update($var_con, "sp_order_transaction", $var_data_approve, $var_update_cond);

            $var_insert_schedule = ['od_id' => $var_id];
            $var_insert = insert($var_con, "sp_schedule", $var_insert_schedule);

            $var_lastid_schedule = mysqli_insert_id($var_con);

            //hitung minimum bayar untuk setip Pemesanan
            $var_data_selling = mysqli_fetch_row(mysqli_query($var_con, "SELECT od_selling FROM sp_order_transaction WHERE od_id = '{$var_id}'"));
            $var_minpayment = (float) $_ENV['PAYMENT_MIN'] * $var_data_selling[0];

            //config send mail
            $mail = new Mail();
            $from = "noreply@travelindo.com";
            $subject = "Konfirmasi Pesanan";
            $dir = "content-mail";
            $template = "armada-confirm";
            $var_opt = [
                'wrapper_member' => $var_data_order[3],
                'wrapper_bookingcode' => $var_id,
                'wrapper_period' => 'Berangkat tanggal ' . dateFormat($var_data_order[1]) . ' sampai ' . dateFormat($var_data_order[2]),
                'wrapper_minpayment' => number_format($var_minpayment, 2, ',','.'),
                'wrapper_totalpayment' => number_format($var_selling, 2, ',','.'),
                'footer_year' => date('Y')
            ];

            if ($var_update && $var_insert) {
                $mail->sendMail($from, $var_data_order[4], $subject, $template, $dir, $var_opt);
                mysqli_commit($var_con);
                try {
                    sendPhone($var_con, [
                        'phone' => $var_data_order[6],
                        'message' => 'Pesanan dikonfirmasi, kode pesan ' . $var_id . ' | periode' . ' ' . dateFormat($var_data_order[1]) . ' - ' . dateFormat($var_data_order[2]) . ' | min bayar ' . number_format($var_minpayment, 0, ',','.') . ' | total tagihan ' . number_format($var_selling, 0, ',','.')
                    ]);
                    writeLog($var_con, ['pesan' => 'melakukan konfirmasi pemesanan armada']);
                    notification($var_con, [
                        'from' => $_SESSION['userid'],
                        'to' => $var_data_order[7],
                        'description' => 'pesanan anda telah dikonfirmasi. Cek pesanan anda',
                        'type' => 'konfirmasi',
                    ]);
                } catch (Exception $e) {

                }

                setNotif(['success' => 'Konfirmasi pesanan kode ' . $var_id . ' berhasil diproses.']);
                routeUrl('index.php?p=booking-armada');
            } else {
                mysqli_rollback($var_con);
                setNotif(['danger' => 'Konfirmasi pesanan kode ' . $var_id . ' gagal diproses.']);
                routeUrl('index.php?p=booking-armada');
            }
        }
        mysqli_close($var_con);
        die();
    }
    break;

    case 'canceled':

    $var_order_id = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) :"";
    $var_order_id = base64_decode($var_order_id);

    $var_sql_cancel = "
    SELECT ot.od_id, ot.od_from, ot.od_to, concat(udm.user_detail_firstname, ' ', udm.user_detail_lastname) as member, u.user_email, udm.user_detail_phone, ot.user_id
    FROM sp_order_transaction ot
    JOIN sp_user u ON u.user_id = ot.user_id
    JOIN sp_user_detail udm ON udm.user_id = u.user_id
    WHERE od_id = '{$var_order_id}'
    ";

    $var_query_cancel = mysqli_query($var_con, $var_sql_cancel);
    $var_num_rows_cancel = mysqli_num_rows($var_query_cancel);
    $var_data_cancel = mysqli_fetch_row($var_query_cancel);
    if ($var_num_rows_cancel > 0) {
        //update status kru menjadi dibatalkan apabila pesanan telah dijadwalkan
        $var_sql_checkschedule = "
        SELECT s.schedule_id
        FROM sp_crew_detail cd
        JOIN sp_schedule s ON s.od_id = cd.od_id
        WHERE s.od_id = {$var_id}
        ";
        $var_query_checkschedule = mysqli_query($var_con, $var_sql_checkschedule);
        $var_numrows_checkschedule = mysqli_num_rows($var_query_checkschedule);

        if ($var_numrows_check > 0) {
            $var_data_checkschedule = mysqli_fetch_row($var_query_checkschedule);
            $var_data_update = ['crew_action' => '3'];
            $var_cond_update = ['schedule_id' => $var_data_checkschedule[0]];

            update($var_con, "sp_crew_detail", $var_data_update, $var_cond_update);
        }

        $var_update_cancel = ['od_status' => 3, 'od_confirmedby' => $var_uid, 'od_createat' => date('Y-m-d H:i:s')];
        $var_update_id = ['od_id' => $var_order_id];
        $var_update_process = update($var_con, "sp_order_transaction", $var_update_cancel, $var_update_id);

        //config send mail
        $mail = new Mail();
        $from = "noreply@travelindo.com";
        $subject = "Konfirmasi Pesanan Dibatalkan";
        $dir = "content-mail";
        $template = "armada-confirm";
        $var_opt = [
            'wrapper_member' => $var_data_order[3],
            'wrapper_bookingcode' => $var_id,
            'wrapper_period' => 'Berangkat tanggal ' . dateFormat($var_data_order[1]) . ' sampai ' . dateFormat($var_data_order[2]),
            'wrapper_minpayment' => number_format($var_minpayment, 2, ',','.'),
            'wrapper_totalpayment' => number_format($var_selling, 2, ',','.'),
            'footer_year' => date('Y')
        ];

        if (($var_update_process)) {
            $mail->sendMail($from, $var_data_cancel[4], $subject, $template, $dir, $var_opt);
            notification($var_con, [
                'from' => $_SESSION['userid'],
                'to' => $var_data_cancel[6],
                'description' => 'pesanan anda telah dibatalkan.',
                'type' => 'konfirmasi',
            ]);
            //kirim sms
            sendPhone($var_con, ['phone' => $var_data_cancel[5], 'message' => 'Pesanan anda telah dibatalkan.']);
            setNotif(['success' => 'Konfirmasi pesanan kode ' . $var_order_id . ' berhasil dibatalkan.']);
            routeUrl('index.php?p=booking-armada');
            die();
        } else {
            setNotif(['success' => 'Konfirmasi pesanan kode ' . $var_order_id . ' gagal dibatalkan.']);
            routeUrl('index.php?p=booking-armada');
            die();
        }
    } else {
        setNotif(['danger' => 'data tidak ditemukan.']);
        routeUrl('index.php?p=booking-armada');
        die();
    }

    break;

    case 'rejected':
    if (isset($_POST['btn_order_reject'])) {

        $var_id = isset($_POST['frm_orderid']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_orderid'], FILTER_SANITIZE_STRING)) : "";
        $var_reason = isset($_POST['frm_reject']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_reject'], FILTER_SANITIZE_STRING)) : "";

        //retrive data order
        $var_sql_order = "
        SELECT ot.od_id, ot.od_from, ot.od_to, concat(udm.user_detail_firstname, ' ', udm.user_detail_lastname) as member, u.user_email, udm.user_detail_phone, ot.user_id
        FROM sp_order_transaction ot
        JOIN sp_user u ON u.user_id = ot.user_id
        JOIN sp_user_detail udm ON udm.user_id = u.user_id
        WHERE od_id = '{$var_id}'
        ";
        $var_query_order = mysqli_query($var_con, $var_sql_order);
        $var_num_rows = mysqli_num_rows($var_query_order);
        $var_data_order = mysqli_fetch_row($var_query_order);

        if ($var_num_rows > 0) {
            $var_data_reject = ['od_status' => 2, 'od_confirmedby' => $var_uid, 'od_createat' => date('Y-m-d H:i:s')];
            $var_cond_id = ['od_id' => $var_id];
            $var_update = update($var_con, "sp_order_transaction", $var_data_reject, $var_cond_id);
            $var_data_reason = ['canceled_reason' => $var_reason, 'canceled_status' => 0, 'od_id' => $var_id];
            $var_insert = insert($var_con, "sp_order_canceled", $var_data_reason);

            //config send mail
            $mail = new Mail();
            $from = "noreply@travelindo.com";
            $subject = "Konfirmasi Pesanan Ditolak";
            $dir = "content-mail";
            $template = "armada-confirm";
            $var_opt = [
                'wrapper_member' => $var_data_order[3],
                'wrapper_bookingcode' => $var_id,
                'wrapper_period' => 'Berangkat tanggal ' . dateFormat($var_data_order[1]) . ' sampai ' . dateFormat($var_data_order[2]),
                'wrapper_minpayment' => number_format($var_minpayment, 2, ',','.'),
                'wrapper_totalpayment' => number_format($var_selling, 2, ',','.'),
                'footer_year' => date('Y')
            ];

            if (($var_update && $var_insert) && ($mail->sendMail($from, $var_data_order[4], $subject, $template, $dir, $var_opt))) {
                notification($var_con, [
                    'from' => $_SESSION['userid'],
                    'to' => $var_data_order[6],
                    'description' => 'pesanan anda telah ditolak oleh admin.',
                    'type' => 'konfirmasi',
                ]);
                sendPhone($var_con, ['phone' => $var_data_order[5], 'message' => 'Pesanan anda ditolak.']);
                setNotif(['success' => 'Konfirmasi pesanan kode ' . $var_id . ' berhasil diproses.']);
                routeUrl('index.php?p=booking-armada');
                die();
            } else {
                setNotif(['danger' => 'Konfirmasi pesanan kode ' . $var_id . ' gagal diproses.']);
                routeUrl('index.php?p=booking-armada');
                die();
            }
        }
        mysqli_close($var_con);
    }
    break;
}

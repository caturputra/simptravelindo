<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk operasi pemesanan
*/

require_once '../core/init.php';
require_once '../inc/function/inc-func-users.php';
require_once '../inc/helper/Mail.php';

$var_id = isset($_POST['frm_order_id']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_order_id'], FILTER_SANITIZE_STRING)) :"";
$var_act = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var($_GET['act'])) : "" ;
$var_reason = isset($_POST['frm_reason']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_reason'], FILTER_SANITIZE_STRING)) : "";

//proses untuk mengirim pesan dari member ke admin transport
if (strtolower($var_act) === 'sendmessage') {
    $var_subject = isset($_POST['frm_subject']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_subject'], FILTER_SANITIZE_STRING)) : "";
    $var_message = isset($_POST['frm_message']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_message'], FILTER_SANITIZE_STRING)) : "";

    $var_sql_getsingle = "
    SELECT od_confirmedby
    FROM sp_order_transaction
    WHERE od_id = '{$var_id}'
    LIMIT 1
    ";
    $var_query_getsingle = mysqli_query($var_con, $var_sql_getsingle);
    $var_data_getsingle = mysqli_fetch_row($var_query_getsingle);

    //cek apakah pernah dikonfirmasi atau belum
    if ($var_data_getsingle[0] == null) {
        $var_sql_getadm = "
        SELECT u.user_email, ud.user_detail_phone
        FROM sp_user u
        LEFT JOIN sp_user_detail ud ON ud.user_id = u.user_id
        WHERE u.user_level = '1'
        ";
        $var_query_getadm = mysqli_query($var_con, $var_sql_getadm);
        $mail = new Mail();
        $from = "noreply@travelindo.com";
        $subject = "Pesan dari member (" . $var_subject . ")";
        $dir = "content-mail";
        $template = "message";
        $var_opt = [
            'wrapper_message' => $var_message,
            'footer_year' => date('Y')
        ];

        //kirim pesan ke semua akun yang berlevel admin transport
        while ($var_data_getadm = mysqli_fetch_row($var_query_getadm)) {
            $mail->sendMail($from, $var_data_getadm[0], $subject, $template, $dir, $var_opt);
            sendPhone($var_con, ['phone' => $var_data_getadm[1], 'message' => 'Notifikasi! Mohon cek email: ' . $var_data_getadm[0] .', ada pesan dari member.']);
        }

        setNotif(['success' => 'Pesan anda berhasil dikirim. Mohon tunggu tindakan selanjutnya.']);
        routeUrl('index.php?p=member-orders');
        die();

    } else {
        $var_sql_getadm = "
        SELECT u.user_email, ud.user_detail_phone
        FROM sp_user u
        LEFT JOIN sp_user_detail ud ON ud.user_id = u.user_id
        WhERE u.user_id = '{$var_data_getsingle[0]}'
        LIMIT 1
        ";
        $var_query_getadm = mysqli_query($var_con, $var_sql_getadm);
        $var_data_getadm = mysqli_fetch_row($var_query_getadm);

        if (!empty($var_subject) && !empty($var_message)) {
            //send email confirmation
            $mail = new Mail();
            $from = "noreply@travelindo.com";
            $subject = "Pesan dari member (" . $var_subject . ")";
            $dir = "content-mail";
            $template = "message";
            $var_opt = [
                'wrapper_message' => $var_message,
                'footer_year' => date('Y')
            ];

            //kirim ke admin transport yang melakukan konfirmasi
            if($mail->sendMail($from, $var_data_getadm[0], $subject, $template, $dir, $var_opt)) {
                sendPhone($var_con, ['phone' => $var_data_getadm[1], 'message' => 'Notifikasi! Mohon cek email: ' . $var_data_getadm[0] .', ada pesan dari member.']);
                setNotif(['success' => 'Pesan anda berhasil dikirim. Mohon tunggu tindakan selanjutnya.']);
                routeUrl('index.php?p=member-orders');
                die();
            }
        }
    }
}

if (strtolower($var_act) === 'cancel' && isset($_POST['btn_cancel'])) {
    //pembatalan untuk pesanan yang sudah melakukan pembayaran
    $var_sql_check = "
    SELECT s.od_id, datediff(ot.od_from, CURRENT_TIMESTAMP()) as date_dispute, ot.od_status, ot.od_confirmedby
    FROM sp_payment p
    JOIN sp_schedule s ON s.schedule_id = p.schedule_id
    JOIN sp_order_transaction ot ON ot.od_id = s.od_id
    WHERE s.od_id = '{$var_id}'
    LIMIT 1
    ";
    $var_query_check = mysqli_query($var_con, $var_sql_check);
    $var_numrows_check = mysqli_num_rows($var_query_check);
    // debug($var_sql_check);

    if ($var_numrows_check > 0) {
        $var_data_check = mysqli_fetch_row($var_query_check);

        switch ($var_data_check[2]) {
            case $var_data_check[2] == 4:
            // debug($var_data_check[2]);

            mysqli_autocommit($var_con, FALSE);
            mysqli_begin_transaction($var_con);
            $var_data = ['od_status' => 0];
            $var_data_cond_id = ['od_id' => $var_id];
            $var_update = update($var_con, "sp_order_transaction", $var_data, $var_data_cond_id);

            $var_data_cancel = ['canceled_reason' => $var_reason, 'canceled_status' => 0, 'od_id' => $var_id];
            $var_insert = insert($var_con, "sp_order_canceled", $var_data_cancel);

            if ($var_update && $var_insert) {
                switch ($var_data_check[1]) {
                    case $var_data_check[1] < 0 :
                    setNotif(['danger' => 'Tidak bisa dilakukan pembatalan, Hubungi Admin.']);
                    routeUrl('index.php?p=member-orders');
                    die();
                    break;

                    case $var_data_check[1] <= 7 :
                    notification($var_con, [
                        'from' => $_SESSION['userid'],
                        'to' => $var_data_check[3],
                        'description' => 'pesanan '. $var_data_check[0] .' dibatalkan.',
                        'type' => 'konfirmasi',
                    ]);
                    mysqli_commit($var_con);
                    setNotif(['success' => 'Pembatalan pesanan kurang dari 7 hari sebelum keberangkatan, maka anda dikenanakan pinalti sejumlah DP yang dibayarkan.']);
                    routeUrl('index.php?p=member-orders');
                    die();
                    break;

                    default:
                    notification($var_con, [
                        'from' => $_SESSION['userid'],
                        'to' => $var_data_check[3],
                        'description' => 'pesanan '. $var_data_check[0] .' dibatalkan.',
                        'type' => 'konfirmasi',
                    ]);
                    mysqli_commit($var_con);
                    setNotif(['success' => 'Pembatalan pesanan berhasil diproses. Silakan datang ke Travelindo untuk pengajuan klaim DP.']);
                    routeUrl('index.php?p=member-orders');
                    die();
                    break;
                }
            } else {
                mysqli_rollback($var_con);
                setNotif(['warning' => 'Pembatalan pesanan gagal diproses.']);
                routeUrl('index.php?p=member-orders');
                die();
            }
            break;

            default:
            $var_data = ['od_status' => 0];
            $var_data_cond_id = ['od_id' => $var_id];
            $var_update = update($var_con, "sp_order_transaction", $var_data, $var_data_cond_id);

            if ($var_update) {
                switch ($var_data_check[1]) {
                    case $var_data_check[1] <= 1 :
                    notification($var_con, [
                        'from' => $_SESSION['userid'],
                        'to' => $var_data_check[3],
                        'description' => 'pesanan '. $var_data_check[0] .' dibatalkan.',
                        'type' => 'konfirmasi',
                    ]);
                    setNotif(['success' => 'Pembatalan pesanan berhasil diproses. DP tidak bisa diklaim.']);
                    routeUrl('index.php?p=member-orders');
                    die();
                    break;

                    case $var_data_check[1] <= 7 :
                    notification($var_con, [
                        'from' => $_SESSION['userid'],
                        'to' => $var_data_check[3],
                        'description' => 'pesanan '. $var_data_check[0] .' dibatalkan.',
                        'type' => 'konfirmasi',
                    ]);
                    setNotif(['success' => 'Pembatalan pesanan kurang dari 7 hari sebelum keberangkatan, maka anda dikenanakan pinalti sejumlah DP yang dibayarkan.']);
                    routeUrl('index.php?p=member-orders');
                    die();
                    break;

                    default:
                    notification($var_con, [
                        'from' => $_SESSION['userid'],
                        'to' => $var_data_check[3],
                        'description' => 'pesanan '. $var_data_check[0] .' dibatalkan.',
                        'type' => 'konfirmasi',
                    ]);
                    setNotif(['success' => 'Pembatalan pesanan berhasil diproses. Silakan datang ke Travelindo untuk pengajuan klaim DP jika anda sudah membayar.']);
                    routeUrl('index.php?p=member-orders');
                    die();
                    break;
                }
            } else {
                setNotif(['warning' => 'Pembatalan pesanan gagal diproses.']);
                routeUrl('index.php?p=member-orders');
                die();
            }
            break;
        }
    } else {
        $var_data = ['od_status' => 3];
        $var_data_cond_id = ['od_id' => $var_id];
        $var_update = update($var_con, "sp_order_transaction", $var_data, $var_data_cond_id);

        if ($var_update) {
            setNotif(['success' => 'Pembatalan pesanan berhasil diproses.']);
            routeUrl('index.php?p=member-orders');
            die();
        }
    }

    //update status kru apabila pesanan telah dijadwalkan
    $var_sql_checkschedule = "
    SELECT s.schedule_id
    FROM sp_crew_detail cd
    JOIN sp_schedule s ON s.schedule_id = cd.schedule_id
    WHERE s.od_id = '{$var_id}'
    LIMIT 1
    ";
    $var_query_checkschedule = mysqli_query($var_con, $var_sql_checkschedule);
    $var_numrows_checkschedule = mysqli_num_rows($var_query_checkschedule);

    if ($var_numrows_check > 0) {
        $var_data_checkschedule = mysqli_fetch_row($var_query_checkschedule);
        $var_data_update = ['crew_action' => '3'];
        $var_cond_update = ['schedule_id' => $var_data_checkschedule[0]];

        if (update($var_con, "sp_crew_detail", $var_data_update, $var_cond_update)) {
            // setNotif(['success' => 'Pembatalan pesanan berhasil diproses.']);
            // routeUrl('index.php?p=member-orders');
            // die();
        }
    }
}

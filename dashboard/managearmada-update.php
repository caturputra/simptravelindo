<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk memperbaharui data armada
*/

require_once '../core/init.php';
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Ubah Armada</h4>
        </div>
        <div class="modal-body">
            <form action="managearmada.php?act=update" method="post" enctype="multipart/form-data">
                <?php

                $var_armada_id = isset($_GET['armada_regnumber'])? mysqli_escape_string($var_con, filter_var($_GET['armada_regnumber'], FILTER_SANITIZE_STRING)) :"";
                $var_sql_armada = "
                SELECT *
                FROM sp_armada a
                JOIN sp_model_type mt ON mt.armada_regnumber = a.armada_regnumber
                JOIN sp_armada_model am ON am.model_id = mt.model_id
                JOIN sp_armada_type at ON at.type_id = am.type_id
                WHERE a.armada_regnumber = '{$var_armada_id}'";
                $var_query_armada = mysqli_query($var_con, $var_sql_armada);
                while ($var_data_armada = mysqli_fetch_array($var_query_armada)) :
                    ?>
                    <div class="box-body">
                        <form action="index.php?p=managearmada.php&amp;act=update" method="post" enctype="multipart/form-data" id="form_armada_add">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="frm_armada_id" class=" control-label">No. Plat <span class="required">*</span></label>
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control" id="frm_armada_id" name="frm_armada_id" placeholder="No Plat" value="<?= $var_data_armada['armada_regnumber'] ?>" autofocus title='contoh: AB XXXX DL' pattern="([A-Z]{1,2})[ s].*?(\d{1,4}).*?[ s]([A-Z]{1,2})" readonly>
                                            <span class="help-block warning-text"><?php if (!empty($var_error['id'])) { echo $var_error['id'];
                                            } ?></span>
                                        </div>
                                    </div>
                                    <!-- /form-group -->

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="frm_brand" class=" control-label">Merk <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control" id="frm_brand" name="frm_brand" placeholder="Merk" value="<?= $var_data_armada['armada_brand'] ?>" pattern="[A-Z]{1}.*?" title="Travelindo">
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['brand'])) { echo $var_error['brand'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="frm_variant" class=" control-label">Varian <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control" id="frm_variant" name="frm_variant" placeholder="Varian" value="<?= $var_data_armada['armada_typevariant'] ?>" pattern="[A-Z]{1}.*?" title="Travelindo">
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['variant'])) { echo $var_error['variant'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="frm_type" class=" control-label">Tipe <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <select class="form-control select2" name="frm_type" id="frm_type">
                                                        <?php if ($var_data_armada['armada_type'] == 1) : ?>
                                                            <option value="1" selected>Kendaraan Ringan</option>
                                                            <option value="2">Kendaraan Berat</option>
                                                        <?php else: ?>
                                                            <option value="1">Kendaraan Ringan</option>
                                                            <option value="2" selected>Kendaraan Berat</option>
                                                        <?php endif; ?>
                                                    </select>
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['type'])) { echo $var_error['type'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="frm_model" class=" control-label">Model <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <select class="form-control select2" name="frm_model" id="frm_model">
                                                        <?php switch($var_data_armada['armada_model']): case 'minibus' : ?>
                                                        <option value="sedan">Sedan</option>
                                                        <option value="minibus" selected>Minibus</option>
                                                        <option value="microbus">Microbus</option>
                                                        <option value="bus">Bus</option>
                                                        <option value="elf">Elf</option>
                                                        <?php break; case 'sedan': ?>
                                                        <option value="sedan" selected>Sedan</option>
                                                        <option value="minibus">Minibus</option>
                                                        <option value="microbus">Microbus</option>
                                                        <option value="bus">Bus</option>
                                                        <option value="elf">Elf</option>
                                                        <?php break; case 'microbus': ?>
                                                        <option value="sedan">Sedan</option>
                                                        <option value="minibus">Minibus</option>
                                                        <option value="microbus" selected>Microbus</option>
                                                        <option value="bus">Bus</option>
                                                        <option value="elf">Elf</option>
                                                        <?php break; case 'bus': ?>
                                                        <option value="sedan">Sedan</option>
                                                        <option value="minibus">Minibus</option>
                                                        <option value="microbus">Microbus</option>
                                                        <option value="bus" selected>Bus</option>
                                                        <option value="elf">Elf</option>
                                                        <?php break; case 'elf': ?>
                                                        <option value="sedan">Sedan</option>
                                                        <option value="minibus">Minibus</option>
                                                        <option value="microbus">Microbus</option>
                                                        <option value="bus">Bus</option>
                                                        <option value="elf" selected>Elf</option>
                                                        <?php break; endswitch; ?>
                                                    </select>
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['model'])) { echo $var_error['model'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="frm_total_seat" class=" control-label">Jumlah kursi <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control mask_seat" id="frm_total_seat" name="frm_total_seat" placeholder="Jumlah kursi" value="<?= $var_data_armada['armada_seat'] ?>" min="0">
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['seat'])) { echo $var_error['seat'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->

                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="frm_color" class=" control-label">Warna <span class="required">*</span></label>
                                                <select class="form-control select2" name="frm_color" id="frm_color">
                                                    <?php switch($var_data_armada['armada_color']): case 'Merah' : ?>
                                                    <option value="Merah" selected>Merah</option>
                                                    <option value="Jingga">Jingga</option>
                                                    <option value="Kuning">Kuning</option>
                                                    <option value="Putih">Putih</option>
                                                    <option value="Biru">Biru</option>
                                                    <option value="Nila">Nila</option>
                                                    <option value="Ungu">Ungu</option>
                                                    <option value="Hitam">Hitam</option>
                                                    <?php break; case 'Jingga': ?>
                                                    <option value="Merah">Merah</option>
                                                    <option value="Jingga" selected>Jingga</option>
                                                    <option value="Kuning">Kuning</option>
                                                    <option value="Putih">Putih</option>
                                                    <option value="Biru">Biru</option>
                                                    <option value="Nila">Nila</option>
                                                    <option value="Ungu">Ungu</option>
                                                    <option value="Hitam">Hitam</option>
                                                    <?php break; case 'Kuning' : ?>
                                                    <option value="Merah">Merah</option>
                                                    <option value="Jingga">Jingga</option>
                                                    <option value="Kuning" selected>Kuning</option>
                                                    <option value="Putih">Putih</option>
                                                    <option value="Biru">Biru</option>
                                                    <option value="Nila">Nila</option>
                                                    <option value="Ungu">Ungu</option>
                                                    <option value="Hitam">Hitam</option>
                                                    <?php break; case 'Putih' : ?>
                                                    <option value="Merah">Merah</option>
                                                    <option value="Jingga">Jingga</option>
                                                    <option value="Kuning">Kuning</option>
                                                    <option value="Putih" selected>Putih</option>
                                                    <option value="Biru">Biru</option>
                                                    <option value="Nila">Nila</option>
                                                    <option value="Ungu">Ungu</option>
                                                    <option value="Hitam">Hitam</option>
                                                    <?php break; case 'Biru' : ?>
                                                    <option value="Merah">Merah</option>
                                                    <option value="Jingga">Jingga</option>
                                                    <option value="Kuning">Kuning</option>
                                                    <option value="Putih">Putih</option>
                                                    <option value="Biru" selected>Biru</option>
                                                    <option value="Nila">Nila</option>
                                                    <option value="Ungu">Ungu</option>
                                                    <option value="Hitam">Hitam</option>
                                                    <?php break; case 'Nila' : ?>
                                                    <option value="Merah">Merah</option>
                                                    <option value="Jingga">Jingga</option>
                                                    <option value="Kuning">Kuning</option>
                                                    <option value="Putih">Putih</option>
                                                    <option value="Biru">Biru</option>
                                                    <option value="Nila" selected>Nila</option>
                                                    <option value="Ungu">Ungu</option>
                                                    <option value="Hitam">Hitam</option>
                                                    <?php break; case 'Ungu' : ?>
                                                    <option value="Merah">Merah</option>
                                                    <option value="Jingga">Jingga</option>
                                                    <option value="Kuning">Kuning</option>
                                                    <option value="Putih">Putih</option>
                                                    <option value="Biru">Biru</option>
                                                    <option value="Nila">Nila</option>
                                                    <option value="Ungu" selected>Ungu</option>
                                                    <option value="Hitam">Hitam</option>
                                                    <?php break; case 'Hitam' : ?>
                                                    <option value="Merah">Merah</option>
                                                    <option value="Jingga">Jingga</option>
                                                    <option value="Kuning">Kuning</option>
                                                    <option value="Putih">Putih</option>
                                                    <option value="Biru">Biru</option>
                                                    <option value="Nila">Nila</option>
                                                    <option value="Ungu">Ungu</option>
                                                    <option value="Hitam" selected>Hitam</option>
                                                    <?php break; endswitch; ?>
                                                </select>
                                                <span class="help-block warning-text"><?php if (!empty($var_error['color'])) { echo $var_error['color'];
                                                } ?></span>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                    </div>
                                </div>
                                <!-- /.col-sm-6 -->

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="frm_chassis_number" class=" control-label">Nomor rangka <span class="required">*</span></label>
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control mask_chassis" id="frm_chassis_number" name="frm_chassis_number" placeholder="Nomor rangka" value="<?= $var_data_armada['armada_chassisnumber'] ?>">
                                            <span class="help-block warning-text"><?php if (!empty($var_error['chassis'])) { echo $var_error['chassis'];
                                            } ?></span>
                                        </div>
                                    </div>
                                    <!-- /form-group -->

                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="frm_prodcution_year" class=" control-label">Tahun <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control mask_year" id="frm_prodcution_year" name="frm_prodcution_year" placeholder="Tahun Produksi" value="<?= $var_data_armada['armada_productionyear'] ?>">
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['production_year'])) { echo $var_error['production_year'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>

                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                <label for="frm_engine_number" class=" control-label">Nomor Mesin <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control mask_engine" id="frm_engine_number" name="frm_engine_number" placeholder="Nomor Mesin" value="<?= $var_data_armada['armada_enginenumber'] ?>">
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['engine_number'])) { echo $var_error['engine_number'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="frm_fuel_type" class=" control-label">Tipe bahan bakar <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <select class="form-control select2" name="frm_fuel_type" id="frm_fuel_type">
                                                        <?php switch ($var_data_armada['armada_typefuel']) : case 'bensin' : ?>
                                                        <option value="bensin" selected>Bensin</option>
                                                        <option value="solar">Solar</option>
                                                        <?php break; case 'solar' : ?>
                                                        <option value="bensin">Bensin</option>
                                                        <option value="solar" selected>Solar</option>
                                                        <?php break; endswitch; ?>
                                                    </select>
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['fuel_type'])) { echo $var_error['fuel_type'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="frm_fuel_size" class=" control-label">Kap. Tangki <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control mask_number" id="frm_fuel_size" name="frm_fuel_size" placeholder="Kapasitas bahan bakar (Liter)" value="<?= $var_data_armada['armada_fuelsize'] ?>" maxlength="3">
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['fuel_size'])) { echo $var_error['fuel_size'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="frm_odometer" class=" control-label">Odometer <span class="required">*</span></label>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control mask_number" id="frm_odometer" name="frm_odometer" placeholder="Odometer (KM)" value="<?= $var_data_armada['armada_odometer'] ?>" maxlength="7">
                                                    <span class="help-block warning-text"><?php if (!empty($var_error['odometer'])) { echo $var_error['odometer'];
                                                    } ?></span>
                                                </div>
                                            </div>
                                            <!-- /form-group -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="frm_status" class=" control-label">Pilih Status <span class="required">*</span></label>
                                            <div class="input-icon right">
                                                <select class="form-control select2" name="frm_status" id="frm_status">
                                                    <?php switch ($var_data_armada['armada_status']) : case 'approved' : ?>
                                                    <option value="approved" selected>Setujui</option>
                                                    <option value="rejected">Tolak</option>
                                                    <?php break; case 'rejected' : ?>
                                                    <option value="approved">Setujui</option>
                                                    <option value="rejected" selected>Tolak</option>
                                                    <?php break; endswitch; ?>
                                                </select>
                                                <span class="help-block warning-text"><?php if (!empty($var_error['status'])) { echo $var_error['status'];
                                                } ?></span>
                                            </div>
                                        </div>
                                        <!-- /form-group -->
                                    </div>
                                    </div>
                                </div>
                                <!-- /.col-sm-6 -->
                            </div>
                        </form>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn green" name="btn_update_armada"><i class="fa fa-save"></i> Ubah</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    <?php endwhile; ?>

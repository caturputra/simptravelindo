<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk menampikan daftar pesanan pengguna
*/

require_once '../core/init.php';

$var_title = "Pesanan";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
    // 'jcountdown' => '../assets/global/plugins/countdown/css/jquery.countdown.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-info bg-dark">
            <p class="font-white"><i class="fa fa-info"></i> Segera lakukan pembayaran untuk mengunci pesanan anda.
                <ul class="font-white m-b-10">
                    <li>Batas pembayaran untuk keberangkatan <?= dateFormat(date('Y-m-d')) ?> adalah 1 jam dari persetujuan</li>
                    <li>Batas pembayaran untuk keberangkatan lebih dari <?= dateFormat(date('Y-m-d', strtotime('+3 days', strtotime(date('Y-m-d'))))) ?> (3 hari) adalah 2 jam dari persetujuan</li>
                    <li>Batas pembayaran untuk keberangkatan lebih dari <?= dateFormat(date('Y-m-d', strtotime('+7 days', strtotime(date('Y-m-d'))))) ?> (7 hari) adalah 4 jam dari persetujuan</li>
                </ul>
            </p>
        </div>
    </div>
</div>

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<div class="row">
    <div class="col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-basket-loaded font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Daftar Pesanan</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="table-responsive">
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped table-bordered" id="tableListOrder">
                                <thead>
                                    <th class="text-center" style="width: 2em;">No.</th>
                                    <th class="text-center">Tanggal pesan</th>
                                    <th class="text-center">Berangkat-Pulang</th>
                                    <th class="text-center">Persetujuan</th>
                                    <th class="text-center">Status</th>
                                    <th></th>
                                </thead>
                                <tbody>
                                    <?php
                                    //menampilkan order
                                    $var_sql_order = "
                                    SELECT ot.od_id, otd.armada_regnumber, ot.od_orderdate, ot.od_invoice, ot.od_service, ot.od_selling, ot.od_from, ot.od_to, ot.od_request, ot.od_status, ot.od_confirmedby, u.user_detail_firstname, u.user_detail_lastname, u.user_detail_phone, ot.od_modifyat
                                    FROM sp_order_transaction ot
                                    JOIN sp_order_transaction_detail otd ON otd.od_id = ot.od_id
                                    JOIN sp_user_detail u ON u.user_id = ot.user_id
                                    WHERE ot.user_id = '{$_SESSION['userid']}'
                                    GROUP BY otd.od_id
                                    ORDER BY ot.od_id DESC
                                    ";
                                    $var_query_order = mysqli_query($var_con, $var_sql_order);
                                    $num = (int) 1;
                                    while ($var_data_order = mysqli_fetch_array($var_query_order)) :
                                        ?>
                                        <input type="hidden" name="frm_order_id" value="<?= $var_data_order['od_id']?>">
                                        <tr>
                                            <td class="text-center">
                                                <?php echo $num++; ?>
                                            </td>
                                            <td class="text-center" style="width: 1em;">
                                                <?php $var_date =  dateFormatHours($var_data_order['od_orderdate']); echo $var_date['date'] ?>
                                            </td>
                                            <td class="text-center">
                                                <?php $var_date =  dateFormatHours($var_data_order['od_from']); echo $var_date['date']; ?> <?php echo $var_date['hours']; ?> -  <?php $var_date =  dateFormatHours($var_data_order['od_to']); echo $var_date['date']; ?> <?php echo $var_date['hours']; ?>
                                            </td>
                                            <td class="text-center" style="width: 1em;">
                                                <?php if ($var_data_order['od_status'] == 0): ?>
                                                    <label class="label label-sm bg-red-mint">Tidak</label>
                                                <?php else: ?>
                                                    <label class="label label-sm bg-dark">Ya</label>
                                                <?php endif; ?>
                                                <div data-countdown="<?= $var_data_order['od_modifyat'] ?>"></div>
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                switch ($var_data_order['od_status']) {
                                                    case '0':
                                                    echo '<span class="label label-info label-sm">' . ucwords("ditunda") . '</span>';
                                                    break;

                                                    case '1':
                                                    echo '<span class="label label-success label-sm">' . ucwords("dipesan") . '</span>';
                                                    break;

                                                    case '2':
                                                    echo '<span class="label label-danger label-sm">' . ucwords("ditolak") . '</span>';
                                                    break;

                                                    case '3':
                                                    echo '<span class="label label-warning label-sm">' . ucwords("dibatalkan") . '</span>';
                                                    break;

                                                    case '4':
                                                    echo '<span class="label label-default label-sm">' . ucwords("definit") . '</span>';
                                                    break;
                                                }
                                                ?>
                                            </td>
                                            <td class="text-center">
                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Kirim Pesan" id="<?php echo $var_data_order['od_id'] ?>" class="btn btn-sm purple btn_send_message" onclick="send_message('member-order-process.php?id=<?php echo urlencode($var_data_order['od_id']) ?>&amp;act=<?php echo urlencode('sendmes') ?>');"><i class="fa fa-paper-plane"></i> </a>

                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Detail Pemesanan" class="btn btn-sm blue open_modal_detail" id="<?php echo $var_data_order['od_id']; ?>"><i class="fa fa-eye"></i> </a>

                                                <?php switch ($var_data_order['od_status']) : case '0': ?>
                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Batalkan pesanan" id="<?php echo $var_data_order['od_id'] ?>" class="btn btn-sm yellow btn_cancel_modal" onclick="confirm_modal_cancel('member-order-process.php?id=<?php echo urlencode($var_data_order['od_id']) ?>&amp;act=<?php echo urlencode('cancel') ?>');"><i class="fa fa-exclamation-triangle"></i> </a>

                                                <?php break; case '1': ?>
                                                <!-- <a href="index.php?p=booking-payment.php&amp;id=<?php echo $var_data_order['od_id']; ?>&amp;invoice=<?php echo $var_data_order['od_invoice']; ?>" data-toggle="tooltip" data-placement="bottom" title="Bayar" class="btn btn-sm green-meadow"><i class="fa fa-print"></i> Bayar</a> -->
                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Batalkan pesanan" id="<?php echo $var_data_order['od_id'] ?>" class="btn btn-sm yellow btn_cancel_modal" onclick="confirm_modal_cancel('member-order-process.php?id=<?php echo urlencode($var_data_order['od_id']) ?>&amp;act=<?php echo urlencode('cancel') ?>');"><i class="fa fa-exclamation-triangle"></i> </a>
                                                <a href="index.php?p=booking-invoice.php&amp;id=<?php echo base64_encode($var_data_order['od_id']); ?>&amp;invoice=<?php echo base64_encode($var_data_order['od_invoice']); ?>" data-toggle="tooltip" data-placement="bottom" title="Bayar (Invoice)" class="btn btn-sm green-meadow"><i class="fa fa-credit-card"></i> </a>

                                                <br >
                                                <?php
                                                if ($var_data_order['od_status'] == 1) {

                                                    $time = strtotime($var_data_order['od_modifyat']);

                                                    //cek tanggal
                                                    $interval1 = date_diff(date_create($var_data_order['od_modifyat']), date_create(date($var_data_order['od_from'])));
                                                    if ($interval1->format('%d') >= 0 && $interval1->format('%d') < 3 ) {
                                                        $var_comparedate = date('Y-m-d H:i:s', strtotime('+1 hours', $time));
                                                        echo '<div id="shortly'. $var_data_order['od_id'] .'" class="btn yellow-gold btn shortly" style="margin-top: 10px;">' . $var_comparedate . '</div>';

                                                    } else if ($interval1->format('%d') >= 3 && $interval1->format('%d') < 7) {
                                                        $var_comparedate = date('Y-m-d H:i:s', strtotime('+2 hours', $time));
                                                        echo '<div id="shortly'. $var_data_order['od_id'] .'" class="btn yellow-gold btn shortly" style="margin-top: 10px;">' . $var_comparedate . '</div>';

                                                    } else if ($interval1->format('%d') >= 7 ) {
                                                        $var_comparedate = date('Y-m-d H:i:s', strtotime('+4 hours', $time));
                                                        echo '<div id="shortly'. $var_data_order['od_id'] .'" class="btn yellow-gold btn shortly" style="margin-top: 10px;">' . $var_comparedate . '</div>';
                                                    }
                                                }
                                                ?>

                                                <?php break; case '4': ?>
                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Batalkan pesanan" id="<?php echo $var_data_order['od_id'] ?>" class="btn btn-sm yellow btn_cancel_modal" onclick="confirm_modal_cancel('member-order-process.php?id=<?php echo urlencode($var_data_order['od_id']) ?>&amp;act=<?php echo urlencode('cancel') ?>');"><i class="fa fa-exclamation-triangle"></i> </a>
                                                <?php break; endswitch; ?>
                                            </td>
                                        </tr>
                                    <?php endwhile; ?>
                                </tbody>
                            </table>
                            <!-- /.tableListOrder -->
                        </div>
                        <!-- /.col-sm-12 -->
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.porlet-body -->
        </div>
        <!-- /.portlet -->
    </div>
    <!-- /.col-sm-6 -->
</div>
<!-- END CONTENT -->

<!-- modal untuk view detail data -->
<div class="detail-modal">
    <div id="ModalDetail" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    </div>
    <!-- /.modal -->
</div>
<!-- /.view detail-modal -->

<!-- modal untuk cancel data -->
<div class="cancel-modal">
    <div id="ModalCancel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <form class="form-horizontal" action="member-order-process.php?act=cancel" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="frm_reason" class="control-label col-sm-2 col-xs-4">Alasan <span class="required">*</span></label>
                            <div class="col-sm-10 col-xs-8">
                                <textarea type="text" class="form-control" name="frm_reason" id="frm_reason" placeholder="Alasan pembatalan" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                        <button type="submit" class="btn red" name="btn_cancel"><i class="fa fa-exclamation-triangle"></i> Batal</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.cancel-modal -->

<!-- modal untuk send message -->
<div class="sendmessage-modal">
    <div id="ModalSendMessage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Kirim Pesan</h4>
                </div>
                <form class="form-horizontal" action="member-order-process.php?act=sendmessage" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="frm_subject" class="control-label col-sm-2 col-xs-4">Subjek <span class="required">*</span></label>
                            <div class="col-sm-10 col-xs-8">
                                <input type="text" name="frm_subject" id="frm_subject" class="form-control" placeholder="Subjek" required maxlength="32">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="frm_message" class="control-label col-sm-2 col-xs-4">Pesan <span class="required">*</span></label>
                            <div class="col-sm-10 col-xs-8">
                                <textarea type="text" class="form-control" name="frm_message" id="frm_message" placeholder="Isi Pesan" required cols="4" rows="15" maxlength="500"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                        <button type="submit" class="btn green" name="btn_send_message"><i class="fa fa-paper-plane"></i> Kirim</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.sendmessage-modal -->
<?php
$footer_component = [
    'datatableglobalscript' => '../assets/global/scripts/datatable.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'datableplugin' => '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
    'jcountdownplugin' => '../assets/global/plugins/countdown/js/jquery.plugin.js',
    'jcountdown' => '../assets/global/plugins/countdown/js/jquery.countdown.js',
    'jcountdownlocal' => '../assets/global/plugins/countdown/js/jquery.countdown-id.js',
];
require_once 'inc-template/inc-template-footer.php';
?>
<script type="text/javascript">
function confirm_modal_cancel(cancel_url) {
    $('#ModalCancel').modal('show', {
        backdrop: 'static'
    });
    document.getElementById('cancel_order').setAttribute('href', cancel_url);
}

function send_message(url) {
    $('#ModalSendMessage').modal('show', {
        backdrop: 'static'
    });
}

$(document).ready(function() {
    $('input[name=frm_order_id]').each(function(key, value) {
        var id  = value.value;
        var temp = $('#shortly'+id).text();
        var countdown = new Date(temp);

        $('#shortly'+id).countdown({
            until: countdown,
            layout: 'Jatuh tempo: <b>{hnn}{sep}{mnn}{sep}{snn}</b>',
            onExpiry: ajaxProcess(id),
        });
    });

    function ajaxProcess(id){
        $.ajax({
            url : "ajax-update-booking.php" ,
            type: "GET",
            data: {id:id},
            success: function(response) {
                console.log(response);
                // if (response === true) {
                //     alert('success');
                // } else {
                //     console.log(response);
                // }
            }
        });
    }

    $('.btn_send_message').click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "ajax-get-id.php",
            type: "GET",
            data: { id: id, },
            success: function(ajaxData) {
                var data = JSON.parse(ajaxData);
                $.each(data, function(key, val){
                    $('.modal-body').prepend('<input type=\'hidden\' name=\'frm_order_id\' value=\''+ val.od_id +'\'>');
                });
            }
        });
    });

    $('.btn_cancel_modal').click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "ajax-get-id.php",
            type: "GET",
            data: { id: id, },
            success: function(ajaxData) {
                var data = JSON.parse(ajaxData);
                $.each(data, function(key, val){
                    $('.modal-body').prepend('<input type=\'hidden\' name=\'frm_order_id\' value=\''+ val.od_id +'\'>');
                });
            }
        });
    });

    $(".open_modal_detail").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "booking-detail.php",
            type: "GET",
            data: {
                orderid: id,
            },
            success: function(ajaxData) {
                $("#ModalDetail").html(ajaxData);
                $("#ModalDetail").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    // $(".open_modal_cancel").click(function(e) {
    //     var id = $(this).attr("id");
    //     $.ajax({
    //         url: "member-order-cancel.php",
    //         type: "GET",
    //         data: {
    //             id: id,
    //         },
    //         success: function(ajaxData) {
    //             $("#ModalCancel").html(ajaxData);
    //             $("#ModalCancel").modal('show', {
    //                 backdrop: 'true'
    //             });
    //         }
    //     });
    // });

    $("#tableListOrder").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autowidth": false,
        "lengthChange": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        }
    });
});
</script>

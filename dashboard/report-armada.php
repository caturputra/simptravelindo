<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk form laporan armada dll
*/

require_once '../core/init.php';

$var_title = "Laporan";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- CONTENT -->
<div class="row">
    <div class="col-sm-4">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-book"></i> Pemesanan Armada
                </div>
            </div>
            <div class="portlet-body">
                <form class="" action="print-report.php?act=pa" method="post">
                    <div class="form-group">
                        <label for="frm_armada" class="control-label">Periode</label>
                        <div class="mt-radio-list">
                            <label class="mt-radio">
                                <input type="radio" name="frm_armada" id="frm_armada" value="1" checked> Tanggal
                                <span></span>
                            </label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="frm_datefrom" class="control-label">Dari</label>
                                    <input type="text" class="form-control mask_date2" name="frm_datefrom" id="frm_datefrom" value="">
                                </div>
                                <div class="col-sm-6">
                                    <label for="frm_dateto" class="control-label">Sampai</label>
                                    <input type="text" class="form-control mask_date2" name="frm_dateto" id="frm_dateto" value="">
                                </div>
                            </div>
                            <br>
                            <label class="mt-radio">
                                <input type="radio" name="frm_armada" id="frm_armadark" value="2"> Bulan
                                <span></span>
                            </label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <select class="form-control select2" name="frm_month">
                                        <?php
                                        $month = [
                                            '' => '',
                                            '01' => 'Januari',
                                            '02' => 'Februari',
                                            '03' => 'Maret',
                                            '04' => 'April',
                                            '05' => 'Mei',
                                            '06' => 'Juni',
                                            '07' => 'Juli',
                                            '08' => 'Agustus',
                                            '09' => 'September',
                                            '10' => 'Oktober',
                                            '11' => 'November',
                                            '12' => 'Desember',
                                        ]
                                        ?>
                                        <?php foreach ($month as $key => $value) : ?>
                                            <option value="<?= $key ?>" <?php if ($key == date('m')) echo 'selected'?>><?= $value ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <select class="form-control select2" name="frm_year">
                                        <option value=""></option>
                                        <?php for($i=date('Y')-5;$i<=date('Y');$i++): ?>
                                            <option value="<?= $i ?>" selected><?= $i ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pull-right">
                                <button type="submit" class="btn green" name="btn_generate" formtarget="_blank"><i class="fa fa-clone"></i> Buat Laporan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Portlet PORTLET-->
    </div>

    <div class="col-sm-4">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet box dark">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-book"></i> Armada
                </div>
            </div>
            <div class="portlet-body">
                <!-- List group -->
                <ul class="list-group">
                    <li class="list-group-item"> Laporan Armada
                        <span class="pull-right"><a href="print-report.php?act=vh" class="btn btn-xs green" target="_blank"> Generate </a></span>
                    </li>
                    <li class="list-group-item"> Laporan Kru Armada
                        <span class="pull-right"><a href="print-report.php?act=ca" class="btn btn-xs green" target="_blank"> Generate </a></span>
                    </li>
                </ul>
            </div>
        </div>
        <!-- END Portlet PORTLET-->
    </div>
</div>
<!-- END CONTENT -->

<?php
$footer_component = [
    'datatableglobalscript' => '../assets/global/scripts/datatable.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'datableplugin' => '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    //'sample' => '../assets/custom/form-validation-user-add.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
?>
<?php require_once 'inc-template/inc-template-footer.php'; ?>
<script type="text/javascript">
$(document).ready(function() {
    $("#tableListpayments").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autowidth": false,
        "lengthChange": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        }
    });
});
</script>

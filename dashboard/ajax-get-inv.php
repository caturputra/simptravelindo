<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Ajax untuk ambil data invoice
*/

require_once '../core/init.php';

$var_action = isset($_GET['for']) ? mysqli_escape_string($var_con, filter_var($_GET['for'], FILTER_SANITIZE_STRING)) : "";

$var_id = isset($_POST['id']) ? mysqli_escape_string($var_con, filter_var($_POST['id'], FILTER_SANITIZE_STRING)) : "";
$var_sql_schedule = "
SELECT ot.od_id, ot.od_service, ot.od_from, ot.od_to, ot.od_request, concat(ud.user_detail_firstname, ' ', ud.user_detail_lastname) as od_customer
FROM sp_order_transaction ot
JOIN sp_user_detail ud ON ud.user_id = ot.user_id
WHERE ot.od_invoice = '{$var_id}'
LIMIT 1
";

$var_query_schedule = mysqli_query($var_con, $var_sql_schedule);
$var_numrows_schedule = mysqli_num_rows($var_query_schedule);
$var_data_schedule = mysqli_fetch_array($var_query_schedule);

switch (strtolower($var_action)) {
    case 'inv':
    $var_schedule_id = '';

    if ($var_numrows_schedule > 0) {
        $var_data = [];
        $var_temp = [
            'service' => $var_data_schedule['od_service'],
            'from' => $var_data_schedule['od_from'],
            'to' => $var_data_schedule['od_to'],
            'request' => $var_data_schedule['od_request'],
            'customer' => $var_data_schedule['od_customer'],
        ];

        $var_data2 =  array_push($var_data, $var_temp);
        $var_service = '';
        switch ($var_temp['service']) { case '1':$var_service = 'City Tour'; break; case '2': $var_service = 'Transfer In/Out'; break; }
        $html = '
        <label class="control-label">Nama: ' . $var_temp['customer'] . '</label><br>
        <label class="control-label">Berangkat: ' . dateFormat($var_temp['from']) . '</label><br>
        <label class="control-label">Pulang: ' . dateFormat($var_temp['to']) . '</label><br>
        <label class="control-label">Layanan: '. $var_service .'</label><br>
        ';
        echo $html;
    }
    break;

    case 'detail':
    $var_data = [];
    $var_temp = [
        'service' => $var_data_schedule['od_service'],
        'from' => $var_data_schedule['od_from'],
        'to' => $var_data_schedule['od_to'],
        'request' => $var_data_schedule['od_request'],
        'customer' => $var_data_schedule['od_customer'],
    ];

    $var_data2 =  array_push($var_data, $var_temp);
    echo json_encode($var_data2);
    break;
}
?>

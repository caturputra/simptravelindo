<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk konfirmasi penugasan oleh kru armada
*/

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';

//get action
$var_uid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "";
$var_action = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var($_GET['act'], FILTER_SANITIZE_STRING)) : "";
$var_caid = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : "";
$var_caid = base64_decode($var_caid);
$var_sid = isset($_GET['s']) ? mysqli_escape_string($var_con, filter_var($_GET['s'], FILTER_SANITIZE_STRING)) : "";
$var_sid = base64_decode($var_sid);

$var_datefrom = isset($_GET['from']) ? mysqli_escape_string($var_con, $_GET['from']) : "";
$var_dateto = isset($_GET['to']) ? mysqli_escape_string($var_con, $_GET['to']) : "";

$var_sql_sender = "SELECT crew_createdby FROM sp_crew_detail WHERE crew_armadaid = '{$var_caid}' LIMIT 1";
$var_get_sender = mysqli_fetch_row(mysqli_query($var_con, $var_sql_sender));

switch(strtolower($var_action)) {
    case 'accept':

    $var_sql_check = "
    SELECT cd.crew_armadaid FROM sp_crew_detail cd WHERE ((cd.crew_datefrom BETWEEN '{$var_datefrom}' AND '{$var_dateto}') OR (cd.crew_dateto BETWEEN '{$var_datefrom}' AND '{$var_dateto}' )) AND (cd.crew_action IN ('0','1'))
    ";
    $var_query_check = mysqli_query($var_con, $var_sql_check);
    
    if (mysqli_num_rows($var_query_check) > 0) {
        setNotif(['warning' => 'Anda telah ditugaskan pada tanggal tersebut.']);
        routeUrl('index.php?p=crew-assignment');
        die();
    }

    $var_data_assignment = ['crew_action' => '1'];
    $var_cond_assignment = ['crew_armadaid' => $var_caid, 'schedule_id' => $var_sid];
    $var_update_assignment = update($var_con, "sp_crew_detail", $var_data_assignment, $var_cond_assignment);

    if ($var_update_assignment) {
        notification($var_con, [
            'from' => $_SESSION['userid'],
            'to' => $var_get_sender[0],
            'description' => 'Kru ' . $_SESSION['username'] . ' menerima penugasan.',
            'type' => 'konfirmasi',
        ]);
        setNotif(['success' => 'Terima kasih telah menerima Penugasan, silahkan tunggu informasi selanjutnya.']);
        routeUrl('index.php?p=crew-assignment');
        die();
    } else {
        setNotif(['danger' => 'Galat!']);
        routeUrl('index.php?p=crew-assignment');
        die();
    }
    break;

    case 'reject':
    $var_data_assignment = ['crew_action' => '2'];
    $var_cond_assignment = ['crew_armadaid' => $var_caid, 'schedule_id' => $var_sid];
    $var_update_assignment = update($var_con, "sp_crew_detail", $var_data_assignment, $var_cond_assignment);

    if ($var_update_assignment) {
        notification($var_con, [
            'from' => $_SESSION['userid'],
            'to' => $var_get_sender[0],
            'description' => 'Kru ' . $_SESSION['username'] . ' menolak penugasan.',
            'type' => 'Tindakan',
        ]);
        setNotif(['success' => 'Penolakan penugasan berhasil diproses.']);
        routeUrl('index.php?p=crew-assignment');
        die();
    } else {
        setNotif(['danger' => 'Galat!']);
        routeUrl('index.php?p=crew-assignment');
        die();
    }
    break;

    case 'cancel':
    //update 3-01-2018 limit 12 jam pembatalan penugasan
    $var_sql_check = "SELECT crew_datefrom FROM sp_crew_detail WHERE crew_armadaid ='{$var_caid}' AND schedule_id = '{$var_sid}'";
    $var_query_check = mysqli_query($var_con, $var_sql_check);
    if (mysqli_num_rows($var_query_check) > 0) {
        $var_data_check = mysqli_fetch_row($var_query_check);
        $interval = date_diff(date_create(date('Y-m-d H:i')), date_create($var_data_check[0]));

        if ($interval->format('%h') >= 12) {
            setNotif(['danger' => 'Anda tidak bisa membatalkan tugas karena kurang dari 12 Jam waktu berangkat, Hubungi admin untuk informasi lebih lanjut.']);
            routeUrl('index.php?p=crew-assignment');
            die();
        } else {
            $var_data_assignment = ['crew_action' => '3'];
            $var_cond_assignment = ['crew_armadaid' => $var_caid, 'schedule_id' => $var_sid];
            $var_update_assignment = update($var_con, "sp_crew_detail", $var_data_assignment, $var_cond_assignment);

            if ($var_update_assignment) {
                notification($var_con, [
                    'from' => $_SESSION['userid'],
                    'to' => $var_get_sender[0],
                    'description' => 'Kru ' . $_SESSION['username'] . ' membatalkan penugasan.',
                    'type' => 'konfirmasi',
                ]);
                setNotif(['success' => 'Tugas berhasil dibatalkan.']);
                routeUrl('index.php?p=crew-assignment');
                die();
            } else {
                setNotif(['danger' => 'Galat!']);
                routeUrl('index.php?p=crew-assignment');
                die();
            }
            break;
        }
    }
}

$var_title = "Penugasan";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<div class="row">
    <?php
    $var_sql_assign = "
    SELECT d.crew_armadaid, d.crew_datefrom, d.crew_dateto, d.schedule_id, a.armada_image, concat(a.armada_brand, ' ', a.armada_typevariant) as armada_brand, d.crew_action, s.schedule_id
    FROM sp_crew_detail d
    JOIN sp_schedule s ON s.schedule_id = d.schedule_id
    JOIN sp_order_transaction ot ON ot.od_id = s.od_id
    JOIN sp_armada a ON a.armada_regnumber = ot.armada_regnumber
    JOIN sp_crew_armada ca ON ca.crew_armadaid = d.crew_armadaid
    WHERE ca.user_id = '{$var_uid}' AND s.schedule_status = '1'
    ORDER BY crew_detailid DESC
    ";
    $var_query_assign = mysqli_query($var_con, $var_sql_assign);
    $var_numrows_assign = mysqli_num_rows($var_query_assign);
    ?>
    <?php if ($var_numrows_assign > 0): ?>
        <?php while ($var_data_assign = mysqli_fetch_array($var_query_assign)) : ?>
            <div class="col-sm-4 col-sm-6 col-xs-12">
            <style>
            /* Large Devices, Wide Screens */
            @media only screen and (min-width : 1200px) {
                img.img-thumbnails {
                    width: 100%; height: 15em; width: 22em; margin: 1em; margin-left: 7em;
                }
            }

            /* Medium Devices, Desktops */
            @media only screen and (max-width : 992px) {
                img.img-thumbnails {
                    width: 100%; height: 15em; width: 22em; margin: 1em; margin-left: 4em;
                }
            }

            /* Small Devices, Tablets */
            @media only screen and (max-width : 768px) {
                img.img-thumbnails {
                    width: 100%; height: 15em; width: 22em; margin: 1em; margin-left: 10em;
                }
            }

            /* Extra Small Devices, Phones */
            @media only screen and (max-width : 480px) {
                img.img-thumbnails {
                    width: 100%; height: 15em; width: 22em; margin: 1em; margin-left: 2em;
                }
            }

            /* Custom, iPhone Retina */
            @media only screen and (max-width : 320px) {
                img.img-thumbnails {
                    width: 100%; height: 15em; width: 22em; margin: 1em; margin-left: 0.1em;
                }
            }
            </style>
                <div class="thumbnail">
                    <div style="height: 170px; margin-top: -1em;">
                        <img src="../images/armada/<?= $var_data_assign['armada_image'] ?>" alt="<?= $var_data_assign['armada_brand'] ?>" class="img img-responsive img-thumbnails">
                    </div>
                    <div class="caption">
                        <h3>
                            <?= $var_data_assign['armada_brand'] ?>
                            <?php switch($var_data_assign['crew_action']): case '1': ?>
                            <label class="label label-success label-sm">Diterima</label>
                            <?php break; case '2': ?>
                            <label class="label label-danger label-sm">Ditolak</label>
                            <?php break; case '3': ?>
                            <label class="label label-warning label-sm">Dibatalkan</label>
                            <?php endswitch; ?>
                        </h3>
                        <ul class="list-group">
                            <li class="list-group-item"> Tanggal Berangkat
                                <span class="badge badge-default"> <?= dateFormat($var_data_assign['crew_datefrom']) ?> </span>
                            </li>
                            <li class="list-group-item"> Tanggal Pulang
                                <span class="badge badge-success"> <?= dateFormat($var_data_assign['crew_dateto']) ?> </span>
                            </li>
                        </ul>
                        <?php switch ($var_data_assign['crew_action']) : case '0': ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Terima" class="btn red pull-left" onclick="confirm_reject('index.php?p=crew-assignment.php&amp;id=<?php echo base64_encode($var_data_assign['crew_armadaid']) ?>&amp;s=<?= base64_encode($var_data_assign['schedule_id'])?>&amp;act=reject');"><i class="fa fa-ban"></i> Tolak</a>
                                    <div class="pull-right">
                                        <a href="#" data-toggle="tooltip" data-placement="bottom" title="Terima" class="btn blue" onclick="confirm_approve('index.php?p=crew-assignment.php&amp;id=<?php echo base64_encode($var_data_assign['crew_armadaid']) ?>&amp;s=<?= base64_encode($var_data_assign['schedule_id'])?>&amp;act=accept&amp;from=<?= $var_data_assign['crew_datefrom'] ?>&amp;to=<?= $var_data_assign['crew_dateto'] ?>');"><i class="fa fa-check-square-o"></i> Terima</a>
                                    </div>
                                </div>
                            </div>
                        <?php break; case '1': ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Terima" class="btn red" onclick="confirm_cancel('index.php?p=crew-assignment.php&amp;id=<?php echo base64_encode($var_data_assign['crew_armadaid']) ?>&amp;s=<?= base64_encode($var_data_assign['schedule_id'])?>&amp;act=cancel');"><i class="fa fa-ban"></i> Batalkan</a>
                                </div>
                            </div>
                        </div>
                        <?php break; endswitch; ?>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    <?php else: ?>
        <div class="col-sm-12">
            <div class="note note-info">
                <span class="fa fa-signal"></span> Tidak ada penugasan untuk anda!
            </div>
        </div>
    <?php endif; ?>
</div>
<!-- /.row -->

<!-- MODAL DETAIL CREW -->
<div class="detail-modal">
    <div id="ModalDetail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Detail Crew</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center"></h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- END MODAL DETAIL CREW -->

<!-- BEGIN MODAL APPROVED CREW -->
<div class="approve-modal">
    <div id="ModalApproved" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Yakin menerima penugasan?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                    <a href="#"class="btn green" id="url_link"><i class="fa fa-check"></i> Terima</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- END MODAL APPROVED CREW -->

<!-- BEGIN MODAL REJECT CREW -->
<div class="reject-modal">
    <div id="ModalRejected" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Yakin menolak penugasan?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                    <a href="#"class="btn red" id="url_link_reject"><i class="fa fa-ban"></i> Tolak</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- END MODAL REJECT CREW -->

<!-- BEGIN MODAL CANCELLED CREW -->
<div class="cancel-modal">
    <div id="ModalCanceled" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Yakin membatalkan penugasan?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                    <a href="#"class="btn green" id="url_link_cancel"><i class="fa fa-exclamation-triangle"></i> Batalkan</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- END MODAL CANCELLED CREW -->

<?php
$footer_component = [
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'sample' => '../assets/custom/form-validation-user-add.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
require_once 'inc-template/inc-template-footer.php';
?>

<script type="text/javascript">
function confirm_approve(url) {
    $('#ModalApproved').modal('show', {backdrop: 'static'});
    document.getElementById('url_link').setAttribute('href' , url);
}

function confirm_reject(url) {
    $('#ModalRejected').modal('show', {backdrop: 'static'});
    document.getElementById('url_link_reject').setAttribute('href' , url);
}

function confirm_cancel(url) {
    $('#ModalCanceled').modal('show', {backdrop: 'static'});
    document.getElementById('url_link_cancel').setAttribute('href' , url);
}
$(document).ready(function () {
    $(".open_modal_detail").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "managecrew-detail.php",
            type: "GET",
            data: { id: id, },
            success: function(ajaxData) {
                $("#ModalDetail").html(ajaxData);
                $("#ModalDetail").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $("#tableListcrew").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autowidth": false,
        "lengthChange": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        }
    });
});
</script>

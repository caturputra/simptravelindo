<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk memperbaharui data pengguna secara mandiri
*/

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';
require_once '../inc/function/inc-func-users.php';

//username id
$var_userid = isset($_SESSION['userid']) ? mysqli_real_escape_string($var_con, $_SESSION['userid']) : "";

//prosedur untuk update profile
if (isset($_POST['btn_update_profile'])) {
    // deklarasi variabel untuk nilai form
    $var_firstname = isset($_POST['frm_firstname']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_firstname'], FILTER_SANITIZE_STRING)) : "" ;
    $var_lastname = isset($_POST['frm_lastname']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_lastname'], FILTER_SANITIZE_STRING)) : "" ;
    $var_address = isset($_POST['frm_address']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_address'], FILTER_SANITIZE_STRING)) : "" ;
    $var_phone = isset($_POST['frm_phone']) ? mysqli_real_escape_string($var_con, filter_var(trim($_POST['frm_phone']), FILTER_SANITIZE_STRING)) : "" ;
    $var_gender = isset($_POST['frm_gender']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_gender'], FILTER_SANITIZE_STRING)) : "" ;
    $var_birthdate = isset($_POST['frm_birthdate']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_birthdate'], FILTER_SANITIZE_STRING)) : "" ;
    $var_error = array();

    if (empty($var_firstname)) {
        $var_error['firstname'] = "Mohon masukkan nama depan.";
    }

    if (empty($var_lastname)) {
        $var_error['lastname'] = "Mohon masukkan nama belakang.";
    }

    //validasi Gender
    if (empty($var_gender)) {
        $var_error['gender'] = "Mohon pilih jenis kelamin.";
    }

    //validasi phone number
    if (empty($var_phone)) {
        $var_error['phone'] = "Mohon masukkan telepon.";
    }

    //validasi address
    if (empty($var_address)) {
        $var_error['address'] = "Mohon masukkan alamat.";
    }

    //validasi birthdate
    if (empty($var_birthdate)) {
        $var_error['birthdate'] = "Mohon masukkan tanggal lahir.";
    }

    if (empty($var_error)) {
        // jika semua kondisi terpenuhi
        $var_update_profile =  userUpdateProfile($var_con, $var_firstname, $var_lastname, $var_gender, '+62'.$var_phone, $var_address, dateDb($var_birthdate), $var_userid);
        if ($var_update_profile) {
            setNotif(['success'=>'data pengguna berhasil diubah.']);
            routeUrl('index.php?p=manageuser-profile');
            die();
            $var_error = array();
        } else {
            setNotif(['danger'=>'data pengguna gagal diubah.']);
            routeUrl('index.php?p=manageuser-profile');
            die();
        }

    } else {
        setNotif(['warning'=>'Mohon cek kembali data pengguna.']);
        routeUrl('index.php?p=manageuser-profile');
        die();
    }
}

//prosedur penggantian password
if (isset($_POST['btn_change_password'])) {
    //set variabel
    $var_old_password = isset($_POST['frm_old_password']) ? filter_var($_POST['frm_old_password'], FILTER_SANITIZE_STRING) : "" ;
    $var_new_password = isset($_POST['frm_new_password']) ? filter_var($_POST['frm_new_password'], FILTER_SANITIZE_STRING) : "" ;
    $var_new_password_confirm = isset($_POST['frm_new_password_confirm']) ? filter_var($_POST['frm_new_password_confirm'], FILTER_SANITIZE_STRING) : "" ;
    $var_error = array();

    //validasi
    if (empty($var_old_password)) {
        $var_error['oldPassword'] = "Mohon masukkan kata sandi lama.";
    }

    if (empty($var_new_password)) {
        $var_error['newPassword'] = "Mohon masukkan kata sandi baru.";
    }

    if (empty($var_new_password_confirm)) {
        $var_error['newPasswordConfirm'] = "Mohon masukkan konfirmasi kata sandi baru.";
    }

    if (strlen($var_new_password) < 8 || strlen($var_new_password) > 32) {
        $var_error['newPasswordConfirm'] = "Panjang password minimum 8 karakter.";
    }

    if ($var_new_password !== $var_new_password_confirm) {
        $var_error['newPasswordConfirm'] = "Kata sandi tidak sama.";
    }

    $var_sql_change = "SELECT user_password FROM sp_user WHERE user_id = '{$var_userid}'";
    $var_query_change = mysqli_query($var_con, $var_sql_change);
    $var_data_change = mysqli_fetch_row($var_query_change);

    //jika lolos validasi
    if (empty($var_error)) {
        //jalan prosedur change password
        if (mysqli_num_rows($var_query_change) > 0) {
            $var_cred_password = password_verify($var_old_password, $var_data_change[0]);

            if ($var_cred_password) {
                $var_hash_passsword = password_hash($var_new_password, PASSWORD_BCRYPT);
                $var_update = userChangePasswd($var_con, $var_hash_passsword, $var_userid);
                if ($var_update) {
                    setNotif(['success'=>'Kata sandi berhasil diubah.']);
                    $var_error = array();
                    include 'logout.php';
                } else {
                    setNotif(['danger'=>'Kata sandi gagal diubah.']);
                }
            } else {
                setNotif(['danger'=>'Kata sandi tidak dapat diubah.']);
            }
        } else {
            setNotif(['danger'=>'data pengguna tidak ditemukan.']);
        }
    } else {
        setNotif(['danger'=>'galat!']);
    }
}

//prosedur penggantian username
if (isset($_POST['btn_change_username'])) {
    //set variabel
    $var_new_username = isset($_POST['frm_new_username']) ? filter_var($_POST['frm_new_username'], FILTER_SANITIZE_STRING) : "";
    $var_error = array();

    //validasi
    if (empty($var_new_username)) {
        $var_error['newUsername'] = "Mohon masukkan nama pengguna baru.";
    }

    $var_sql_session = "SELECT user_username FROM sp_user";
    $var_query_session = mysqli_query($var_con, $var_sql_session);

    //die(print_r(mysqli_num_rows($var_query_session) > 0));
    while ($var_data_session = mysqli_fetch_row($var_query_session)) {
        if ($var_new_username === $var_data_session[0]) {
            setNotif(['danger'=>'Nama pengguna tidak dapat diubah.']);
            $var_error['newUsername'] = "Name pengguna sudah digunakan.";
        }
    }

    //prosedur
    if (empty($var_error)) {
        $var_update_username = userChangeUsername($var_con, $var_new_username, $var_userid);
        if ($var_update_username) {
            setNotif(['success'=>'Nama pengguna berhasil diubah.']);
            $var_error = array();
            include 'logout.php';
        } else {
            setNotif(['success'=>'nama pengguna gagal diubah.']);
        }
    }
}

//prosedur change avatar
if (isset($_POST['btn_change_avatar'])) {
    $var_ava_name = isset($_FILES['frm_ava_new']) ? filter_var($_FILES['frm_ava_new']) : "";
    $var_ava_filename = $_FILES['frm_ava_new']['name'];
    $var_ava_temp = $_FILES['frm_ava_new']['tmp_name'];
    $var_ava_size = $_FILES['frm_ava_new']['size'];
    $var_ava_err = $_FILES['frm_ava_new']['error'];
    $var_ava_dir = "../images/ava/";
    $var_ava_type = strtolower(pathinfo($var_ava_filename, PATHINFO_EXTENSION));
    $var_ava_ext = array("jpeg", "png", "jpg");
    $var_ava_newName = "img-ava-" . $_SESSION['userid'] .".png";
    $var_init = true;

    //validasi avatar
    if ($var_ava_size > 1024) {
        setNotif(['warning' => 'Maximum ukuran image hanya 1 MB']);
    }

    if (!getimagesize($var_ava_temp)) {
        setNotif(['warning' => 'Hanya gambar yang boleh diunggah']);
    }

    if (!in_array($var_ava_type, $var_ava_ext)) {
        setNotif(['warning' => 'hanya gambar berformat .png, .jpg, .jpeg yang diijinkan']);
    }

    if (empty($var_err) && $var_init === true) {
        if (move_uploaded_file($var_ava_temp, $var_ava_dir.$var_ava_newName)) {
            $var_update_ava = ['user_detail_avatar' => $var_ava_newName];
            $var_cond = ['user_id' => $var_userid];
            if (update($var_con, "sp_user_detail", $var_update_ava, $var_cond)) {
                setNotif(['success' => 'foto profil berhasil diubah.']);
                $var_error = array();
            } else {
                setNotif(['danger' => 'foto profil gagal diubah.']);
            }
        }
    }

}
?>
<?php
$var_title = "Profil Pengguna";

$head_component = [
    'profile' => '../assets/pages/css/profile.min.css',
    'fileinputcss' => '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
];
require 'inc-template/inc-template-header.php';
require 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<?php
$var_sql_username = "
SELECT
u.user_username, u.user_email, l.level_name, ud.user_detail_avatar, ud.user_detail_lastname, ud.user_detail_firstname, ud.user_detail_phone, ud.user_detail_birthdate, ud.user_detail_gender, ud.user_detail_address
FROM
sp_user u
JOIN
sp_level l
ON
l.level_id = u.user_level
LEFT JOIN
sp_user_detail ud
ON
ud.user_id = u.user_id
WHERE
u.user_id = '{$var_userid}'
OR
u.user_email = '{$_SESSION['userid']}'
LIMIT 1
";
$var_data_user = mysqli_fetch_array(mysqli_query($var_con, $var_sql_username));
?>

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet bordered">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="../images/ava/<?php echo $var_data_user['user_detail_avatar']; ?>" class="img img-responsive img-circle" alt="<?php echo $var_data_user['user_username']; ?>"/>
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name uppercase"> <?php echo $var_data_user['user_detail_firstname'] ." ".$var_data_user['user_detail_lastname']; ?> </div>
                    <div class="profile-usertitle-job"> <?php echo $var_data_user['level_name']; ?> </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="active">
                            <a href="page_user_profile_1_account.html">
                                <i class="icon-settings"></i> Akun Manajemen
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
        </div>
        <!-- END PROFILE SIDEBAR -->

        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Profil</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab">Data Pribadi</a>
                                </li>
                                <li>
                                    <a href="#tab_1_2" data-toggle="tab">Ubah Foto</a>
                                </li>
                                <li>
                                    <a href="#tab_1_3" data-toggle="tab">Ubah Kata Sandi</a>
                                </li>
                                <li>
                                    <a href="#tab_1_4" data-toggle="tab">Ubah Nama Pengguna</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <!-- PERSONAL INFO TAB -->
                                <div class="tab-pane active" id="tab_1_1">
                                    <form class="form-horizontal" role="form" action="manageuser-profile.php" method="post">
                                        <div class="form-group">
                                            <label for="frm_firstname" class="col-sm-2 control-label">Nama depan</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="frm_firstname" name="frm_firstname" placeholder="Nama depan" value="<?php echo $var_data_user['user_detail_firstname'] ?>">
                                                <span class="help-block warning-text"><?php if (!empty($var_error['firstname'])) {
                                                    echo $var_error['firstname'];
                                                } ?></span>
                                            </div>
                                        </div>

                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="frm_lastname" class="col-sm-2 control-label">Nama belakang</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="frm_lastname" name="frm_lastname" placeholder="Nama belakang" value="<?php echo $var_data_user['user_detail_lastname'] ?>">
                                                <span class="help-block warning-text"><?php if (!empty($var_error['lastname'])) {
                                                    echo $var_error['lastname'];
                                                } ?></span>
                                            </div>
                                        </div>

                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="frm_birthdate" class="col-sm-2 control-label">Tanggal lahir</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control mask_date2" id="frm_birthdate" name="frm_birthdate" placeholder="Tanggal lahir" value="<?php echo dateDbRev($var_data_user['user_detail_birthdate']) ?>">
                                                <span class="help-block">e.g. 1 Januari 1999 = 01011999</span>
                                                <span class="help-block warning-text"><?php if (!empty($var_error['birthdate'])) {echo $var_error['birthdate'];
                                                } ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="frm_gender" class=" control-label col-sm-2">Jenis kelamin</label>
                                            <div class="col-sm-10">
                                                <div class="row">
                                                    <?php
                                                    switch ($var_data_user['user_detail_gender']) {
                                                        case 'M':
                                                        $var_checked_male = "checked";
                                                        break;

                                                        case 'F':
                                                        $var_checked_female = "checked";
                                                        break;
                                                    }
                                                    ?>
                                                    <div class="col-sm-3">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="frm_gender" id="optionsRadios1" value="M" <?php if(isset($var_checked_male))
                                                                echo  $var_checked_male;
                                                                ?>> Laki-laki
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="frm_gender" id="optionsRadios2" value="F" <?php if(isset($var_checked_female)) echo  $var_checked_female;?>> Perempuan
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.form-group -->

                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="frm_address" class="col-sm-2 control-label">Alamat</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="frm_address" name="frm_address" rows="3" cols="40" placeholder="Alamat"><?php echo $var_data_user['user_detail_address'] ?></textarea>
                                                <span class="help-block warning-text"><?php if (!empty($var_error['address'])) {
                                                    echo $var_error['address'];
                                                } ?></span>
                                            </div>
                                        </div>

                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="frm_phone" class="col-sm-2 control-label">Telepon</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control mask_phone" id="frm_phone" name="frm_phone" placeholder="Telepon" value="<?php echo $var_data_user['user_detail_phone'] ?>">
                                                <span class="help-block warning-text"><?php if (!empty($var_error['phone'])) {
                                                    echo $var_error['phone'];
                                                } ?></span>
                                            </div>
                                        </div>
                                        <!-- /.form-group -->

                                        <div class="margiv-top-10">
                                            <div class="col-sm-offset-2">
                                                <button type="submit" class="btn green" name="btn_update_profile"> Simpan Perubahan </button>
                                                <button type="reset" class="btn default"> Batal </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- END PERSONAL INFO TAB -->
                                <!-- CHANGE AVATAR TAB -->
                                <div class="tab-pane" id="tab_1_2">
                                    <form action="manageuser-profile.php" enctype="multipart/form-data" method="post" role="form">
                                        <div class="form-group">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    <img src="../images/ava/<?php echo $var_data_user['user_detail_avatar'] ?>" alt="" /> </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Pilih foto </span>
                                                            <span class="fileinput-exists"> Ubah </span>
                                                            <input type="file" name="frm_ava_new"> </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Hapus </a>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix margin-top-10">
                                                        <span class="label label-danger">NOTE! </span>
                                                        <span>&nbsp;Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only and maximum size is 1 MB </span>
                                                    </div>
                                                </div>
                                                <div class="margin-top-10">
                                                    <button type="submit" class="btn green" name="btn_change_avatar">Unggah
                                                    </button>
                                                    <!-- <a href="javascript:;" class="btn default"> Cancel </a> -->
                                                </div>
                                            </form>
                                        </div>
                                        <!-- END CHANGE AVATAR TAB -->
                                        <!-- CHANGE PASSWORD TAB -->
                                        <div class="tab-pane" id="tab_1_3">
                                            <form class="form-horizontal" action="manageuser-profile.php" method="post">
                                                <div class="form-group">
                                                    <label for="frm_old_password" class="col-sm-4 control-label">Kata sandi lama</label>
                                                    <div class="col-sm-8">
                                                        <input type="password" class="form-control" id="frm_old_password" placeholder="Kata sandi lama" name="frm_old_password" required autofocus>
                                                        <span class="help-block warning-text"><?php if(isset($var_error['oldPassword'])) {echo $var_error['oldPassword']; } ?></span>
                                                    </div>
                                                </div>
                                                <!-- /.form-group -->

                                                <div class="form-group">
                                                    <label for="frm_new_password" class="col-sm-4 control-label">Kata sandi baru</label>
                                                    <div class="col-sm-8">
                                                        <input type="password" class="form-control" id="frm_new_password" placeholder="Kata sandi baru" name="frm_new_password" required>
                                                        <span class="help-block warning-text"><?php if(isset($var_error['newPassword'])) {echo $var_error['newPassword']; } ?></span>
                                                    </div>
                                                </div>
                                                <!-- /.form-group -->

                                                <div class="form-group">
                                                    <label for="frm_new_password_confirm" class="col-sm-4 control-label">Konfirmasi kata sandi</label>
                                                    <div class="col-sm-8">
                                                        <input type="password" class="form-control" id="frm_new_password_confirm" placeholder="Konfirmasi kata sandi" name="frm_new_password_confirm" required>
                                                        <span class="help-block warning-text"><?php if(isset($var_error['newPasswordConfirm'])) {echo $var_error['newPasswordConfirm']; } ?></span>
                                                    </div>
                                                </div>
                                                <!-- /.form-group -->

                                                <div class="form-group">
                                                    <div class="col-sm-12 col-sm-offset-4">
                                                        <button class="col-sm-4 btn green btn-flat" type="submit" name="btn_change_password">Ubah Kata Sandi
                                                        </button>
                                                    </div>
                                                </div>
                                                <!-- /.form-group -->
                                            </form>
                                            <!--/form change password  -->
                                        </div>
                                        <!-- END CHANGE PASSWORD TAB -->
                                        <!-- PRIVACY SETTINGS TAB -->
                                        <div class="tab-pane" id="tab_1_4">
                                            <form class="form-horizontal" action="" method="post" id="userCheck">
                                                <div class="form-group">
                                                    <label for="frm_new_username" class="col-sm-4 control-label">Nama pengguna baru</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="frm_new_username" placeholder="Nama pengguna baru" name="frm_new_username" required autofocus>
                                                        <span class="help-block warning-text"><?php if(isset($var_error['newUsername'])) {echo $var_error['newUsername'];} ?></span>
                                                    </div>
                                                </div>
                                                <!-- /.form-group -->

                                                <div class="form-group">
                                                    <div class="col-sm-8 col-sm-offset-4">
                                                        <button class="col-sm-6 btn btn green" type="submit" name="btn_change_username">Ubah Nama Pengguna</button>
                                                    </div>
                                                </div>
                                                <!-- /.form-group -->

                                                <hr>

                                                <div class="form-group">
                                                    <label for="frm_old_username" class="col-sm-4 control-label">Hapus nama pengguna</label>
                                                    <div class="col-sm-8">
                                                        <a class="col-sm-6 btn btn-danger btn red" href="#" onclick="confirm_modal('manageuser-profile-delete.php?id=<?= $var_userid ?>');">Hapus Nama Pengguna</a>
                                                    </div>
                                                </div>
                                                <!-- /.form-group -->

                                            </form>
                                            <!--/form change username  -->
                                        </div>
                                        <!-- END PRIVACY SETTINGS TAB -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PROFILE CONTENT -->

                <!-- modal untuk hapus data -->
                <div class="delete-modal">
                    <div id="ModalDelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">Konfirmasi</h4>
                                </div>
                                <div class="modal-body">
                                    <h4 class="text-center">Yakin menghapus pengguna?</h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Tutup</button>
                                    <a href="#" class="btn btn-flat btn-primary" id="delete_link">Hapus</a>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                </div>
                <!-- /.hapus-modal -->

                <?php
                $footer_component = [
                    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
                    'profilejs' => '../assets/pages/scripts/profile.min.js',
                    'fileinput' => '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                    'sparkline' => '../assets/global/plugins/jquery.sparkline.min.js',
                    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
                    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
                    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
                    'check username' => '../assets/custom/form-validate-dataexist.js',
                ];
                require 'inc-template/inc-template-footer.php';
                ?>

                <!-- Script konfirmasi modal -->
                <script type="text/javascript">
                function confirm_modal(delete_url) {
                    $('#ModalDelete').modal('show', {backdrop: 'static'});
                    document.getElementById('delete_link').setAttribute('href' , delete_url);
                }
                </script>

                <?php mysqli_close($var_con);?>

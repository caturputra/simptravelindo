<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman sebagai router url
*/

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';

?>
<?php
$pages_dir = '.';
if(!empty($_GET['p'])) {
    $pages = scandir($pages_dir, 0);
    unset($pages[0], $pages[1]);

    $p = mysqli_escape_string($var_con, filter_var($_GET['p'], FILTER_SANITIZE_STRING));
    if(in_array($p, $pages)) {
        $var_sql_ses = "SELECT p.*, m.menu_url FROM sp_previllages p JOIN sp_menu m ON m.menu_id = p.menu_id WHERE m.menu_url = '{$p}' && p.level_id = '{$_SESSION['levelid']}'";
        $var_query_ses = mysqli_query($var_con, $var_sql_ses);
        $var_data_ses = mysqli_fetch_row($var_query_ses);

        if ((mysqli_num_rows($var_query_ses) > 0) && ($var_data_ses[0] == $_SESSION['levelid'])) {
            include $p;
        } else {
            include 'error/403.php';
            die();
        }
    } else {
        include 'error/404.php';
    }
} else {
    include 'home-'.trim(strtolower($_SESSION['levelname']) . '.php');
}
?>
<!-- /main-content -->
<!-- END PAGE BASE CONTENT -->
</body>
</html>

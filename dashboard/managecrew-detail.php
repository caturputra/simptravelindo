<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Modal untuk melihat detail kru
*/

require_once '../core/init.php';

$var_user_id = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : "";
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detail Crew</h4>
            </div>
            <div class="modal-body">
                <?php
                $var_sql_user = "
                SELECT u.user_id, u.user_username, u.user_password, u.user_email, u.user_level, u.user_status, l.level_name, ud.user_detail_avatar, ud.user_detail_firstname, ud.user_detail_lastname, ud.user_detail_gender, ud.user_detail_phone, ud.user_detail_address, ud.user_detail_birthdate
                FROM sp_crew_armada ca
                JOIN sp_user u ON u.user_id = ca.user_id
                JOIN sp_level l ON l.level_id = u.user_level
                JOIN sp_user_detail ud ON ud.user_id = u.user_id
                WHERE ca.crew_armadaid='{$var_user_id}'
                LIMIT 1
                ";

                $var_query_user = mysqli_query($var_con, $var_sql_user);
                ?>
                <?php while ($var_data_user = mysqli_fetch_array($var_query_user)) : ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <!-- BEGIN PROFILE SIDEBAR -->
                            <div class="profile-sidebar">
                                <!-- PORTLET MAIN -->
                                <div class="portlet light profile-sidebar-portlet bordered">
                                    <!-- SIDEBAR USERPIC -->
                                    <div class="profile-userpic mt-element-ribbon">
                                        <?php
                                        switch ($var_data_user['user_status']) {
                                            case 'active':
                                            echo '<div class="ribbon ribbon-vertical-right ribbon-shadow ribbon-color-primary uppercase">
                                            <div class="ribbon-sub ribbon-bookmark"></div>
                                            <i class="fa fa-check"></i>
                                            </div>';
                                            break;

                                            default:
                                            echo '<div class="ribbon ribbon-vertical-right ribbon-shadow ribbon-color-primary uppercase">
                                            <div class="ribbon-sub ribbon-bookmark"></div>
                                            <i class="fa fa-minus"></i>
                                            </div>';
                                            break;
                                        }
                                        ?>
                                        <img src="../images/ava/<?php echo $var_data_user['user_detail_avatar']; ?>" class="img img-responsive img-circle" alt="<?php echo $var_data_user['user_detail_avatar']; ?>"/>
                                    </div>
                                    <!-- END SIDEBAR USERPIC -->
                                </div>
                                <!-- END PORTLET MAIN -->
                            </div>
                            <!-- END PROFILE SIDEBAR -->
                        </div>

                        <div class="col-sm-8">
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title tabbable-line">
                                                <div class="caption caption-md">
                                                    <i class="icon-globe theme-font hide"></i>
                                                    <span class="caption-subject font-blue-madison bold uppercase"><?php echo $var_data_user['user_detail_firstname'] ." " . $var_data_user['user_detail_lastname'] . " (".$var_data_user['user_username'].")";?></span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="tab-content">
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        Alamat surel:
                                                                        <td>
                                                                            <?php echo $var_data_user['user_email'] ?>
                                                                        </td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Tanggal Lahir:
                                                                        <td>
                                                                            <?php echo dateFormat($var_data_user['user_detail_birthdate']) ?>
                                                                        </td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Jenis Kelamin:
                                                                        <td>
                                                                            <?php
                                                                            switch ($var_data_user['user_detail_gender']) {
                                                                                case 'F':
                                                                                echo "Perempuan";
                                                                                break;

                                                                                default:
                                                                                echo "Laki-laki";
                                                                                break;
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Alamat:
                                                                        <td>
                                                                            <?php echo $var_data_user['user_detail_address'] ?>
                                                                        </td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Telepon:
                                                                        <td>
                                                                            <?php echo $var_data_user['user_detail_phone'] ?>
                                                                        </td>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                                $var_sql_license = "
                                                                SELECT detail_licensenumber, detail_licensetype, crew_armadaid
                                                                FROM sp_crew_armada_detail
                                                                WHERE crew_armadaid = '{$var_user_id}';
                                                                ";
                                                                $var_query_license = mysqli_query($var_con, $var_sql_license);
                                                                ?>
                                                                <?php while($var_data_license = mysqli_fetch_row($var_query_license)) : ?>
                                                                    <tr>
                                                                        <td>
                                                                            SIM <?php echo $var_data_license[1] ?>:
                                                                            <td>
                                                                                <?php echo $var_data_license[0] ?>
                                                                            </td>
                                                                        </td>
                                                                    </tr>
                                                                <?php endwhile; ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PROFILE CONTENT -->
                            <button type="button" class="btn btn-outline dark pull-right btn-md" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->

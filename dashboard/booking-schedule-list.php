<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman yang memuat jadwal yang telah diatur
*/

require_once '../core/init.php';

$var_title = "Jadwal";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Daftar Jadwal</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="table-responsive">
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped table-bordered" id="tableListSchedule">
                                <thead>
                                    <th class="text-center" style="width: 2em;">No.</th>
                                    <th class="text-center">Pemesan</th>
                                    <th class="text-center">Berangkat-Pulang</th>
                                    <th class="text-center">Armada</th>
                                    <th class="text-center">Crew</th>
                                    <th></th>
                                </thead>
                                <tbody>
                                    <?php
                                    //menampilkan order
                                    $var_sql_schedule = "
                                    SELECT ot.od_id, ot.od_from, ot.od_to, ot.od_request, ot.od_status, u.user_detail_firstname, u.user_detail_lastname, u.user_detail_phone, s.schedule_id, a.armada_regnumber, DATEDIFF(ot.od_from, CURRENT_TIMESTAMP()) as date_dispute
                                    FROM sp_order_transaction ot
                                    JOIN sp_schedule s ON s.od_id = ot.od_id
                                    JOIN sp_user_detail u ON u.user_id = ot.user_id
                                    JOIN sp_armada a ON a.armada_regnumber = ot.armada_regnumber
                                    WHERE od_status = '4'
                                    ORDER BY ot.od_id DESC
                                    ";
                                    $var_query_schedule = mysqli_query($var_con, $var_sql_schedule);
                                    $num = (int) 1;
                                    while ($var_data_schedule = mysqli_fetch_array($var_query_schedule)) :
                                        $_SESSION['temp']['sid'] = $var_data_schedule['schedule_id'];
                                        ?>
                                        <tr>
                                            <td class="text-center">
                                                <?php echo $num++; ?>
                                            </td>
                                            <td class="text-center">
                                                <?php echo $var_data_schedule['user_detail_firstname']; ?>
                                            </td>
                                            <td class="text-center">
                                                <?php echo dateFormat($var_data_schedule['od_from'])?> - <?php echo dateFormat($var_data_schedule['od_to']); ?>
                                            </td>
                                            <td class="text-center">
                                                <span class="label label-primary"><?php echo $var_data_schedule['armada_regnumber'] ?></span>
                                            </td>
                                            <td class="text-left">
                                                <?php
                                                $var_sql_crew = "
                                                SELECT concat(udc.user_detail_firstname, ' ', udc.user_detail_lastname) as crews_name, cd.crew_action, cd.crew_detailid, cd.crew_armadaid
                                                FROM sp_crew_detail cd
                                                JOIN sp_schedule s ON s.schedule_id = cd.schedule_id
                                                JOIN sp_order_transaction ot ON ot.od_id = s.od_id
                                                JOIN sp_crew_armada ca ON cd.crew_armadaid = ca.crew_armadaid
                                                JOIN sp_user_detail udc ON udc.user_id = ca.user_id
                                                WHERE cd.schedule_id = '{$var_data_schedule['schedule_id']}'
                                                ";
                                                // debug($var_sql_crew);
                                                $var_query_crew = mysqli_query($var_con, $var_sql_crew);
                                                $var_numrows_crew = mysqli_num_rows($var_query_crew);
                                                ?>
                                                <?php if ($var_numrows_crew > 0) : ?>
                                                    <?php while ($var_data_crew = mysqli_fetch_row($var_query_crew)) : ?>
                                                        <?php echo $var_data_crew[0] ?>
                                                        <?php switch ($var_data_crew[1]) : case '0' :?>
                                                        <label class="label label-sm label-default">Menuggu</label>
                                                        <?php break; case '1' : ?>
                                                        <label class="label label-sm label-primary">Menerima</label>
                                                        <?php break; case '2' : ?>
                                                        <label class="label label-sm label-danger">Menolak</label>
                                                        <a href="index.php?p=booking-schedule-update.php&amp;id=<?= base64_encode($var_data_schedule['schedule_id']); ?>&amp;from=<?= $var_data_schedule[1] ?>&amp;to=<?= $var_data_schedule[2] ?>&amp;c=<?= $var_data_crew[3] ?>" data-toggle="tooltip" data-placement="bottom" title="Perbaharui Jadwal" class="btn btn-xs yellow open_modal_update"><i class="fa fa-pencil-square-o"></i> </a>
                                                        <?php break; case '3' : ?>
                                                        <label class="label label-sm label-danger">Membatalkan</label>
                                                        <a href="index.php?p=booking-schedule-update.php&amp;id=<?= base64_encode($var_data_schedule['schedule_id']); ?>&amp;from=<?= $var_data_schedule[1] ?>&amp;to=<?= $var_data_schedule[2] ?>&amp;c=<?= $var_data_crew[3] ?>" data-toggle="tooltip" data-placement="bottom" title="Perbaharui Jadwal" class="btn btn-xs yellow open_modal_update"><i class="fa fa-pencil-square-o"></i> </a>
                                                        <?php endswitch; ?>
                                                        <br><br>
                                                    <?php endwhile; ?>
                                                <?php endif; ?>
                                            </td>
                                            <!-- TAMBAHAN -->


                                            <td class="text-center">
                                                <?php if ($var_data_schedule['date_dispute'] <= '1') : ?>
                                                    <?php
                                                    $var_sql_exp = "SELECT * FROM sp_expenses WHERE schedule_id = '{$var_data_schedule['schedule_id']}'";
                                                    $var_query_exp = mysqli_query($var_con, $var_sql_exp);
                                                    ?>
                                                    <?php if (mysqli_num_rows($var_query_exp) == 0) : ?>
                                                        <a href="#" onclick="confirm_modal('booking-schedule.php?id=<?php echo $var_data_schedule['schedule_id']; ?>&amp;act=del');" data-toggle="tooltip" data-placement="bottom" title="Hapus Jadwal" class="btn btn-sm red"><span class="fa fa-trash"></span></a>

                                                        <a href="index.php?p=booking-expenses.php&amp;act=exnew&amp;id=<?= base64_encode($var_data_schedule['schedule_id']); http_build_query($_GET) ?>" data-toggle="tooltip" data-placement="bottom" title="Expenses Report" class="btn btn-sm green-meadow"><i class="fa fa-pencil"></i> Buat Expenses</a>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endwhile; ?>
                                </tbody>
                            </table>
                            <!-- /.tableListSchedule -->
                        </div>
                        <!-- /.col-sm-12 -->
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.porlet-body -->
        </div>
        <!-- /.portlet -->
    </div>
    <!-- /.col-sm-6 -->
</div>
<!-- END CONTENT -->

<!-- modal untuk hapus data -->
<div class="delete-modal">
    <div id="ModalDelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Yakin menghapus jadwal?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
                    <a href="#" class="btn red" id="delete_link">Hapus</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.hapus-modal -->

<?php
$footer_component = [
    'datatableglobalscript' => '../assets/global/scripts/datatable.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'datableplugin' => '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
require_once 'inc-template/inc-template-footer.php';
?>
<script type="text/javascript">
function confirm_modal(delete_url) {
    $('#ModalDelete').modal('show', {
        backdrop: 'static'
    });
    document.getElementById('delete_link').setAttribute('href', delete_url);
}
$(document).ready(function() {

    $("#tableListSchedule").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autowidth": false,
        "lengthChange": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        }
    });
});
</script>

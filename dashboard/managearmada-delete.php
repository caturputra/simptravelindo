<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk menghapus data armada
*/

require_once '../core/init.php';
require_once '../inc/function/inc-func-users.php';

$var_armada_id = $_GET['armada_id'];

$var_sql_delete = "SELECT armada_image FROM sp_armada WHERE armada_regnumber='{$var_armada_id}'";
$var_query_delete = mysqli_query($var_con, $var_sql_delete);
$var_data_delete = mysqli_fetch_row($var_query_delete);
$var_ava_dir = "../images/armada/" . $var_data_delete[0];
$var_ava_name = $var_data_delete[0];

//kkondisi delete
$var_data_delete_field = ['armada_regnumber' => $var_armada_id];

if ($var_data_delete[0] == $var_ava_name) {
    if (file_exists($var_ava_dir)) {
        unlink($var_ava_dir);
        $var_delete = delete($var_con, "sp_armada", $var_data_delete_field);
        if ($var_delete) {
            setNotif(['success' => 'Data armada berhasil dihapus.']);
            routeUrl('index.php?managearmada');
            die();
        } else {
            setNotif(['danger' => 'Data armada gagal dihapus.']);
            routeUrl('index.php?managearmada');
            die();
        }
    } else {
        $var_delete = delete($var_con, "sp_armada", $var_data_delete_field);
        if ($var_delete) {
            setNotif(['success' => 'Data armada berhasil dihapus.']);
            routeUrl('index.php?p=managearmada');
            die();
        } else {
            setNotif(['danger' => 'Data armada gagal dihapus.']);
            routeUrl('index.php?p=managearmada');
            die();
        }
    }
}
?>

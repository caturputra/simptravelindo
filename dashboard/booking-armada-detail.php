<?php
require_once '../core/init.php';
require_once '../inc/helper/Mail.php';

$var_title = "Pemesanan Armada";

$head_component = [

];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Detail</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <?php
                $var_id = isset($_GET['id']) ? $_GET['id'] : "";
                $var_sql_detail = "
                SELECT ot.*, otd.*, am.*
                FROM sp_order_transaction ot
                JOIN sp_order_transaction_detail otd ON otd.od_id = ot.od_id
                JOIN sp_armada_model am ON am.model_id = otd.model_id
                WHERE ot.od_id = '{$var_id}'
                ";
                $var_query_detail = mysqli_query($var_con, $var_sql_detail);
                ?>
                <?php while($var_data_detail = mysqli_fetch_array($var_query_detail)) : ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <?php echo $var_data_detail['model_name'] ?>
                        </div>
                    </div>
                <?php endwhile; ?>
                <!-- /.row -->
            </div>
            <!-- /.porlet-body -->
        </div>
        <!-- /.portlet -->
    </div>
    <!-- /.col-sm-6 -->
</div>
<!-- END CONTENT -->

<!-- modal untuk persetujuan data -->
<div class="approve-modal">
    <div id="ModalApprove" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">

    </div>
    <!-- /.modal -->
</div>
<!-- /.approve-modal -->

<!-- modal untuk penolakan data -->
<div class="reject-modal">
    <div id="ModalReject" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">

    </div>
    <!-- /.modal -->
</div>
<!-- /.reject-modal -->

<!-- modal untuk hapus data -->
<div class="delete-modal">
    <div id="ModalDelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Yakin membatalkan pesanan?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
                    <a href="#" class="btn red" id="delete_link">Batal</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.hapus-modal -->

<!-- modal untuk view detail data -->
<div class="detail-modal">
    <div id="ModalDetail" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    </div>
    <!-- /.modal -->
</div>
<!-- /.view detail-modal -->
<?php
$footer_component = [
    'datatableglobalscript' => '../assets/global/scripts/datatable.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'datableplugin' => '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
    'datatblescroll' => 'https://cdn.datatables.net/scroller/1.4.4/js/dataTables.scroller.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
require_once 'inc-template/inc-template-footer.php';
?>
<script type="text/javascript">
function confirm_modal(delete_url) {
    $('#ModalDelete').modal('show', {
        backdrop: 'static'
    });
    document.getElementById('delete_link').setAttribute('href', delete_url);
}

$(document).ready(function() {
    $(".open_modal").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "booking-approval.php",
            type: "GET",
            data: {
                orderid: id,
            },
            success: function(ajaxData) {
                $("#ModalApprove").html(ajaxData);
                $("#ModalApprove").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $(".open_modal_reject").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "booking-rejected.php",
            type: "GET",
            data: {
                orderid: id,
            },
            success: function(ajaxData) {
                $("#ModalReject").html(ajaxData);
                $("#ModalReject").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $(".open_modal_detail").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "booking-detail.php",
            type: "GET",
            data: {
                orderid: id,
            },
            success: function(ajaxData) {
                $("#ModalDetail").html(ajaxData);
                $("#ModalDetail").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $("#tableListOrder").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "deferRender": true,
        "scroller": true,
        "scrollX": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        },
    });
});
</script>

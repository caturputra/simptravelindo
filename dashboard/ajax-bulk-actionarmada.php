<?php
    require_once '../core/init.php';

    $var_action = isset($_GET['a']) ? filter_var($_GET['a'], FILTER_SANITIZE_NUMBER_INT) : "";
    $var_aid = isset($_POST['id']) ? $_POST['id'] : "";
    $var_userid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "";

    switch ($var_action) {
        case 1:

        foreach ($var_aid as $key => $value) {
            $var_data_update = ['armada_status' => 'approved', 'user_id' => $var_userid, 'armada_modifyat' => date('Y-m-d H:i:s')];
            $var_cond_update = ['armada_regnumber' => $value];
            $var_update = update($var_con, "sp_armada", $var_data_update, $var_cond_update);
        }

        if ($var_update) {
            return true;
        }

        break;

        case 2:

        foreach ($var_aid as $key => $value) {
            $var_data_update = ['armada_status' => 'rejected', 'user_id' => $var_userid, 'armada_modifyat' => date('Y-m-d H:i:s')];
            $var_cond_update = ['armada_regnumber' => $value];
            $var_update = update($var_con, "sp_armada", $var_data_update, $var_cond_update);
        }

        if ($var_update) {
            return true;
        }

        break;
    }
?>

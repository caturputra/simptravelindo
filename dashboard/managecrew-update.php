<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk memperbaharui data kru armada
*/

require_once '../core/init.php';
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Crew Update</h4>
            </div>
            <form class="form-horizontal" action="index.php?p=managecrew.php&amp;s=update" method="post" enctype="multipart/form-data">
                <?php

                $var_crew_id = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : "";
                $var_sql_crew = "SELECT crew_status FROM sp_crew_armada WHERE crew_armadaid = '{$var_crew_id}'";
                $var_query_crew = mysqli_query($var_con, $var_sql_crew);
                while ($var_data_crew = mysqli_fetch_array($var_query_crew)) :
                    ?>
                    <div class="box-body">
                        <div class="row">
                                <div class="box-body">
                                    <input type="hidden" class="form-control" id="frm_crew_id" name="frm_crew_id" placeholder="Driver Name" value="<?= $var_crew_id ?>">

                                    <div class="form-group">
                                        <label for="frm_status" class="control-label col-sm-4">Status</label>
                                        <div class="col-sm-8 col-sm-6 col-xs-12">
                                            <div class="row">
                                                <?php if ($var_data_crew['crew_status'] === '1') : ?>
                                                    <div class="col-sm-4 col-sm-4">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="frm_status" id="optionsRadios1" value="1" checked>
                                                                Ready
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-2 col-sm-2">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="frm_status" id="optionsRadios2" value="Break">
                                                                Break
                                                            </label>
                                                        </div>
                                                    </div>
                                                <?php else: ?>
                                                    <div class="col-sm-4 col-sm-4">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="frm_status" id="optionsRadios1" value="Ready">
                                                                Ready
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-2 col-sm-2">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="frm_status" id="optionsRadios2" value="Break" checked>
                                                                Break
                                                            </label>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <!-- /form-group -->

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                                        <input type="submit" class="btn btn-flat btn-primary" name="btn_update_crew" value="Update">
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    <?php endwhile; ?>

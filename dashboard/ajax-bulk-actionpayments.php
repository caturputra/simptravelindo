<?php
    require_once '../core/init.php';

    $var_action = isset($_GET['a']) ? filter_var($_GET['a'], FILTER_SANITIZE_NUMBER_INT) : "";
    $var_pid = isset($_POST['id']) ? $_POST['id'] : "";
    $var_sid = isset($_POST['sid']) ? $_POST['sid'] : "";
    $var_userid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "";

    switch ($var_action) {
        case '1':
        $var_data_update = ['payment_status' => '1', 'payment_confirmedby' => $var_userid, 'payment_verdate' => date('Y-m-d H:i:s')];
        $var_cond_update = ['payment_id' => $var_pid];
        $var_update = update($var_con, "sp_payment", $var_data_update, $var_cond_update);

        $var_data_updateorder = ['od_status' => '4'];
        $var_cond_updateorder = ['od_id' => $var_oid];
        $var_update_order = update($var_con, "sp_order_transaction", $var_data_updateorder, $var_cond_updateorder);

        mysqli_autocommit($var_con, FALSE);
        mysqli_begin_transaction($var_con);
        if ($var_update) {
            mysqli_commit($var_con);
            echo $var_pid;
        }
        break;

        case 2:
        $var_data_update = ['payment_status' => '2', 'payment_confirmedby' => $var_userid, 'payment_verdate' => date('Y-m-d H:i:s')];
        $var_cond_update = ['payment_id' => $var_pid];
        $var_update = update($var_con, "sp_payment", $var_data_update, $var_cond_update);

        if ($var_update) {
            mysqli_commit($var_con);
            echo $var_pid;
        }
        break;
    }
?>

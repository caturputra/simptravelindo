<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk manambah data kru armada
*/

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';

//get action
$var_action = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var($_GET['act'], FILTER_SANITIZE_STRING)) : "";
$var_caid = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : "";
$var_caid = base64_decode($var_caid);

$var_lid = isset($_SESSION['levelid']) ? filter_var($_SESSION['levelid'], FILTER_SANITIZE_NUMBER_INT) : "";

switch(strtolower($var_action)) {
    case 'add':
    break;

    case 'update':
    $var_data_delete = ['crew_status' => ''];
    $var_cond_delete = ['crew_armadaid' => $var_caid];
    $var_delete_crew = update($var_con, "sp_crew_armada", $var_data_delete, $var_cond_delete);

    if ($var_delete_crew) {
        setNotif(['success' => 'Data crew berhasil dihapus.']);
        routeUrl('index.php?p=managecrew');
        die();
    }
    break;

    case 'delete':
    $var_data_delete = ['crew_status' => '3'];
    $var_cond_delete = ['crew_armadaid' => $var_caid];
    $var_delete_crew = update($var_con, "sp_crew_armada", $var_data_delete, $var_cond_delete);

    if ($var_delete_crew) {
        setNotif(['success' => 'Data crew berhasil dihapus.']);
        routeUrl('index.php?p=managecrew');
        die();
    }
    break;
}

$var_title = "Manajemen Kru";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- DATA CREW -->
<div class="row">
    <div class="col-sm-12">
        <!-- BEGIN TAB PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="fa fa-bars font-green"></i>
                    <span class="caption-subject font-green bold uppercase">Daftar kru armada</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="table-responsive">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-striped table-hover" id="tableListcrew">
                                <thead>
                                    <th class="text-center" style="width: 10px;">No.</th>
                                    <th class="text-center">Nama</th>
                                    <th class="text-center" style="width: 9em">Telepon</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center"></th>
                                </thead>
                                <tbody>

                                    <?php
                                    $var_sql_crew = "
                                    SELECT concat(ud.user_detail_firstname, ' ',ud.user_detail_lastname) as crew_name, ud.user_detail_phone, ud.user_detail_avatar, ca.crew_status, ca.crew_armadaid, d.detail_licensenumber
                                    FROM sp_crew_armada ca
                                    JOIN sp_crew_armada_detail d ON d.crew_armadaid = ca.crew_armadaid
                                    JOIN sp_user u ON u.user_id = ca.user_id
                                    JOIN sp_user_detail ud ON ud.user_id = u.user_id
                                    JOIN sp_level l ON l.level_id = u.user_level
                                    #WHERE (l.level_name = '". strtolower('driver') ."' or l.level_name = '". strtolower('co-driver') ."')
                                    WHERE ca.crew_status != '3'
                                    GROUP BY ca.crew_armadaid
                                    ";
                                    // debug($var_sql_crew);
                                    $var_query_crew = mysqli_query($var_con, $var_sql_crew);
                                    $no= (int) 1;

                                    while ($var_data_crew = mysqli_fetch_array($var_query_crew)) :
                                        $var_crewarmadaid = base64_encode($var_data_crew['crew_armadaid']);
                                        ?>
                                        <tr>
                                            <td class="text-center">
                                                <?php echo $no++; ?>
                                            </td>
                                            <td>
                                                <?php echo ucwords($var_data_crew['crew_name']) ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo $var_data_crew['user_detail_phone']; ?>
                                            </td>
                                            <td class="text-center">
                                                <?php switch($var_data_crew['crew_status']) : case '0': ?>
                                                <label class="label label-warning label-sm">Ditunda</label>
                                                <?php break; case '1': ?>
                                                <label class="label label-success label-sm">Disetujui</label>
                                                <?php break; case '2': ?>
                                                <label class="label label-danger label-sm">Ditolak</label>
                                                <?php break; endswitch; ?>
                                            </td>
                                            <td class="text-center">
                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Detail Crew" class="btn blue btn-sm open_modal_detail" id="<?php echo  $var_data_crew['crew_armadaid']; ?>"><i class="fa fa-eye"></i></a>

                                                <!-- <a href="#" data-toggle="tooltip" data-placement="bottom" title="Ubah" class="btn yellow btn-sm open_modal" id="<?php echo  $var_data_crew['crew_armadaid']; ?>"><i class="fa fa-edit"></i></a> -->
                                                <?php if ($var_lid == '9' or $var_lid == '10'): ?>
                                                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Hapus" class="btn red btn-sm" onclick="confirm_modal('index.php?p=managecrew.php&amp;id=<?php echo $var_crewarmadaid ?>&amp;act=delete');"><i class="fa fa-trash"></i></a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endwhile; ?>
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.portlet-body -->
        </div>
        <!-- /.portlet -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
<!-- END DATA CREW -->


<!-- MODAL DETAIL CREW -->
<div class="detail-modal">
    <div id="ModalDetail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Detail Kru</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center"></h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- END MODAL DETAIL CREW -->

<!-- BEGIN MODAL UPDATE CREW -->
<div class="update-modal">
    <div id="ModalUpdate" class="modal bs-modal-lg fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    </div>
    <!-- /.modal -->
</div>
<!-- END MODAL UPDATE CREW -->

<!-- BEGIN MODAL DELETE CREW -->
<div class="delete-modal">
    <div id="ModalDelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Yakin menghapus kru?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Tutup</button>
                    <a href="#"class="btn red" id="delete_link">Hapus</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- END MODAL DELETE CREW -->

<?php
$footer_component = [
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'sample' => '../assets/custom/form-validation-user-add.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
require_once 'inc-template/inc-template-footer.php';
?>

<script type="text/javascript">
function confirm_modal(delete_url) {
    $('#ModalDelete').modal('show', {backdrop: 'static'});
    document.getElementById('delete_link').setAttribute('href' , delete_url);
}
$(document).ready(function () {
    $(".open_modal").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "managecrew-update.php",
            type: "GET",
            data : {id: id,},
            success: function (ajaxData){
                $("#ModalUpdate").html(ajaxData);
                $("#ModalUpdate").modal('show',{backdrop: 'true'});
            }
        });
    });

    $(".open_modal_detail").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "managecrew-detail.php",
            type: "GET",
            data: { id: id, },
            success: function(ajaxData) {
                $("#ModalDetail").html(ajaxData);
                $("#ModalDetail").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $("#tableListcrew").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autowidth": false,
        "lengthChange": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        }
    });
});
</script>

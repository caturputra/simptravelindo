<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Ajax untuk ambil data order id
*/

require_once '../core/init.php';

$var_oid = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : "";

$var_sql_detail = "
SELECT ot.od_from, ot.od_to, concat(ud.user_detail_firstname, ' ', ud.user_detail_lastname) as member, CASE ot.od_service WHEN '1' THEN 'City Tour' WHEN '2' THEN 'Transfer In/Out' END as service
FROM sp_order_transaction ot
JOIN sp_user_detail ud ON ud.user_id = ot.user_id
WHERE ot.od_id = '{$var_oid}'
LIMIT 1
";
$var_query_detail = mysqli_query($var_con, $var_sql_detail);

if (mysqli_num_rows($var_query_detail) > 0) {
    while ($var_data_detail = mysqli_fetch_array($var_query_detail)) {
        echo json_encode($var_data_detail);
    }
}
?>

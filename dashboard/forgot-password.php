<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk recovery kata sandi
*/

require_once '../core/init.php';
require_once '../inc/function/inc-func-users.php';
//require_once '../inc/inc-session-user.php';
require_once '../inc/helper/Mail.php';

if (isset($_POST['btn_user_reset'])) {
    $var_useremail = isset($_POST['frm_user_reset']) ? mysqli_real_escape_string($var_con, filter_var($_POST['frm_user_reset'], FILTER_SANITIZE_EMAIL)) : "";
    $var_captcha = isset($_POST['g-recaptcha-response']) ? filter_var($_POST['g-recaptcha-response'], FILTER_SANITIZE_STRING):'';

    /* Validasi config recaptcha */
    $var_data = recaptcha($var_captcha);

    if ($var_data->success === true) {
        $var_sql_reset = "SELECT user_email, user_username FROM sp_user WHERE user_email = '{$var_useremail}'";
        $var_query_reset = mysqli_query($var_con, $var_sql_reset);
        $var_data_reset = mysqli_fetch_row($var_query_reset);
        if ($var_useremail === $var_data_reset[0]) {

            //generate random Password
            $var_passwd_change = randpass(12);

            //proses hash passaword
            $var_passwd_hash = password_hash($var_passwd_change, PASSWORD_BCRYPT);

            //send email confirmation
            $mail = new Mail();
            $from = "noreply@travelindo.com";
            $subject = "Reset kata sandi akun";
            $dir = "content-mail";
            $template = "forgot-password";
            $var_opt = [
                'wrapper_username' => $var_data_reset[1],
                'wrapper_email' => $var_useremail,
                'wrapper_newpassword' => $var_passwd_change,
                'footer_year' => date('Y')
            ];

            if ($mail->sendMail($from, $var_useremail, $subject, $template, $dir, $var_opt)) {
                //proses update password
                $var_passwd_update = array('user_password' => $var_passwd_hash);
                $var_cond_update = array('user_email' => $var_useremail);
                $var_passwd_process = update($var_con, "sp_user", $var_passwd_update, $var_cond_update);
                setNotif(['success' => 'Kata sandi telah berhasil direset, silahkan cek kotak masuk <strong>' . $var_useremail . '</strong>.']);
                routeUrl('forgot-password');
                die();
            }
        } else {
            setNotif(['warning' => 'Tidak ada akun terkait dengan surel tersebut.']);
            routeUrl('forgot-password');
            die();
        }
    } else {
        setNotif(['danger' => 'Mohon konfirmasi bahwa anda bukan robot.']);
        routeUrl('forgot-password');
        die();
    }
}
?>
<?php
$var_title = "Lupa kata sandi";

$head_component = array(
    'logincss' => '../assets/pages/css/login-3.min.css',
);

require_once 'inc-template/inc-template-header.php';
?>
<style media="screen">
    body {
        background-image:url(../assets/pages/img/login/bg3.jpg);
        background-position: center;
        background-size: cover;
    }
</style>
<body class="login">
    <!-- BEGIN LOGO -->
    <div class="logo" style="width: 125px;">
        <a href="login.php">
            <img src="../images/logo-travelindo.png" class="img img-responsive img-logo">
        </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <div class="content">
        <form action="forgot-password.php" method="post">
            <p class="alert alert-info font-blue"> Masukkan alamat surel. </p>

            <!-- MESSAGE -->
            <div class="row">
                <div class="col-sm-12">
                    <?php if (isset($_SESSION['notif']['success'])) :  ?>
                        <div class="alert alert-success"><p><i class="fa fa-check"></i> Sukses <br><?php echo $_SESSION['notif']['success'] ?></p></div>
                        <?php unsetNotif() ?>
                    <?php elseif (isset($_SESSION['notif']['info'])) : ?>
                        <div class="alert alert-info"><p><i class="fa fa-info"></i>I nformasi <br> <?php echo $_SESSION['notif']['info'] ?></p></div>
                        <?php unsetNotif() ?>
                    <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
                        <div class="alert alert-warning"><p><i class="fa fa-exclamation"></i> Perhatian <br><?php echo $_SESSION['notif']['warning'] ?></p></div>
                        <?php unsetNotif() ?>
                    <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
                        <div class="alert alert-danger"><p><i class="fa fa-ban"></i> Peringatan <br><?php echo $_SESSION['notif']['danger'] ?></p></div>
                        <?php unsetNotif() ?>
                    <?php endif; ?>
                </div>
            </div>
            <!-- END MESSAGE -->

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="input-icon">
                            <i class="fa fa-envelope"></i>
                            <input type="email" class="form-control placeholder-no-fix" placeholder="Alamat Surel" name="frm_user_reset" autofocus required>
                        </div>
                    </div>
                </div>
            </div>

            <!-- validasi recaptcha -->
            <div class="row">
                <div class="col-sm-12 col-sm-12 col-xs-12">
                    <div class="g-recaptcha" data-sitekey="6Lfy3CkUAAAAADoyBTd4-t27Wy90DPoGpTrKDwhC"></div>
                </div>
            </div>
            <!-- /.g-recaptcha -->
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-actions">
                        <a type="button" id="back-btn" class="btn grey-salsa btn-outline" href="login.php"> Kembali </a>
                        <button type="submit" class="btn green pull-right" name="btn_user_reset">Reset</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- /.content -->
</div>
<!-- END FORGOT PASSWORD FORM -->
<script src='https://www.google.com/recaptcha/api.js?hl=en'></script>
</body>

</html>

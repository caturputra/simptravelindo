<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk masuk ke sistem
*/

$var_title="Login";

require_once '../core/init.php';

$head_component = array(
    'logincss' => '../assets/pages/css/login-3.min.css',
);

if (isset($_SESSION['userid'])) {
    routeUrl('logout');
    die();
}

require_once 'inc-template/inc-template-header.php';
?>
<style media="screen">
body {
    background-image:url(../assets/pages/img/login/bg3.jpg);
    background-position: center;
    background-size: cover;
}
</style>
<body class="login">
    <!-- BEGIN LOGO -->
    <div class="logo" style="width: 125px;">
        <a href="login.php">
            <img src="../images/logo-travelindo.png" class="img img-responsive img-logo">
        </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <form class="login-form" action="login-check.php" method="post">
            <p class="form-title note note-info">Masuk untuk memulai sesi anda</p>

            <!-- MESSAGE -->
            <div class="row">
                <div class="col-sm-12">
                    <?php if (isset($_SESSION['notif']['success'])) :  ?>
                        <div class="alert alert-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
                        <?php unsetNotif() ?>
                    <?php elseif (isset($_SESSION['notif']['info'])) : ?>
                        <div class="alert alert-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
                        <?php unsetNotif() ?>
                    <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
                        <div class="alert alert-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
                        <?php unsetNotif() ?>
                    <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
                        <div class="alert alert-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
                        <?php unsetNotif() ?>
                    <?php endif; ?>
                </div>
            </div>
            <!-- END MESSAGE -->

            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Nama pengguna</label>
                <div class="input-icon">
                    <i class="fa fa-user"></i>
                    <input type="text" class="form-control placeholder-no-fix" placeholder="Nama pengguna/alamat surel" name="frm_username" id="frm_username" <?php echo isset($_COOKIE['user_login']) ? "" : "autofocus" ?> value="<?php if (isset($_COOKIE['user_login'])) { echo $_COOKIE['user_login']; } ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Kata sandi</label>
                <div class="input-icon input-group">
                    <i class="fa fa-lock"></i>
                    <input type="password" class="form-control placeholder-no-fix password" placeholder="Kata sandi" name="frm_password" id="frm_password" <?php echo isset($_COOKIE['user_login']) ? "autofocus" : "" ?> required>
                    <span class="input-group-btn">
                        <button type="button" name="button" class="btn default visible-password"><i class="fa fa-eye"></i></button>
                    </span>
                </div>
            </div>
            <div class="form-actions">
                <label class="rememberme mt-checkbox mt-checkbox-outline">
                    <input type="checkbox" name="frm_remember" <?php if (isset($_COOKIE['user_login'])) { echo 'checked'; } ?> checked> Ingat saya
                    <span></span>
                </label>
                <button type="submit" class="btn blue pull-right" name="btn_login">Masuk</button>
            </div>
            <div class="forget-password">
                <h4>Lupa kata sandi?</h4>
                <p>klik
                    <a href="forgot-password.php" id="forget-password"> disini </a> untuk mengatur ulang kata sandi.
                </p>
            </div>
            <div class="create-account">
                <p> Belum mempunyai akun?&nbsp;
                    <a href="register.php" id="register-btn"> Register </a>
                </p>
            </div>
        </form>
        <!-- END LOGIN FORM -->
    </div>
    <!-- END LOGIN -->
    <!--[if lt IE 9]>
    <script src="../assets/global/plugins/respond.min.js"></script>
    <script src="../assets/global/plugins/excanvas.min.js"></script>
    <script src="../assets/global/plugins/ie8.fix.min.js"></script>
    <![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

    <script>
    $(document).ready(function() {
        $('.visible-password').hover(function () {
           $('.password').attr('type', 'text');
        }, function () {
           $('.password').attr('type', 'password');
        });
    });
    </script>
</body>

</html>
<?php exit(); ?>

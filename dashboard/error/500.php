<?php
    $var_title = "500";
    include 'inc-template/inc-core-template.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="index.html">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?= $var_title;  ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-sm-6 col-sm-push-2 page-404">
        <div class="number font-red" style="font-size: 10em;"> <?= $var_title; ?>! </div>
    </div>
    <div class="col-sm-6">
        <div class="details">
            <h3>Oops! Ada kesalahan.</h3>
            <p> Kami sedang melakukan perbaikan, kembali lagi beberapa waktu kemudian. <br/>
                <a href="index.html"> Kembali ke beranda </a> atau coba cari halaman. </p>
                <form action="#">
                    <div class="input-group input-medium">
                        <input type="text" class="form-control" placeholder="keyword...">
                        <span class="input-group-btn">
                            <button type="submit" class="btn green">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                </form>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
    <?php include 'inc-template/inc-template-footer.php'; ?>

<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk konfirmasi pendaftaran pengguna
*/

require_once '../core/init.php';

$confirm = urldecode($_GET['id']);
$confirm = base64_decode($confirm);
if (isset($confirm)) {
    $var_sql = "SELECT user_username, user_status FROM sp_user WHERE user_username='{$confirm}' LIMIT 1";
    $find = mysqli_query($var_con, $var_sql);
    $data = mysqli_fetch_row($find);

    if (mysqli_num_rows($find) > 0) {
        if ($data[1] !== "active") {
            $update = "UPDATE sp_user SET user_status = 'active' WHERE user_username = '{$confirm}'";
            $set = mysqli_query($var_con, $update);
            if ($set) {
                setNotif(['info' => 'Konfirmasi akun berhasil, Silahkan masuk ke dalam sistem.']);
                routeUrl('login');
            }
        } else {
            setNotif(['info' => 'akun anda sudah diaktifkan.']);
            routeUrl('login');
        }
    } else {
        setNotif(['danger' => 'Galat! data tidak ditemukan.']);
        routeUrl('login');
    }
} else {
    setNotif(['danger' => 'Galat! data tidak ditemukan.']);
    routeUrl('login');
}

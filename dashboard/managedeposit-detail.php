<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Modal untuk menampilkan detail pembayaran
*/

require_once '../core/init.php';
require_once '../inc/function/inc-func-users.php';

$var_iid = isset($_GET['id']) ? filter_var($_GET['id'], FILTER_SANITIZE_STRING) :"";

?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Deposit : <?php echo $var_iid ?></h4>
        </div>
        <form action="booking-action-approve.php" method="post" class="form-horizontal">
            <div class="modal-body">
                <?php
                $var_sql_detail = "
                SELECT sp_deposit_temp.temp_id, sp_deposit_temp.temp_amount, sp_deposit_temp.temp_status, sp_deposit_temp.temp_type, sp_deposit_temp.temp_to, sp_deposit_temp.temp_from, sp_deposit_temp.temp_fromname, sp_deposit_temp.temp_receipt, sp_deposit_temp.temp_createdat, sp_deposit_temp.rekening_id, sp_bank_account.account_name
                FROM sp_deposit_temp
                JOIN sp_bank_account ON sp_bank_account.account_number = sp_deposit_temp.temp_to
                JOIN sp_bank ON sp_bank_account.bank_id = sp_bank.bank_id
                WHERE rekening_id = '{$var_iid}'
                LIMIT 1
                ";
                $var_query_detail = mysqli_query($var_con, $var_sql_detail);
                while ($var_data_detail = mysqli_fetch_array($var_query_detail)) :
                    ?>
                    <form class="form-horizontal" role="form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="" class="control-label col-sm-4">Rekening Pengirim: </label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static"><?php echo $var_data_detail['temp_from']; ?> a/n <?php echo $var_data_detail['temp_fromname'] ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="" class="control-label col-sm-4">Penerima: </label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static"><?php echo $var_data_detail['temp_to']; ?> a/n <?php echo $var_data_detail['account_name'] ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="" class="control-label col-sm-4">Tanggal Konfirmasi: </label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static"><?php echo dateFormat($var_data_detail['temp_createdat']); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="" class="control-label col-sm-4">Nominal: </label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static"><?php echo $var_data_detail['temp_amount']; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="" class="control-label col-sm-4">Bukti Resi: </label>
                                        <div class="col-sm-8">
                                            <div style="width: 20em">
                                                <img class="img img-responsive" src="../images/deposit/<?php echo $var_data_detail['temp_receipt'] ?>" alt="<?php echo $var_data_detail['temp_receipt'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <?php endwhile; ?>
                    <div class="modal-footer">
                        <div class="margiv-top-10 col-sm-offset-2">
                            <button type="button" class="btn blue" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                        </div>
                    </div>
                </form>
                <!-- /.form-horizontal -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->

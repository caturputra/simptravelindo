<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Modal untuk menampilkan detail armada
*/

require_once '../core/init.php';
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detail Armada</h4>
            </div>
            <div class="modal-body">
                <!-- BEGIN Portlet PORTLET-->
                <div class="portlet box light">
                    <div class="portlet-body">
                        <?php
                        $var_armada_id = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : "";
                        $var_sql_armada = "
                        SELECT
                        a.armada_regnumber,
                        concat(a.armada_brand, ' ', a.armada_typevariant) as armada_brand,
                        IF(a.armada_type = 1, 'Kend. Ringan', 'Kend. Berat') as armada_type,
                        a.armada_model, a.armada_productionyear, a.armada_chassisnumber, a.armada_enginenumber,
                        a.armada_typefuel, a.armada_fuelsize, a.armada_image, a.armada_color, a.armada_status, a.armada_seat,
                        concat(ud.user_detail_firstname, ' ', ud.user_detail_lastname) as confirmedby
                        FROM sp_armada a
                        LEFT JOIN sp_user u ON u.user_id = a.user_id
                        LEFT JOIN sp_user_detail ud ON ud.user_id = u.user_id
                        WHERE a.armada_regnumber = '{$var_armada_id}'";
                        $var_query_armada = mysqli_query($var_con, $var_sql_armada);
                        while ($var_data_armada = mysqli_fetch_array($var_query_armada)) :
                            ?>
                            <div class="row">
                                <div class="col-sm-4 col-xs-12">
                                    <ul class="list-unstyled profile-nav">
                                        <li>
                                            <img src="../images/armada/<?= $var_data_armada['armada_image']; ?>" alt="<?= $var_data_armada['armada_image']; ?>" class="img img-responsive img-rounded" />
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-sm-8 col-xs-12">
                                    <form class="form-horizontal" role="form">
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="frm_armada_id" class=" control-label col-sm-4 col-xs-4">No. Plat: </label>
                                                    <div class="col-sm-8 col-xs-8">
                                                        <p class="form-control-static">
                                                            <?= $var_data_armada['armada_regnumber']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="frm_color" class=" control-label col-sm-4 col-xs-4">No. Rangka: </label>
                                                    <div class="col-sm-8 col-xs-8">
                                                        <p class="form-control-static">
                                                            <?= $var_data_armada['armada_chassisnumber']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="frm_name" class=" control-label col-sm-4 col-xs-4">Merk: </label>
                                                    <div class="col-sm-8 col-xs-8">
                                                        <p class="form-control-static">
                                                            <?= $var_data_armada['armada_brand']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="frm_color" class=" control-label col-sm-4 col-xs-4">No. Mesin: </label>
                                                    <div class="col-sm-8 col-xs-8">
                                                        <p class="form-control-static">
                                                            <?= $var_data_armada['armada_enginenumber']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="frm_color" class=" control-label col-sm-4 col-xs-4">Warna: </label>
                                                    <div class="col-sm-8 col-xs-8">
                                                        <p class="form-control-static">
                                                            <?= $var_data_armada['armada_color']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="frm_color" class=" control-label col-sm-4 col-xs-4">Tipe bahan bakar: </label>
                                                    <div class="col-sm-8 col-xs-8">
                                                        <p class="form-control-static">
                                                            <?= $var_data_armada['armada_typefuel']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="frm_type" class=" control-label col-sm-4 col-xs-4">Tipe Armada: </label>
                                                    <div class="col-sm-8 col-xs-8">
                                                        <p class="form-control-static">
                                                            <?= $var_data_armada['armada_type']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="frm_color" class=" control-label col-sm-4 col-xs-4">Kap. Tangki: </label>
                                                    <div class="col-sm-8 col-xs-8">
                                                        <p class="form-control-static">
                                                            <?= $var_data_armada['armada_fuelsize']; ?> Liter
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="frm_condition" class=" control-label col-sm-4 col-xs-4">Kursi: </label>
                                                    <div class="col-sm-8 col-xs-8">
                                                        <p class="form-control-static">
                                                            <?= $var_data_armada['armada_seat']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="frm_condition" class=" control-label col-sm-4 col-xs-4">Status: </label>
                                                    <div class="col-sm-8 col-xs-8">
                                                        <p class="form-control-static">
                                                            <?php switch ($var_data_armada['armada_status']) : case 'approved': ?>
                                                            Dikonfirmasi oleh <?php echo $var_data_armada['confirmedby'] ?>
                                                            <?php break; case 'rejected': ?>
                                                            Ditolak oleh <?php echo $var_data_armada['confirmedby'] ?>
                                                            <?php break; case 'pending': ?>
                                                            Belum divalidasi.
                                                            <?php break; endswitch; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
                <!-- END Portlet PORTLET-->
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <button type="button" class="btn green pull-right" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->

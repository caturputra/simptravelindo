<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Proses topup deposit agent
*/

require_once '../core/init.php';

//ambil sesi user
$var_userid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)): "";

//set variabel
$var_accountnumber = isset($_POST['frm_accountnumber']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_accountnumber'], FILTER_SANITIZE_STRING)) : "";
$var_amount = isset($_POST['frm_amount']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_amount'], FILTER_SANITIZE_STRING)) : "";
$var_to = isset($_POST['frm_to']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_to'], FILTER_SANITIZE_STRING)) : "";
$var_from = isset($_POST['frm_from']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_from'], FILTER_SANITIZE_STRING)) : "";
$var_fromname = isset($_POST['frm_fromname']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_fromname'], FILTER_SANITIZE_STRING)) : "";
$var_error = [];

if (isset($_POST['btn_topup'])) {
    //validasi input
    if (empty(trim($var_accountnumber))) {
        $var_error['err']['account_number'] = 'Mohon pilih nomor rekening.';
    }

    if (empty(trim($var_amount))) {
        $var_error['err']['amount'] = 'Mohon masukan nominal topup.';
    }

    if (empty(trim($var_from))) {
        $var_error['err']['from'] = 'Mohon masukan rekening pengirim.';
    }

    if (empty(trim($var_fromname))) {
        $var_error['err']['fromname'] = 'Mohon masukan nama rekening pengirim.';
    }

    if (empty(trim($var_to))) {
        $var_error['err']['to'] = 'Mohon masukan rekening penerima topup.';
    }

    $var_receipt_name = addslashes($_FILES['frm_img_receipt']['name']);
    $var_receipt_tmpname = addslashes($_FILES['frm_img_receipt']['tmp_name']);
    $var_receipt_err = $_FILES['frm_img_receipt']['error'];
    $var_receipt_size = $_FILES['frm_img_receipt']['size'];
    $var_receipt_dir = "../images/deposit/";
    $var_receipt_type = strtolower(pathinfo(isset($var_receipt_name) ? $var_receipt_name : "", PATHINFO_EXTENSION));
    $var_receipt_ext = array("jpeg", "png", "jpg");
    $var_receipt_newName = strtoupper("img-deposit-" . $var_accountnumber ).".jpg";

    if (empty($var_receipt_name)) {
        $var_error['err']['receipt'] = "Mohon masukkan bukti topup.";
    }

    // $check = getimagesize($var_receipt_tmpname);
    // if ($check === false) {
    //     $var_error['err']['receipt']  = 'Bukti topup harus berformat .jpg, .png, .jpeg.';
    // }

    if (!in_array($var_receipt_type, $var_receipt_ext)) {
        $var_error['err']['receipt']  = 'Bukti topup harus berformat .jpg, .png, .jpeg.';
    }

    if (empty($var_error) && (move_uploaded_file($var_receipt_tmpname, $var_receipt_dir.$var_receipt_newName))) {
        $var_data_log = [
            'temp_type' => '1',
            'temp_amount' => $var_amount,
            'rekening_id' => $var_accountnumber,
            'temp_status' => '0',
            'temp_to' => $var_to,
            'temp_from' => $var_from,
            'temp_fromname' => $var_fromname,
            'temp_receipt' => $var_receipt_newName
        ];
        $var_insert_log = insert($var_con, "sp_deposit_temp", $var_data_log);

        if ($var_insert_log) {
            setNotif(['success' => 'Topup berhasil diproses, Silahkan tunggu konfirmasi.']);
            routeUrl('index.php?p=agent-deposit');
            die();
        } else {
            setNotif(['warning' => 'Topup gagal diproses, Cek kembali data anda.']);
            routeUrl('index.php?p=agent-deposit');
            die();
        }
    }
}

$var_title = "Topup Saldo";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-sm-8">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-archive font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Koran Saldo</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form action="#" method="post" id="saldo" class="form-inline">
                            <label for="frm_saldo" class="control-label">Nomor Rekening <span class="required">*</span></label>
                            <select class="form-control " name="frm_saldo" id="frm_saldo">
                                <option value="" selected>Pilih</option>
                                <?php
                                $var_sql_account = "SELECT rekening_id FROM sp_rekening WHERE user_id = '{$var_userid}'";
                                $var_query_account = mysqli_query($var_con, $var_sql_account);
                                $var_numrows_account = mysqli_num_rows($var_query_account);
                                if ($var_numrows_account > 0) :
                                    while ($var_data_account = mysqli_fetch_array($var_query_account)) :
                                        ?>
                                        <option value="<?php echo $var_data_account['rekening_id']?>"><?php echo $var_data_account['rekening_id']?></option>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </select>
                        </form>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-sm-12">
                        <!-- menampilkan data hasil request dari ajax -->
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover table_checksaldo">
                                <thead>
                                    <th class="text-center">Tanggal</th>
                                    <th class="text-center">Debet</th>
                                    <th class="text-center">Kredit</th>
                                    <!-- <th class="text-center">Total Saldo</th> -->
                                </thead>
                            </table>
                        </div>
                        <div class="agent_saldo"></div>
                    </div>
                </div>
            </div>
            <!-- /.porlet-body -->
        </div>
        <!-- /.portlet -->
    </div>
    <!-- /.col-sm-8 -->

    <div class="col-sm-4">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-arrow-up font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Top Up Saldo</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <form class="" action="agent-deposit.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="frm_accountnumber" class="control-label">Nomor rekening <span class="required">*</span></label>
                        <select class="form-control select2" name="frm_accountnumber" required>
                            <option value="" selected>Pilih</option>
                            <?php
                            $var_sql_account = "SELECT rekening_id FROM sp_rekening WHERE user_id = '{$var_userid}'";
                            $var_query_account = mysqli_query($var_con, $var_sql_account);
                            $var_numrows_account = mysqli_num_rows($var_query_account);
                            if ($var_numrows_account > 0) :
                                while ($var_data_account = mysqli_fetch_array($var_query_account)) :
                                    ?>
                                    <option value="<?php echo $var_data_account['rekening_id']?>"><?php echo $var_data_account['rekening_id']?></option>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </select>
                        <span class="help-block warning-text">
                            <?php if (isset($var_error['err']['account_number'])): ?>
                                <?php echo $var_error['err']['account_number'] ?>
                            <?php endif; ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <label for="frm_amount" class="control-label">Nominal <span class="required">*</span></label>
                        <div class="input-group">
                            <span class="input-group-btn"><button type="button" class="btn blue">IDR</button></span>
                            <input type="text" class="form-control mask_number" name="frm_amount" id="frm_amount" placeholder="Nominal" required autofocus>
                        </div>
                        <span class="help-block warning-text">
                            <?php if (isset($var_error['err']['amount'])): ?>
                                <?php echo $var_error['err']['amount'] ?>
                            <?php endif; ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <label for="frm_from" class="control-label">Pengirim <span class="required">*</span></label>
                        <div class="row">
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="frm_from" id="frm_from" placeholder="Nomor rekening pengirim" required pattern="[0-9]+">
                            <p class="help-block warning-text"><?php if(isset($var_error['err']['from'])) { echo $var_error['err']['from']; }; ?></p>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="frm_fromname" id="frm_fromname" placeholder="Nama rekening pengirim" required>
                            <p class="help-block warning-text"><?php if(isset($var_error['err']['fromname'])) { echo $var_error['err']['fromname']; }; ?></p>
                        </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="frm_to" class="control-label">Penerima <span class="required">*</span></label>
                            <select class="form-control select2" name="frm_to" id="frm_to" required>
                                <option value=""></option>
                                <?php
                                $var_sql_bank = "
                                SELECT account_name, account_number, bank_name
                                FROM sp_bank_account
                                JOIN sp_bank ON sp_bank.bank_id = sp_bank_account.bank_id
                                WHERE account_status = '1' ORDER BY account_number
                                ";
                                $var_query_bank = mysqli_query($var_con, $var_sql_bank);
                                $var_numrows_bank = mysqli_num_rows($var_query_bank);
                                ?>
                                <?php if ($var_numrows_bank > 0): ?>
                                    <?php while ($var_data_bank = mysqli_fetch_row($var_query_bank)) : ?>
                                        <option value="<?= $var_data_bank[1] ?>"><?= strtoupper($var_data_bank[0]) ?> - <?= $var_data_bank[2] ?></option>
                                    <?php endwhile; ?>
                                <?php else: ?>
                                    <option value=""></option>
                                <?php endif; ?>
                            </select>
                            <p class="help-block warning-text"><?php if(isset($var_error['err']['bank'])) { echo $var_error['err']['bank']; }; ?></p>
                    </div>

                    <div class="form-group">
                        <label for="frm_img_receipt" class="control-label">Bukti topup <span class="required">*</span></label>
                            <input type="file" class="form-control" name="frm_img_receipt" id="frm_img_receipt" placeholder="" required>
                            <p class="help-block warning-text"><?php if(isset($var_error['err']['receipt'])) { echo $var_error['err']['receipt']; }; ?></p>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pull-right">
                                <button type="submit" class="btn green" name="btn_topup"><i class="fa fa-save"></i> Topup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.porlet-body -->
        </div>
        <!-- /.portlet -->
    </div>
</div>
<!-- END CONTENT -->

<?php
$footer_component = [
    'datatableglobalscript' => '../assets/global/scripts/datatable.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'datableplugin' => '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
require_once 'inc-template/inc-template-footer.php';
?>
<script type="text/javascript">
$(document).ready(function() {
    $(".table_checksaldo").hide();
    $('#frm_saldo').change( function() {
        var items = [];
        var seletedid = $(this).find("option:selected").text();
        var selectedvalue = $(this).val();
        var formData = 'rekening_id='+ selectedvalue;

        $.ajax({
            type        : 'GET',
            url         : 'ajax-get-saldo.php',
            data        : formData,
            success: function(saldoData) {
                if (saldoData) {
                    var data = JSON.parse(saldoData);
                    $(".table_checksaldo").show();
                    $.each(data, function(key, val){
                        items.push('<tr>');
                        items.push('<td class=\'text-center\'>'+val.log_createdate.substring(0, 10)+'</td>');
                        if (val.log_type == '1') {
                            items.push('<td class=\'text-right\'>'+val.deposit_debit+'</td>');
                            // items.push('<td class=\'text-right\'>'+val.log_amount+'</td>');
                            items.push('<td>'+' '+'</td>');
                            // items.push('<td class=\'text-right\'>'+val.deposit_debit+'</td>');
                        } else {
                            items.push('<td>'+' '+'</td>');
                            // items.push('<td class=\'text-right\'>'+val.log_amount+'</td>');
                            items.push('<td class=\'text-right\'>'+val.deposit_credit+'</td>');
                        }
                        items.push('</tr>');
                    });
                    $('<tbody/>', {html: items.join('')}).appendTo('table');
                } else {
                    $(".table_checksaldo").hide();
                    items.length = 0;
                    console.log('no data');
                }
            }
        })
    });
});
</script>

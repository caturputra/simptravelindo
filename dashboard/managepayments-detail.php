<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Modal untuk menampilkan detail pembayaran
*/

require_once '../core/init.php';
require_once '../inc/function/inc-func-users.php';

$var_iid = isset($_GET['id']) ? filter_var($_GET['id'], FILTER_SANITIZE_STRING) :"";

?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Detail Pembayaran : <?php echo $var_iid ?></h4>
        </div>
        <form action="booking-action-approve.php" method="post" class="form-horizontal">
            <div class="modal-body">
                <?php
                $var_sql_detail = "
                SELECT sp_payment.payment_id, sp_payment.payment_date, sp_payment.payment_accountfrom, sp_payment.payment_accountto, sp_payment.payment_confirmdate, sp_payment.payment_verdate, sp_payment.payment_lackofpay, sp_payment.payment_balance, sp_payment.payment_howto, sp_payment.payment_type, sp_payment.payment_receipt, sp_payment.payment_status, sp_payment.schedule_id, sp_payment.payment_confirmedby, sp_payment.payment_accountname, sp_bank_account.account_name
                FROM sp_payment
                JOIN sp_bank_account ON sp_bank_account.account_number = sp_payment.payment_accountto
                JOIN sp_bank ON sp_bank_account.bank_id = sp_bank.bank_id
                WHERE schedule_id = '{$var_iid}'
                LIMIT 1
                ";
                //debug($var_sql_detail);
                $var_query_detail = mysqli_query($var_con, $var_sql_detail);
                while ($var_data_detail = mysqli_fetch_array($var_query_detail)) :
                    ?>
                    <form class="form-horizontal" role="form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="" class="control-label col-sm-4">Rekening Pengirim: </label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static"><?php echo $var_data_detail['payment_accountfrom']; ?> a/n <?php echo $var_data_detail['payment_accountname'] ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="" class="control-label col-sm-4">Penerima: </label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static"><?php echo $var_data_detail['payment_accountto']; ?> a/n <?php echo $var_data_detail['account_name'] ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="" class="control-label col-sm-4">Tanggal Konfirmasi: </label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static"><?php echo dateFormat($var_data_detail['payment_confirmdate']); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="" class="control-label col-sm-4">Nominal: </label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static"><?php echo $var_data_detail['payment_balance']; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="" class="control-label col-sm-4">Bukti Resi: </label>
                                        <div class="col-sm-8">
                                            <div style="width: 20em">
                                                <img class="img img-responsive" src="../images/receipt/<?php echo $var_data_detail['payment_receipt'] ?>" alt="<?php echo $var_data_detail['payment_receipt'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <?php endwhile; ?>
                    <div class="modal-footer">
                        <div class="margiv-top-10 col-sm-offset-2">
                            <button type="button" class="btn blue" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                        </div>
                    </div>
                </form>
                <!-- /.form-horizontal -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->

<?php
    require_once '../core/init.php';

    $var_action = isset($_GET['a']) ? filter_var($_GET['a'], FILTER_SANITIZE_NUMBER_INT) : "";
    $var_cid = isset($_POST['id']) ? $_POST['id'] : "";
    $var_userid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "";

    switch ($var_action) {
        case 1:

        foreach ($var_cid as $key => $value) {

            $var_sql_getphone = "
            SELECT ud.user_detail_phone, ca.user_id
            FROM sp_crew_armada ca
            JOIN sp_user_detail ud ON ud.user_id = ca.user_id
            WHERE ca.crew_armadaid = '{$value}'
            LIMIT 1
            ";
            $var_get_phone = mysqli_fetch_row(mysqli_query($var_con, $var_sql_getphone));

            $var_data_update = ['crew_status' => '1'];
            $var_cond_update = ['crew_armadaid' => $value];
            $var_update = update($var_con, "sp_crew_armada", $var_data_update, $var_cond_update);

            if ($var_update) {
                notification($var_con, [
                    'from' => $var_userid,
                    'to' => $var_get_phone[1],
                    'description' => 'Data anda telah divalidasi. Selamat anda diterima.',
                    'type' => 'konfirmasi',
                ]);
                sendPhone($var_con, ['phone' => $var_get_phone[0], 'message' => 'Data anda telah divalidasi dan diterima.']);
            }
        }


        break;

        case 2:
        foreach ($var_cid as $key => $value) {

            $var_sql_getphone = "
            SELECT ud.user_detail_phone, ca.user_id
            FROM sp_crew_armada ca
            JOIN sp_user_detail ud ON ud.user_id = ca.user_id
            WHERE ca.crew_armadaid = '{$value}'
            LIMIT 1
            ";
            $var_get_phone = mysqli_fetch_row(mysqli_query($var_con, $var_sql_getphone));

            $var_data_update = ['crew_status' => '2'];
            $var_cond_update = ['crew_armadaid' => $value];
            $var_update = update($var_con, "sp_crew_armada", $var_data_update, $var_cond_update);
            
            if ($var_update) {
                notification($var_con, [
                    'from' => $var_userid,
                    'to' => $var_get_phone[1],
                    'description' => 'Data anda telah divalidasi. Mohon maaf anda belum bisa bergabung sebagai kru',
                    'type' => 'konfirmasi',
                ]);
                sendPhone($var_con, ['phone' => $var_get_phone[0], 'message' => 'Data anda telah divalidasi dan anda belum diterima sebagai kru.']);
            }
        }

    }
?>

kwin <?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Proses pengecekan armada
*/

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';

//set variabel
$var_uid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_STRING)) : "";
$var_aid = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : "";
$var_aid = base64_decode($var_aid);
$var_act = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var($_GET['act'], FILTER_SANITIZE_STRING)) : "";
$var_sid = isset($_GET['sid']) ? mysqli_escape_string($var_con, filter_var($_GET['sid'], FILTER_SANITIZE_STRING)) : "";
$var_sid = base64_decode($var_sid);

if (isset($_POST['btn_check'])) {
    if ($_POST['frm_action'] == 'day') {
        $var_p_aid = isset($_POST['frm_armadaid']) ? $_POST['frm_armadaid'] : routeUrl('index.php?p=armada-monitor-byschedule');
        $var_condition = isset($_POST['frm_condition']) ? $_POST['frm_condition'] : "0";
        $var_param = isset($_POST['frm_param']) ? $_POST['frm_param'] : "";

        mysqli_autocommit($var_con, FALSE);
        mysqli_begin_transaction($var_con);

        $var_data_confirm = [
            'condition_checkdate' => date('Y-m-d H:i:s'),
            'user_id' => $var_uid,
            'condition_type' => '0',
            'armada_regnumber' => $var_p_aid
        ];
        $var_insert_confirm = insert($var_con, "sp_armada_condition", $var_data_confirm);
        $var_get_lastid = mysqli_insert_id($var_con);

        foreach ($var_param as $key => $value) {
            $var_data_params[$key] = [
                'detail_condition' => $value,
                'params_id' => $key,
                'condition_id' => $var_get_lastid
            ];
            $var_insert_param = insert($var_con, "sp_armada_condition_detail", $var_data_params[$key]);
        }

        $var_update_one = ['condition_id' => $var_get_lastid];
        $var_cond = ['schedule_id' => $_POST['frm_scheduleid']];
        $var_update = update($var_con, "sp_armada_conditiononday", $var_update_one, $var_cond);

        $var_sql_meter = "call countAvgCond(". $var_get_lastid .")";
        $var_query_meter = mysqli_query($var_con, $var_sql_meter);

        if ($var_query_meter) {
            mysqli_commit($var_con);
            $var_sql_getmeter = "
            SELECT ac.condition_meter, acd.schedule_id
            FROM sp_armada_conditiononday acd
            JOIN sp_armada_condition ac ON ac.condition_id = acd.condition_id
            WHERE acd.schedule_id = '{$_POST['frm_scheduleid']}'
            LIMIT 1
            ";
            $var_query_getmeter = mysqli_query($var_con, $var_sql_getmeter);
            $var_data_getmeter = mysqli_fetch_row($var_query_getmeter);

            if ($var_data_getmeter[0] > $_ENV['MIN_VEHICLE']) {
                update($var_con, "sp_schedule", ['schedule_status' => '3'], ['schedule_id' => $var_data_getmeter[1]]);
            }
            setNotif(['success' => 'Pengecekan armada selesai dilakukan.']);
            routeUrl('index.php?p=armada-monitor-byschedule');
            die();
        } else {
            mysqli_rollback($var_con);
            die();
        }
    } elseif ($_POST['frm_action'] == '') {
        $var_p_aid = isset($_POST['frm_armadaid']) ? $_POST['frm_armadaid'] : routeUrl('index.php?p=armada-condition');
        $var_condition = isset($_POST['frm_condition']) ? $_POST['frm_condition'] : "0";
        $var_param = isset($_POST['frm_param']) ? $_POST['frm_param'] : "";

        mysqli_autocommit($var_con, FALSE);
        mysqli_begin_transaction($var_con);

        $var_data_confirm = [
            'condition_checkdate' => date('Y-m-d H:i:s'),
            'user_id' => $var_uid,
            'armada_regnumber' => $var_p_aid
        ];
        $var_insert_confirm = insert($var_con, "sp_armada_condition", $var_data_confirm);
        $var_get_lastid = mysqli_insert_id($var_con);

        foreach ($var_param as $key => $value) {
            $var_data_params[$key] = [
                'detail_condition' => $value,
                'params_id' => $key,
                'condition_id' => $var_get_lastid
            ];
            $var_insert_param = insert($var_con, "sp_armada_condition_detail", $var_data_params[$key]);
        }

        $var_sql_meter = "call countAvgCond(". $var_get_lastid .")";
        $var_query_meter = mysqli_query($var_con, $var_sql_meter);

        if ($var_query_meter) {
            mysqli_commit($var_con);
            routeUrl('index.php?p=armada-condition');
            die();
        } else {
            mysqli_rollback($var_con);
            die();
        }
    }
}

$var_title = "Cek Armada";
require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>

<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-sm-8">
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="fa fa-gear font-green"></i>
                    <span class="caption-subject font-green bold uppercase">Cek Kondisi Armada</span>
                </div>
            </div>
            <div class="portlet-body">
                <form action="index.php?p=armada-condition-check.php" method="post">
                    <input type="hidden" name="frm_armadaid" value="<?= $var_aid ?>">
                    <input type="hidden" name="frm_action" value="<?= $var_act ?>">
                    <input type="hidden" name="frm_scheduleid" value="<?= $var_sid ?>">
                    <div class="form-group">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <tbody>
                                    <?php
                                    $var_sql_param = "SELECT params_id, params_name FROM sp_check_params ORDER BY params_id";
                                    $var_query_param = mysqli_query($var_con, $var_sql_param);
                                    ?>
                                    <?php while ($var_data_param = mysqli_fetch_row($var_query_param)) : ?>
                                        <tr>
                                            <td class="text-right">
                                                <span class="pull-left"><?= $var_data_param[1] ?></span>
                                                <div class="mt-radio-inline">
                                                    <input type="radio" name="frm_param[<?= $var_data_param[0] ?>]" value="1" checked class="make-switch switch-radio1" data-on-text="<i class='fa fa-thumbs-up'></i>" data-off-text="<i class='fa fa-thumbs-down'></i>">
                                                    <input type="radio" name="frm_param[<?= $var_data_param[0] ?>]" value="0" class="make-switch switch-radio1" data-on-text="<i class='fa fa-thumbs-up'></i>" data-off-text="<i class='fa fa-thumbs-down'></i>">
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endwhile; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <a href="index.php?p=armada-condition.php" class="btn dark btn-outline"><i class="fa fa-chevron-left"></i> Kembali</a>
                            <div class="pull-right">
                                <button type="submit" name="btn_check" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /.form check -->
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<!-- END PAGE BASE CONTENT -->

<?php
$footer_component = [
    'bs' => '../assets/pages/scripts/components-bootstrap-switch.js',
];
require_once 'inc-template/inc-template-footer.php';
?>

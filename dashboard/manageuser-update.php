<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk memperbaharui data pengguna
*/

require_once '../core/init.php';
require_once '../inc/function/inc-func-users.php';

$var_userid = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT)) :"";
?>
<div class="modal-dialog modal-xs">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ubah pengguna : <?php echo $var_userid ?></h4>
            </div>
            <form action="index.php?p=manageuser.php&amp;act=update" method="post" enctype="multipart/form-data" id="form_user_add">
                <?php
                $var_sql_user = "
                SELECT u.user_level, l.level_name, u.user_status
                FROM sp_user u JOIN sp_level l ON l.level_id = u.user_level
                WHERE u.user_id = '{$var_userid}'";

                $var_query_user = mysqli_query($var_con, $var_sql_user);
                while ($var_data_user = mysqli_fetch_array($var_query_user)) :
                    ?>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="frm_userid" value="<?php echo $var_userid?>">
                                <div class="form-group">
                                    <label for="frm_role" class=" control-label">Peran</label>
                                    <select class="form-control select2" name="frm_role" id="frm_role">
                                        <option value="<?php echo $var_data_user['user_level'] ?>"><?php echo $var_data_user['level_name'] ?></option>
                                        <?php
                                        $var_sql = "SELECT * FROM sp_level";
                                        $var_Level = mysqli_query($var_con, $var_sql);
                                        while ($var_data = mysqli_fetch_array($var_Level)) :
                                            ?>
                                            <option value="<?php echo $var_data['level_id']; ?>">
                                                <?php echo $var_data['level_name']; ?>
                                            </option>
                                        <?php endwhile; ?>
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                        </div>
                                    </select>
                                    <span class="help-block warning-text"><?php if (!empty($var_error['role'])) {
                                        echo $var_error['role'];
                                    } ?></span>
                                </div>
                                <!-- /form-group -->

                                <div class="form-group">
                                    <label for="frm_status" class=" control-label">Status</label>
                                    <?php switch ($var_data_user['user_status']) : case 'active': ?>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="frm_status" id="optionsRadios1" value="active" checked> Aktif
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="frm_status" id="optionsRadios2" value="suspend"> Nonaktif
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php break; case 'suspend': ?>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="frm_status" id="optionsRadios1" value="active"> Aktif
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="frm_status" id="optionsRadios2" value="suspend" checked> Nonaktif
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php break; endswitch; ?>
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                    <input type="submit" class="btn btn-flat btn-primary" name="btn_update" value="Ubah">
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->

<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk menghapus avatar pengguna
*/

require_once '../core/init.php';
require_once '../inc/function/inc-func-users.php';

$var_user_id = isset($_GET['id']) ? (int) mysqli_real_escape_string($var_con, $_GET['id']):"";

$var_sql_delete = "SELECT user_detail_avatar FROM sp_user_detail WHERE user_id='{$var_user_id}'";
$var_query_delete = mysqli_query($var_con, $var_sql_delete);
$var_data_delete = mysqli_fetch_row($var_query_delete);
$var_ava_dir = "../images/ava/" . $var_data_delete[0];
$var_ava_name = $var_data_delete[0];

if ($var_data_delete[0] == $var_ava_name) {
    if (file_exists($var_ava_dir)) {
        unlink($var_ava_dir);
        $var_delete = userDelete($var_con, $var_user_id);
        if ($var_delete) {
            include 'logout.php';
        }
    } else {
        $var_delete = userDelete($var_con, $var_user_id);
        if ($var_delete) {
            include 'logout.php';
        }
    }
}
?>

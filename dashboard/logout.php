<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk keluar dari sistem
*/

require_once '../core/init.php';
session_unset($_SESSION['userid']);
session_unset($_SESSION['levelid']);
$_SESSION = array();
// if(isset($_GET["session_expired"])) {
// 	$var_url .= "?session_expired=" . $_GET["session_expired"];
// }
session_destroy();
setNotif(['success' => 'Anda telah keluar dari Sistem!']);
routeUrl('login');
die();
?>

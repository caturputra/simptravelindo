<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman yang memuat daftar pembayaran yang telah dilakukan
*/

$var_title = "Tagihan";

require_once '../core/init.php';

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<div class="row">
    <div class="col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Daftar Tagihan</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="table-responsive">
                        <div class="col-sm-12">
                            <table class="table table-hover table-striped table-bordered" id="tableListPayment">
                                <thead>
                                    <th class="text-center" style="width: 2em;">No.</th>
                                    <th class="text-center">Invoice</th>
                                    <th class="text-center">Nominal</th>
                                    <th class="text-center">Kekurangan</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center"></th>
                                </thead>
                                <tbody>
                                    <?php
                                    //menampilkan payment
                                    $var_sql_payment = "
                                    SELECT ot.od_invoice, p.payment_balance, p.payment_lackofpay, p.payment_status, IF (p.payment_lackofpay = '0', 'Lunas', 'Belum Lunas') as status_tagihan, ot.od_id, ot.od_invoice
                                    FROM sp_payment p
                                    JOIN sp_schedule s ON s.schedule_id = p.schedule_id
                                    JOIN sp_order_transaction ot ON ot.od_id = s.od_id
                                    WHERE ot.user_id ='{$_SESSION['userid']}' && p.payment_confirmdate = (SELECT MAX(payment_confirmdate) FROM sp_payment)
                                    GROUP BY ot.od_invoice
                                    ORDER BY ot.od_id DESC
                                    ";
                                    $var_query_payment = mysqli_query($var_con, $var_sql_payment);
                                    $num = (int) 1;
                                    ?>
                                    <?php while ($var_data_payment = mysqli_fetch_array($var_query_payment)) : ?>
                                        <tr>
                                            <td class="text-center">
                                                <?php echo $num++; ?>
                                            </td>
                                            <td class="text-center">
                                                <?php echo $var_data_payment['od_invoice'] ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo "<span class=\"pull-left\">IDR</span> " . number_format($var_data_payment['payment_balance'], 2,',','.') ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo "<span class=\"pull-left\">IDR</span> " . number_format($var_data_payment['payment_lackofpay'], 2,',','.') ?>
                                            </td>
                                            <td class="text-center">
                                                <?php switch ($var_data_payment['payment_status']) : case '0': ?>
                                                <label class="label label-info label-sm"> <?php echo ucwords("pending") ?> </label>
                                                <?php break; case '1': ?>
                                                <label class="label label-success label-sm"><?php echo ucwords("diterima") ?> </label>
                                                <?php break; case '2': ?>
                                                <label class="label label-danger label-sm"><?php echo  ucwords("ditolak") ?> </label>
                                                <?php break; endswitch; ?>
                                            </td>
                                            <td class="text-center">
                                                <a href="booking-payment-timeline.php?invoice=<?php echo base64_encode($var_data_payment['od_invoice']); ?>" data-toggle="tooltip" data-placement="bottom" title="Histori Pembayaran" class="btn btn-sm blue"><i class="fa fa-eye"></i></a>

                                                <?php if ($var_data_payment['payment_lackofpay'] > 0) :  ?>
                                                    <a href="index.php?p=booking-payment.php&amp;id=<?php echo base64_encode($var_data_payment['od_id']); ?>&amp;invoice=<?php echo base64_encode($var_data_payment['od_invoice']); ?>" data-toggle="tooltip" data-placement="bottom" title="Bayar" class="btn btn-sm green-meadow"><i class="fa fa-play"></i> Bayar</a>
                                                <?php else: ?>
                                                    <small class="btn purple btn-sm"><?php echo ucwords("lunas") ?> </small>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endwhile; ?>
                                </tbody>
                            </table>
                            <!-- /.tableListPayment -->
                        </div>
                        <!-- /.col-sm-12 -->
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.porlet-body -->
        </div>
        <!-- /.portlet -->
    </div>
    <!-- /.col-sm-6 -->
</div>
<!-- END CONTENT -->
<?php
$footer_component = [
    'datatableglobalscript' => '../assets/global/scripts/datatable.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'datableplugin' => '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
require_once 'inc-template/inc-template-footer.php';
?>
<script type="text/javascript">
$(document).ready(function() {
    $("#tableListPayment").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autowidth": false,
        "lengthChange": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        }
    });
});
</script>

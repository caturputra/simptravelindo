<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk validasi armada
*/

$var_title = "Validasi Armada";

require_once '../core/init.php';

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

$var_id = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : "";
$var_action = isset($_GET['s']) ? mysqli_escape_string($var_con, filter_var($_GET['s'], FILTER_SANITIZE_NUMBER_INT)) : "";
$var_userid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "";

switch ($var_action) {
    case 1:
    $var_data_update = ['armada_status' => 'approved', 'user_id' => $var_userid, 'armada_modifyat' => date('Y-m-d H:i:s')];
    $var_cond_update = ['armada_regnumber' => $var_id];
    $var_update = update($var_con, "sp_armada", $var_data_update, $var_cond_update);

    if ($var_update) {
        setNotif(['success' => 'Armada dengan nomor plat ' . $var_id . ' berhasil divalidasi.']);
        routeUrl('index.php?p=validate-vehicles');
        die();
    } else {
        setNotif(['danger' => 'Armada dengan nomor plat ' . $var_id . ' gagal divalidasi.']);
        routeUrl('index.php?p=validate-vehicles');
        die();
    }
    break;

    case 2:
    $var_data_update = ['armada_status' => 'rejected', 'user_id' => $var_userid, 'armada_modifyat' => date('Y-m-d H:i:s')];
    $var_cond_update = ['armada_regnumber' => $var_id];
    $var_update = update($var_con, "sp_armada", $var_data_update, $var_cond_update);

    if ($var_update) {
        setNotif(['success' => 'Armada dengan nomor plat ' . $var_id . ' berhasil divalidasi.']);
        routeUrl('index.php?p=validate-vehicles');
        die();
    } else {
        setNotif(['danger' => 'Armada dengan nomor plat ' . $var_id . ' gagal divalidasi.']);
        routeUrl('index.php?p=validate-vehicles');
        die();
    }
    break;
}

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<div class="mt-bootstrap-tables">
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bus font-blue"></i>
                        <span class="caption-subject font-blue bold uppercase">Daftar Armada</span>
                    </div>
                    <div class="actions">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                                <a class="btn green" href="javascript:;" data-toggle="dropdown">
                                    <i class="fa fa-gear"></i> Aksi
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a id="approve_bulk"><i class="fa fa-check"></i> Setujui <span class="rows_selected select_count" >0 Terpilih</span></a></li>
                                    <li><a id="reject_bulk"><i class="fa fa-exclamation-triangle"></i> Tolak <span class="rows_selected select_count" >0 Terpilih</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body flip-scroll">
                    <div class="row">
                        <div class="table-responsive">
                            <div class="col-sm-12">
                                <table id="tableListArmada" class="table table-hover table-striped table-bordered flip-content">
                                    <thead class="flip-content">
                                        <th class="text-center"><label class="mt-checkbox mt-checkbox-outline"><input type="checkbox" id="select_all"><span></span></label></th>
                                        <th class="text-center">No. Plat</th>
                                        <th class="text-center">Merk (Tipe)</th>
                                        <th class="text-center">Model</th>
                                        <th class="text-center">Warna</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center"></th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        //menampilkan Armada
                                        $var_sql_armada = "
                                        SELECT a.armada_regnumber, a.armada_brand, a.armada_typevariant, a.armada_type, a.armada_model, a.armada_productionyear, a.armada_chassisnumber, a.armada_enginenumber, a.armada_typefuel, a.armada_fuelsize, a.armada_odometer, a.armada_image, a.armada_color, a.armada_status, a.armada_createat, a.armada_modifyat, a.armada_seat
                                        FROM sp_armada a
                                        WHERE a.armada_status = '". strtolower('pending')."'
                                        ";
                                        $var_query_armada = mysqli_query($var_con, $var_sql_armada);
                                        $num = (int) 1;
                                        while ($var_data_armada = mysqli_fetch_array($var_query_armada)) :
                                            ?>
                                            <tr id="<?= trim($var_data_armada['armada_regnumber']) ?>">
                                                <td class="text-center">
                                                    <label class="mt-checkbox mt-checkbox-outline"><input type="checkbox" class="emp_checkbox" data-emp-id="<?php echo $var_data_armada['armada_regnumber']; ?>"><span></span></label>
                                                </td>
                                                <td class="text-center">
                                                    <?php echo $var_data_armada['armada_regnumber']; ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php echo ucwords($var_data_armada['armada_brand'] . ' ' . '( '. $var_data_armada['armada_typevariant'] .' )'); ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php echo ucwords($var_data_armada['armada_model']); ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php echo ucwords($var_data_armada['armada_color']); ?>
                                                </td>
                                                <td class="text-center" >
                                                    <?php
                                                    switch ($var_data_armada['armada_status']) {
                                                        case 'approved':
                                                        echo '<span class="label label-success label-sm">' . ucwords('disetujui') . '</span>';
                                                        break;

                                                        case 'rejected':
                                                        echo '<span class="label label-danger label-sm">' . ucwords('ditolak') . '</span>';
                                                        break;

                                                        default:
                                                        echo '<span class="label label-warning label-sm">' . ucwords('ditunda') . '</span>';
                                                        break;
                                                    }
                                                    ?>
                                                </td>
                                                <td class="text-center">
                                                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Detail Armada" class="btn btn-sm blue open_modal_detail" id="<?php echo $var_data_armada['armada_regnumber'] ?>"><i class="fa fa-eye"></i> </a>
                                                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Setujui" class="btn btn-sm green" onclick="confirm_modal_approved('validate-vehicles.php?s=1&amp;id=<?php echo $var_data_armada['armada_regnumber'] ?>');"><i class="fa fa-check"></i> </a>
                                                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Tolak" class="btn btn-sm red" onclick="confirm_modal_rejected('validate-vehicles.php?s=2&amp;id=<?php echo $var_data_armada['armada_regnumber'] ?>');"><i class="fa fa-exclamation-triangle"></i> </a>
                                                </td>
                                            </tr>
                                        <?php endwhile; ?>
                                    </tbody>
                                </table>
                                <!-- /.tableListArmada -->
                            </div>
                            <!-- /.col-sm-12 -->
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.porlet-body -->
            </div>
            <!-- /.portlet -->
        </div>
        <!-- /.col-sm-6 -->
    </div>
</div>
<!-- /.mt-bootstrap-tables -->
<!-- END CONTENT -->

<!-- modal untuk approve data -->
<div class="approval-modal">
    <div id="ModalApprove" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Yakin menyetujui armada ini?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                    <a href="#" class="btn green" id="approve_link"><i class="fa fa-check"></i> Setujui</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.approve-modal -->

<!-- modal untuk reject data -->
<div class="reject-modal">
    <div id="ModalReject" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Yakin menolak armada ini?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal"><i class="fa fa-chevron-left"></i> Tutup</button>
                    <a href="#" class="btn red" id="reject_link"><i class="fa fa-exclamation-triangle"></i> Tolak</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.reject-modal -->

<!-- modal untuk penolakan data -->
<div class="detail-modal">
    <div id="ModalDetail" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    </div>
    <!-- /.modal -->
</div>
<!-- /.reject-modal -->

<?php
$footer_component = [
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    //'sample' => '../assets/custom/form-validation-user-add.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
?>
<?php require 'inc-template/inc-template-footer.php'; ?>
<script type="text/javascript">
$(document).ready(function() {
    $(".open_modal_detail").click(function(e) {
        var id = $(this).attr("id");
        $.ajax({
            url: "managearmada-detail.php",
            type: "GET",
            data: {
                id: id,
            },
            success: function(ajaxData) {
                $("#ModalDetail").html(ajaxData);
                $("#ModalDetail").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    });

    $("#tableListArmada").DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autowidth": false,
        "lengthChange": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
        }
    });

    // select all checkbox
    $('#select_all').on('click', function(e) {
        if($(this).is(':checked',true)) {
            $(".emp_checkbox").prop('checked', true);
        }
        else {
            $(".emp_checkbox").prop('checked',false);
        }
        // set all checked checkbox count
        $(".select_count").html($("input.emp_checkbox:checked").length+" Selected");
    });

    // set particular checked checkbox count
    $(".emp_checkbox").on('click', function(e) {
        $(".select_count").html($("input.emp_checkbox:checked").length+" Selected");
    });

    $('#approve_bulk').on('click', function(e) {
        var employee = [];
        $(".emp_checkbox:checked").each(function() {
            employee.push($(this).data('emp-id'));
        });

        if(employee.length <=0) {
            alert("Pilih item terlebih dahulu.");
        } else {
            WRN_PROFILE_APPROVE = "Yakin akan menyetujui "+(employee.length>1?"banyak":"ini")+" item?";

            var checked = confirm(WRN_PROFILE_APPROVE);

            if(checked == true) {
                var selected_values = employee;

                $.ajax({
                    type: "POST",
                    url: "ajax-bulk-actionarmada.php?a=1",
                    cache:false,
                    data: {id: selected_values},
                    dataType: "html",
                    success: function(response) {
                        location.reload();
                    }
                });
            }
        }
    });

    $('#reject_bulk').on('click', function(e) {
        var employee = [];
        $(".emp_checkbox:checked").each(function() {
            employee.push($(this).data('emp-id'));
        });

        if(employee.length <=0) {
            alert("Pilih item terlebih dahulu.");
        } else {
            WRN_PROFILE_APPROVE = "Yakin akan menolak "+(employee.length>1?"banyak":"ini")+" item?";

            var checked = confirm(WRN_PROFILE_APPROVE);

            if(checked == true) {
                var selected_values = employee;

                $.ajax({
                    type: "POST",
                    url: "ajax-bulk-actionarmada.php?a=2",
                    cache:false,
                    data: {id: selected_values},
                    dataType: "html",
                    success: function(response) {
                        location.reload();
                    }
                });
            }
        }
    });
});
</script>

<!-- Script konfirmasi modal -->
<script type="text/javascript">
function confirm_modal_approved(approve_url) {
    $('#ModalApprove').modal('show', {
        backdrop: 'static'
    });
    document.getElementById('approve_link').setAttribute('href', approve_url);
}

function confirm_modal_rejected(reject_url) {
    $('#ModalReject').modal('show', {
        backdrop: 'static'
    });
    document.getElementById('reject_link').setAttribute('href', reject_url);
}
</script>

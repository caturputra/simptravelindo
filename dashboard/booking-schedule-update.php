<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk atur ulang penjadwalan kru armada
*/

require_once '../core/init.php';

$var_title = "Perbaharui Jadwal";

$var_uid = isset($_SESSION['userid']) ? filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT) : "";
//retrieve query string schedule_id
$var_sid = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : "";
$var_cid = isset($_GET['c']) ? mysqli_escape_string($var_con, filter_var($_GET['c'], FILTER_SANITIZE_STRING)) : "";
$var_sid = base64_decode($var_sid);

$var_driver = isset($_POST['frm_driver']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_driver'], FILTER_SANITIZE_STRING)) : "";
$var_codriver = isset($_POST['frm_codriver']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_codriver'], FILTER_SANITIZE_STRING)) : "";
$var_from = isset($_POST['frm_from']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_from'], FILTER_SANITIZE_STRING)) : "";
$var_to = isset($_POST['frm_to']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_to'], FILTER_SANITIZE_STRING)) : "";
$var_psid = isset($_POST['frm_sid']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_sid'], FILTER_SANITIZE_STRING)) : "";
$var_pcid = isset($_POST['frm_cid']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_cid'], FILTER_SANITIZE_STRING)) : "";
$var_temp = [];

$var_sql_creator = "
SELECT ca.user_id, ud.user_detail_phone
FROM sp_crew_armada ca
JOIN sp_user_detail ud ON ud.user_id = ca.user_id
WHERE ca.crew_armadaid = '{$var_pcid}'";
$var_get_creator = mysqli_fetch_row(mysqli_query($var_con, $var_sql_creator));

if (isset($_POST['btn_update'])) {
    if (!empty($var_driver)) {
        $var_sql_update = "
        UPDATE sp_crew_detail SET crew_armadaid = '{$var_driver}', crew_action = '0' WHERE schedule_id = '{$var_psid}' AND crew_armadaid = ( SELECT ca.crew_armadaid FROM sp_crew_armada ca WHERE ca.crew_armadaid = '{$var_pcid}')
        ";
        $var_query_udpate = mysqli_query($var_con, $var_sql_update);
        if ($var_query_udpate && mysqli_affected_rows($var_con)) {
            notification($var_con, [
                'from' => $var_uid,
                'to' => $var_get_creator[0],
                'description' => 'Anda mendapat penugasan, segera konfirmasi.',
                'type' => 'penugasan',
            ]);
            sendPhone($var_con, [
                'phone' => $var_get_creator[1],
                'message' => 'Anda mendapat penugasan. Segara konfirmasi 1x24 jam. http://' . $_SERVER['SERVER_NAME']
            ]);
            setNotif(['success' => 'Perbaharuan jadwal baru berhasil.']);
            routeUrl('index.php?p=booking-schedule-list');
            die();
        }
    }

    if (!empty($var_codriver)) {
        $var_sql_update = "
        UPDATE sp_crew_detail SET crew_armadaid = '{$var_codriver}', crew_action = '0' WHERE schedule_id = '{$var_psid}' AND crew_armadaid = ( SELECT ca.crew_armadaid FROM sp_crew_armada ca WHERE ca.crew_armadaid = '{$var_pcid}')
        ";
        $var_query_udpate = mysqli_query($var_con, $var_sql_update);
        if ($var_query_udpate && mysqli_affected_rows($var_con)) {
            notification($var_con, [
                'from' => $var_uid,
                'to' => $var_get_creator[0],
                'description' => 'Anda mendapat penugasan, segera konfirmasi.',
                'type' => 'penugasan',
            ]);
            sendPhone($var_con, [
                'phone' => $var_get_creator[1],
                'message' => 'Anda mendapat penugasan. Segara konfirmasi 1x24 jam. http://' . $_SERVER['SERVER_NAME']
            ]);
            setNotif(['success' => 'Perbaharuan jadwal baru berhasil.']);
            routeUrl('index.php?p=booking-schedule-list');
            die();
        }
    }

}

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN CONTENT -->
<div class="row">
    <div class="col-sm-6">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Perbaharui Jadwal</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form class="form-horizontal" action="index.php?p=booking-schedule-update.php" method="post">
                            <input type="hidden" name="frm_sid" value="<?= $var_sid ?>">
                            <input type="hidden" name="frm_cid" value="<?= $_GET['c'] ?>">
                            <input type="hidden" name="frm_from" value="<?= $_GET['from']?>">
                            <input type="hidden" name="frm_to" value="<?= $_GET['to']?>">
                            <?php
                            $var_sql_driver = "
                            SELECT cd.crew_armadaid, concat(udc.user_detail_firstname, ' ', udc.user_detail_lastname) as crews_name, ud.user_level
                            FROM sp_crew_detail cd
                            JOIN sp_schedule s ON s.schedule_id = cd.schedule_id
                            JOIN sp_order_transaction ot ON ot.od_id = s.od_id
                            JOIN sp_crew_armada ca ON cd.crew_armadaid = ca.crew_armadaid
                            JOIN sp_user ud ON ud.user_id = ca.user_id
                            JOIN sp_user_detail udc ON udc.user_id = ud.user_id
                            WHERE cd.schedule_id = '{$var_sid}' AND cd.crew_armadaid = '{$var_cid}'
                            ";
                            $var_query_driver = mysqli_query($var_con, $var_sql_driver);
                            ?>
                            <?php while($var_data_driver = mysqli_fetch_row($var_query_driver)): ?>
                                <?php if ($var_data_driver[2] == '8'): ?>
                                    <div class="form-group">
                                        <label for="frm_driver" class="col-sm-4 control-label">Sopir</label>
                                        <div class="col-sm-8">
                                            <select type="text" class="form-control select2" name="frm_driver" id="frm_driver">
                                                <!-- <option value="<?//= $var_data_driver[0] ?>" selected><?//= $var_data_driver[1] ?></option> -->
                                                <?php
                                                $var_sql_drivers = "
                                                SELECT ca.crew_armadaid, u.user_level, concat(ud.user_detail_firstname, ' ', ud.user_detail_lastname) as crew_name
                                                FROM sp_crew_armada ca
                                                JOIN sp_user u ON u.user_id = ca.user_id
                                                JOIN sp_user_detail ud ON ud.user_id = u.user_id
                                                JOIN sp_crew_armada_detail cad ON cad.crew_armadaid = ca.crew_armadaid
                                                WHERE (u.user_level = '8' && ca.crew_status = '1') && (ca.crew_armadaid
                                                NOT IN ( SELECT cd.crew_armadaid FROM sp_schedule s JOIN sp_crew_detail cd ON cd.schedule_id = s.schedule_id WHERE (cd.crew_datefrom BETWEEN '{$_GET['from']}' AND '{$_GET['to']}') OR (cd.crew_dateto BETWEEN '{$_GET['from']}' AND '{$_GET['to']}')))
                                                GROUP BY ca.crew_armadaid
                                                ";
                                                $var_query_drivers = mysqli_query($var_con, $var_sql_drivers);
                                                $var_num_rows = mysqli_num_rows($var_query_drivers);
                                                ?>
                                                <?php if ($var_num_rows > 0) : ?>
                                                    <?php while ($var_data_drivers = mysqli_fetch_array($var_query_drivers)) : ?>
                                                        <option value="<?= $var_data_drivers['crew_armadaid']; ?>"><?= $var_data_drivers['crew_name']; ?></option>
                                                    <?php endwhile; ?>
                                                <?php else: ?>
                                                    <option value="" disabled>Data tidak ditemukan</option>
                                                <?php endif; ?>
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if ($var_data_driver[2] == '12'): ?>
                                    <div class="form-group">
                                        <label for="frm_codriver" class="col-sm-4 control-label">Kernet</label>
                                        <div class="col-sm-8">
                                            <select type="text" class="form-control select2" name="frm_codriver" id="frm_codriver">
                                                <!-- <option value="<?//= $var_data_driver[0] ?>" selected><?//= $var_data_driver[1] ?></option> -->
                                                <?php
                                                $var_sql_driver = "
                                                SELECT ca.crew_armadaid, u.user_level, concat(ud.user_detail_firstname, ' ', ud.user_detail_lastname) as crew_name
                                                FROM sp_crew_armada ca
                                                JOIN sp_user u ON u.user_id = ca.user_id
                                                JOIN sp_user_detail ud ON ud.user_id = u.user_id
                                                JOIN sp_crew_armada_detail cad ON cad.crew_armadaid = ca.crew_armadaid
                                                WHERE (u.user_level = '12' && ca.crew_status = '1') && (ca.crew_armadaid
                                                NOT IN ( SELECT cd.crew_armadaid FROM sp_schedule s JOIN sp_crew_detail cd ON cd.schedule_id = s.schedule_id WHERE (cd.crew_datefrom BETWEEN '{$_GET['from']}' AND '{$_GET['to']}') OR (cd.crew_dateto BETWEEN '{$_GET['from']}' AND '{$_GET['to']}')))
                                                ";
                                                $var_query_driver = mysqli_query($var_con, $var_sql_driver);
                                                $var_num_rows = mysqli_num_rows($var_query_driver);
                                                ?>
                                                <?php if ($var_num_rows > 0) : ?>
                                                    <?php while ($var_data_driver = mysqli_fetch_array($var_query_driver)) : ?>
                                                        <option value="<?= $var_data_driver['crew_armadaid']; ?>"><?= $var_data_driver['crew_name']; ?></option>
                                                    <?php endwhile; ?>
                                                <?php else: ?>
                                                    <option value="" disabled>Data tidak ditemukan</option>
                                                <?php endif; ?>
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endwhile; ?>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pull-right">
                                        <button type="submit" class="btn green" name="btn_update"><i class="fa fa-pencil"></i> Perbaharui Kru</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.porlet-body -->
        </div>
        <!-- /.portlet -->
    </div>
    <!-- /.col-sm-6 -->
</div>
<!-- END CONTENT -->

<?php
$footer_component = [
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
require_once 'inc-template/inc-template-footer.php';
?>

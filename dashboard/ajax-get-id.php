<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Ajax untuk ambil data order id
*/

require_once '../core/init.php';

$var_oid = isset($_GET['id']) ? mysqli_escape_string($var_con, filter_var($_GET['id'], FILTER_SANITIZE_STRING)) : "";

$var_sql_id = "
SELECT od_id FROM sp_order_transaction WHERE od_id = '{$var_oid}' LIMIT 1
";

$var_query_id = mysqli_query($var_con, $var_sql_id);
$var_numrows_id = mysqli_num_rows($var_query_id);
$var_data = [];

if ($var_numrows_id > 0) {
    while ($var_data_id = mysqli_fetch_row($var_query_id)) {
        $temp = [];
        $temp['od_id'] = $var_data_id[0];

        array_push($var_data, $temp);
    }
    echo json_encode($var_data);
}
?>

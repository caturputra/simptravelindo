<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk invoice
*/

require_once '../core/init.php';

$var_action = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var($_GET['act'], FILTER_SANITIZE_STRING)) : '' ;
$var_inv_num = isset($_GET['invoice']) ? mysqli_escape_string($var_con, filter_var($_GET['invoice'], FILTER_SANITIZE_STRING)) : '' ;
$var_inv_num = base64_decode($var_inv_num);

//retrieve invoice number
$var_sql_inv = "
SELECT concat(u.user_detail_firstname,' ',u.user_detail_lastname) as member, u.user_detail_phone, ot.od_invoice, ot.od_id, ot.od_selling
FROM sp_order_transaction ot
JOIN sp_user_detail u ON ot.user_id = u.user_id
WHERE od_invoice = '{$var_inv_num}'
LIMIT 1
";
$var_query_inv = mysqli_query($var_con, $var_sql_inv);
$var_rows_inv = mysqli_num_rows($var_query_inv);
$var_data_inv = mysqli_fetch_array($var_query_inv);

$var_title = "Invoice";

$head_component = [
    'datatablescss'=>'../assets/global/plugins/datatables/datatables.min.css',
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
    'invoicecss' => '../assets/pages/css/invoice.min.css'
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';

?>

<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb hidden-print">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- BEGIN PAGE BASE CONTENT -->
<div class="invoice">
    <div class="row invoice-logo">
        <div class="col-xs-6 invoice-logo-space">
            <img src="../assets/global/home/images/logo.png" class="img-responsive" alt="" />
        </div>
        <div class="col-xs-6">
            <p>
                #<?php echo $var_data_inv['od_invoice'] ?>
            </p>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-xs-4">
            <h3>Tamu:</h3>
            <ul class="list-unstyled">
                <li> <?php echo $var_data_inv['member'] ?> </li>
                <li> <?php echo $var_data_inv['user_detail_phone'] ?> </li>
            </ul>
        </div>
        <div class="col-xs-4 invoice-payment">
            <h3>Pembayaran:</h3>
            <ul class="list-unstyled">
                <?php
                $var_sql_bank = "
                SELECT account_name, account_number, bank_name
                FROM sp_bank_account
                JOIN sp_bank ON sp_bank.bank_id = sp_bank_account.bank_id
                ORDER BY account_number
                ";
                $var_query_bank = mysqli_query($var_con, $var_sql_bank);
                ?>
                <?php while ($var_data_bank = mysqli_fetch_row($var_query_bank)) : ?>
                    <li>
                        <strong>Atas nama rekening:</strong> <?php echo $var_data_bank[0] ?>
                    </li>
                    <li>
                        <strong>Nomor Rekening code:</strong> <?php echo $var_data_bank[1] ?>
                    </li>
                    <li>
                        <strong>Nama Bank:</strong> <?php echo $var_data_bank[2] ?>
                    </li>
                <?php endwhile; ?>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> Armada </th>
                        <th> Total </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // $var_sql_item = "
                    // SELECT am.model_name, ot.od_selling, SUM(ot.od_selling) as total
                    // FROM sp_order_transaction ot
                    // JOIN sp_order_transaction_detail otd ON otd.od_id = ot.od_id
                    // JOIN sp_armada_model am ON am.model_id = otd.model_id
                    // WHERE ot.od_invoice = '{$var_data_inv['od_invoice']}'
                    // ";
                    $var_sql_item = "
                    SELECT am.model_name, at.type_price
                    FROM sp_order_transaction_detail otd
                    JOIN sp_order_transaction ot ON ot.od_id = otd.od_id
                    JOIN sp_armada_model am ON am.model_id = otd.model_id
                    JOIN sp_armada_type at ON at.type_id = am.type_id
                    WHERE ot.od_invoice = '{$var_data_inv['od_invoice']}'
                    ";
                    $var_query_item = mysqli_query($var_con, $var_sql_item);
                    $num = 1;
                    ?>
                    <?php while($var_data_item = mysqli_fetch_row($var_query_item)) : ?>
                        <?php $_SESSION['temp']['total'] = $var_data_inv['od_selling'] ?>
                        <tr>
                            <td> <?php echo $num++ ?> </td>
                            <td> <?php echo $var_data_item[0] ?> </td>
                            <td> Rp <?php echo number_format($var_data_item[1], 2, ',','.') ?> </td>
                        </tr>
                    <?php endwhile; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <div class="well">
                <address>
                    <strong>Travelindo.com.</strong>
                    <br/> Jalan Pringgodani 12 Mrican baru, Yogyakarta 55281.
                    <br/>
                    <abbr title="Phone">P1:</abbr> +62 274 511 100
                    <br/><abbr title="Phone">P2:</abbr> +62 813 251 221 90
                </address>
            </div>
        </div>
        <div class="col-xs-8 invoice-block">
            <ul class="list-unstyled amounts">
                <li>
                    <strong>Grand Total:</strong> <?php echo number_format($_SESSION['temp']['total'], 2, ',','.') ?>
                </li>
            </ul>
            <br/>
            <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();"> Print
                <i class="fa fa-print"></i>
            </a>
            <a class="btn btn-lg green hidden-print margin-bottom-5" href="index.php?p=booking-payment.php&amp;id=<?php echo base64_encode($var_data_inv['od_id'])?>&amp;invoice=<?php echo base64_encode($var_data_inv['od_invoice'])?>"> Konfirmasi Pembayaran
                <i class="fa fa-check"></i>
            </a>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<?php
$footer_component = [
    'datatableglobalscript' => '../assets/global/scripts/datatable.js',
    'datatables' => '../assets/global/plugins/datatables/datatables.all.min.js',
    'datableplugin' => '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
require_once 'inc-template/inc-template-footer.php';
?>

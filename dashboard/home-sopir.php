<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman untuk beranda sopir
*/

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';

$var_uid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "";
$var_action = isset($_GET['act']) ? mysqli_escape_string($var_con, filter_var($_GET['act'], FILTER_SANITIZE_STRING)) : "";

switch (strtolower($var_action)) {
    case 'complete':
    $var_groups = isset($_POST['group-'.$var_uid]) ? $_POST['group-'.$var_uid] : "";
    $var_error = [];

    if (isset($_POST['btn_complete'])) {
        for ($i=0; $i <= sizeof($var_groups); $i++) {
            $var_licensenumber[$i] = isset($var_groups[$i]['frm_licensenumber']) ? mysqli_escape_string($var_con, filter_var($var_groups[$i]['frm_licensenumber'], FILTER_SANITIZE_STRING)) : "";
            $var_licensetype[$i] = isset($var_groups[$i]['frm_licensetype']) ? mysqli_escape_string($var_con, filter_var($var_groups[$i]['frm_licensetype'], FILTER_SANITIZE_STRING)) : "";
            // $var_licenseimg = isset($_FILES['frm_licenseimg']['name']) ? addslashes($_FILES['frm_licenseimg']['name']) : "";

            if (empty(trim($var_licensenumber[$i]))) {
                $var_error['err']['licensenumber'] = 'Mohon masukkan nomor SIM.';
            }

            if (empty(trim($var_licensetype[$i]))) {
                $var_error['err']['licensetype'] = 'Mohon masukkan tipe SIM.';
            }

            // if (empty(trim($var_groups[$i]['frm_licenseimg']))) {
            //     $var_error['err']['licenseimg'] = 'Mohon masukkan gambar SIM.';
            // } else {
            //     $var_ava_tmp = $_FILES['frm_licenseimg']['tmp_name'][$i];
            //     $var_ava_dir = "../images/sim/";
            //     $var_ava_type = strtolower(pathinfo(isset($var_licenseimg[$i]) ? $var_licenseimg[$i] : "", PATHINFO_EXTENSION));
            //     $var_ava_ext = array("jpeg", "png", "jpg");
            //     $var_ava_newName = "img-sim-" . $_SESSION['username'].$var_groups[$i]['frm_licensenumber'] .".jpg";
            // }

            //jika lolos Validasi
            if (empty($var_error)) {
                foreach ($var_groups as $key => $var_group) {
                    $data[] = $var_group;
                }
                $var_sql_getid = "SELECT crew_armadaid FROM sp_crew_armada WHERE user_id = '{$var_uid}' LIMIT 1";
                $var_query_getid = mysqli_query($var_con, $var_sql_getid);
                $var_data_getid = mysqli_fetch_row($var_query_getid);

                $var_data_user[$i] = [
                    'detail_licensenumber' => $data[$i]['frm_licensenumber'],
                    'detail_licensetype' => $data[$i]['frm_licensetype'],
                    'crew_armadaid' => $var_data_getid[0]
                ];
                $var_update_user = insert($var_con, "sp_crew_armada_detail", $var_data_user[$i]);
                // if (move_uploaded_file($var_ava_tmp[$i], $var_ava_dir.$var_ava_newName[$i])) {
                if ($var_update_user) {
                    setNotif(['success' => 'Terima kasih telah menyelesaikan registrasi akun.']);
                    routeUrl('index.php?p=home-sopir');
                    // die();
                } else {
                    setNotif(['warning' => 'Mohon cek kembali data registrasi.']);
                    routeUrl('index.php?p=home-sopir');
                    // die();
                }
                // }
            }
        }
        die();
    }
    break;
}

$var_title = "Dashboard Sopir";
$head_component = [
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
    'morris' => '../assets/global/plugins/morris/morris.css',
];
require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>

<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<?php
$var_uid = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)): "";

$var_sql_user = "
SELECT d.detail_licensenumber, d.detail_licensetype
FROM sp_crew_armada ca
LEFT JOIN sp_crew_armada_detail d ON d.crew_armadaid = ca.crew_armadaid
WHERE ca.user_id = '{$var_uid}'
LIMIT 1
";

$var_query_user = mysqli_query($var_con, $var_sql_user);
$var_numrows_user = mysqli_num_rows($var_query_user);
$var_data_user = mysqli_fetch_array($var_query_user);
if ($var_numrows_user > 0) :
    if ($var_data_user[0] == null || $var_data_user[1] == null) :
        ?>
        <div class="profile-modal">
            <div id="ModalProfile" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Profil</h4>
                        </div>
                        <div class="modal-body">
                            <?php if (!empty($var_error['err'])) : ?>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="alert alert-danger">
                                            <ol>
                                                <?php foreach ($var_error['err'] as $key => $value) : ?>
                                                    <li><?php echo $value; ?></li>
                                                <?php endforeach; ?>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <form action="index.php?p=home-sopir.php&amp;act=complete" class="mt-repeater form-horizontal" method="post" enctype="multipart/form-data">
                                <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add"><i class="fa fa-plus"></i> Tambah</a>
                                <br><br>
                                <div data-repeater-list="group-<?= $var_uid ?>">
                                    <div data-repeater-item class="mt-repeater-item">
                                        <!-- jQuery Repeater Container -->
                                        <div class="mt-repeater-input">
                                            <label class="control-label">Nomor SIM</label>
                                            <br/>
                                            <input type="text" name="frm_licensenumber" class="form-control" autofocus maxlength="12" required min="12">
                                            <span class="help-block warning-text"><?php if (isset($var_error['err']['licensenumber'])) {echo $var_error['err']['licensenumber'];}?></span>
                                        </div>

                                        <div class="mt-repeater-input">
                                            <label class="control-label">Tipe SIM</label>
                                            <br/>
                                            <select name="frm_licensetype" class="form-control" required>
                                                <option value=""></option>
                                                <option value="A">SIM A</option>
                                                <option value="B">SIM B</option>
                                                <option value="BP">SIM B Polos</option>
                                            </select>
                                            <span class="help-block warning-text"><?php if (isset($var_error['err']['licensetype'])) {echo $var_error['err']['licensetype'];}?></span>
                                        </div>

                                        <!-- <div class="mt-repeater-input">
                                        <label class="control-label">Gambar SIM</label>
                                        <br/>
                                        <input type="file" name="frm_licenseimg[]" class="form-control" />
                                        <span class="help-block warning-text"><?php if (isset($var_error['err']['licenseimg'])) {echo $var_error['err']['licenseimg'];}?></span>
                                    </div> -->
                                    <div class="mt-repeater-input">
                                        <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete"><i class="fa fa-close"></i> Hapus</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pull-right">
                                        <button type="submit" class="btn green" name="btn_complete"><i class="fa fa-save"></i> Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
<?php endif; ?>
<?php endif; ?>

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<!-- BEGIN DASHBOARD STATS 1-->
<div class="row">
    <div class="col-lg-4 col-sm-4 col-xs-4 col-xs-12">
        <div class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-money"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $var_sql_countorder = "
                    SELECT count(*) as count
                    FROM sp_crew_detail cd
                    LEFT JOIN sp_crew_armada ca ON ca.crew_armadaid = cd.crew_armadaid
                    WHERE ca.user_id = '{$var_uid}'";
                    $var_query_countorder = mysqli_query($var_con, $var_sql_countorder);
                    $var_numrows_countorder = mysqli_num_rows($var_query_countorder);
                    $var_data_countorder = mysqli_fetch_row($var_query_countorder);
                    if ($var_numrows_countorder > 0) :
                        ?>
                        <span><?php echo $var_data_countorder[0] ?></span>
                    <?php else: ?>
                        <span>0</span>
                    <?php endif; ?>
                </div>
                <div class="desc"> Total Penugasan <a href="#" class="font-white"><span class="fa fa-external-link"> </span></a></div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-sm-4 col-xs-4 col-xs-12">
        <div class="dashboard-stat dashboard-stat-v2 purple" href="#">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $var_sql_countorder = "
                    SELECT count(*) as count
                    FROM sp_crew_detail cd
                    LEFT JOIN sp_crew_armada ca ON ca.crew_armadaid = cd.crew_armadaid
                    WHERE ca.user_id = '{$var_uid}' AND cd.crew_action = '1'";
                    $var_query_countorder = mysqli_query($var_con, $var_sql_countorder);
                    $var_numrows_countorder = mysqli_num_rows($var_query_countorder);
                    $var_data_countorder = mysqli_fetch_row($var_query_countorder);
                    if ($var_numrows_countorder > 0) :
                        ?>
                        <span><?php echo $var_data_countorder[0] ?></span>
                    <?php else: ?>
                        <span>0</span>
                    <?php endif; ?>
                </div>
                <div class="desc"> Total Penugasan Diterima <a href="index.php?p=crew-assignment.php" class="font-white"><span class="fa fa-external-link"> </span></a></div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-sm-4 col-xs-4 col-xs-12">
        <div class="dashboard-stat dashboard-stat-v2 red" href="#">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    $var_sql_countorder = "
                    SELECT count(*) as count
                    FROM sp_crew_detail cd
                    LEFT JOIN sp_crew_armada ca ON ca.crew_armadaid = cd.crew_armadaid
                    WHERE ca.user_id = '{$var_uid}' AND cd.crew_action = '2'";
                    $var_query_countorder = mysqli_query($var_con, $var_sql_countorder);
                    $var_numrows_countorder = mysqli_num_rows($var_query_countorder);
                    $var_data_countorder = mysqli_fetch_row($var_query_countorder);
                    if ($var_numrows_countorder > 0) :
                        ?>
                        <span><?php echo $var_data_countorder[0] ?></span>
                    <?php else: ?>
                        <span>0</span>
                    <?php endif; ?>
                </div>
                <div class="desc"> Total Penugasan Ditolak <a href="index.php?p=crew-assignment.php" class="font-white"><span class="fa fa-external-link"> </span></a></div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<!-- END DASHBOARD STATS 1-->

<div class="row">
    <div class="col-sm-12">
        <?php
        $var_sql_request = "
        SELECT d.crew_detailid, d.crew_armadaid, d.crew_datefrom, d.crew_dateto, d.crew_action, d.schedule_id
        FROM sp_crew_detail d
        JOIN sp_crew_armada ca ON ca.crew_armadaid = d.crew_armadaid
        WHERE crew_action = 0 AND ca.user_id = '{$var_uid}'
        ";
        $var_query_request = mysqli_query($var_con, $var_sql_request);
        $var_numrows_request = mysqli_num_rows($var_query_request);
        ?>
        <?php if ($var_numrows_request > 0) : ?>
            <?php while($var_data_request = mysqli_fetch_array($var_query_request)) : ?>

            <?php endwhile; ?>
        <?php else: ?>
            <div class="note note-info">
                <span>TIdak ada jadwal untuk anda.</span>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php
//FOOTER PAGE BASE LEVEL
$footer_component = [
    'momentdate' => '../assets/global/plugins/moment.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'uimodals' => '../assets/pages/scripts/ui-modals.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
    'jsfr' => '../assets/global/plugins/jquery-repeater/jquery.repeater.js',
    'fr' => '../assets/pages/scripts/form-repeater.min.js'
];
require_once 'inc-template/inc-template-footer.php';
?>
<script type="text/javascript">
$(document).ready(function () {
    $('#ModalProfile').modal('show', {
        backdrop: 'static'
    });
});
</script>

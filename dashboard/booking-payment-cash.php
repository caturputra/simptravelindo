<?php
/**
** dibuat oleh : Muhamad Catur Putra
** tanggal : 19/11/2017
** Halaman yang memuat daftar pembayaran yang telah dilakukan
*/

require_once '../core/init.php';
require_once '../inc/inc-session-user.php';

$var_confirmedby = isset($_SESSION['userid']) ? mysqli_escape_string($var_con, filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT)) : "";

$var_invoice = isset($_POST['frm_invoice']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_invoice'], FILTER_SANITIZE_STRING)) : "";
$var_amount = isset($_POST['frm_amount']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_amount'], FILTER_SANITIZE_NUMBER_INT)) : "";
$var_sender = isset($_POST['frm_sender']) ? mysqli_escape_string($var_con, filter_var($_POST['frm_sender'], FILTER_SANITIZE_STRING)) : "";
$var_error = [];

if (isset($_POST['btn_payment_cash'])) {
    if (empty(trim($var_invoice))) {
        $var_error['err']['invoice'] = 'Mohon pilih nomor invoice.';
    }

    if (empty(trim($var_amount))) {
        $var_error['err']['amount'] = 'Mohon masukan nominal bayar.';
    }

    if (empty(trim($var_sender))) {
        $var_error['err']['sender'] = 'Mohon masukan penyetor.';
    }

    if (!is_numeric($var_amount)) {
        $var_error['err']['amount'] = 'Nominal bayar harus angka.';
    }

    $var_sql_getid = "
    SELECT ot.od_id, ot.od_selling, p.payment_balance, p.payment_lackofpay, s.schedule_id
    FROM sp_payment p
    LEFT JOIN sp_schedule s ON p.schedule_id = s.schedule_id
    LEFT JOIN sp_order_transaction ot ON ot.od_id = s.od_id
    WHERE ot.od_invoice = '{$var_invoice}' AND p.payment_confirmdate =
    (
        SELECT MAX(payment_confirmdate)
        FROM sp_payment
        GROUP BY p.schedule_id
    )
    ";
    $var_query_getid = mysqli_query($var_con, $var_sql_getid);
    $var_data_getid = mysqli_fetch_row($var_query_getid);
    // debug($var_sql_getid);

    if (mysqli_num_rows($var_query_getid) == 0) {
        $var_sql_getid = "
        SELECT ot.od_id, ot.od_selling, s.schedule_id
        FROM sp_order_transaction ot
        LEFT JOIN sp_schedule s ON s.od_id = ot.od_id
        WHERE ot.od_invoice = '{$var_invoice}'
        ";
        $var_query_getid = mysqli_query($var_con, $var_sql_getid);
        $var_data_getid = mysqli_fetch_row($var_query_getid);

        //hitung minimal bayar sesuai total penjualan
        $var_minpayment = (float) $_ENV['PAYMENT_MIN'] * $var_data_getid[1];

        //validasi amount harus minimal 30% dari total tagihan
        if ($var_amount < $var_minpayment) {
            $var_error['err']['amount'] = 'Pembayaran minimal IDR '. number_format($var_minpayment, 2, ',','.') .'.';
        } else {
            $var_count_lackofpay = (int) $var_data_getid[1] - $var_amount;

            if ($var_amount > $var_data_getid[1]) {
                $var_error['err']['amount'] = 'Total tagihan anda IDR' . number_format($var_data_getid[1], 2 ,',','.') .'.';
            } else if ($var_count_lackofpay > 0 && $var_count_lackofpay <= $var_data_getid[1]) {
                $var_lackofpay = $var_count_lackofpay;
                $var_type = '0'; //uangmuka
            } else if ($var_count_lackofpay === 0) {
                $var_lackofpay .= 0;
                $var_type = '1'; //lunas
            }
        }

        if (empty($var_error)) {
            $var_data_payment = [
                'payment_accountname' => $var_sender,
                'payment_date' => date('Y-m-d H:i:s'),
                'payment_verdate' => date('Y-m-d H:i:s'),
                'payment_confirmdate' => date('Y-m-d H:i:s'),
                'payment_balance' => $var_amount,
                'payment_howto' => 1,
                'payment_status' => 1,
                'payment_lackofpay' => $var_lackofpay,
                'payment_type' => $var_type,
                'schedule_id' => $var_data_getid[2],
                'payment_confirmedby' => $var_confirmedby
            ];
            // debug($var_data_payment);
            $var_insert_payment = insert($var_con, "sp_payment", $var_data_payment);
            if ($var_insert_payment) {
                update($var_con, "sp_order_transaction", ['od_status' => 4], ['od_invoice' => $var_invoice]);
                setNotif(['success' => 'Pembayaran dengan nomor invoice '. $var_invoice .' berhasil diproses.']);
                routeUrl('index.php?p=booking-payment-cash');
                die();
            } else {
                setNotif(['danger' => 'Pembayaran dengan nomor invoice '. $var_invoice .' gagal diproses.']);
                routeUrl('index.php?p=booking-payment-cash');
                die();
            }
        }
    } else {
        if ($var_data_getid[3] == 0 ) {
            setNotif(['info' => 'Pembayaran untuk ' . $var_invoice . ' telah dilunasi']);
            routeUrl('index.php?p=booking-payment-cash');
            die();
        }

        $var_minpayment = $var_data_getid[3];

        //validasi amount harus minimal 30% dari total tagihan
        if ($var_amount < $var_minpayment) {
            $var_error['err']['amount'] = 'Pembayaran minimal IDR '. number_format($var_minpayment, 2, ',','.') .'.';
        } else {
            $var_count_lackofpay = (int) $var_data_getid[3] - $var_amount;

            if ($var_amount > $var_data_getid[3]) {
                $var_error['err']['amount'] = 'Total tagihan anda IDR' . number_format($var_data_getid[3], 2, ',','.') .'.';
            } else if ($var_count_lackofpay > 0 && $var_count_lackofpay <= $var_data_getid[3]) {
                $var_lackofpay = $var_count_lackofpay;
                $var_type = '0'; //uangmuka
            } else if ($var_count_lackofpay === 0) {
                $var_lackofpay .= 0;
                $var_type = '1'; //lunas
            }
        }

        if (empty($var_error)) {
            $var_data_payment = [
                'payment_date' => date('Y-m-d H:i:s'),
                'payment_verdate' => date('Y-m-d H:i:s'),
                'payment_confirmdate' => date('Y-m-d H:i:s'),
                'payment_balance' => $var_amount,
                'payment_howto' => 1,
                'payment_status' => 1,
                'payment_lackofpay' => $var_lackofpay,
                'payment_type' => $var_type,
                'schedule_id' => $var_data_getid[4],
                'payment_confirmedby' => $var_confirmedby
            ];
            // debug($var_data_payment);
            $var_insert_payment = insert($var_con, "sp_payment", $var_data_payment);
            if ($var_insert_payment) {
                update($var_con, "sp_order_transaction", ['od_status' => 4], ['od_invoice' => $var_invoice]);
                setNotif(['success' => 'Pembayaran dengan nomor invoice '. $var_invoice .' berhasil diproses.']);
                routeUrl('index.php?p=booking-payment-cash');
                die();
            } else {
                setNotif(['danger' => 'Pembayaran dengan nomor invoice '. $var_invoice .' gagal diproses.']);
                routeUrl('index.php?p=booking-payment-cash');
                die();
            }
        }
    }
}

$var_title = "Pembayaran tunai";

$head_component = [
    'select2' => '../assets/global/plugins/select2/css/select2.min.css',
    'select2boot' => '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
];

require_once 'inc-template/inc-template-header.php';
require_once 'inc-template/inc-template-main-header.php';
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="?p=home-<?php echo trim(strtolower($_SESSION['levelname'])) .'.php'?>"><i class="fa fa-dashboard"></i> Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active"><?php echo $var_title; ?></span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- MESSAGE -->
<div class="row">
    <div class="col-sm-12">
        <?php if (isset($_SESSION['notif']['success'])) :  ?>
            <div class="note note-success"><p><i class="fa fa-check"></i> <?php echo $_SESSION['notif']['success'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['info'])) : ?>
            <div class="note note-info"><p><i class="fa fa-info"></i> <?php echo $_SESSION['notif']['info'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['warning'])) : ?>
            <div class="note note-warning"><p><i class="fa fa-exclamation"></i> <?php echo $_SESSION['notif']['warning'] ?></p></div>
            <?php unsetNotif() ?>
        <?php elseif (isset($_SESSION['notif']['danger'])) : ?>
            <div class="note note-danger"><p><i class="fa fa-ban"></i> <?php echo $_SESSION['notif']['danger'] ?></p></div>
            <?php unsetNotif() ?>
        <?php endif; ?>
    </div>
</div>
<!-- END MESSAGE -->

<div class="row">
    <div class="col-sm-8">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-social-dribbble font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Pembayaran</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <form class="form-horizontal" action="booking-payment-cash.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="frm_invoice" class="control-label col-sm-2">Invoice <span class="required">*</span></label>
                        <div class="col-sm-10">
                            <?php
                            $var_check_lackofpay = '
                            SELECT ot.od_id, ot.od_selling, p.payment_balance, p.payment_lackofpay, s.schedule_id, ot.od_invoice
                            FROM sp_payment p
                            LEFT JOIN sp_schedule s ON p.schedule_id = s.schedule_id
                            LEFT JOIN sp_order_transaction ot ON ot.od_id = s.od_id
                            WHERE p.payment_lackofpay = 0 AND p.payment_confirmdate =
                            (
                                SELECT MAX(payment_confirmdate)
                                FROM sp_payment
                                GROUP BY p.schedule_id
                                )
                                LIMIT 1
                            ';
                            $var_query_check = mysqli_query($var_con, $var_check_lackofpay);
                            $var_data_check = mysqli_fetch_array($var_query_check);
                            ?>

                            <select class="form-control select2" name="frm_invoice" id="frm_invoice" required>
                                <option value="" selected>Pilih</option>
                                <option value="<?= $var_data_check['od_invoice'] ?>"><?= $var_data_check['od_invoice'] ?></option>
                            </select>
                            <p class="help-block warning-text"><?php echo isset($var_error['err']['invoice']) ? $var_error['err']['invoice'] : "" ; ?></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="frm_sender" class="control-label col-sm-2">Penyetor <span class="required">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="frm_sender" id="frm_sender" required>
                            <p class="help-block warning-text"><?php echo isset($var_error['err']['sender']) ? $var_error['err']['sender'] : "" ; ?></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="frm_amount" class="control-label col-sm-2">Nominal <span class="required">*</span></label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-btn"><button type="button" class="btn blue">IDR</button></span>
                                <input type="text" class="form-control mask_number" name="frm_amount" id="frm_amount" placeholder="Amount" required>
                            </div>
                            <p class="help-block warning-text"><?php echo isset($var_error['err']['amount']) ? $var_error['err']['amount'] : "" ; ?></p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pull-right">
                                <a href="index.php?p=home-cashier.php" class="btn btn-outline dark">Batal</a>
                                <button type="submit" class="btn green" name="btn_payment_cash"><i class="fa fa-save"></i> Proses</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.portlet-body -->
    </div>
    <!-- /.col-sm-8 -->

    <div class="col-sm-4">
        <div id="detail-card">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase">Detail</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="invoice-detail">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT -->

<?php
$footer_component = [
    'momentdate' => '../assets/global/plugins/moment.js',
    'jquery-ui' => '../assets/global/plugins/jquery-ui/jquery-ui.min.js',
    'select2' => '../assets/global/plugins/select2/js/select2.full.min.js',
    'select2 script' => '../assets/pages/scripts/components-select2.min.js',
    'jqueryvalidate' => '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
    'validatemethod' => '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
    'inputmask' => '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
    'inputmask script' => '../assets/pages/scripts/form-input-mask.js',
];
require_once 'inc-template/inc-template-footer.php';
?>
<script type="text/javascript">
$(document).ready(function () {
    $('#frm_invoice').change( function() {
        var seletedinv = $(this).find("option:selected").text();
        var selectedvalue = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'ajax-get-inv.php?for=inv',
            data: {id: selectedvalue},
            success: function(data) {
                $('#detail-card').show();
                $('#invoice-detail').html(data);
            }
        });
    });

    $('#detail-card').hide();
});
</script>

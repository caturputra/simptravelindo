</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> <?php echo date('Y'); ?> &copy; All Right Reserved
        <a target="_blank" href="http://www.travelindo.com">Travelindo</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->

<!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script>
<script src="../assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../assets/layouts/layout4/scripts/layout.js" type="text/javascript"></script>
<script src="../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="../assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<!-- BEGIN PLUGINS -->
<?php
if (isset($footer_component)) {
    foreach ($footer_component as $key => $component) {
        echo"<script src=\"". $component ."\" type=\"text/javascript\"></script>";
    }
}
?>
<script type="text/javascript">
    $(document).ready(function(){
        function load_unseen_notification(view = '') {
            $.ajax({
                url:"ajax-notification.php",
                cache:false,
                method:"POST",
                data:{view:view},
                dataType:"json",
                success:function(data) {
                    $('.dropdown-menu-list').html(data.notification);
                    // if (data.unseen_notification > 0) {
                        $('.count').html(data.unseen_notification);
                    // }
                }
            })
        }

        load_unseen_notification();

        $(document).on('click', '.dropdown-notification', function() {
            load_unseen_notification('yes');
        })

        setInterval(function() {
            load_unseen_notification();
        }, 5000);
        
    })
</script>
<!-- END PLUGINS -->

<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="lse" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="lse" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item">
                <div class="container">
                    <!-- BEGIN GOTO SITE -->
                    <?php if ($_SESSION['levelid'] == '4' || $_SESSION['levelid'] == '5') : ?>
                        <a href="../home" class="btn blue-madison col-sm-12 col-md-12 col-xs-12 col-lg-2">Ke Situs Web</a>
                    <?php endif; ?>
                    <!-- END GOTO SITE -->
                </div>
            </li>
            <?php
            $var_level_id = $_SESSION['levelid'];
            $var_sql_menu = "
            SELECT m.menu_id, m.menu_name, m.menu_url, m.menu_parent, m.menu_icon
            FROM sp_previllages p
            JOIN sp_menu m ON m.menu_id = p.menu_id
            JOIN sp_level l ON l.level_id = p.level_id
            WHERE p.level_id = '$var_level_id' AND m.menu_parent = 0 AND m.menu_visibility = 1
            GROUP BY m.menu_name
            ORDER BY m.menu_order";
            $var_query_menu = mysqli_query($var_con, $var_sql_menu);

            foreach ($var_query_menu as $var_data_menu) :
                $var_submenu = $var_data_menu['menu_id'];
                $var_sql_menu2 = "
                SELECT m.menu_id, m.menu_name, m.menu_url, m.menu_parent, m.menu_icon FROM sp_previllages p
                JOIN sp_menu m ON m.menu_id = p.menu_id
                JOIN sp_level l ON l.level_id = p.level_id
                WHERE m.menu_parent = '{$var_submenu}' AND l.level_id = '{$var_level_id}' AND m.menu_visibility = 1
                GROUP BY m.menu_name
                ORDER BY m.menu_order";
                ;
                $var_menu2 = mysqli_query($var_con, $var_sql_menu2);
                $var_query_menu2 = mysqli_fetch_array($var_menu2);
                if (mysqli_num_rows($var_menu2) > 0) :
                    ?>
                    <!-- <li class="heading">
                        <h3 class="uppercase"><?php echo $var_data_menu['menu_name']; ?></h3>
                    </li> -->
                    <li class="nav-item">
                        <a href="#" class="nav-link nav-toggle">
                            <i class="<?php echo $var_data_menu['menu_icon']; ?>"></i><?php echo $var_data_menu['menu_name']; ?>
                            <span class="arrow"></span>
                        </a>
                        <?php
                        echo "<ul class='sub-menu'>";
                        foreach ($var_menu2 as $var_query_sub) :
                            ?>
                            <li class="nav-item"><a href="index.php?p=<?php echo $var_query_sub['menu_url']; ?>" class="nav-link"><i class="<?php echo $var_query_sub['menu_icon']; ?>"></i> <span class="title"><?php echo $var_query_sub['menu_name']; ?></span></a></li>
                            <?php
                        endforeach;
                        echo"</ul></li>";
                else :
                            ?>
                            <li class="nav-item">
                                <a href="index.php?p=<?php echo $var_data_menu['menu_url']; ?>" class="nav-link">
                                    <i class=" <?php echo $var_data_menu['menu_icon']; ?>"></i> <span class="title"><?php echo $var_data_menu['menu_name']; ?></span>
                                </a>
                            </li>
                            <?php
                endif;
            endforeach;
                    ?>
                </ul>
                <!-- END SIDEBAR MENU -->
            </div>
            <!-- END SIDEBAR -->
        </div>
        <!-- END SIDEBAR -->

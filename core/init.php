<?php
session_start();

ini_set('display_errors', 1);
error_reporting(E_ALL);

date_default_timezone_set('Asia/Jakarta');

//memanggil file-file yang selalu ada
require_once '../vendor/autoload.php';

//membuat objek dotenv dan menggunakannya
$dotenv = new Dotenv\Dotenv(__DIR__ . '/../');
$dotenv->load();

require_once '../inc/db/config.php';
require_once '../inc/function/inc-func-db.php';
require_once '../inc/helper/smsGateway.php';
require_once '../inc/function/inc-func-functions.php';

?>

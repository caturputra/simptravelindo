<?php

//fungsi untuk menambah data user
function userAdd($connect, $username, $password, $email, $role, $status)
{

    //data dalam tabel
    $dataUserAdd = array(
        'user_username' => $username,
        'user_password' => $password,
        'user_email' => $email,
        'user_level' => $role,
        'user_status' => $status,
        'user_createat' => date('Y-m-d H:i:s'),
        'user_modifyat' => date('Y-m-d H:i:s')
    );
    //insert ke tabel sp_user
    if (insert($connect, "sp_user", $dataUserAdd)) {
        return true;
    } else {
        return false;
    }
}

//fungsi untuk menambah data user detail
function userAddDetail($connect, $firstname, $lastname, $gender, $phone, $address, $userid, $ava, $birthdate)
{

    //data dalam tabel
    $dataUserAdd = array(
        'user_detail_firstname' => $firstname,
        'user_detail_lastname' => $lastname,
        'user_detail_gender' => $gender,
        'user_detail_phone' => '+62'.trim($phone),
        'user_detail_address' => $address,
        'user_id' => $userid,
        'user_detail_avatar' => $ava,
        'user_detail_birthdate' => $birthdate,
    );
    //insert ke tabel sp_user
    if (insert($connect, "sp_user_detail", $dataUserAdd)) {
        return true;
    } else {
        return false;
    }
}

// fungsi untuk cek user
function userAuth($connect, $username, $passwd, $role)
{
    $validate = false;

    $queryUserAuth = "SELECT user_username, user_password, user_role FROM sp_user WHERE user_username = '{$username}' and user_password = '{$passwd}' and user_role = '{$role}'";

    $varUserAuth = mysqli_query($connect, $queryUserAuth);
    $dataUserAuth = mysqli_fetch_array($varUserAuth);

    if ($dataUserAuth['user_username'] != $username) {
        $validate = false;
    } else {
        $validate = true;
    }
}

//fungsi update user
function userUpdate($connect, $userid, $role, $status)
{
    $varDataUpate = array(
        'user_level' => $role,
        'user_status' => $status,
        'user_modifyat' => date('Y-m-d H:i:s')
    );

    $varDataUpdateID = array('user_id' => $userid);
    if (update($connect, "sp_user", $varDataUpate, $varDataUpdateID)) {
        return true;
    } else {
        return false;
    }
}

//fungsi hapus user
function userDelete($connect, $userid)
{
    $varDataDeleteID = array('user_id' => $userid);

    if (delete($connect, "sp_user", $varDataDeleteID)) {
        //if (delete($connect, "sp_user_detail", $varDataDeleteID)) {
            return true;
        } else {
            return false;
        // }
    }
}

//fungsi change password
function userChangePasswd($connect, $password, $username)
{
    $varDataUpate = array(
        'user_password' => $password,
        'user_modifyat' => date('Y-m-d H:i:s')
    );

    $varDataUpdateID = array('user_id' => $username);

    if (update($connect, "sp_user", $varDataUpate, $varDataUpdateID)) {
        return true;
    } else {
        return false;
    }
}

//fungsi change password
function userChangeUsername($connect, $new_user, $userid)
{
    $varDataUpate = array(
        'user_username' => $new_user,
        'user_modifyat' => date('Y-m-d H:i:s')
    );

    $varDataUpdateID = array('user_id' => $userid);

    if (update($connect, "sp_user", $varDataUpate, $varDataUpdateID)) {
        return true;
    } else {
        return false;
    }
}

//funsgi untuk update avatar
function userChangeAvatar($connect, $ava, $userid)
{
    $varDataUpate = array(
        'user_detail_avatar' => $ava,
    );

    $varDataUpdateID = array('user_id' => $userid);

    if (update($connect, "sp_user_detail", $varDataUpate, $varDataUpdateID)) {
        return true;
    } else {
        return false;
    }
}

//proses update profile
function userUpdateProfile($connect, $firstname, $lastname, $gender, $phone, $address, $birthdate, $userid)
{
    $varDataUpate = array(
        'user_detail_firstname' => $firstname,
        'user_detail_lastname' => $lastname,
        'user_detail_gender' => $gender,
        'user_detail_phone' => $phone,
        'user_detail_address' => $address,
        'user_detail_birthdate' => $birthdate,
    );

    $varDataUpdateID = array('user_id' => $userid);

    if (update($connect, "sp_user_detail", $varDataUpate, $varDataUpdateID)) {
        return true;
    } else {
        return false;
    }
}

<?php
/*
** Fungsi untuk input data
*/
function insert($connection, $tblname, array $val_cols)
{

    $keysString = implode(", ", array_keys($val_cols));

    // print key and value for the array
    $i=0;
    foreach($val_cols as $key=>$value) {
        $StValue[$i] = "'".$value."'";
        $i++;
    }

    $StValues = implode(", ", $StValue);

    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    if(mysqli_query($connection, "INSERT INTO $tblname ($keysString) VALUES ($StValues)")) {
        return true;
    }
    else{
        echo "Error inserting record: " . mysqli_error($connection);
        die();
        return false;
    }
}

/*
** Fungsi untuk input data
*/
function insertReplace($connection, $tblname, array $val_cols, $cod_val_cols)
{

    $keysString = implode(", ", array_keys($val_cols));

    // print key and value for the array
    $i=0;
    foreach($val_cols as $key=>$value) {
        $StValue[$i] = "'".$value."'";
        $i++;
    }

    $StValues = implode(", ", $StValue);

    $i=0;
    foreach($cod_val_cols as $key=>$value) {
        $cod[$i] = $key." = '".$value."'";
        $i++;
    }

    $Stcod = implode(" AND ", $cod);

    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    if(mysqli_query($connection, "INSERT INTO $tblname ($keysString) VALUES ($StValues) ON DUPLICATE KEY UPDATE WHERE $Stcod")) {
        return true;
    }
    else{
        echo "Error inserting record: " . mysqli_error($connection);
        die();
        return false;
    }
}

/*
** Fungsi untuk input data mutli query
*/
function insertMulti($connection, $tblname, array $val_cols)
{

    $keysString = implode(", ", array_keys($val_cols));

    // print key and value for the array
    $i=0;
    foreach($val_cols as $key=>$value) {
        $StValue[$i] = "'".$value."'";
        $i++;
    }

    $StValues = implode(", ", $StValue);

    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    if($var_ = mysqli_multi_query($connection, "SET FOREIGN_KEY_CHECKS = 0; INSERT INTO $tblname ($keysString) VALUES ($StValues); SET FOREIGN_KEY_CHECKS = 1;")) {
        return true;
    }
    else
    {
        return false;
        echo "Error inserting record: " . mysqli_error($connection);
        die();
    }
}

/*
** Fungsi untuk update data
*/
function update($connection, $tblname, array $set_val_cols, array $cod_val_cols)
{

    $i=0;
    foreach($set_val_cols as $key=>$value) {
        $set[$i] = $key." = '".$value."'";
        $i++;
    }

    $Stset = implode(", ", $set);

    $i=0;
    foreach($cod_val_cols as $key=>$value) {
        $cod[$i] = $key." = '".$value."'";
        $i++;
    }

    $Stcod = implode(" AND ", $cod);

    if(mysqli_query($connection, "UPDATE $tblname SET $Stset WHERE $Stcod")) {
        if(mysqli_affected_rows($connection)) {
            return true;
        }
        else{
            return false;
        }
    }
    else{
        echo "Error updating record: " . mysqli_error($connection);
        die();
    }
}

/*
** Fungsi untuk update data multiquery
*/
function updateMulti($connection, $tblname, array $set_val_cols, array $cod_val_cols)
{

    $i=0;
    foreach($set_val_cols as $key=>$value) {
        $set[$i] = $key." = '".$value."'";
        $i++;
    }

    $Stset = implode(", ", $set);

    $i=0;
    foreach($cod_val_cols as $key=>$value) {
        $cod[$i] = $key." = '".$value."'";
        $i++;
    }

    $Stcod = implode(" AND ", $cod);

    if($var_ = mysqli_multi_query($connection, "SET FOREIGN_KEY_CHECKS = 0; UPDATE $tblname SET $Stset WHERE $Stcod; SET FOREIGN_KEY_CHECKS = 1;")) {
        if($var_) {
            return true;
        }
        else{
            return false;
        }
    }
    else{
        echo "Error updating record: " . mysqli_error($connection);
        die();
    }
}

/*
** Fungsi untuk hapus data
*/
function delete($connection, $tblname, array $val_cols)
{

    $i=0;
    foreach($val_cols as $key=>$value) {
        $exp[$i] = $key." = '".$value."'";
        $i++;
    }

    $Stexp = implode(" AND ", $exp);

    if(mysqli_query($connection, "DELETE FROM $tblname WHERE $Stexp")) {
        if(mysqli_affected_rows($connection)) {
            return true;
        }
        else{
            return false;
        }
    }
    else{
        echo "Error deleting record: " . mysqli_error($connection);
        die();
    }
}

/*
** Fungsi untuk hapus data multiquery
*/
function deleteMulti($connection, $tblname, array $val_cols)
{

    $i=0;
    foreach($val_cols as $key=>$value) {
        $exp[$i] = $key." = '".$value."'";
        $i++;
    }

    $Stexp = implode(" AND ", $exp);

    if($var_ = mysqli_multi_query($connection, "SET FOREIGN_KEY_CHECKS = 0; DELETE FROM $tblname WHERE $Stexp; SET FOREIGN_KEY_CHECKS = 1;")) {
        if($var_) {
            return true;
        }
        else{
            return false;
        }
    }
    else{
        echo "Error deleting record: " . mysqli_error($connection);
        die();
    }
}

/*
** Fungsi untuk ambil data
*/
function fetch($connection, $table, array $columns)
{
    $columns = implode(",", $columns);
    $result = mysqli_query($connection, "SELECT $columns FROM $table");

    if(mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        die();
    }

    //return tow dimentional array as required columns result
    return mysqli_fetch_all($result, MYSQLI_ASSOC);
}

<?php


/**
 * notification mengirim notifikasi ke sistem yang login
 * @param  string $connect variabel dari koneksi database
 * @param  array  $params  ['from', 'to', 'description', 'type']
 *                         from => pengirim
 *                         to => penerima
 *                         description => pesan yang dikirim
 *                         type => header notifikasi
 * @return boolean          true or false
 */
function notification($connect, array $params, $type = 'add', $id = null)
{
    if ($type === 'add') {
        $data = [
            'notif_fromid' => $params['from'],
            'notif_to' => $params['to'],
            'notif_desc' => ucwords($params['description']),
            'notif_type' => ucwords($params['type']),
        ];

        if (insert($connect, "sp_notification",$data)) {
            return true;
        }
    }

    if ($type === 'read') {
        if ($id === null) {
            return false;
        }

        $update = ['notif_isread' => 1];
        $condition = ['notif_id' => $id];

        if (update($connect, "sp_notification", $update, $condition)) {
            return true;
        }
    }

    return false;
}

/**
 * debug funsi untuk debugging script
 * @param  string $string menampung variabel yang akan debug
 * @return mixed         hasil dari fungsi var_dump
 */
function debug($string)
{
    echo "<pre>";
    die(var_dump($string));
}

/**
 * getBaseUrl untuk mendapatkan project root directory
 * @return string url
 */
function getBaseUrl()
{
    $currentPath = $_SERVER['PHP_SELF'];
    $pathInfo = pathinfo($currentPath);
    $hostName = $_SERVER['HTTP_HOST'];
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';

    return $protocol.$hostName.$pathInfo['dirname']."/";
}

/**
 * sendPhone untuk mengirim sms
 * @param  string $connect   variabel koneksi dari fungsi mysqli_connect
 * @param  array  $condition empat inputan yang harus ada
 * @return boolean
 */
function sendPhone($connect, array $condition) {
    $smsGateway = new SmsGateway($_ENV['SMSG_USER'], $_ENV['SMSG_PASS']);

    $deviceID = $_ENV['SMSG_DEVICE'];
    $number = $condition['phone'];
    $message = $condition['message'];

    $options = [
        'send_at' => strtotime('+2 minutes'),
        'expires_at' => strtotime('+1 hour')
    ];

    //Please note options is no required and can be left out
    $result = $smsGateway->sendMessageToNumber($number, $message, $deviceID);

    if (!$result) {
        return false;
    }

    return true;
}

/**
 * sendPhone untuk mengirim sms
 * @param  string $connect   variabel koneksi dari fungsi mysqli_connect
 * @param  array  $condition empat inputan yang harus ada
 * @return boolean
 */
// function sendPhone($connect, array $condition) {
//     $now = date('Y-m-d H:i:s');
//
//     $data = [
//         'UpdatedInDB' => $now,
//         'DestinationNumber' => $condition['phone'],
//         'TextDecoded' => $condition['message'],
//         'CreatorID' => 'travelindo'
//     ];
//
//     $insert = insert($connect, "gammu.outbox", $data);
//
//     //debug($data);
//     if (!$insert) {
//         return false;
//     }
//
//     return true;
// }

/**
 * writeLogArmada
 * @param  string  $connect
 * @param  array   $message
 * @param  boolean $status
 * @return string           akan ditulisakan kedalam file .txt
 */
function writeLogArmada($connect, array $message, $status = true) {
    $i=0;
    foreach ($message as $key => $value) {
        $exp[$i] = "".$value."";
        $i++;

        switch ($key) {
            case strtolower($key):
            $ip = $_SERVER['REMOTE_ADDR'];
            $url = basename(dirname(__DIR__, 2));
            $sname = $_SERVER['SERVER_NAME'];
            $jam = date('H:i');
            $isi = "[ ".$ip." ] "."##". date('Y-m-d') . " ##" . $jam . " ##" . $key . " ##" . $value . "\n";
            $logfilename = "../log/monitor/log-".date("Ymd").".txt";

            if ($status == true) {
                $sql = "SELECT user_username FROM sp_user WHERE user_id = '". filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT) ."'";
                $query = mysqli_query($connect, $sql);
                $result = mysqli_fetch_row($query);
                $isi = "[ ".$ip." ] "."##". date('Y-m-d') . " ##" . $jam . " ##" . $key ." ##". $result[0] . " ##" . $value . "\n";
            }

            if (!file_exists($logfilename)) {
                $_logfilehandler = fopen($logfilename,'a+');
                fopen($logfilename,'a+');
                $_logfilehandler;
                fwrite($_logfilehandler,$isi);
                fclose($_logfilehandler);
            } else {
                $_logfilehandler = fopen($logfilename,'a+');
                fopen($logfilename,'a+');
                $_logfilehandler;
                fwrite($_logfilehandler,$isi);
                fclose($_logfilehandler);
            }
            break;
        }
        return true;
    }

    return false;
}

/**
 * writeLog
 * @param  string  $connect variabel untuk koneksi dengan db
 * @param  array   $message value dari pesan log
 * @param  boolean $status
 * @return string           ditulis kedalam file log
 */
function writeLog($connect, array $message, $status = true) {
    $i=0;
    foreach ($message as $key => $value) {
        $exp[$i] = "".$value."";
        $i++;

        switch ($key) {
            case strtolower($key):
            $ip = $_SERVER['REMOTE_ADDR'];
            $url = basename(dirname(__DIR__, 2));
            $sname = $_SERVER['SERVER_NAME'];
            $jam = date('H:i');
            $isi = "[ ".$ip." ] "."##". date('Y-m-d') . " ##" . $jam . " ##" . $key . " ##" . $value ."\n";
            $logfilename = "../log/log-".date("Ym").".txt";

            if ($status == true) {
                $sql = "SELECT user_username FROM sp_user WHERE user_id = '". filter_var($_SESSION['userid'], FILTER_SANITIZE_NUMBER_INT) ."'";
                $query = mysqli_query($connect, $sql);
                $result = mysqli_fetch_array($query);
                $isi = "[ ".$ip." ] "."##". date('Y-m-d') . " ##" . $jam . " ##" . $key ." ##". $result[0] . " ##" . $value ."\n";
            }

            if (!file_exists($logfilename)) {
                $_logfilehandler = fopen($logfilename,'a+');
                // fopen($logfilename,'a+');
                $_logfilehandler;
                fwrite($_logfilehandler,$isi);
                fclose($_logfilehandler);
            } else {
                $_logfilehandler = fopen($logfilename,'a+', $sname."/log/");
                // fopen($logfilename,'a+');
                $_logfilehandler;
                fwrite($_logfilehandler,$isi);
                fclose($_logfilehandler);
            }
            break;
        }
        return true;
    }

    return false;
}

/**
 * checkSid cek session untuk akses suatu halaman
 * @return boolean
 */
function checkSid() {
    if (!isset($_SESSION['userid']) && !isset($_SESSION['levelid'])) {
        return false;
    } else {
        return true;
    }

    return false;
}

/**
 * dateFormat format tanggal sesuai dengan format Indonesia
 * @param  string $date format tanggal format umum
 * @return string       tanggal dengan format indonesia
 */
function dateFormat($date)
{
    $BulanIndo = array("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Des");

    $tahun = substr($date, 0, 4);
    $bulan = substr($date, 5, 2);
    $tgl   = substr($date, 8, 2);

    $jam = substr($date, 11, 5);

    $result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun . " " . $jam;
    return $result;
}

/**
 * dateFormatHours format tanggal dalam format Indonesia
 * @param  string $date format umum tanggal
 * @return array        untuk menampilkan tanggal dan jam atau salahsatunya
 */
function dateFormatHours($date)
{
    $BulanIndo = array("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Des");

    $tahun = substr($date, 0, 4);
    $bulan = substr($date, 5, 2);
    $tgl   = substr($date, 8, 2);

    $jam = substr($date, 11, 5);

    $result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;
    return [
        'date' => $result,
        'hours' => $jam
    ];
}

/**
 * dateDb merubah format untuk dimasukkan ke database
 * @param  string $date format dd/mm/yyyy
 * @return string       masuk ke database dengan format yyyy-mm-dd
 */
function dateDb($date)
{
    $result = explode("/", trim($date));
    $resulttrue = $result[2].'-'.$result[1].'-'.$result[0];
    return $resulttrue;
}

/**
 * dateDb merubah format untuk ambil data dari database
 * @param  string $date format yyyy-mm-dd
 * @return string       masuk ke database dengan format dd/mm/yyyy
 */
function dateDbRev($date)
{
    $result = explode("-", trim($date));
    $resulttrue = $result[2].'/'.$result[1].'/'.$result[0];
    return $resulttrue;
}

/**
 * [autonumber description]
 * @param  [type]  $connect [description]
 * @param  [type]  $tblname [description]
 * @param  [type]  $colname [description]
 * @param  integer $width   [description]
 * @param  string  $start   [description]
 * @return [type]           [description]
 */
function autonumber($connect, $tblname, $colname, $width = 0, $start = '')
{
    $sql = "
    SELECT
    {$colname}
    FROM
    {$tblname}
    WHERE
    {$colname}
    LIKE
    '{$start}%'
    ORDER BY
    {$colname} DESC
    LIMIT 1
    ";
    $auto = mysqli_query($connect, $sql);
    $numrows = mysqli_num_rows($auto);

    if ($numrows == 0) {
        $num = 1;
    } else {
        $data = mysqli_fetch_row($auto);
        $num = intval(substr($data[0], strlen($start)))+1;
    }

    if ($width > 0) {
        $number = $start.str_pad($num, $width, '0', STR_PAD_LEFT);
    } else {
        $number = $start.$num;
    }

    return $number;
}

/**
 * [setNotif description]
 * @param array $notif [description]
 */
function setNotif(array $notif)
{
    $i=0;
    foreach ($notif as $key => $value) {
        $exp[$i] = "".$value."";
        $i++;
        //debug($value);

        switch ($key) {
            case strtolower('success'):
            $notifcation = $_SESSION['notif'][$key] = ucwords($value);
            break;
            case strtolower('info'):
            $notifcation = $_SESSION['notif'][$key] = ucwords($value);
            break;
            case strtolower('warning'):
            $notifcation = $_SESSION['notif'][$key] = ucwords($value);
            break;
            case strtolower('danger'):
            $notifcation = $_SESSION['notif'][$key] = ucwords($value);
            break;
        }
    }
    return $notifcation;
}

/**
 * [unsetNotif description]
 */
function unsetNotif()
{
    if (!empty($_SESSION['notif']['success']) ) {
        unset($_SESSION['notif']['success']);
    } else

    if (!empty($_SESSION['notif']['info']) ) {
        unset($_SESSION['notif']['info']);
    } else

    if (!empty($_SESSION['notif']['warning']) ) {
        unset($_SESSION['notif']['warning']);
    } else

    if (!empty($_SESSION['notif']['danger']) ) {
        unset($_SESSION['notif']['danger']);
    }
}

/**
 * [routeSelf description]
 * @return [type] [description]
 */
function routeSelf()
{
    return htmlentities($_SERVER['PHP_SELF']);
}

/**
 * [setTitle description]
 * @param string $title [description]
 */
function setTitle($title="Travelindo")
{
    return $title;
}

/**
 * [validateSecurity description]
 * @param  [type] $var_data [description]
 * @return [type]           [description]
 */
function validateSecurity($var_data)
{
    $data = addslashes($var_data);
    $data = htmlentities($var_data);
    $data = strip_tags($var_data);
    $data = trim($var_data);
    return $data;
}

/**
 * [checkEmail description]
 * @param  [type] $email [description]
 * @return [type]        [description]
 */
function checkEmail($email)
{
    $emailPattern = "/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix";
    return preg_match($emailPattern, $email);
}

/**
 * [checkLength description]
 * @param  [type]  $input [description]
 * @param  integer $min   [description]
 * @param  integer $max   [description]
 * @param  boolean $trim  [description]
 * @return [type]         [description]
 */
function checkLength($input, $min=1, $max=10000, $trim=true)
{
    if ($trim) {
        $input = trim(empty($input));

        if (strlen($input) < $min || strlen($input) > $max) {
            return false;
        }
    }

    return true;
}

/**
 * [checkPassword description]
 * @param  [type] $pw1 [description]
 * @param  [type] $pw2 [description]
 * @return [type]      [description]
 */
function checkPassword($pw1, $pw2)
{
    $pw1 = trim($pw1);
    $pw2 = trim($pw2);
    return checkLength($pw1, 6, 32) && strcmp($pw1, $pw2) == 0;
}

/**
 * [recaptcha description]
 * @param  [type] $captcha [description]
 * @return [type]          [description]
 */
function recaptcha($captcha)
{
    $var_url = $_ENV['CAP_URL'];
    $var_secret = $_ENV['CAP_SECRET'];
    $var_response = file_get_contents($var_url . '?secret=' . urlencode($var_secret) . '&response=' . $captcha . '&remoteip=' . $_SERVER['REMOTE_ADDR']);
    return json_decode($var_response);
}

/**
 * [routeUrl description]
 * @param  [type] $url [description]
 * @return [type]      [description]
 */
function routeUrl($url)
{
    header('location: ' . $url . '.php');
}

/**
 * [encUrl description]
 * @param  [type] $url [description]
 * @return [type]      [description]
 */
function encUrl($url)
{
    return urlencode($url);
}

/**
 * [decUrl description]
 * @param  [type] $url [description]
 * @return [type]      [description]
 */
function decUrl($url)
{
    return urldecode($url);
}

/**
 * [randpass description]
 * @param  [type] $length [description]
 * @return [type]         [description]
 */
function randpass($length)
{
    //    karakter yang bisa dipakai sebagai password
    $pass = "";
    $string = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    $len = strlen($string);

    //    mengenerate password
    for ($i=1;$i<=$length; $i++) {
        $start = rand(0, $len);
        $pass .= substr($string, $start, 1);
    }

    return $pass;
}

/**
 * [isLoginSessionExpired description]
 * @return boolean [description]
 */
function isLoginSessionExpired()
{
    $login_session_duration = 3600;
    $current_time = time();
    if (isset($_SESSION['userLoggedin_time']) and isset($_SESSION['userid'])) {
        if (((time() - $_SESSION['userloggedin_time']) > $login_session_duration)) {
            return true;
        }
    }
    return false;
}

/**
 * [pagination description]
 * @param  [type] $baseUrl          [description]
 * @param  [type] $totalResults     [description]
 * @param  [type] $resultsPerPage   [description]
 * @param  [type] $currentPage      [description]
 * @param  array  $queryStringArray [description]
 * @return [type]                   [description]
 */
function pagination($baseUrl, $totalResults=null, $resultsPerPage, $currentPage, $queryStringArray=[])
{
    //total pages to show
    $totalPages = ceil($totalResults/$resultsPerPage);

    //if only one page then no point in showing a single paginated link
    if ($totalPages <=1) {
        return '';
    }

    //build the query string if provided
    $queryString = '';
    if ($queryStringArray) {
        $queryString = '&'.http_build_query($queryStringArray);
    }

    //show not more than 3 paginated links on right and left side
    $rightLinks = $currentPage+3;
    $previousLinks = $currentPage-3;
    ob_start(); ?>
    <nav aria-label="Page navigation" class="">
        <ul class="pagination">
            <?php
            //if page number 1 is not shown then show the "First page" link
            if ($previousLinks>1) {
                ?>
                <li>
                    <a href="<?php echo $baseUrl.'?page=1'.$queryString; ?>" aria-label="First">
                        <span aria-hidden="true">&laquo;&laquo;</span>
                    </a>
                </li>
                <?php

            }

            //disable previous button when first page
            if ($currentPage == 1) {
                ?>
                <li class="disabled">
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <?php

            }

            //if current page > 1 only then show previous page
            if ($currentPage > 1) {
                ?>
                <li>
                    <a href="<?php echo $baseUrl.'?page='.($currentPage-1).$queryString; ?>" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <?php

            }

            //Create left-hand side links
            for ($i = $previousLinks; $i <= $currentPage; $i++) {
                if ($i>0) {
                    if ($i==$currentPage) {
                        ?>
                        <li class="active"><a href="#"><?php echo $i; ?></a></li>
                        <?php

                    } else {
                        ?>
                        <li>
                            <a href="<?php echo $baseUrl.'?page='.$i.$queryString; ?>"><?php echo $i; ?></a>
                        </li>
                        <?php

                    }
                }
            }

            //middle pages
            if (false) {
                for ($i=1; $i<=$totalPages; $i++) {
                    if ($i==$currentPage) {
                        ?>
                        <li class="active"><a href="#"><?php echo $i; ?></a></li>
                        <?php

                    } else {
                        ?>
                        <li>
                            <a href="<?php echo $baseUrl.'?page='.$i.$queryString; ?>"><?php echo $i; ?></a>
                        </li>
                        <?php

                    }
                }
            }

            //right side links
            for ($i = $currentPage+1; $i < $rightLinks ; $i++) {
                if ($i<=$totalPages) {
                    if ($i==$currentPage) {
                        ?>
                        <li class="active"><a href="#"><?php echo $i; ?></a></li>
                        <?php

                    } else {
                        ?>
                        <li>
                            <a href="<?php echo $baseUrl.'?page='.$i.$queryString; ?>"><?php echo $i; ?></a>
                        </li>
                        <?php

                    }
                }
            }

            //if current page is not last page then only show next page link
            if ($currentPage != $totalPages) {
                ?>
                <li>
                    <a href="<?php echo $baseUrl.'?page='.($currentPage+1).$queryString; ?>" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
                <?php

            }

            //if current page is last page then show next page link disabled
            if ($currentPage == $totalPages) {
                ?>
                <li class="disabled">
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
                <?php

            }

            if ($rightLinks<$totalPages) {
                ?>
                <li>
                    <a href="<?php echo $baseUrl.'?page='.$totalPages.$queryString; ?>" aria-label="First">
                        <span aria-hidden="true">&raquo;&raquo;</span>
                    </a>
                </li>
                <?php

            } ?>
        </ul>
    </nav>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

/**
 * @function    backupDatabaseTables
 * @author      CodexWorld
 * @link        http://www.codexworld.com
 * @usage       Backup database tables and save in SQL file
 */
function backupDatabaseTables($dbHost,$dbUsername,$dbPassword,$dbName,$tables = '*'){
    //connect & select the database
    $db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);

    //get all of the tables
    if($tables == '*'){
        $tables = array();
        $result = $db->query("SHOW TABLES");
        while($row = $result->fetch_row()){
            $tables[] = $row[0];
        }
    }else{
        $tables = is_array($tables)?$tables:explode(',',$tables);
    }

    //loop through the tables
    foreach($tables as $table){
        $result = $db->query("SELECT * FROM $table");
        $numColumns = $result->field_count;

        $return .= "DROP TABLE $table;";

        $result2 = $db->query("SHOW CREATE TABLE $table");
        $row2 = $result2->fetch_row();

        $return .= "\n\n".$row2[1].";\n\n";

        for($i = 0; $i < $numColumns; $i++){
            while($row = $result->fetch_row()){
                $return .= "INSERT INTO $table VALUES(";
                for($j=0; $j < $numColumns; $j++){
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = ereg_replace("\n","\\n",$row[$j]);
                    if (isset($row[$j])) { $return .= '"'.$row[$j].'"' ; } else { $return .= '""'; }
                    if ($j < ($numColumns-1)) { $return.= ','; }
                }
                $return .= ");\n";
            }
        }

        $return .= "\n\n\n";
    }

    //save file
    $handle = fopen('db-backup-'.time().'.sql','w+');
    fwrite($handle,$return);
    fclose($handle);
}

?>

<?php
//require_once '../inc/lib/phpmailer/PHPMailerAutoload.php';
require_once '../vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;

class Mail extends PHPMailer
{

    public function sendMail($from, $to, $subject, $template, $dircontent, $additional = array(), $altbody = "", $html=true)
    {
        $mail = new PHPMailer();

        $mail->Host = $_ENV['SMTP_HOST'];
        $mail->Username = $_ENV['SMTP_USER'];
        $mail->Password = $_ENV['SMTP_PASS'];
        $mail->Port = $_ENV['SMTP_PORT'];
        // $mail->SMTPDebug = 2;

        //setdefault
        $mail->SMTPSecure = $_ENV['SMTP_SECURE'];
        $mail->SMTPAuth = true;
        $mail->isSMTP();
        $mail->Debugoutput = 'html';

        $mail_from = explode('@', $from);
        $mail_to = explode('@', $to);
        $mail->setFrom($from, $mail_from[0]);
        $mail->addAddress($to, $mail_to[0]);
        $mail->Subject = $subject;
        $body = file_get_contents($dircontent .'/'. $template .'.html');
        $body = $this->getEmailTemplate($body, $additional);
        $mail->msgHTML($body);
        $mail->AltBody = $altbody;
        $mail->isHTML($html);

        if ($mail->send()) {
            return true;
        } else {
            echo "Not Send " . $mail->ErrorInfo;
        }
    }

    public function getEmailTemplate($template, $variablesArr = array())
    {
        foreach($variablesArr as $key => $value) {
            $dataKey[] =  "{".$key."}";
            $dataValue[] = $value;
        }
        $data = str_replace($dataKey, $dataValue, $template);
        return $data;

    }
}
?>

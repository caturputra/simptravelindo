<?php
  // $var_host = 'localhost';
  // $var_user = 'root';
  // $var_pass = 'caturputra';
  // $var_name = 'travelindodb';

  $var_host = $_ENV['DB_HOST'];
  $var_user = $_ENV['DB_USER'];
  $var_pass = $_ENV['DB_PASS'];
  $var_name = $_ENV['DB_NAME'];

  $var_con = mysqli_connect($var_host, $var_user, $var_pass, $var_name);

if (!$var_con) {
    die("Failed to connect to MySQL: " . mysqli_connect_errno() . "-" . mysqli_connect_error());
}

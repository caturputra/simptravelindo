// var FormValidation = function () {
//
//     // validation using icons
//     var handleValidationUser = function() {
//         // for more info visit the official plugin documentation:
//             // http://docs.jquery.com/Plugins/Validation
//
//             var formUser = $('#userCheck');
//             var error2 = $('.alert-danger', formUser);
//             var success2 = $('.alert-success', formUser);
//
//             formUser.validate({
//                 errorElement: 'span', //default input error message container
//                 errorClass: 'help-block help-block-error',
//                 focusInvalid: false, // do not focus the last invalid input
//                 ignore: "",  // validate all fields including form hidden input
//                 rules: {
//                     'frm_new_username': {
//                         required: true,
//                         remote: {
//                             url: "manageuser-check.php",
//                             type: 'POST',
//                             //dataType: 'HTML',
//                         },
//                     },
//                 },
//
//                 messages: {
//                     'frm_new_username': {
//                         required: 'Please Enter Your Username',
//                         remote: jQuery.validator.format("{0} Has Been Registered."),
//                     }
//                 },
//
//                 invalidHandler: function (event, validator) { //display error alert on form submit
//                     success2.hide();
//                     error2.show();
//                     App.scrollTo(error2, -200);
//                 },
//
//                 errorPlacement: function (error, element) { // render error placement for each input type
//                     var icon = $(element).parent('.input-icon').children('i');
//                     icon.removeClass('fa-check').addClass("fa-warning");
//                     icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
//                     if (icon) {
//                         error.insertAfter(element);
//                     } else {
//                         error.insertAfter(element);
//                     }
//                 },
//
//                 highlight: function (element) { // hightlight error inputs
//                     $(element)
//                         .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
//                 },
//
//                 unhighlight: function (element) { // revert the change done by hightlight
//
//                 },
//
//                 success: function (label, element, data) {
//                     if (data === true) {
//                         var icon = $(element).parent('.input-icon').children('i');
//                         $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); //set success class to the control group
//                         icon.removeClass("fa-warning").addClass("fa-check");
//                     }
//                 },
//
//                 submitHandler: function (form) {
//                     success2.show();
//                     error2.hide();
//                     form[0].submit(); // submit the form
//                 }
//             });
//
//
//     }
//
//     return {
//         //main function to initiate the module
//         init: function () {
//
//             handleValidationUser();
//
//         }
//
//     };
//
// }();
//
// jQuery(document).ready(function() {
//     FormValidation.init();
// });

var FormValidation = function () {

    // validation using icons
    var handleValidationUser = function() {
        // for more info visit the official plugin documentation:
            // http://docs.jquery.com/Plugins/Validation

            var formUser = $('#form_user_add');
            var error2 = $('.alert-danger', formUser);
            var success2 = $('.alert-success', formUser);

            formUser.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error',
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    'frm_fullname': {
                        required: true,
                    },
                    'frm_email': {
                        required: true,
                        email: true
                    },
                    'frm_username': {
                        required: true,
                        minlength: 4,
                        maxlength: 32,
                    },
                    'frm_password': {
                        required: true,
                        minlength: 12,
                        maxlength: 32,
                    },
                    'frm_confirm_password': {
                        required: true,
                    },
                    'frm_role': {
                        required: true,
                    },
                    'frm_phone': {
                        required: true,
                    },
                    'frm_firstname': {
                        required: true,
                        maxlength: 32,
                    },
                    'frm_lastname': {
                        required: true,
                        maxlength: 32,
                    },
                    'frm_address': {
                        required: true,
                        maxlength: 150,
                    },
                    // 'frm_status':{
                    //     required: true,
                    // },
                },

                messages: {
                    'frm_fullname': "Please provide a name",
                    'frm_email': "Please enter your lastname",
                    'frm_username' : {
                        required: "Please provide a username",
                        minlength: "Your username must be at least 4 characters long",
                    },
                    'frm_password': {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 12 characters long",
                    },
                    'frm_email': "Please enter a valid email address",
                    'frm_confirm_password':{
                        required: "Please provide confirm password",
                    },
                    'frm_role': "Please provide a role",
                    'frm_firstname': {
                        required: "Please provide a firstname",
                        maxlength: "Your firstname must be lower than  12 characters long",
                    },
                    'frm_lastname': {
                        required: "Please provide a lastname",
                        maxlength: "Your lastname must be lower than 32 characters",
                    },
                    'frm_address': {
                        required: "Please provide a address",
                        maxlength: "Your address must be lower than 150 characters",
                    },
                    'frm_gender': "Please provide a gender",
                    //'frm_status': "Please provide a status",
                },

                invalidHandler: function (event, validator) { //display error alert on form submit
                    success2.hide();
                    error2.show();
                    App.scrollTo(error2, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
                    if (icon) {
                        error.insertAfter(element);
                    } else {
                        error.insertAfter(element);
                    }
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight

                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    icon.removeClass("fa-warning").addClass("fa-check");
                },

                submitHandler: function (form) {
                    success2.show();
                    error2.hide();
                    form[0].submit(); // submit the form
                }
            });


    }

    // validation using icons
    var handleValidationArmada = function() {
        // for more info visit the official plugin documentation:
            // http://docs.jquery.com/Plugins/Validation

            var formArmada = $('#form_armada_add');
            var errorArmada = $('.alert-danger', formArmada);
            var successArmada = $('.alert-success', formArmada);

            formArmada.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error',
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    'frm_armada_id': {
                        required: true,
                    },
                    'frm_name': {
                        required: true,
                    },
                    'frm_type': {
                        required: true,
                    },
                    'frm_total_seat': {
                        required: true,
                        minlength: 0,
                    },
                    'frm_color': {
                        required: true,
                    },
                    'frm_condition': {
                        required: true,
                    },
                    'frm_avatar':{
                        required: true,
                    },
                    'frm_status': {
                        required: true,
                    },
                },

                messages: {
                    'frm_armada_id': "Please provide a name",
                    'frm_name': "Please enter your lastname",
                    'frm_type' : "Please Provide type",
                    'frm_total_seat': "Please provide number of seat",
                    'frm_color': "Please select a color",
                    'frm_condition': "Please select the condition",
                    'frm_avatar': "Please give a image",
                    'frm_status': "Please provide a status",
                },

                invalidHandler: function (event, validator) { //display error alert on form submit
                    successArmada.hide();
                    errorArmada.show();
                    App.scrollTo(errorArmada, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
                    if (icon) {
                        error.insertAfter(element);
                    } else {
                        error.insertAfter(element);
                    }
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight

                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    icon.removeClass("fa-warning").addClass("fa-check");
                },

                submitHandler: function (form) {
                    successArmada.show();
                    errorArmada.hide();
                    form[0].submit(); // submit the form
                }
            });


    }

    return {
        //main function to initiate the module
        init: function () {

            handleValidationUser();
            handleValidationArmada();

        }

    };

}();

jQuery(document).ready(function() {
    FormValidation.init();
});

var ComponentsDateTimePickers = function () {

    var handleDateRangePickers = function () {
        if (!jQuery().daterangepicker) {
            return;
        }

        $('#defaultrange').daterangepicker(//{
        //         format: 'DD/MM/YYYY H:i',
        //         locale: {
        //     format: 'MM/DD/YYYY h:mm A'
        // }
        //         timePicker: true,
        //         timePickerIncrement: 30,
        //         separator: ' to ',
        //         startDate: moment(),
        //         endDate: moment(),
        //         minDate: moment(),
        //         maxDate: moment().endOf('year')
        //     },
        //     function (start, end, label) {
        //         $('#defaultrange input').val(start.format('DD/MM/YYYY H:i') + ' - ' + end.format('DD/MM/YYYY'));
        //     }
        );
    }

    return {
        //main function to initiate the module
        init: function () {
            handleDateRangePickers();
        }
    };

}();

    jQuery(document).ready(function() {
        ComponentsDateTimePickers.init();
    });

-- MySQL dump 10.16  Distrib 10.2.13-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: travelindodb
-- ------------------------------------------------------
-- Server version	10.2.13-MariaDB-10.2.13+maria~xenial

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sp_armada`
--

DROP TABLE IF EXISTS `sp_armada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_armada` (
  `armada_regnumber` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `armada_brand` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `armada_typevariant` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `armada_type` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT '1 = kendaraan berat, 0 = kendaraan ringan',
  `armada_model` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `armada_productionyear` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `armada_chassisnumber` char(17) COLLATE utf8_unicode_ci NOT NULL,
  `armada_enginenumber` char(7) COLLATE utf8_unicode_ci NOT NULL,
  `armada_typefuel` enum('bensin','solar') COLLATE utf8_unicode_ci NOT NULL,
  `armada_fuelsize` smallint(5) unsigned NOT NULL,
  `armada_odometer` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `armada_image` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `armada_color` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `armada_status` enum('approved','rejected','pending') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `armada_createat` datetime NOT NULL DEFAULT current_timestamp(),
  `armada_modifyat` datetime NOT NULL,
  `armada_seat` tinyint(3) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`armada_regnumber`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `sp_armada_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sp_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_armada`
--

LOCK TABLES `sp_armada` WRITE;
/*!40000 ALTER TABLE `sp_armada` DISABLE KEYS */;
INSERT INTO `sp_armada` VALUES ('AB 29XX BU','Luxury','Big Bus','2','bus','2012','12345678945646546','5646546','solar',1500,'150005','img-ava-ab 2968 bu.jpg','Biru','approved','2018-02-20 15:43:00','2018-02-20 16:03:17',45,3),('AB 56XX KF','Luxury','Medium Bus','2','bus','2014','87945613213216546','5465462','solar',750,'45623','img-ava-ab 5688 kf.jpg','Biru','approved','2018-02-20 15:43:59','2018-02-20 16:03:17',29,3),('AB 57XX VW','Toyota','Elf','1','minibus','2016','37894873999837493','8379498','bensin',70,'1','img-ava-ab 5774 vw.jpg','Putih','approved','2018-02-21 22:47:46','2018-02-21 23:06:25',29,4),('AB 64XX SU','Toyota','Elf','1','minibus','2016','83749399999736361','8239487','bensin',70,'1','img-ava-ab 6498 su.jpg','Kuning','approved','2018-02-21 22:49:13','2018-02-21 23:06:25',29,4),('AB 68XX NF','Toyota','Innova','1','minibus','2014','73213213546546662','4564612','bensin',45,'11','img-ava-ab 6854 nf.jpg','Hitam','approved','2018-02-20 15:51:28','2018-02-20 16:03:17',5,3),('H 39XX GJ','Isuzu','Luxury','1','microbus','2014','12134654654654213','5846546','bensin',600,'897556','img-ava-h 3925 gj.jpg','Putih','approved','2018-02-20 15:47:24','2018-02-20 16:03:17',28,3),('H 57XX ZM','Toyota','Hi Ace','1','minibus','2014','12638716238888821','7283947','bensin',70,'1','img-ava-h 5732 zm.jpg','Putih','approved','2018-02-21 22:45:52','2018-02-21 23:06:25',29,4);
/*!40000 ALTER TABLE `sp_armada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_armada_condition`
--

DROP TABLE IF EXISTS `sp_armada_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_armada_condition` (
  `condition_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `condition_checkdate` datetime DEFAULT NULL,
  `condition_meter` varchar(3) COLLATE utf8_unicode_ci DEFAULT '0',
  `condition_type` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '0: tipe harian, 1: bulanan',
  `user_id` int(10) unsigned DEFAULT NULL,
  `armada_regnumber` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`condition_id`),
  KEY `condition_id` (`condition_id`),
  KEY `user_id` (`user_id`),
  KEY `armada_regnumber` (`armada_regnumber`),
  CONSTRAINT `fk_sp_armada_condition_1` FOREIGN KEY (`user_id`) REFERENCES `sp_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sp_armada_condition_ibfk_1` FOREIGN KEY (`armada_regnumber`) REFERENCES `sp_armada` (`armada_regnumber`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_armada_condition`
--

LOCK TABLES `sp_armada_condition` WRITE;
/*!40000 ALTER TABLE `sp_armada_condition` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_armada_condition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_armada_condition_detail`
--

DROP TABLE IF EXISTS `sp_armada_condition_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_armada_condition_detail` (
  `detail_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `detail_condition` char(1) CHARACTER SET utf8 NOT NULL DEFAULT '1' COMMENT '1: baik, 0: buruk',
  `params_id` tinyint(3) unsigned NOT NULL,
  `condition_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`detail_id`),
  KEY `fk_sp_armada_condition_detail_1_idx` (`condition_id`),
  KEY `params_id` (`params_id`),
  CONSTRAINT `fk_sp_armada_condition_detail_1` FOREIGN KEY (`condition_id`) REFERENCES `sp_armada_condition` (`condition_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sp_armada_condition_detail_2` FOREIGN KEY (`params_id`) REFERENCES `sp_check_params` (`params_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_armada_condition_detail`
--

LOCK TABLES `sp_armada_condition_detail` WRITE;
/*!40000 ALTER TABLE `sp_armada_condition_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_armada_condition_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_armada_conditiononday`
--

DROP TABLE IF EXISTS `sp_armada_conditiononday`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_armada_conditiononday` (
  `acd_id` int(10) NOT NULL AUTO_INCREMENT,
  `schedule_id` int(10) unsigned NOT NULL,
  `condition_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`acd_id`),
  KEY `schedule_id` (`schedule_id`),
  KEY `condition_id` (`condition_id`),
  CONSTRAINT `sp_armada_conditiononday_ibfk_2` FOREIGN KEY (`condition_id`) REFERENCES `sp_armada_condition` (`condition_id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `sp_armada_conditiononday_ibfk_3` FOREIGN KEY (`schedule_id`) REFERENCES `sp_schedule` (`schedule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_armada_conditiononday`
--

LOCK TABLES `sp_armada_conditiononday` WRITE;
/*!40000 ALTER TABLE `sp_armada_conditiononday` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_armada_conditiononday` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_bank`
--

DROP TABLE IF EXISTS `sp_bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_bank` (
  `bank_id` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `bank_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_bank`
--

LOCK TABLES `sp_bank` WRITE;
/*!40000 ALTER TABLE `sp_bank` DISABLE KEYS */;
INSERT INTO `sp_bank` VALUES ('002','BANK BRI'),('003','BANK EKSPOR INDONESIA'),('008','BANK MANDIRI'),('009','BANK BNI'),('011','BANK DANAMON'),('013','PERMATA BANK'),('014','BANK BCA'),('016','BANK BII'),('019','BANK PANIN'),('020','BANK ARTA NIAGA KENCANA'),('022','BANK NIAGA'),('023','BANK BUANA IND'),('026','BANK LIPPO'),('028','BANK NISP'),('030','AMERICAN EXPRESS BANK LTD'),('031','CITIBANK N.A.'),('032','JP. MORGAN CHASE BANK, N.A.'),('033','BANK OF AMERICA, N.A'),('034','ING INDONESIA BANK'),('036','BANK MULTICOR TBK.'),('037','BANK ARTHA GRAHA'),('039','BANK CREDIT AGRICOLE INDOSUEZ'),('040','THE BANGKOK BANK COMP. LTD'),('041','THE HONGKONG & SHANGHAI B.C.'),('042','THE BANK OF TOKYO MITSUBISHI U'),('045','BANK SUMITOMO MITSUI INDONESIA'),('046','BANK DBS INDONESIA'),('047','BANK RESONA PERDANIA'),('048','BANK MIZUHO INDONESIA'),('050','STANDARD CHARTERED BANK'),('052','BANK ABN AMRO'),('053','BANK KEPPEL TATLEE BUANA'),('054','BANK CAPITAL INDONESIA, TBK.'),('057','BANK BNP PARIBAS INDONESIA'),('058','BANK UOB INDONESIA'),('059','KOREA EXCHANGE BANK DANAMON'),('060','RABOBANK INTERNASIONAL INDONES'),('061','ANZ PANIN BANK'),('067','DEUTSCHE BANK AG.'),('068','BANK WOORI INDONESIA'),('069','BANK OF CHINA LIMITED'),('076','BANK BUMI ARTA'),('087','BANK EKONOMI'),('088','BANK ANTARDAERAH'),('089','BANK HAGA'),('093','BANK IFI'),('095','BANK CENTURY, TBK.'),('097','BANK MAYAPADA'),('110','BANK JABAR'),('111','BANK DKI'),('112','BPD DIY'),('113','BANK JATENG'),('114','BANK JATIM'),('115','BPD JAMBI'),('116','BPD ACEH'),('117','BANK SUMUT'),('118','BANK NAGARI'),('119','BANK RIAU'),('120','BANK SUMSEL'),('121','BANK LAMPUNG'),('122','BPD KALSEL'),('123','BPD KALIMANTAN BARAT'),('124','BPD KALTIM'),('125','BPD KALTENG'),('126','BPD SULSEL'),('127','BANK SULUT'),('128','BPD NTB'),('129','BPD BALI'),('130','BANK NTT'),('131','BANK MALUKU'),('132','BPD PAPUA'),('133','BANK BENGKULU'),('134','BPD SULAWESI TENGAH'),('135','BANK SULTRA'),('145','BANK NUSANTARA PARAHYANGAN'),('146','BANK SWADESI'),('147','BANK MUAMALAT'),('151','BANK MESTIKA'),('152','BANK METRO EXPRESS'),('153','BANK SHINTA INDONESIA'),('157','BANK MASPION'),('159','BANK HAGAKITA'),('161','BANK GANESHA'),('162','BANK WINDU KENTJANA'),('164','HALIM INDONESIA BANK'),('166','BANK HARMONI INTERNATIONAL'),('167','BANK KESAWAN'),('200','BANK TABUNGAN NEGARA (PERSERO)'),('212','BANK HIMPUNAN SAUDARA 1906, TB'),('213','BANK TABUNGAN PENSIUNAN NASION'),('405','BANK SWAGUNA'),('422','BANK JASA ARTA'),('426','BANK MEGA'),('427','BANK JASA JAKARTA'),('441','BANK BUKOPIN'),('451','BANK SYARIAH MANDIRI'),('459','BANK BISNIS INTERNASIONAL'),('466','BANK SRI PARTHA'),('472','BANK JASA JAKARTA'),('484','BANK BINTANG MANUNGGAL'),('485','BANK BUMIPUTERA'),('490','BANK YUDHA BHAKTI'),('491','BANK MITRANIAGA'),('494','BANK AGRO NIAGA'),('498','BANK INDOMONEX'),('501','BANK ROYAL INDONESIA'),('503','BANK ALFINDO'),('506','BANK SYARIAH MEGA'),('513','BANK INA PERDANA'),('517','BANK HARFA'),('520','PRIMA MASTER BANK'),('521','BANK PERSYARIKATAN INDONESIA'),('523','BANK DIPO INTERNATIONAL'),('525','BANK AKITA'),('526','LIMAN INTERNATIONAL BANK'),('531','ANGLOMAS INTERNASIONAL BANK'),('535','BANK KESEJAHTERAAN EKONOMI'),('536','BANK UIB'),('542','BANK ARTOS IND'),('547','BANK PURBA DANARTA'),('548','BANK MULTI ARTA SENTOSA'),('553','BANK MAYORA'),('555','BANK INDEX SELINDO'),('558','BANK EKSEKUTIF'),('559','CENTRATAMA NASIONAL BANK'),('562','BANK FAMA INTERNASIONAL'),('564','BANK SINAR HARAPAN BALI'),('566','BANK VICTORIA INTERNATIONAL'),('567','BANK HARDA'),('945','BANK FINCONESIA'),('946','BANK MERINCORP'),('947','BANK MAYBANK INDOCORP'),('948','BANK OCBC – INDONESIA'),('949','BANK CHINA TRUST INDONESIA'),('950','BANK COMMONWEALTH');
/*!40000 ALTER TABLE `sp_bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_bank_account`
--

DROP TABLE IF EXISTS `sp_bank_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_bank_account` (
  `account_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `account_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `account_status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '0: pending, 1:approved, 2:rejected',
  `bank_id` char(3) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`account_number`),
  KEY `bank_id` (`bank_id`),
  CONSTRAINT `sp_bank_account_ibfk_1` FOREIGN KEY (`bank_id`) REFERENCES `sp_bank` (`bank_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_bank_account`
--

LOCK TABLES `sp_bank_account` WRITE;
/*!40000 ALTER TABLE `sp_bank_account` DISABLE KEYS */;
INSERT INTO `sp_bank_account` VALUES ('09812367123','Travelindo','1','002'),('789756456465','Travelindo','1','008'),('8795465465465','Travelindo','0','002');
/*!40000 ALTER TABLE `sp_bank_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_check_params`
--

DROP TABLE IF EXISTS `sp_check_params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_check_params` (
  `params_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `params_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`params_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_check_params`
--

LOCK TABLES `sp_check_params` WRITE;
/*!40000 ALTER TABLE `sp_check_params` DISABLE KEYS */;
INSERT INTO `sp_check_params` VALUES (1,'Fisik Mesin'),(2,'Tali Kipas'),(3,'Air Radiator'),(4,'Oli Mesin'),(5,'Aki Kendaraan'),(6,'Spooring'),(7,'Kaki-kaki'),(8,'Rem'),(9,'Kondisi Ban'),(10,'Kondisi Kopling'),(11,'Suspensi'),(12,'Lampu-lampu'),(14,'AC'),(17,'Wiper');
/*!40000 ALTER TABLE `sp_check_params` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_crew_armada`
--

DROP TABLE IF EXISTS `sp_crew_armada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_crew_armada` (
  `crew_armadaid` char(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `crew_status` enum('0','1','2','3') COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '0:pending, 1:setujui, 2:ditolak, 3:deleted',
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`crew_armadaid`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `fk_sp_crew_armada_1` FOREIGN KEY (`user_id`) REFERENCES `sp_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_crew_armada`
--

LOCK TABLES `sp_crew_armada` WRITE;
/*!40000 ALTER TABLE `sp_crew_armada` DISABLE KEYS */;
INSERT INTO `sp_crew_armada` VALUES ('CA001','2',14),('CA002','1',15),('CA003','1',16),('CA004','1',17),('CA005','1',18),('CA006','1',19),('CA007','1',20),('CA008','2',21);
/*!40000 ALTER TABLE `sp_crew_armada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_crew_armada_detail`
--

DROP TABLE IF EXISTS `sp_crew_armada_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_crew_armada_detail` (
  `detail_licensenumber` char(12) COLLATE utf8_unicode_ci NOT NULL COMMENT 'nomor sim',
  `detail_licensetype` char(2) COLLATE utf8_unicode_ci NOT NULL COMMENT 'SIM A, SIM B, SIM B POLOS, DLL',
  `crew_armadaid` char(5) COLLATE utf8_unicode_ci NOT NULL,
  KEY `crew_armadaid` (`crew_armadaid`),
  CONSTRAINT `sp_crew_armada_detail_ibfk_1` FOREIGN KEY (`crew_armadaid`) REFERENCES `sp_crew_armada` (`crew_armadaid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_crew_armada_detail`
--

LOCK TABLES `sp_crew_armada_detail` WRITE;
/*!40000 ALTER TABLE `sp_crew_armada_detail` DISABLE KEYS */;
INSERT INTO `sp_crew_armada_detail` VALUES ('827394827394','A','CA002'),('454654654654','A','CA003'),('643132165732','BP','CA003'),('487946514564','A','CA004'),('546574621321','B','CA004'),('546543213213','BP','CA004'),('879645645654','A','CA001'),('564654621321','A','CA008'),('532132146576','B','CA007'),('514321321354','A','CA005'),('546543213213','A','CA006');
/*!40000 ALTER TABLE `sp_crew_armada_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_crew_detail`
--

DROP TABLE IF EXISTS `sp_crew_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_crew_detail` (
  `crew_detailid` int(10) NOT NULL AUTO_INCREMENT,
  `crew_armadaid` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `crew_datefrom` datetime NOT NULL,
  `crew_dateto` datetime NOT NULL,
  `crew_action` char(1) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '0:pending, 1:setujui, 2:ditolak, 3:dibatalkan',
  `crew_createdat` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `schedule_id` int(10) unsigned NOT NULL,
  `crew_createdby` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`crew_detailid`),
  KEY `index2` (`crew_armadaid`),
  KEY `schedule_id` (`schedule_id`),
  CONSTRAINT `fk_sp_crew_detail_1` FOREIGN KEY (`crew_armadaid`) REFERENCES `sp_crew_armada` (`crew_armadaid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sp_crew_detail_ibfk_1` FOREIGN KEY (`schedule_id`) REFERENCES `sp_schedule` (`schedule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sp_crew_detail_ibfk_2` FOREIGN KEY (`schedule_id`) REFERENCES `sp_schedule` (`schedule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_crew_detail`
--

LOCK TABLES `sp_crew_detail` WRITE;
/*!40000 ALTER TABLE `sp_crew_detail` DISABLE KEYS */;
INSERT INTO `sp_crew_detail` VALUES (1,'CA002','2018-03-02 10:00:00','2018-03-02 11:00:00','1','2018-03-02 11:37:55',2,1);
/*!40000 ALTER TABLE `sp_crew_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_deposit`
--

DROP TABLE IF EXISTS `sp_deposit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_deposit` (
  `deposit_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deposit_balance` int(10) DEFAULT 0,
  `rekening_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`deposit_id`),
  KEY `user_id` (`rekening_id`),
  CONSTRAINT `sp_deposit_ibfk_1` FOREIGN KEY (`rekening_id`) REFERENCES `sp_rekening` (`rekening_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_deposit`
--

LOCK TABLES `sp_deposit` WRITE;
/*!40000 ALTER TABLE `sp_deposit` DISABLE KEYS */;
INSERT INTO `sp_deposit` VALUES (1,0,'TRV1802202FELK');
/*!40000 ALTER TABLE `sp_deposit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_deposit_temp`
--

DROP TABLE IF EXISTS `sp_deposit_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_deposit_temp` (
  `temp_id` int(11) NOT NULL AUTO_INCREMENT,
  `temp_amount` int(10) NOT NULL,
  `temp_status` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT '0: pending, 1:approved, 2:rejected',
  `temp_type` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1: debit, 2:kredit',
  `temp_to` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `temp_from` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `temp_fromname` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `temp_receipt` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `temp_createdat` datetime NOT NULL DEFAULT current_timestamp(),
  `rekening_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`temp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_deposit_temp`
--

LOCK TABLES `sp_deposit_temp` WRITE;
/*!40000 ALTER TABLE `sp_deposit_temp` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_deposit_temp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_expenses`
--

DROP TABLE IF EXISTS `sp_expenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_expenses` (
  `expenses_id` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `expenses_status` enum('0','1','2','3') COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '0:pending, 1:setujui, 2:ditolak',
  `expenses_createat` timestamp NULL DEFAULT NULL,
  `schedule_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `expenses_confirmedby` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`expenses_id`),
  KEY `user_id` (`user_id`),
  KEY `fk_sp_expenses_1_idx` (`schedule_id`),
  KEY `expenses_confirmedby` (`expenses_confirmedby`),
  CONSTRAINT `fk_sp_expenses_2` FOREIGN KEY (`user_id`) REFERENCES `sp_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sp_expenses_ibfk_1` FOREIGN KEY (`schedule_id`) REFERENCES `sp_schedule` (`schedule_id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `sp_expenses_ibfk_2` FOREIGN KEY (`expenses_confirmedby`) REFERENCES `sp_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_expenses`
--

LOCK TABLES `sp_expenses` WRITE;
/*!40000 ALTER TABLE `sp_expenses` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_expenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_expenses_detail`
--

DROP TABLE IF EXISTS `sp_expenses_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_expenses_detail` (
  `detail_amount` int(10) unsigned DEFAULT NULL,
  `detail_peritem` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `detail_amountperitem` int(10) unsigned NOT NULL,
  `detail_countitem` tinyint(2) unsigned NOT NULL,
  `expenses_id` char(12) COLLATE utf8_unicode_ci NOT NULL,
  KEY `expenses_id` (`expenses_id`),
  CONSTRAINT `sp_expenses_detail_ibfk_1` FOREIGN KEY (`expenses_id`) REFERENCES `sp_expenses` (`expenses_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_expenses_detail`
--

LOCK TABLES `sp_expenses_detail` WRITE;
/*!40000 ALTER TABLE `sp_expenses_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_expenses_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_going`
--

DROP TABLE IF EXISTS `sp_going`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_going` (
  `going_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `schedule_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`going_id`),
  KEY `schedule_id` (`schedule_id`),
  CONSTRAINT `sp_going_ibfk_1` FOREIGN KEY (`schedule_id`) REFERENCES `sp_schedule` (`schedule_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_going`
--

LOCK TABLES `sp_going` WRITE;
/*!40000 ALTER TABLE `sp_going` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_going` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_going_log`
--

DROP TABLE IF EXISTS `sp_going_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_going_log` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `log_desc` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `log_status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '0: home, 1: onroad, 2:garage',
  `log_createat` datetime NOT NULL,
  `going_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `going_id` (`going_id`),
  CONSTRAINT `sp_going_log_ibfk_1` FOREIGN KEY (`going_id`) REFERENCES `sp_going` (`going_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_going_log`
--

LOCK TABLES `sp_going_log` WRITE;
/*!40000 ALTER TABLE `sp_going_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_going_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_level`
--

DROP TABLE IF EXISTS `sp_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_level` (
  `level_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `level_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_level`
--

LOCK TABLES `sp_level` WRITE;
/*!40000 ALTER TABLE `sp_level` DISABLE KEYS */;
INSERT INTO `sp_level` VALUES (1,'Admin'),(3,'Pengecek'),(4,'Member'),(5,'Agen'),(6,'Kasir'),(7,'Keuangan'),(8,'Sopir'),(9,'Manajer'),(10,'Direktur'),(11,'Guest'),(12,'Kernet');
/*!40000 ALTER TABLE `sp_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_menu`
--

DROP TABLE IF EXISTS `sp_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_menu` (
  `menu_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `menu_parent` tinyint(3) unsigned NOT NULL,
  `menu_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `menu_icon` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `menu_order` tinyint(2) unsigned NOT NULL,
  `menu_visibility` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_menu`
--

LOCK TABLES `sp_menu` WRITE;
/*!40000 ALTER TABLE `sp_menu` DISABLE KEYS */;
INSERT INTO `sp_menu` VALUES (1,'Dashboard',0,'home-admin.php','icon-home',0,'1'),(2,'Transaksi',0,'#','icon-notebook',1,'1'),(3,'Booking Armada',2,'booking-armada.php','fa fa-circle-o',1,'1'),(4,'Manajemen',0,'#','icon-equalizer',3,'1'),(5,'Pengguna',4,'manageuser.php','fa fa-circle-o',3,'1'),(6,'Profile',4,'manageuser-profile.php','fa fa-circle-o',2,'0'),(7,'Armada',4,'managearmada.php','fa fa-circle-o',1,'1'),(13,'Dashboard ',0,'home-pengecek.php','icon-home',0,'1'),(14,'Dashboard',0,'home-manajer.php','icon-home',0,'1'),(15,'Dashboard',0,'home-direktur.php','icon-home',0,'1'),(16,'Dashboard',0,'home-member.php','icon-home',0,'1'),(17,'Dashboard',0,'home-sopir.php','icon-home',0,'1'),(18,'Dashboard',0,'home-keuangan.php','icon-home',0,'1'),(19,'Dashboard',0,'home-agen.php','icon-home',0,'1'),(20,'Dashboard',0,'home-kasir.php','icon-home',0,'1'),(21,'Beranda',0,'home.php','fa fa-chevron-down',0,'1'),(22,'Layanan',0,'services.php','fa fa-chevron-down',1,'1'),(23,'Tentang Kami',0,'about-us.php','fa fa-chevron-down',4,'1'),(24,'Hubungi Kami',0,'contact-us.php','fa fa-chevron-down',5,'1'),(25,'Pemesanan',0,'reservation.php','fa fa-chevron-down',3,'1'),(26,'Armada',0,'#','fa fa-chevron-down',2,'1'),(27,'Mini Bus',26,'armadas.php&type=minibus','',0,'1'),(28,'Micro Bus',26,'armadas.php&type=microbus','',1,'1'),(29,'ELF',26,'armadas.php&type=elf','',2,'1'),(31,'Laporan',0,'#','icon-book-open',4,'1'),(33,'Laporan Pemesanan',30,'report-reservation.php','fa fa-circle-o',1,'1'),(34,'Pengaturan Parameter',0,'armada-check-params.php','icon-settings',4,'1'),(37,'Monitor Kondisi Armada',0,'armada-condition.php','icon-layers',3,'0'),(38,'Booking Invoice',2,'booking-invoice.php','fa fa-circle-o',5,'0'),(39,'Crew Armada',4,'managecrew.php','fa fa-circle-o',4,'1'),(40,'Dashboard',0,'home-manajer.php','icon-home',0,'1'),(41,'Validasi',0,'#','icon-pencil',1,'1'),(42,'Validasi Armada',41,'validate-vehicles.php','fa fa-circle-o',0,'1'),(43,'Pemesanan',2,'member-orders.php','fa fa-circle-o',0,'1'),(44,'Pembayaran',2,'booking-payment.php','fa fa-circle-o',2,'0'),(45,'Daftar Tagihan',2,'booking-list-payment.php','fa fa-circle-o',4,'1'),(46,'Validasi Pembayaran',41,'validate-payments.php','fa fa-circle-o',0,'1'),(47,'Pembayaran Tunai',0,'booking-payment-cash.php','icon-diamond',0,'0'),(49,'Penjadwalan',0,'#','icon-calendar',2,'1'),(50,'Pengaturan',0,'#','icon-settings',4,'1'),(51,'Rekening Bank',50,'managebank-account.php','fa fa-circle-o',0,'1'),(52,'Deposit',0,'agent-deposit.php','icon-social-dropbox',1,'1'),(53,'Pengecekan',0,'armada-condition-check.php','icon-wrench',10,'0'),(54,'Dashboard',0,'home-kernet.php','icon-home',0,'1'),(55,'Dashboard',0,'home-direktur.php','icon-home',0,'1'),(56,'Crew Armada',41,'validate-crew-armada.php','fa fa-circle-o',0,'1'),(58,'Penugasan',0,'crew-assignment.php','icon-pin',1,'1'),(59,'Atur Jadwal',49,'booking-schedule.php','fa fa-circle-o',0,'1'),(60,'Daftar Jadwal',49,'booking-schedule-list.php','fa fa-circle-o',1,'1'),(61,'Monitor Perjalanan',0,'armada-monitor.php','icon-rocket',3,'1'),(62,'Detail Monitoring Perjalanan',0,'armada-monitor-timeline.php','icon-rocket',0,'0'),(63,'Monitor Armada',0,'armada-monitor-update.php','icon-settings',2,'1'),(64,'Buat Expenses',0,'booking-expenses.php','icon-social-dropbox',0,'0'),(65,'Pembatalan Pesanan',0,'booking-canceled-list.php','icon-basket',2,'1'),(66,'Validasi Expenses',41,'validate-expenses.php','fa fa-circle-o',1,'1'),(67,'Laporan',0,'#','icon-notebook',6,'1'),(68,'Expenses',67,'report-expenses.php','fa fa-circle-o',0,'1'),(69,'Armada',67,'report-armada.php','fa fa-circle-o',2,'1'),(70,'Perbaharui',2,'booking-schedule-update.php','fa fa-circle-o',4,'0'),(71,'Daftar Pelunasan',0,'booking-invoice-list.php','fa fa-circle-o',7,'0'),(72,'Validasi Deposit',41,'validate-deposit.php','fa fa-circle-o',3,'1'),(73,'Histori Pembayaran',0,'booking-payment-timelilne.php','fa fa-circle-thin-o',1,'0'),(74,'Monitor Armada',0,'armada-monitor-byschedule.php','fa fa-cog',4,'1'),(75,'Pengecekan',0,'armada-condition-checkone.php','icon-wrench',10,'0'),(76,'Daftar Tagihan Tamu',0,'payment-claim.php','icon-layers',1,'1'),(77,'Kirim Pesan',0,'payment-claim-message.php','fa fa-paper-plane',0,'0'),(78,'Histori Perjalanan',0,'armada-goinglog.php','fa fa-paper-plane',5,'1'),(79,'Histori Perjalanan Detail',0,'armada-goinglogdetail.php','fa fa-circle-o',7,'0');
/*!40000 ALTER TABLE `sp_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_notification`
--

DROP TABLE IF EXISTS `sp_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_notification` (
  `notif_id` int(10) NOT NULL AUTO_INCREMENT,
  `notif_fromid` int(10) unsigned DEFAULT NULL,
  `notif_to` int(10) unsigned DEFAULT NULL,
  `notif_desc` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `notif_type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notif_isread` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`notif_id`),
  KEY `notif_fromid` (`notif_fromid`),
  KEY `notif_to` (`notif_to`),
  KEY `notif_fromid_2` (`notif_fromid`),
  KEY `notif_to_2` (`notif_to`),
  KEY `fk_sp_notification_1_idx` (`notif_fromid`,`notif_to`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_notification`
--

LOCK TABLES `sp_notification` WRITE;
/*!40000 ALTER TABLE `sp_notification` DISABLE KEYS */;
INSERT INTO `sp_notification` VALUES (3,5,23,'Pembayaran Dengan Kode 2 Telah Dikonfirmasi .','Konfirmasi','0'),(4,1,15,'Anda Mendapat Penugasan, Segera Konfirmasi.','Penugasan','0'),(5,15,1,'Kru Abraham Menerima Penugasan.','Konfirmasi','1'),(6,15,1,'Kru Abraham Menerima Penugasan.','Konfirmasi','1');
/*!40000 ALTER TABLE `sp_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_order_canceled`
--

DROP TABLE IF EXISTS `sp_order_canceled`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_order_canceled` (
  `canceled_reason` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `canceled_status` enum('0','1') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '0: reject, 1:approved',
  `od_id` char(6) COLLATE utf8_unicode_ci NOT NULL,
  KEY `od_id` (`od_id`),
  CONSTRAINT `sp_order_canceled_ibfk_1` FOREIGN KEY (`od_id`) REFERENCES `sp_order_transaction` (`od_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_order_canceled`
--

LOCK TABLES `sp_order_canceled` WRITE;
/*!40000 ALTER TABLE `sp_order_canceled` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_order_canceled` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trg_cancelOrder` AFTER UPDATE ON `sp_order_canceled` FOR EACH ROW BEGIN
DECLARE xorder CHAR(6);

SET xorder = NEW.od_id;
CALL cancelOrder(xorder);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `sp_order_temp`
--

DROP TABLE IF EXISTS `sp_order_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_order_temp` (
  `temp_id` int(10) unsigned DEFAULT NULL COMMENT '\n',
  `temp_data` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `temp_sessionid` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `temp_id` (`temp_id`),
  KEY `temp_data` (`temp_data`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_order_temp`
--

LOCK TABLES `sp_order_temp` WRITE;
/*!40000 ALTER TABLE `sp_order_temp` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_order_temp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_order_transaction`
--

DROP TABLE IF EXISTS `sp_order_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_order_transaction` (
  `od_id` char(6) COLLATE utf8_unicode_ci NOT NULL,
  `armada_regnumber` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `od_orderdate` datetime DEFAULT NULL,
  `od_invoice` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `od_service` enum('1','2','3') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '1: city tour, 2: transfer in/out, 3:paket wisata',
  `od_selling` int(10) unsigned NOT NULL DEFAULT 0,
  `od_from` datetime NOT NULL,
  `od_to` datetime NOT NULL,
  `od_request` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `od_dropship` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `od_status` enum('0','1','2','3','4','5') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0:pending, 1:setujui, 2:ditolak, 3: dibatalakan, 4:definiti',
  `user_id` int(10) unsigned DEFAULT NULL,
  `od_confirmedby` int(10) unsigned DEFAULT NULL,
  `od_createat` timestamp NULL DEFAULT NULL,
  `od_modifyat` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`od_id`),
  KEY `user_id` (`user_id`),
  KEY `od_confirmedby` (`od_confirmedby`),
  KEY `od_booktype` (`armada_regnumber`),
  CONSTRAINT `fk_sp_order_transaction_1` FOREIGN KEY (`user_id`) REFERENCES `sp_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sp_order_transaction_3` FOREIGN KEY (`od_confirmedby`) REFERENCES `sp_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sp_order_transaction_ibfk_1` FOREIGN KEY (`armada_regnumber`) REFERENCES `sp_armada` (`armada_regnumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_order_transaction`
--

LOCK TABLES `sp_order_transaction` WRITE;
/*!40000 ALTER TABLE `sp_order_transaction` DISABLE KEYS */;
INSERT INTO `sp_order_transaction` VALUES ('TR0001','H 57XX ZM','2018-03-07 19:21:34',NULL,'2',0,'2018-03-07 10:00:00','2018-03-07 15:00:00','','Bandara','0',23,NULL,NULL,'2018-03-07 12:21:34'),('TR0002','AB 68XX NF','2018-03-07 19:28:46',NULL,'2',0,'2018-03-07 11:00:00','2018-03-07 17:00:00','','Test','0',22,NULL,NULL,'2018-03-07 12:28:46'),('TR0003','AB 68XX NF','2018-03-08 11:16:10',NULL,'2',0,'2018-03-08 10:00:00','2018-03-08 11:00:00','','Bandara','0',23,NULL,NULL,'2018-03-08 04:16:10'),('TR0004','H 39XX GJ','2018-03-08 11:20:25',NULL,'2',0,'2018-03-08 11:00:00','2018-03-08 13:00:00','','Bandara','0',22,NULL,NULL,'2018-03-08 04:20:25');
/*!40000 ALTER TABLE `sp_order_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_payment`
--

DROP TABLE IF EXISTS `sp_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_payment` (
  `payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_date` datetime DEFAULT NULL,
  `payment_accountfrom` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_accountname` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_accountto` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_confirmdate` datetime DEFAULT NULL,
  `payment_verdate` datetime DEFAULT NULL,
  `payment_lackofpay` int(10) unsigned DEFAULT 0,
  `payment_balance` int(10) unsigned DEFAULT 0,
  `payment_howto` char(1) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '0: transfer, 1:tunai',
  `payment_type` char(1) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '0: Uang Muka, 1: Lunas',
  `payment_receipt` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_status` enum('0','1','2') COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '0: pending, 1:approved, 2:rejected',
  `schedule_id` int(10) unsigned DEFAULT NULL,
  `payment_confirmedby` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `fk_sp_payment_1_idx` (`schedule_id`),
  KEY `payment_noregto` (`payment_accountto`),
  CONSTRAINT `sp_payment_ibfk_1` FOREIGN KEY (`payment_accountto`) REFERENCES `sp_bank_account` (`account_number`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `sp_payment_ibfk_2` FOREIGN KEY (`schedule_id`) REFERENCES `sp_schedule` (`schedule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_payment`
--

LOCK TABLES `sp_payment` WRITE;
/*!40000 ALTER TABLE `sp_payment` DISABLE KEYS */;
INSERT INTO `sp_payment` VALUES (1,'2018-03-02 11:30:00','2634789263784','caturputra','09812367123','2018-03-02 11:34:41','2018-03-02 11:35:01',0,1500000,'0','1','IMG-RECEIPT-ITR030001-1.jpg','1',2,5);
/*!40000 ALTER TABLE `sp_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_previllages`
--

DROP TABLE IF EXISTS `sp_previllages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_previllages` (
  `level_id` tinyint(3) unsigned NOT NULL,
  `menu_id` tinyint(3) unsigned NOT NULL,
  KEY `level_id` (`level_id`),
  KEY `menu_id` (`menu_id`),
  CONSTRAINT `sp_previllages_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `sp_menu` (`menu_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sp_previllages_ibfk_2` FOREIGN KEY (`level_id`) REFERENCES `sp_level` (`level_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_previllages`
--

LOCK TABLES `sp_previllages` WRITE;
/*!40000 ALTER TABLE `sp_previllages` DISABLE KEYS */;
INSERT INTO `sp_previllages` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,39),(1,33),(1,49),(1,59),(1,60),(1,61),(1,62),(1,64),(1,67),(1,68),(1,69),(1,70),(3,13),(3,34),(3,37),(3,53),(3,6),(3,74),(3,75),(4,2),(4,16),(4,43),(4,44),(4,45),(4,77),(4,6),(5,6),(5,19),(5,38),(5,43),(5,44),(5,45),(5,52),(5,73),(5,2),(6,6),(6,20),(6,47),(7,6),(7,18),(7,41),(7,46),(7,66),(7,67),(7,72),(7,76),(7,77),(8,6),(8,17),(8,58),(8,63),(12,6),(12,17),(12,58),(12,63),(9,6),(9,14),(9,56),(9,62),(9,65),(9,42),(10,6),(10,15),(10,56),(10,62),(10,65),(10,41),(9,41),(10,15),(10,62),(9,62),(10,61),(9,61),(11,21),(11,22),(11,23),(11,24),(11,25),(4,38),(5,38),(12,54),(10,42),(7,50),(7,51),(9,39),(9,4),(10,4),(10,39),(1,78),(1,79);
/*!40000 ALTER TABLE `sp_previllages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_rekening`
--

DROP TABLE IF EXISTS `sp_rekening`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_rekening` (
  `rekening_id` char(15) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`rekening_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `fk_sp_rekening_1` FOREIGN KEY (`user_id`) REFERENCES `sp_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_rekening`
--

LOCK TABLES `sp_rekening` WRITE;
/*!40000 ALTER TABLE `sp_rekening` DISABLE KEYS */;
INSERT INTO `sp_rekening` VALUES ('TRV1802202FELK',22);
/*!40000 ALTER TABLE `sp_rekening` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_saldo_log`
--

DROP TABLE IF EXISTS `sp_saldo_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_saldo_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `log_type` enum('1','2') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '1: debet, 2: kredit',
  `log_amount` int(10) NOT NULL,
  `log_createdate` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deposit_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `deposit_id` (`deposit_id`),
  CONSTRAINT `sp_saldo_log_ibfk_1` FOREIGN KEY (`deposit_id`) REFERENCES `sp_deposit` (`deposit_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_saldo_log`
--

LOCK TABLES `sp_saldo_log` WRITE;
/*!40000 ALTER TABLE `sp_saldo_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_saldo_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trg_updateAmount` AFTER INSERT ON `sp_saldo_log` FOR EACH ROW BEGIN
DECLARE x_amount INT(10);

SELECT deposit_balance INTO x_amount FROM sp_deposit WHERE deposit_id = NEW.deposit_id LIMIT 1;

IF NEW.log_type = '1' THEN
BEGIN
	UPDATE sp_deposit SET deposit_balance = x_amount + NEW.log_amount WHERE deposit_id = (SELECT deposit_id FROM sp_saldo_log GROUP BY NEW.deposit_id HAVING MAX(log_id));
END;
ELSEIF NEW.log_type = '2' THEN
BEGIN
	UPDATE sp_deposit SET deposit_balance = x_amount - NEW.log_amount WHERE deposit_id = (SELECT deposit_id FROM sp_saldo_log GROUP BY NEW.deposit_id HAVING MAX(log_id));
END;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `sp_schedule`
--

DROP TABLE IF EXISTS `sp_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_schedule` (
  `schedule_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `schedule_status` enum('0','1','2','3','4') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1: approved, 2:definite, 3: onhold, 4: goback',
  `od_id` char(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`schedule_id`),
  KEY `od_id` (`od_id`),
  CONSTRAINT `fk_sp_schedule_2` FOREIGN KEY (`od_id`) REFERENCES `sp_order_transaction` (`od_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_schedule`
--

LOCK TABLES `sp_schedule` WRITE;
/*!40000 ALTER TABLE `sp_schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_schedule` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trg_uot` AFTER UPDATE ON `sp_schedule` FOR EACH ROW BEGIN
	IF NEW.schedule_status = '4' THEN
    	BEGIN
			UPDATE sp_order_transaction SET od_status = '5' WHERE od_id = OLD.od_id;
        END;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `sp_user`
--

DROP TABLE IF EXISTS `sp_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_level` tinyint(3) unsigned NOT NULL,
  `user_status` enum('active','suspend') COLLATE utf8_unicode_ci NOT NULL,
  `user_createat` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user_modifyat` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_level` (`user_level`),
  CONSTRAINT `sp_user_ibfk_1` FOREIGN KEY (`user_level`) REFERENCES `sp_level` (`level_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_user`
--

LOCK TABLES `sp_user` WRITE;
/*!40000 ALTER TABLE `sp_user` DISABLE KEYS */;
INSERT INTO `sp_user` VALUES (1,'budi','$2y$10$OW4eJ7SyYOaw4VXtKEMyFus74e7AcERXcJMFNMQbAd/j.cfy9GxAa','admin@codeshare.web.id',1,'active','2018-02-21 17:05:14','2018-01-30 13:31:17'),(3,'imron','$2y$10$a/T78hWMbJ7ktUNhHXE0Q.eAg.rc3Z4UzfzHu09HRkE9Yz6h31FLy','caturputra99@gmail.com',9,'active','2018-02-21 17:04:00','2018-02-20 09:01:33'),(4,'radix','$2y$10$4kdOaSuMr26iEvHF0BOgXe4tbhbPPxCkhuMnYft8LnJTu/842P.Y.','caturputra99@gmail.com',10,'active','2018-02-21 17:09:11','2018-01-28 05:32:21'),(5,'abdon','$2y$10$YqnbHF43HZbsOpM7ZLcrlu0RGxO0K6piFgSsRyDJI0rJWFbEqfiUe','caturputra99@gmail.com',7,'active','2018-02-21 17:01:13','2018-01-28 05:34:07'),(6,'dies','$2y$10$DArLCzsWsFTb6pCu04SnYeUbiaRP4Ji370t6rplpyEQl0NCEaAcEe','caturputra99@gmail.com',3,'active','2018-02-21 17:02:33','2018-01-28 05:35:19'),(7,'harry','$2y$10$L03JmcKlArkpA0Y.RZ3o1OQFmqEXm437vnDbJhal6iP0fqZ/od.0e','caturputra99@gmail.com',6,'active','2018-02-21 17:06:45','2018-01-28 05:37:00'),(14,'siswanto','$2y$10$hUsDXZo81lXaL7uN5X92l.c9TO6RyLkd.ysztoKUAK/XAnLmTwUEK','siswanto@gmail.com',8,'active','2018-02-20 08:55:55','2018-02-20 08:55:55'),(15,'abraham','$2y$10$4lvtbnFB5C7u099L42DO3ui5GikH7xW4TpwZjQhT4WnG9dWX9E/Le','abraham@gmail.com',8,'active','2018-02-20 08:56:45','2018-02-20 08:56:45'),(16,'anang','$2y$10$5GEnH7L996Q/QMG0qPBKaODBF0KgAkv6IjeqxzadBZPe58kHE2e/K','anang@gmail.com',8,'active','2018-02-20 08:57:42','2018-02-20 08:57:42'),(17,'krisnandung','$2y$10$3Yavh9tzGzAQAFFax4jbF.nDEtd9xiF8dIQQgGtZlZEuICpiGSdxu','kris@gmail.com',8,'active','2018-02-20 08:58:41','2018-02-20 08:58:41'),(18,'margono','$2y$10$bUmn5yePrReJj.j2G2095.FWXK8Wrtxyc7iMdU2rfmuox2CCThBkG','margono@gmail.com',12,'active','2018-02-20 08:59:33','2018-02-20 08:59:33'),(19,'sulistya','$2y$10$6hGYfLHCllIkOBJwbFfl1.y4FLNbPoUwGX/R0Fjj.0AWhuECKaPUi','sulistya@gmail.com',12,'active','2018-02-20 09:00:14','2018-02-20 09:00:14'),(20,'kristy','$2y$10$Cd3J3qShFWoo1nMQj737MuHHJMSDiaJgCgxcIzLwrn4GpBdTpRi4W','kristy@gmail.com',12,'active','2018-02-20 09:00:46','2018-02-20 09:00:46'),(21,'angga','$2y$10$77aI9SMGKFHJCJ2Wf1JnmukU8FENKa..HjYQJ470Y9pMGaO8Itt5e','angga@gmail.com',12,'active','2018-02-20 09:01:13','2018-02-20 09:01:13'),(22,'bagas','$2y$10$arr5OVM7CU50QAROgOJ2Iesk.Deejvypk/q6ARRz2rD2wEQQp/64i','caturptrcp@gmail.com',4,'active','2018-02-27 07:00:06','2018-02-20 10:54:11'),(23,'customer','$2y$10$dCZU3KardYUquf0IZOQmZeLP9zJkUczgSWlPIgiWXKYIYh.SS0/BG','caturputra@tuta.io',4,'active','2018-02-21 17:11:31','2018-02-20 11:01:02');
/*!40000 ALTER TABLE `sp_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_user_detail`
--

DROP TABLE IF EXISTS `sp_user_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_user_detail` (
  `user_detail_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_detail_firstname` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_detail_lastname` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_detail_birthdate` date DEFAULT NULL,
  `user_detail_gender` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_detail_address` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_detail_phone` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_detail_avatar` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_detail_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `fk_sp_user_detail_1` FOREIGN KEY (`user_id`) REFERENCES `sp_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_user_detail`
--

LOCK TABLES `sp_user_detail` WRITE;
/*!40000 ALTER TABLE `sp_user_detail` DISABLE KEYS */;
INSERT INTO `sp_user_detail` VALUES (1,'Budi','Prasetya','1997-01-01','M','Sleman','+6285641388273','img-ava-budi.png',1),(2,'Imron','Hanafi','1999-01-01','M','Mrican','+6285641388273','img-ava-imron.png',3),(3,'Radix','Atmono','1999-01-01','M','Mrican','+6285641388273','img-ava-radix.png',4),(4,'Abdon','Sipayung','1999-01-01','F','Mrican','+6285641388273','img-ava-abdon.png',5),(5,'Dies','Agung','1999-01-01','M','Mrican','+6285641388273','img-ava-dies.png',6),(6,'Harry','Wijanarko','1999-01-01','M','Mrican','+6285641388273','img-ava-harry.png',7),(13,'Siswanto','Siswanto','1991-01-01','M','Mrican Baru','+6285641388273','img-ava-siswanto.png',14),(14,'Abraham','Abraham','1991-01-01','M','Mrican Baru','+6285641388273','img-ava-abraham.png',15),(15,'Anang','anang','1991-01-01','M','Mrican Baru','+6285641388273','img-ava-anang.png',16),(16,'Krisnandung','krisnandung','1991-01-01','M','Mrican ','+6285641388273','img-ava-krisnandung.png',17),(17,'Margono','Margono','1991-01-01','M','Mrican','+6285641388273','img-ava-margono.png',18),(18,'Soelistya','sulistya','1991-01-01','M','Mrican','+6285641388273','img-ava-sulistya.png',19),(19,'Kristy','D','1991-01-01','M','Mrican','+6285641388273','img-ava-kristy.png',20),(20,'Angga','angga','1991-01-01','M','Mrican','+6285641388273','img-ava-angga.png',21),(21,'Bagas','Adiputro','1999-01-01','F','Sleman','+6285641388273','img-ava-agent.png',22),(22,'Muhamad','Catur Putra','1996-02-15','M','Jetis, Sleman, Yogyakarta','+6285641388273','img-ava-customer.jpg',23);
/*!40000 ALTER TABLE `sp_user_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'travelindodb'
--
/*!50003 DROP PROCEDURE IF EXISTS `cancelOrder` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `cancelOrder`(IN `xorderid` CHAR(6))
BEGIN
	DECLARE xstatus CHAR; 

   SELECT canceled_status INTO xstatus FROM sp_order_canceled WHERE od_id = xorderid LIMIT 1;
   
   IF xstatus = '1' THEN
   BEGIN
   	UPDATE sp_order_transaction SET od_status = '3' WHERE od_id = xorderid;
   END;

   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `countAvgCond` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `countAvgCond`(IN `xCondId` INT)
BEGIN
	DECLARE xTotal FLOAT DEFAULT 0; 
    DECLARE xCheck FLOAT DEFAULT 0; 
    DECLARE err INTEGER DEFAULT 0;
    DECLARE xParams INTEGER DEFAULT 0;
    DECLARE xId INTEGER DEFAULT 0;
        
    BEGIN
		SET err = 1;
    END;
    
    START TRANSACTION;
    
    SELECT condition_id INTO xId FROM sp_armada_condition_detail WHERE condition_id = xCondId GROUP BY condition_id;
    
    IF xId = xCondId THEN
		IF err = 1 THEN
        
			SELECT count(detail_id) INTO xParams FROM sp_armada_condition_detail WHERE detail_condition = '1';
            
            IF err = 1 THEN
				SELECT count(detail_id) INTO xTotal FROM sp_armada_condition_detail;
                
                IF err = 1 THEN
                
					SET xCheck = (xParams / xTotal) * 100;
					UPDATE sp_armada_condition SET condition_meter = ROUND(xCheck) WHERE condition_id = xId;
                    
                    IF err = 1 THEN
						COMMIT;
					ELSE
						ROLLBACK;
					END IF;
				ELSE 
					ROLLBACK;
				END IF;
			ELSE 
				ROLLBACK;
			END IF;
		ELSE 
			ROLLBACK;
		END IF;
    ELSE 
		SIGNAL SQLSTATE '42000' 
        SET MESSAGE_TEXT = "ID Not Found.";
    END IF;
    
    
    
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `countMinPayment` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `countMinPayment`(IN `xOd_id` CHAR(6))
BEGIN
	DECLARE minBayar INT(10) COLLATE utf8_unicode_ci;
    DECLARE tempBayar INT(10) COLLATE utf8_unicode_ci;
    DECLARE hargaJual INT(10) COLLATE utf8_unicode_ci;
    DECLARE xs CHAR(6) COLLATE utf8_unicode_ci;
    
    SELECT od_selling INTO hargaJual FROM sp_order_transaction WHERE od_id = xOd_id; 
    
    SELECT od_id INTO xs FROM sp_schedule WHERE od_id = xOd_id LIMIT 1;
    
    IF hargaJual != '' THEN
    BEGIN
    	SET minBayar = 0.3 * hargaJual;
    END;
    END IF;
    
    SELECT minBayar, hargaJual, xs;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `genAutoNum` */;
ALTER DATABASE `travelindodb` CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `genAutoNum`(IN `xParam` CHAR(3), IN `xNum` INT(1), IN `xN` INT)
BEGIN
	DECLARE kodebaru CHAR(5);
    DECLARE urut INT;
    DECLARE nomer INT;
    
    SET URUT = IF(nomer IS NULL, 1, nomer + 1);
    SET kodebaru = CONCAT(xParam, LPAD(urut, xNum, 0));
    
    SELECT kodebaru;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `travelindodb` CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-08 21:09:41
